/*
* Copyright (c) 2019 Automation Anywhere.
* All rights reserved.
*
* This software is the proprietary information of Automation Anywhere.
* You shall use it only in accordance with the terms of the license agreement
* you entered into with Automation Anywhere.
*
*/

package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.DEFAULT_VALUE_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.DESCRIPTION_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.FALSE;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.MIME_TYPE_FOLDER;
import static com.automationanywhere.botcommand.gsuite.Constants.TRUE;
import static com.automationanywhere.botcommand.gsuite.Constants.USERNAME;
import static com.automationanywhere.commandsdk.model.AttributeType.CREDENTIAL;
import static com.automationanywhere.commandsdk.model.AttributeType.RADIO;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.AttributeType.VARIABLE;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.api.impl.GDriveImpl;
import com.automationanywhere.botcommand.gsuite.api.impl.GSheetsImpl;
import com.automationanywhere.botcommand.gsuite.dto.sheets.SpreadSheetDto;
import com.automationanywhere.botcommand.gsuite.enums.FileType;
import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommand.gsuite.service.impl.AuthenticationServiceImpl;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Idx.Option;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import com.google.api.services.drive.model.File;
import com.google.common.annotations.VisibleForTesting;

@BotCommand
@CommandPkg(label = "Create workbook", name = "Create", description = "Creates a new google sheet workbook", node_label = "with the name {{name}}| in {{session}}")
public class CreateWorkbook extends AbstractAuthUtils {
	private static final Logger LOGGER = LogManager.getLogger(CreateWorkbook.class);
	private AuthenticationService mAuthenticationService = null;
	// private static final String NAME_DESC = "Name of the spreadsheet";
	// private static final String PATH_DESC = "Path where to create the
	// spreadsheet.If empty, file will be created under My-Drive.";
	Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommands.gsuite.messages.messages");

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public Value<String> execute(
			@Idx(index = "1", type = CREDENTIAL) @Pkg(label = USERNAME) @NotEmpty SecureString userEmailAddress,
			@Idx(index = "2", type = TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String session,
			@Idx(index = "3", type = TEXT) @Pkg(label = "Workbook name", description = "Supported extensions - .xlsx, .xls, .xlsm. eg Leads.xlsx") @NotEmpty String name,
			@Idx(index = "4", type = RADIO, options = {
					@Option(index = "4.1", pkg = @Pkg(label = "From my shared location", value = TRUE)),
					@Option(index = "4.2", pkg = @Pkg(label = "Select existing File variable", value = FALSE)) }) @Pkg(label = "File Path", default_value = TRUE, default_value_type = DataType.STRING) @NotEmpty String driveItemType,
			@Idx(index = "4.1.1", type = TEXT) @Pkg(label = "File Name", description = "Eg. Documents/HR") String driveFolderPath,
			@Idx(index = "4.2.1", type = VARIABLE) @Pkg(label = "") @NotEmpty String driveFolderPathVar) {

		try {

			//validateSessionNotExists(sessionMap,session);
			GSheets gSheets = getSheets(session, userEmailAddress);
			SpreadSheetDto spreadSheetDto = new SpreadSheetDto();
			spreadSheetDto.setSession(session);
			spreadSheetDto.setTitle(name);
			boolean created = false;
			String filePath = null;
			GDrive drive = getDrive(userEmailAddress);
			if (Boolean.valueOf(driveItemType))
				filePath = driveFolderPath;
			else {
				filePath = driveFolderPathVar;
			}

			if ("".equals(filePath) || "my-drive".equalsIgnoreCase(filePath)) {
				filePath = "root"; // Default id for my-drive
				LOGGER.debug("Creating file in my-drive location at = ");
				created = checkAndCreate(name, spreadSheetDto, drive, filePath);
			} else {
				List<File> filesList = drive.parseFilePath(filePath, "create");
				
				for (File file : filesList) {
					LOGGER.debug("File == " + file.getId() + " , name == " + file.getName() + ", type = "
							+ file.getMimeType());
					if (MIME_TYPE_FOLDER.equalsIgnoreCase(file.getMimeType())) {
						// Check for duplicate file with the same name.

						created = checkAndCreate(name, spreadSheetDto, drive, file.getId());
						break;
					}
				}
			}
			if (!created) {
				throw new IOException(MESSAGES.getString("create.invalid.path", filePath));
			}
//			 
//			String spreadSheetId = gSheets.openSpreadSheet(name).getSpreadSheetId();
			String spreadSheetId = gSheets.getSpreadSheetId();

			sessionMap.put(session, gSheets);

			return new StringValue(spreadSheetId);
		} catch (GeneralSecurityException | IOException | JSONException e) {
			LOGGER.error("exception in user authorization: ", e);
			String msg = e.getMessage();
			throw new BotCommandException(msg, e);
		}
	}

	public boolean checkAndCreate(String name, SpreadSheetDto spreadSheetDto, GDrive drive, String pathId)
			throws IOException, JSONException {
		if (!drive.fileExists(name, pathId)) {
			String id = drive.createFile(name, pathId, FileType.EXCEL);
			spreadSheetDto.setSpreadSheetId(id);
			LOGGER.debug("Created new spreadsheet with id = " + id);
			return true;
		} else {
			throw new IOException(MESSAGES.getString("create.duplicate.filename", name));
		}
	}

	GSheets getSheets(String session, SecureString userEmailAddress) throws GeneralSecurityException, IOException {
		GSheets gSheets = (GSheets) sessionMap.get(session);

		if (gSheets == null) {
			AuthenticationService authenticationService = getAuthenticationService();

			gSheets = new GSheetsImpl(authenticationService, session, sessionMap, userEmailAddress.getInsecureString());
		}
		return gSheets;
	}

	GDrive getDrive(SecureString userEmailAddress) throws GeneralSecurityException, IOException {
		AuthenticationService authenticationService = getAuthenticationService();
		return new GDriveImpl(authenticationService, userEmailAddress.getInsecureString(), sessionMap);
	}

	synchronized AuthenticationService getAuthenticationService() {
		if (mAuthenticationService == null) {
			mAuthenticationService = new AuthenticationServiceImpl();
		}
		return mAuthenticationService;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	@VisibleForTesting
	void setAuthenticationService(AuthenticationService mAuthenticationService) {
		this.mAuthenticationService = mAuthenticationService;
	}

}
