
/*
 * Copyright (c) 2019 Automation Anywhere.

 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
package com.automationanywhere.botcommand.gsuite.utils;

import static com.automationanywhere.botcommand.gsuite.Constants.COLUMNS;
import static com.automationanywhere.botcommand.gsuite.Constants.ENDROW;
import static com.automationanywhere.botcommand.gsuite.Constants.MAXCOLUMN;
import static com.automationanywhere.botcommand.gsuite.Constants.ROWS;
import static com.automationanywhere.botcommand.gsuite.Constants.STARTCOLUMN;
import static com.automationanywhere.botcommand.gsuite.Constants.STARTROW;

import com.automationanywhere.botcommand.gsuite.api.impl.GSheetsImpl;
import com.automationanywhere.botcommand.gsuite.helper.sheets.CellSplitter;
import com.automationanywhere.botcommand.gsuite.utils.Utils;

public abstract class GoToCellUtilities {
	public static CellSplitter cellSplitter;
	/**
	 * This method would shifts the active cell to one cell left
	 * 
	 * @param address
	 * @return address of just left of the active cell
	 */
	public static String oneCellLeft(String address) {
		cellSplitter = new CellSplitter();
		cellSplitter.splitCell(address);
		int row = Integer.parseInt(cellSplitter.getRowName());
		String columIndex = cellSplitter.getColumnName();
		int column = Utils.getColIndex(columIndex);
		if(STARTCOLUMN >= column)
			return address;
		String columnName = Utils.getColName(--column);
		return mergeAddress(row, columnName);
	}

	/**
	 * This method would shifts the active cell to one cell right
	 * 
	 * @param address
	 * @return address of just right of the active cell
	 */
	public static String oneCellRight(String address) {
		cellSplitter = new CellSplitter();
		cellSplitter.splitCell(address);
		int row = Integer.parseInt(cellSplitter.getRowName());
		String columIndex = cellSplitter.getColumnName();
		int column = Utils.getColIndex(columIndex);
		if(MAXCOLUMN <= column)
			return address;
		String columnName = Utils.getColName(++column);
		return mergeAddress(row, columnName);
	}

	/**
	 * This method would shifts the active cell to one cell Up
	 * 
	 * @param address
	 * @return address of just above cell
	 */
	public static String oneCellUp(String address) {
		cellSplitter = new CellSplitter();
		cellSplitter.splitCell(address);
		int row = Integer.parseInt(cellSplitter.getRowName());
		if(STARTROW >= row)
			return address;
		String columnName = cellSplitter.getColumnName();
		return mergeAddress(--row, columnName);
	}

	/**
	 * This method would shifts the active cell to one cell down
	 * 
	 * @param address
	 * @return address of just below cell
	 */
	public static String oneCellDown(String address) {
		cellSplitter = new CellSplitter();
		cellSplitter.splitCell(address);
		int row = Integer.parseInt(cellSplitter.getRowName());
		if(ENDROW <= row)
			return address;
		String columnName = cellSplitter.getColumnName();
		return mergeAddress(++row, columnName);
	}

	/**
	 * This method would shifts the active cell to beginning of the row
	 * 
	 * @param address
	 * @return address of beginning of row
	 */
	public static String beginningOfRow(String address) {
		cellSplitter = new CellSplitter();
		cellSplitter.splitCell(address);
		int row = STARTROW;
		String columnName = cellSplitter.getColumnName();
		return mergeAddress(row, columnName);
	}

	/**
	 * This method would shifts the active cell to beginning of the column
	 * 
	 * @param address
	 * @return address of beginning of column
	 */
	public static String beginningOfColumn(String address) {
		cellSplitter = new CellSplitter();
		cellSplitter.splitCell(address);
		int row = Integer.parseInt(cellSplitter.getRowName());
		String columnName = "A";
		return mergeAddress(row, columnName);
	}

	/**
	 * This method would shifts the active cell to end of the row
	 * 
	 * @param address
	 * @return address of end of row
	 */
	public static String endOfRow(GSheetsImpl gsheetsImpl,
			String address) {
		cellSplitter = new CellSplitter();
		cellSplitter.splitCell(address);
		int row = gsheetsImpl.getUsedRowColumns(ROWS);
		String column = cellSplitter.getColumnName();
		return mergeAddress(row, column);
	}

	/**
	 * This method would shifts the active cell to end of the column
	 * 
	 * @param address
	 * @return address of end of column
	 */
	public static String endOfColumn(GSheetsImpl gsheetsImpl,
			String address) {
		cellSplitter = new CellSplitter();
		cellSplitter.splitCell(address);
		int row = Integer.parseInt(cellSplitter.getRowName());
		int column = gsheetsImpl.getUsedRowColumns(COLUMNS);
		String columnName = Utils.getColName(column);
		return mergeAddress(row, columnName);
	}
	
	/**
	 * 
	 * @param row Row number
	 * @param column Column number
	 * @return Cell address
	 */
	public static String mergeAddress(int row, String column)
	{
		StringBuilder stringBuilder = new StringBuilder(column);
		stringBuilder.append(row);
		return stringBuilder.toString();
	}
}
