/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.ACTIVECELL;
import static com.automationanywhere.botcommand.gsuite.Constants.COLUMNS;
import static com.automationanywhere.botcommand.gsuite.Constants.DEFAULT_VALUE_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.DESCRIPTION_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_ACTIVECELL;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_SPECIFICCELL;
import static com.automationanywhere.botcommand.gsuite.Constants.ROWS;
import static com.automationanywhere.botcommand.gsuite.Constants.SPECIFICCELL;
import static com.automationanywhere.botcommand.gsuite.Constants.cellAddressValidator;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Idx.Option;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.MatchesRegex;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

@BotCommand
@CommandPkg(label = "Insert cell", name = "insertCell", description = "Inserts the cell with specified address", node_label = "at {{cellOption}} |{{cellAddress}} |in session {{sessionName}}")
public class InsertCell {
	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public BooleanValue execute(

			@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String sessionName,
			@Idx(index = "2", type = AttributeType.RADIO, options = {
					@Option(index = "2.1", pkg = @Pkg(label = LABEL_ACTIVECELL, value = ACTIVECELL)),
					@Option(index = "2.2", pkg = @Pkg(label = LABEL_SPECIFICCELL, value = SPECIFICCELL)) }) @Pkg(label = "Cell options", default_value_type = DataType.STRING, default_value = ACTIVECELL) @NotEmpty String cellOption,
			@Idx(index = "2.2.1", type = AttributeType.TEXT) @Pkg(label = "Cell name", description = "e.g. A1") @MatchesRegex(cellAddressValidator) @NotEmpty String cellAddress,
			@Idx(index = "3", type = AttributeType.RADIO, options = {
					@Idx.Option(index = "3.1", pkg = @Pkg(label = "Down", value = ROWS)),
					@Idx.Option(index = "3.2", pkg = @Pkg(label = "Right", value = COLUMNS)) }) @Pkg(label = "Shift type", description = "Select the shift type", default_value = "Down", default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String shiftType)
			throws IOException {
		try {
			GSheets gSheets = (GSheets) sessionMap.get(sessionName);
			ExecutorValidator.getInstance().validate(gSheets);
			if (ACTIVECELL.equals(cellOption)) {
				return new BooleanValue(gSheets
						.insertCell(gSheets.getActiveCell(gSheets.getActiveSheetInfo().getSheetName()), shiftType));
			} else {
				return new BooleanValue(gSheets.insertCell(cellAddress, shiftType));
			}
		} catch (IOException exception1) {
			;
			throw new BotCommandException(exception1.getMessage());
		} catch (Exception exception2) {
			throw new BotCommandException(exception2.getMessage());
		}
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}
