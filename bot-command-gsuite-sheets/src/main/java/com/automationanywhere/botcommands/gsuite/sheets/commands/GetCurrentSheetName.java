/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
import static com.automationanywhere.botcommand.gsuite.Constants.*;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.DataType;

@BotCommand
@CommandPkg(label = "Get Current Sheet Name", name = "GetCurrentSheetName", description = "Gets the name of current google sheet", node_label = "from the workbook and assign it to {{returnTo}} in session {{session}}", return_label = "Store Current Sheet Name to", return_type = STRING, return_required = true, icon = "excel.svg")
public class GetCurrentSheetName extends AbstractAuthUtils {

	private static final Logger LOGGER = LogManager.getLogger(GetCurrentSheetName.class);

	@Sessions
	private Map<String, Object> sessionMap;

	private static final String SESSION_EXAMPLE = "e.g. Session1 or S1";

	@Execute
	public Value<?> execute(

			@Idx(index = "1", type = TEXT) @Pkg(label = "Session name", description = SESSION_EXAMPLE, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @NotEmpty String session) {

		String sheetName = null;
		try {
			validateSessionNotExists(sessionMap, session);
			GSheets gSheets = (GSheets) sessionMap.get(session);
			ExecutorValidator.getInstance().validate(gSheets);

			sheetName = gSheets.getActiveSheetName();
			LOGGER.info("Current SheetName =  " + sheetName);

			return new StringValue(sheetName);
		} catch (Exception e) {
			LOGGER.error("exception in getting sheet name: ", e);
			String msg = e.getMessage();
			throw new BotCommandException(msg, e);
		}

	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}
