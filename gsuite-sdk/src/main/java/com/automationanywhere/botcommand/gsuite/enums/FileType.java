package com.automationanywhere.botcommand.gsuite.enums;

public enum FileType {

    EXCEL,
    WORD,
    PRESENTATION,
    PDF,
    TEXT,
    CSV,
    UNSPECIFIED
}
