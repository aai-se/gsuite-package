/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.FALSE;
import static com.automationanywhere.botcommand.gsuite.Constants.TRUE;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;

public class GetMultipleCellTest {
	private static final Logger LOGGER = LogManager.getLogger(GetMultipleCellTest.class);
	
	@Spy
	GetMultipleCell getMultipleCell;

	@Mock
	AuthenticationService authenticationService;

	@Mock
	GSheets mockGSheets;
	
	@Mock
	TableValue tableValue;

	final String session = "session";

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);

		sessionMap.put(session, mockGSheets);
		getMultipleCell.setSessionMap(sessionMap);
	}

	@Test
	public void getMultipleCell_ForRangeTest() {
		try {
			LOGGER.info("Executing the mock test for AutoFit columns");
			when(mockGSheets.getMultipleCells(anyBoolean(), anyString())).thenReturn(tableValue);
			LOGGER.info("Executing the AutofitColumns with spy object");
			getMultipleCell.execute(session, TRUE, "A1:L10");
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			Assert.fail();
		}
	}
	
	@Test
	public void getMultipleCell_AllTest() {
		try {
			LOGGER.info("Executing the mock test for AutoFit columns");
			when(mockGSheets.getMultipleCells(anyBoolean(), anyString())).thenReturn(tableValue);
			LOGGER.info("Executing the AutofitColumns with spy object");
			getMultipleCell.execute(session, FALSE, null);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			Assert.fail();
		}
	}

	@Test(expectedExceptions = Exception.class)
	public void getMultipleCell_NullSessionException() {
		when(mockGSheets.getMultipleCells(anyBoolean(), anyString())).thenThrow(IOException.class);
		getMultipleCell.execute(null, FALSE, null);
		Assert.fail();
	}
	
	@Test(expectedExceptions = Exception.class)
	public void getMultipleCell_InvalidSessionException() {
		when(mockGSheets.getMultipleCells(anyBoolean(), anyString())).thenThrow(IOException.class);
		getMultipleCell.execute("wrong_session", FALSE, null);
		Assert.fail();
	}
	
	@Test(expectedExceptions = Exception.class)
	public void getMultipleCell_NullRangeException() {
		when(mockGSheets.getMultipleCells(anyBoolean(), anyString())).thenThrow(IOException.class);
		getMultipleCell.execute(null, TRUE, null);
		Assert.fail();
	}
	
	@Test(expectedExceptions = Exception.class)
	public void getMultipleCell_InvalidRangeException() {
		when(mockGSheets.getMultipleCells(anyBoolean(), anyString())).thenThrow(IOException.class);
		getMultipleCell.execute(null, TRUE, "99A1:L9");
		Assert.fail();
	}
}