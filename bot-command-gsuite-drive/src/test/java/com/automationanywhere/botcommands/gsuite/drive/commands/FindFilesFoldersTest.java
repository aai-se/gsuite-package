/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommands.gsuite.drive.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;
import com.google.api.services.drive.model.File;

/**
 * Tests find Google Drive files functionality.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

public class FindFilesFoldersTest  {

	public static final SecureString USERNAME = ConstantsTest.getUserName();

	@Spy
	FindFilesFolders findFilesFolders;

	@Mock
	GDrive mockGDrive;

	@Mock
	AuthenticationService mockAuthService;

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {
		MockitoAnnotations.initMocks(this);
		findFilesFolders.setAuthenticationService(mockAuthService);
	}

	@Test
	public void findFiles_returnsListOfMatchingFilesFromGoogleDriveFolder_whenParameterIsSearchString() {
		try {
			Mockito.doReturn(mockGDrive).when(findFilesFolders).getDrive(USERNAME);

			Map<String, Value> expectedMap = new HashMap<>();
			expectedMap.put("NumberOfFiles", new StringValue("" + 1));
			JSONObject json = new JSONObject();
			json.put("fileId", "trialtext_id");
			json.put("fileName", "trialtext.txt");
			expectedMap.put("File1", new StringValue(json.toString()));

			File file = new File();
			file.setName("trialtext.txt");
			file.setId("trialtext_id");
			List<File> fileList = new ArrayList<>();
			fileList.add(file);

			when(mockGDrive.findFileReturnList("folder1", "trialtext", "MAT")).thenReturn(fileList);
			DictionaryValue testId = findFilesFolders.execute(USERNAME, "FIL", "folder1", "MAT", "trialtext");

			Assert.assertEquals(expectedMap.get("NumberOfFiles").toString(), testId.get("NumberOfFiles").toString());
			Assert.assertEquals(expectedMap.get("File1").toString(), testId.get("File1").toString());
		} catch (GeneralSecurityException | IOException | JSONException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void findFiles_throwsBotCommandException_whenFindFileGSuiteSDKThrowsIOException() {
		try {
			Mockito.doReturn(mockGDrive).when(findFilesFolders).getDrive(USERNAME);

			when(mockGDrive.findFileReturnList("folder1", "trialtext", "MAT")).thenThrow(IOException.class);
			findFilesFolders.execute(USERNAME, "FIL", "folder1", "MAT", "trialtext");
			Assert.fail();
		} catch (GeneralSecurityException | JSONException e) {
			Assert.fail();
		} catch (IOException e) {
			Assert.assertTrue(true);
		}
	}

	@Test
	public void getDrive_newGDriveImplObjectIsNotNull() {
		try {
			GDrive drive = findFilesFolders.getDrive(USERNAME);
			assertNotNull(drive);
		} catch (GeneralSecurityException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
	}
}
