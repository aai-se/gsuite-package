/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommand.gsuite.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.Constants;
import com.automationanywhere.botcommand.gsuite.messages.PropertiesReader;
import com.automationanywhere.botcommand.gsuite.service.AESEncrypter;
import com.automationanywhere.botcommand.gsuite.service.AuthDto;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommand.gsuite.utils.Utils;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.sheets.v4.SheetsScopes;

public class AuthenticationServiceImpl implements AuthenticationService {

	private static final Logger LOGGER = LogManager.getLogger(AuthenticationServiceImpl.class);

	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private static final List<String> SCOPES = Arrays.asList(SheetsScopes.SPREADSHEETS, SheetsScopes.DRIVE,
			CalendarScopes.CALENDAR);

	private Credential credential;

	@Override
	public AuthDto getAuthDto(String userName) throws IOException {

		LOGGER.info("Started initiated AuthDTO");
		String filePath = Utils.getCredentialFilePathAsString(userName);

		// check file was dumped or not.
		if (!new File(filePath).exists())
			throw new BotCommandException(PropertiesReader.getValue("GSuiteAuth.User.NotExists"));

		AuthDto authDto = null;

		// read AuthDto object from json file.
		try (FileInputStream file = new FileInputStream(filePath);
				ObjectInputStream fileReader = new ObjectInputStream(file)) {
			authDto = (AuthDto) fileReader.readObject();
			LOGGER.debug("AUTH DTO RETRIEVED.." + authDto);

		} catch (ClassNotFoundException ex) {
			LOGGER.error("NO SUCH FILE FOUND .........");
			// TODO : proper exception
		}
		return authDto;
	}

	@Override
	public Credential getGoogleCredential(String userEmailAddress, String clientId, String clientSecret,
			String redirectURI) throws IOException {

		LOGGER.info("Started fetching details..... in GOOGLE CREDENTIALS");
		if (getCredential() != null) {
			System.out.println("Returned already...");
			return credential;
		}
		GoogleClientSecrets.Details details = new GoogleClientSecrets.Details();
		details.setClientId(clientId);
		details.setClientSecret(clientSecret);
		List<String> uri = new ArrayList<>();
		uri.add(redirectURI);
		details.setRedirectUris(uri);

		details.setAuthUri(Constants.AUTH_URI);
		details.setTokenUri(Constants.TOKEN_URI);

		GoogleClientSecrets clientSecrets = new GoogleClientSecrets();
		clientSecrets.setInstalled(details);

		// Build flow and trigger user authorization request.
		LOGGER.info("\n\nCreated Client Secret");
		try {
			GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
					GoogleNetHttpTransport.newTrustedTransport(), JSON_FACTORY, clientSecrets, SCOPES)

							.setDataStoreFactory(new FileDataStoreFactory(
									Utils.createTempDirectory(AESEncrypter.encrypt(userEmailAddress))))
							.setAccessType("offline").build();
			System.out.println("Created flow");
			LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
			credential = new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
		} catch (GeneralSecurityException exception) {
			LOGGER.error("Google FLOW Error...");
			// TODO : proper exception
		}
		LOGGER.debug("Credentials Generated = " + credential);
		return credential;
	}

	@Override
	public Credential getCredential() {
		return credential;
	}
}
