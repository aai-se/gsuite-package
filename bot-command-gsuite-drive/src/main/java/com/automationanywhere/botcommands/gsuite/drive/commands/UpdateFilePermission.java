/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.USERNAME;
import static com.automationanywhere.commandsdk.model.AttributeType.CHECKBOX;
import static com.automationanywhere.commandsdk.model.AttributeType.RADIO;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.api.impl.GDriveImpl;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommand.gsuite.service.impl.AuthenticationServiceImpl;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import com.google.common.annotations.VisibleForTesting;

/**
 * Updates a Google Drive file permission having specified the file name / id,
 * permission id, role and transfer ownership flag.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

@BotCommand
@CommandPkg(label = "Update file permission", name = "UpdateFilePermission", description = "Update a permission for a file", node_label = "for file |with name {{fileName}}|with id {{fileId}}| and permission id {{permissionId}}", return_type = STRING, return_required = false, return_label = "Returns the permission id of the updated permission")
public class UpdateFilePermission {
	private static final Logger LOGGER = LogManager.getLogger(UpdateFilePermission.class);
	private static final String NAME_DESC = "e.g. MyFolder/MyFile.doc";
	private static final String ID_DESC = "e.g. 17dIAwvrEA4JeLysfFky9oZL0qAzesY0IdbOu-AvHB1U from document "
			+ "link https://docs.google.com/spreadsheets/d/17dIAwvrEA4JeLysfFky9oZL0qAzesY0IdbOu-AvHB1U/edit#gid=0";
	private AuthenticationService mAuthenticationService;

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public StringValue execute(
			@Idx(index = "1", type = AttributeType.CREDENTIAL) @Pkg(label = USERNAME) @NotEmpty SecureString userEmailAddress,
			@Idx(index = "2", type = RADIO, options = {
					@Idx.Option(index = "2.1", pkg = @Pkg(label = "File name (with location)", value = "BYNAME")),
					@Idx.Option(index = "2.2", pkg = @Pkg(label = "File ID", value = "BYID")) }) @Pkg(label = "Create permission for file", default_value_type = DataType.STRING, default_value = "BYNAME") @NotEmpty String fileOption,
			@Idx(index = "2.1.1", type = TEXT) @Pkg(label = "", description = NAME_DESC, default_value = "", default_value_type = DataType.STRING) @NotEmpty String fileName,
			@Idx(index = "2.2.1", type = TEXT) @Pkg(label = "", description = ID_DESC, default_value = "", default_value_type = DataType.STRING) @NotEmpty String fileId,
			@Idx(index = "3", type = TEXT) @Pkg(label = "Permission id") @NotEmpty String permissionId,
			@Idx(index = "4", type = AttributeType.SELECT, options = {
					@Idx.Option(index = "4.1", pkg = @Pkg(label = "Owner", value = "owner")),
					@Idx.Option(index = "4.2", pkg = @Pkg(label = "Organizer", value = "organizer")),
					@Idx.Option(index = "4.3", pkg = @Pkg(label = "File organizer", value = "fileOrganizer")),
					@Idx.Option(index = "4.4", pkg = @Pkg(label = "Writer", value = "writer")),
					@Idx.Option(index = "4.5", pkg = @Pkg(label = "Commenter", value = "commenter")),
					@Idx.Option(index = "4.6", pkg = @Pkg(label = "Reader", value = "reader")) }) @Pkg(label = "Roles", default_value_type = STRING, default_value = "reader") @NotEmpty String role,
			@Idx(index = "4.1.1", type = CHECKBOX) @Pkg(label = "Transfer ownership") @NotEmpty Boolean transferOwnership) {
		try {
			JSONObject inputFileParam = new JSONObject();
			if (fileOption.equals("BYNAME")) {
				inputFileParam.put("fileName", fileName);
			} else if (fileOption.equals("BYID")) {
				inputFileParam.put("fileId", fileId);
			}
			JSONObject otherParam = new JSONObject();
			otherParam.put("role", role);
			if (role.equals("owner")) {
				otherParam.put("transferOwnership", transferOwnership);
			}

			GDrive gDrive = getDrive(userEmailAddress);
			String permission = gDrive.updatePermission(inputFileParam.toString(), otherParam.toString(), permissionId);
			return new StringValue(permission);
		} catch (GeneralSecurityException | IOException | JSONException e) {
			LOGGER.error("Exception in finding Google Drive file: ", e);
			String msg = e.getMessage();
			throw new BotCommandException(msg, e);
		}
	}

	GDrive getDrive(SecureString userEmailAddress) throws GeneralSecurityException, IOException {
		AuthenticationService authenticationService = getAuthenticationService();
		return new GDriveImpl(authenticationService, userEmailAddress.getInsecureString(), sessionMap);
	}

	@VisibleForTesting
	void setAuthenticationService(AuthenticationService mAuthenticationService) {
		this.mAuthenticationService = mAuthenticationService;
	}

	synchronized AuthenticationService getAuthenticationService() {
		if (mAuthenticationService == null) {
			mAuthenticationService = new AuthenticationServiceImpl();
		}
		return mAuthenticationService;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
}
