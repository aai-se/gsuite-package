package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.DEFAULT_VALUE_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.DESCRIPTION_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_SESSION;

import java.util.Map;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

@BotCommand
@CommandPkg(label = "Autofit rows", name = "autofitRows", description = "Autofits the rows in the active worksheet", node_label = "session {{session}}", icon = "excel.svg")
public class AutofitRows {
	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public BooleanValue execute(
			@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String session) {
		GSheets gSheets = (GSheets) sessionMap.get(session);
		ExecutorValidator.getInstance().validate(gSheets);
		return new BooleanValue(gSheets.autofitRows());
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
}
