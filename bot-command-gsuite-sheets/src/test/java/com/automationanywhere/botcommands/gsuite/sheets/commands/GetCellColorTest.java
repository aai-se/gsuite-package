/* Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static org.junit.Assert.fail;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.automationanywhere.botcommand.gsuite.Constants.*;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.dto.sheets.WorksheetDto;

public class GetCellColorTest {
	private final static Logger LOGGER = LogManager.getLogger(DeleteRangeTest.class);

	@Spy
	GetCellColor getCellColor;

	@Mock
	GSheets mockGSheets;

	@Mock
	WorksheetDto worksheetDto;

	@Mock
	ListValue<StringValue> colorList;

	final String session = "session";
	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		sessionMap.put(session, mockGSheets);
		getCellColor.setSessionMap(sessionMap);
		when(mockGSheets.getSpreadSheetId()).thenReturn("spreadsheetId");
	}

	@Test
	public void getActiveCellColorTest() {
		try {
			when(mockGSheets.getActiveSheetInfo()).thenReturn(worksheetDto);
			when(worksheetDto.getSheetName()).thenReturn("Sheet1");
			when(mockGSheets.getActiveCell(anyString())).thenReturn("A1");
			when(mockGSheets.getCellColor(anyString(), anyString(), anyBoolean(), anyBoolean(), anyBoolean()))
					.thenReturn(colorList);

			Assert.assertEquals(getCellColor.execute(session, ACTIVECELL, "", true, true, TRUE), colorList);
		} catch (IOException e) {
			Assert.fail();
		}
	}

	@Test
	public void getSpecificCellColorTest() {
		try {
			when(mockGSheets.getCellColor(anyString(), anyString(), anyBoolean(), anyBoolean(), anyBoolean()))
					.thenReturn(colorList);

			Assert.assertEquals(getCellColor.execute(session, SPECIFICCELL, "A2", true, true, TRUE), colorList);
		} catch (IOException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void invalidCellNameTest() {
		try {
			when(mockGSheets.getCellColor(anyString(), anyString(), anyBoolean(), anyBoolean(), anyBoolean()))
					.thenThrow(new IOException());
			getCellColor.execute(session, SPECIFICCELL, "A2", true, true, TRUE);
		} catch (IOException e) {
			Assert.fail();
		}
	}
}
