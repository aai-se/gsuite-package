/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.api.impl.GSheetsImpl;
import com.automationanywhere.botcommand.gsuite.dto.sheets.WorksheetDto;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;

public class CreateWorksheetTest {
	@Spy
	CreateSheet createWorksheet;

	@Mock
	GSheets mockGSheets;

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		sessionMap.put("session", mockGSheets);
		createWorksheet.setSessionMap(sessionMap);
	}

	@Test
	public void testCreateWorksheet() {

		try {
			// int sheetIndexInt = sheetIndex == null ? 0 : sheetIndex.intValue();
			when(mockGSheets.getSpreadSheetId()).thenReturn("1");
//			WorksheetDto worksheetDto = new WorksheetDto();
//			worksheetDto.setIndex(1);
//			worksheetDto.setSheetName("test123");
//			worksheetDto.setSpreadSheetId("1");
			// String fileId = "trial_file123";
			when(mockGSheets.createWorkSheet(any(String.class), any(Integer.class), any(String.class)))
					.thenReturn(true); // mock gsheets.closespreadsheet, then return true
			BooleanValue testId = createWorksheet.execute("session", "BYNAME", null, "test123"); // type booleanvalue or
				System.out.println("testid" + testId);																				// boolean?
		//	Assert.assertTrue(testId.get()); // true, boolean value testid
			// Assert.assertEquals( true, testId.get() );

		} catch (IOException e) {
			Assert.fail();
		}
	}
	
	@Test(expectedExceptions = BotCommandException.class)
	public void generalSecurityExceptionTest() {
		try {
//			WorksheetDto worksheetDto = new WorksheetDto();
//			worksheetDto.setIndex(1);
//			worksheetDto.setSheetName("test123");
//			worksheetDto.setSpreadSheetId("1");
			when(mockGSheets.getSpreadSheetId()).thenReturn("1");
			when(mockGSheets.createWorkSheet(any(String.class), any(Integer.class), any(String.class)))
					.thenThrow(new IOException("error"));
			createWorksheet.execute("session", "BYNAME", 1, "test123");
		} catch (IOException i) {

		}
	}

}
