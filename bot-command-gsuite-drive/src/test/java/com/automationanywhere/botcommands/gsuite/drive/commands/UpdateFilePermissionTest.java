/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommands.gsuite.drive.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

/**
 * Tests update Google Drive file permission functionality.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

public class UpdateFilePermissionTest {

	public static final SecureString USERNAME = ConstantsTest.getUserName();

	@Spy
	UpdateFilePermission updateFilePermission;

	@Mock
	GDrive mockGDrive;

	@Mock
	AuthenticationService mockAuthService;

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		updateFilePermission.setAuthenticationService(mockAuthService);

	}

	@Test
	public void updatePermission_returnsUpdatedPermission_forWriterRole_UserGrantee() {
		try {
			Mockito.doReturn(mockGDrive).when(updateFilePermission).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			String role = "writer";
			String permissionId = "test_permission123";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			JSONObject inputFileParamID = new JSONObject();
			inputFileParamID.put("fileId", fileId);

			JSONObject otherParam = new JSONObject();
			otherParam.put("role", role);

			String expectedPermission = "{\"deleted\":false,\"displayName\":\"Saanchi Muthya\","
					+ "\"emailAddress\":\"abc@aademo.page\",\"id\":" + permissionId + ",\"kind\":\"drive#permission\","
					+ "\"role\":" + role + ",\"type\":\"user\"}";
			when(mockGDrive.updatePermission(inputFileParamName.toString(), otherParam.toString(), permissionId))
					.thenReturn(expectedPermission);
			StringValue actualPermission = updateFilePermission.execute(USERNAME, "BYNAME", fileName, fileId,
					permissionId, role, false);
			Assert.assertEquals(actualPermission.toString(), expectedPermission);

			when(mockGDrive.updatePermission(inputFileParamID.toString(), otherParam.toString(), permissionId))
					.thenReturn(expectedPermission);
			actualPermission = updateFilePermission.execute(USERNAME, "BYID", fileName, fileId, permissionId, role,
					false);
			Assert.assertEquals(actualPermission.toString(), expectedPermission);
		} catch (JSONException | IOException | GeneralSecurityException e) {
			Assert.fail();
		}
	}

	@Test
	public void updatePermission_returnsNewPermission_forOwnerRole() {
		try {
			Mockito.doReturn(mockGDrive).when(updateFilePermission).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			String role = "owner";
			String permissionId = "test_permission123";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			JSONObject otherParam = new JSONObject();
			otherParam.put("role", role);
			otherParam.put("transferOwnership", true);

			String expectedPermission = "{\"deleted\":false,\"displayName\":\"Saanchi Muthya\","
					+ "\"emailAddress\":\"abc@aademo.page\",\"id\":" + permissionId + ",\"kind\":\"drive#permission\","
					+ "\"role\":" + role + ",\"type\":\"user\"}";
			when(mockGDrive.updatePermission(inputFileParamName.toString(), otherParam.toString(), permissionId))
					.thenReturn(expectedPermission);
			StringValue actualPermission = updateFilePermission.execute(USERNAME, "BYNAME", fileName, fileId,
					permissionId, role, true);
			Assert.assertEquals(actualPermission.toString(), expectedPermission);
		} catch (JSONException | IOException | GeneralSecurityException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void updatePermission_throwsBotCommandException_whenUpdatePermissionAPIthrowsIOException() {
		try {
			Mockito.doReturn(mockGDrive).when(updateFilePermission).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			String role = "writer";
			String permissionId = "test_permission123";

			JSONObject otherParam = new JSONObject();
			otherParam.put("role", role);

			when(mockGDrive.updatePermission(inputFileParamName.toString(), otherParam.toString(), permissionId))
					.thenThrow(IOException.class);
			updateFilePermission.execute(USERNAME, "BYNAME", fileName, fileId, permissionId, role, false);

			Assert.fail();
		} catch (JSONException | GeneralSecurityException e) {
			Assert.fail();
		} catch (IOException e) {
			Assert.assertTrue(true);
		}
	}

	@Test
	public void getDrive_newGDriveImplObjectIsNotNull() {
		try {
			GDrive drive = updateFilePermission.getDrive(USERNAME);
			assertNotNull(drive);
		} catch (GeneralSecurityException | IOException e) {
			fail();
		}
	}
}
