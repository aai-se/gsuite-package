package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommands.gsuite.sheets.commands.GetSingleCell;
import com.automationanywhere.botcommands.gsuite.sheets.commands.OpenSheets;
import com.automationanywhere.botcommands.gsuite.sheets.constants.Constants;
import com.automationanywhere.botcommands.gsuite.sheets.test.utils.ConstantsTest;
import static com.automationanywhere.botcommand.gsuite.Constants.ACTIVECELL;
import com.automationanywhere.core.security.SecureString;

public class GetSingleCellJunit extends AbstractAuthUtils  {
	GetSingleCell getCellValue;
	OpenSheets openSheets;

	
	final String session = "session";
	private final static SecureString USERNAME  =  ConstantsTest.getUserName();

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {				
		launchOAuthConsentScreen(ConstantsTest.getUserName(), ConstantsTest.getClientid(),
				ConstantsTest.getClientSecret(), ConstantsTest.getRedirectUri());
		getCellValue = new GetSingleCell();
		getCellValue.setSessionMap(sessionMap);
		openSheets = new OpenSheets();
		openSheets.setSessionMap(sessionMap);
		openSheets.execute(USERNAME, session, "BYNAME", "Demo111", null, null, false, null, false);
	}
	
	
	@Test
	public void getSingleCellValue() {
		StringValue cellValue = (StringValue) getCellValue.execute(session,Constants.SPECIFICCELL, "A10");
		if(cellValue.toString().isEmpty()) {
			System.out.println("\n\n\tThe cell is Empty");
		}
		else
		System.out.println("\n\n\tThe value is :"+ cellValue);
			
	}
}
