/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.USERNAME;
import static com.automationanywhere.commandsdk.model.AttributeType.RADIO;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.api.impl.GDriveImpl;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommand.gsuite.service.impl.AuthenticationServiceImpl;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import com.google.common.annotations.VisibleForTesting;

/**
 * Renames a Google Drive file from a Google Drive location to the specified
 * name.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

@BotCommand
@CommandPkg(label = "Rename file", name = "RenameFile", description = "Renames a file on Google Drive", node_label = "|with name {{fileName}}|with id {{fileId}}| to {{newFileName}}|")
public class RenameFile {
	private static final Logger LOGGER = LogManager.getLogger(RenameFile.class);
	private static final String NAME_DESC = "e.g. MyFolder/OldFileName.doc";
	private static final String ID_DESC = "e.g. 17Ch3BH3APhX7rKCp2hk6nFEKMwZRwsh8";
	private static final String LOC_DESC = "e.g. NewFileName.doc";
	private AuthenticationService mAuthenticationService;

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public Value<String> execute(
			@Idx(index = "1", type = AttributeType.CREDENTIAL) @Pkg(label = USERNAME) @NotEmpty SecureString userEmailAddress,
			@Idx(index = "2", type = RADIO, options = {
					@Idx.Option(index = "2.1", pkg = @Pkg(label = "File name (with location)", value = "BYNAME")),
					@Idx.Option(index = "2.2", pkg = @Pkg(label = "File ID", value = "BYID")) }) @Pkg(label = "Rename file", default_value_type = DataType.STRING, default_value = "BYNAME") @NotEmpty String fileOption,
			@Idx(index = "2.1.1", type = TEXT) @Pkg(label = "", description = NAME_DESC, default_value = "", default_value_type = DataType.STRING) @NotEmpty String fileName,
			@Idx(index = "2.2.1", type = TEXT) @Pkg(label = "", description = ID_DESC, default_value = "", default_value_type = DataType.STRING) @NotEmpty String fileId,
			@Idx(index = "3", type = TEXT) @Pkg(label = "New file name", description = LOC_DESC) @NotEmpty String newFileName) {
		try {
			LOGGER.info("Into Rename File");
			JSONObject inputFileParam = new JSONObject();
			if (fileOption.equals("BYNAME")) {
				inputFileParam.put("fileName", fileName);
			} else if (fileOption.equals("BYID")) {
				inputFileParam.put("fileId", fileId);
			}
			inputFileParam.put("fileType", "file");
			return new StringValue("" + getDrive(userEmailAddress).renameFile(inputFileParam.toString(), newFileName));
		} catch (GeneralSecurityException | IOException | JSONException e) {
			LOGGER.error("Exception in renaming Google Drive file: ", e);
			String msg = e.getMessage();
			throw new BotCommandException(msg, e);
		}
	}

	GDrive getDrive(SecureString userEmailAddress) throws GeneralSecurityException, IOException {
		AuthenticationService authenticationService = getAuthenticationService();
		return new GDriveImpl(authenticationService, userEmailAddress.getInsecureString(), sessionMap);
	}

	@VisibleForTesting
	void setAuthenticationService(AuthenticationService mAuthenticationService) {
		this.mAuthenticationService = mAuthenticationService;
	}

	synchronized AuthenticationService getAuthenticationService() {
		if (mAuthenticationService == null) {
			mAuthenticationService = new AuthenticationServiceImpl();
		}
		return mAuthenticationService;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
}
