/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.USERNAME;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.api.impl.GDriveImpl;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommand.gsuite.service.impl.AuthenticationServiceImpl;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import com.google.common.annotations.VisibleForTesting;

/**
 * Gets the list of permissions of the Google Drive File.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

@BotCommand
@CommandPkg(label = "Get file permission", name = "GetFilePermission", description = "Gets file permission of a file on Google Drive", node_label = "of file having name {{fileName}}|of file having id {{fileId}}", return_type = DataType.LIST, return_sub_type = DataType.STRING, return_required = true, return_label = "Returns the file's permissions")
public class GetFilePermission {
	private static final Logger LOGGER = LogManager.getLogger(GetFilePermission.class);
	private static final String NAME_DESC = "e.g. MyFolder/MyFile.doc";
	private static final String ID_DESC = "e.g. 17dIAwvrEA4JeLysfFky9oZL0qAzesY0IdbOu-AvHB1U from document "
			+ "link https://docs.google.com/spreadsheets/d/17dIAwvrEA4JeLysfFky9oZL0qAzesY0IdbOu-AvHB1U/edit#gid=0";
	private AuthenticationService mAuthenticationService;

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public Value<List<Value>> execute(
			@Idx(index = "1", type = AttributeType.CREDENTIAL) @Pkg(label = USERNAME) @NotEmpty SecureString userEmailAddress,
			@Idx(index = "2", type = AttributeType.RADIO, options = {
					@Idx.Option(index = "2.1", pkg = @Pkg(label = "File name (with location)", value = "BYNAME")),
					@Idx.Option(index = "2.2", pkg = @Pkg(label = "File id", value = "BYID")) }) @Pkg(label = "File permissions", value = "Open type", default_value_type = DataType.STRING, default_value = "BYNAME") String fileOption,
			@Idx(index = "2.1.1", type = TEXT) @Pkg(label = "", description = NAME_DESC) @NotEmpty String fileName,
			@Idx(index = "2.2.1", type = TEXT) @Pkg(label = "", description = ID_DESC) @NotEmpty String fileId) {
		try {
			JSONObject inputFileParam = new JSONObject();
			if (fileOption.equals("BYNAME")) {
				inputFileParam.put("fileName", fileName);
			} else if (fileOption.equals("BYID")) {
				inputFileParam.put("fileId", fileId);
			}
			ListValue<?> result = new ListValue<>();
			result.set(getDrive(userEmailAddress).getFilePermission(inputFileParam.toString()));
			return result;
		} catch (GeneralSecurityException | IOException | JSONException e) {
			LOGGER.error("Exception in copying Google Drive file: ", e);
			String msg = e.getMessage();
			throw new BotCommandException(msg, e);
		}
	}

	GDrive getDrive(SecureString userEmailAddress) throws GeneralSecurityException, IOException {
		AuthenticationService authenticationService = getAuthenticationService();
		return new GDriveImpl(authenticationService, userEmailAddress.getInsecureString(), sessionMap);
	}

	@VisibleForTesting
	void setAuthenticationService(AuthenticationService mAuthenticationService) {
		this.mAuthenticationService = mAuthenticationService;
	}

	synchronized AuthenticationService getAuthenticationService() {
		if (mAuthenticationService == null) {
			mAuthenticationService = new AuthenticationServiceImpl();
		}
		return mAuthenticationService;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
}
