/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommands.gsuite.drive.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

/**
 * Tests download Google Drive file functionality.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

public class ExportAsPDFTest {

	public static final SecureString USERNAME = ConstantsTest.getUserName();

	@Spy
	ExportAsPDF exportAsPdf;

	@Mock
	GDrive mockGDrive;

	@Mock
	AuthenticationService mockAuthService;

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {
		MockitoAnnotations.initMocks(this);
		exportAsPdf.setAuthenticationService(mockAuthService);
	}

	@Test
	public void exportAsPdf_returnsDownloadedFileId_whenRequiredFileNameProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(exportAsPdf).getDrive(USERNAME);

			String fileName = "trial_file123";
			String fileId = "trial_file.txt";
			String destinationFolder = "";
			Boolean isOverride = true;
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			JSONObject outputFileParam = new JSONObject();
			outputFileParam.put("fileName", destinationFolder);
			outputFileParam.put("isOverride", isOverride);

			when(mockGDrive.exportAsPdf(inputFileParamName.toString(), outputFileParam.toString())).thenReturn(fileId);
			Value<String> testId = exportAsPdf.execute(USERNAME, "BYNAME", fileName, fileId, destinationFolder,
					isOverride);
			Assert.assertEquals(fileId, testId.toString());
		} catch (GeneralSecurityException | IOException | JSONException e) {
			Assert.fail();
		}
	}

	@Test
	public void exportAsPdf_returnsDownloadedFileId_whenRequiredFileIdProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(exportAsPdf).getDrive(USERNAME);
			String fileName = "trial_file123";
			String fileId = "trial_file.txt";
			String destinationFolder = "";
			Boolean isOverride = true;
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileId", fileId);

			JSONObject outputFileParam = new JSONObject();
			outputFileParam.put("fileName", destinationFolder);
			outputFileParam.put("isOverride", isOverride);

			when(mockGDrive.exportAsPdf(inputFileParamName.toString(), outputFileParam.toString())).thenReturn(fileId);
			Value<String> testId = exportAsPdf.execute(USERNAME, "BYID", fileName, fileId, destinationFolder,
					isOverride);
			Assert.assertEquals(fileId, testId.toString());
		} catch (GeneralSecurityException | IOException | JSONException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void exportAsPdf_throwsBotCommandException_whenExportAsPDFAPIthrowsIOException() {
		try {
			Mockito.doReturn(mockGDrive).when(exportAsPdf).getDrive(USERNAME);
			String fileName = "trial_file123";
			String fileId = "trial_file.txt";
			String destinationFolder = "";
			Boolean isOverride = true;
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			JSONObject outputFileParam = new JSONObject();
			outputFileParam.put("fileName", destinationFolder);
			outputFileParam.put("isOverride", isOverride);

			when(mockGDrive.exportAsPdf(inputFileParamName.toString(), outputFileParam.toString()))
					.thenThrow(IOException.class);
			Value<String> testId = exportAsPdf.execute(USERNAME, "BYNAME", fileName, fileId, destinationFolder,
					isOverride);

			Assert.fail();
		} catch (GeneralSecurityException | JSONException e) {
			Assert.fail();
		} catch (IOException e) {
			Assert.assertTrue(true);
		}
	}

	@Test
	public void getDrive_newGDriveImplObjectIsNotNull() {
		try {
			GDrive drive = exportAsPdf.getDrive(USERNAME);
			assertNotNull(drive);
		} catch (GeneralSecurityException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
	}
}
