/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
package com.automationanywhere.botcommands.gsuite.authorization.commands;

import org.junit.BeforeClass;
import org.junit.Test;

import com.automationanywhere.botcommands.gsuite.authorization.utilities.AuthTestSetUp;
import com.automationanywhere.botcommands.gsuite.authorization.utilities.ConstantsTest;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.core.security.SecureString;

/***
 * Tests on the methods found in DeleteCell
 */
public class DisconnectTest extends AuthTestSetUp {

	/***
	 * Creates a valid session and opens WorkBook for testing
	 */
	@BeforeClass
	public static void doSetup() {
		doInitialSetup();
		connect.execute(ConstantsTest.getUserName(), ConstantsTest.getClientid(), ConstantsTest.getRedirectUri(),
				ConstantsTest.getClientSecret());
	}

	/***
	 * Delete Cell with respective inputs parameters.
	 */
	@Test
	public void disconnectTestMethod1() {
		disConnect.execute(ConstantsTest.getUserName(), true);

	}

	/***
	 * Check method with invalid parameters. It will return BotCommandException.
	 */
	@Test(expected = BotCommandException.class)
	public void disconnectTestMethod2() {
		disConnect.execute(new SecureString("RandomEmail".toCharArray()), true);

	}
}
