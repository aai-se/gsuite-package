/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommand.gsuite.dto.calendar;

public enum Visibility {

    DEFAULT ("default"),
    PUBLIC ("public"),
    PRIVATE ("private"),
    CONFIDENTIAL ("confidential");

    private final String name;

    Visibility(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
