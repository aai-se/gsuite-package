/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommands.gsuite.drive.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

/**
 * Tests delete Google Drive file functionality.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

public class DeleteFolderTest {

	public static final SecureString USERNAME = ConstantsTest.getUserName();

	@Spy
	DeleteFolder deleteFolder;

	@Mock
	GDrive mockGDrive;

	@Mock
	AuthenticationService mockAuthService;

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {
		MockitoAnnotations.initMocks(this);
		deleteFolder.setAuthenticationService(mockAuthService);
	}

	@Test
	public void deleteFolder_returnsTrueIfDeleted_whenRequiredFolderNameProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(deleteFolder).getDrive(USERNAME);

			String folderId = "DeleteTest";
			String folderName = "DeleteTest";
			JSONObject inputFolderParamName = new JSONObject();
			inputFolderParamName.put("fileName", folderName);

			when(mockGDrive.deleteFolder(inputFolderParamName.toString())).thenReturn(true);
			BooleanValue testId = deleteFolder.execute(USERNAME, "BYNAME", folderName, folderId);
			Assert.assertTrue(testId.get());
		} catch (GeneralSecurityException | IOException | JSONException e) {
			Assert.fail();
		}
	}

	@Test
	public void deleteFolder_returnsTrueIfDeleted_whenRequiredFolderIdProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(deleteFolder).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			JSONObject inputFolderParamID = new JSONObject();
			inputFolderParamID.put("fileId", fileId);

			when(mockGDrive.deleteFolder(inputFolderParamID.toString())).thenReturn(true);
			BooleanValue testId = deleteFolder.execute(USERNAME, "BYID", fileName, fileId);
			Assert.assertTrue(testId.get());
		} catch (GeneralSecurityException | IOException | JSONException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void deleteFolder_throwsBotCommandException_whenDeleteFolderAPIthrowsIOException() {
		try {
			Mockito.doReturn(mockGDrive).when(deleteFolder).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			JSONObject inputFolderParamName = new JSONObject();
			inputFolderParamName.put("fileName", fileName);

			when(mockGDrive.deleteFolder(inputFolderParamName.toString())).thenThrow(IOException.class);
			deleteFolder.execute(USERNAME, "BYNAME", fileName, fileId);

			Assert.fail();
		} catch (GeneralSecurityException | JSONException e) {
			Assert.fail();
		} catch (IOException e) {
			Assert.assertTrue(true);
		}
	}

	@Test
	public void getDrive_newGDriveImplObjectIsNotNull() {
		try {
			GDrive drive = deleteFolder.getDrive(USERNAME);
			assertNotNull(drive);
		} catch (GeneralSecurityException | IOException e) {
			fail();
		}
	}
}
