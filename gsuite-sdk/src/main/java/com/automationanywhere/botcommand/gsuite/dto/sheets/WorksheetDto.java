/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommand.gsuite.dto.sheets;

public class WorksheetDto {

    private String session;
    private String sheetName;
    private int index;

    private Integer sheetId;

    private String spreadSheetId;       // ID of the parent spread sheet where this worksheet resides
    private boolean ishidden;


    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Integer getSheetId() {
        return sheetId;
    }

    public void setSheetId(Integer sheetId) {
        this.sheetId = sheetId;
    }


    public String getSpreadSheetId() {
        return spreadSheetId;
    }

    public void setSpreadSheetId(String spreadSheetId) {
        this.spreadSheetId = spreadSheetId;
    }
    
    public boolean getIshidden() {
		return ishidden;
	}

	public void setIshidden(boolean ishidden) {
		this.ishidden = ishidden;
	}

	@Override
    public String toString() {
        return "WorksheetDto{" +
                "session='" + session + '\'' +
                ", sheetName='" + sheetName + '\'' +
                ", index=" + index +
                ", sheetId=" + sheetId +
                ", spreadSheetId='" + spreadSheetId + '\'' +
                ", isHidden='" + ishidden + '\'' +
                '}';
    }
}
