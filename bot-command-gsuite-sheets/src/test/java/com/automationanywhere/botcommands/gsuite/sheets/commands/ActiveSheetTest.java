/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.api.impl.GDriveImpl;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.core.security.SecureString;

public class ActiveSheetTest {
	private static final Logger LOGGER = LogManager.getLogger(ActiveSheetTest.class);

	@Spy
	ActivateSheet activateSheet;

	@Spy
	AuthenticationService authenticationService;

	@Spy
	GSheets gsheet;

	@Mock
	GSheets mockGSheets;

	@Mock
	GDriveImpl mockGDriveImpl;

	@Mock
	OpenSheets openSheets;

	final String session = "session";
	final String sheetName = "sheetname";
	final String user = "saanchi@aademo.page";
	final SecureString userName = new SecureString(user.toCharArray());
	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {

		MockitoAnnotations.initMocks(this);
		sessionMap.put(session, mockGSheets);
		activateSheet.setSessionMap(sessionMap);

	}

	@Test
	public void activateSheetByIndex() {
		when(mockGSheets.activateSheet(true, 1, null)).thenReturn(true);
		Boolean returnedValue = activateSheet.execute(session, "true", 1, null).get();
		Assert.assertTrue(returnedValue);
	}

	@Test
	public void activateSheetByName() {
		when(mockGSheets.activateSheet(false, -1, "Megha")).thenReturn(true);
		Boolean returnedValue = activateSheet.execute(session, "false", -1, "Megha").get();
		Assert.assertTrue(returnedValue);
	}

	@Test(expectedExceptions = Exception.class)
	public void aainvalidIndexException() {
		when(mockGSheets.activateSheet(true, -1, null)).thenThrow(IOException.class);
		activateSheet.execute("Default", "true", -1, null);
		Assert.fail();
	}

	@Test(expectedExceptions = Exception.class)
	public void invalidNameException() {
		when(mockGSheets.activateSheet(false, -1, "wrong name")).thenThrow(IOException.class);
		activateSheet.execute("Default", "false", -1, "wrong name");
		Assert.fail();
	}
}
