/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommands.gsuite.sheets.commands.OpenSheets;
import com.automationanywhere.core.security.SecureString;

public class OpenSheetJunit extends AbstractAuthUtils {
	OpenSheets openSheets;

	final String session = "session";
	final String user = "saanchi@aademo.page";
	final String ClientID = "1045113049707-u3fhdtroech2uch19jogs52fg98t3lpu.apps.googleusercontent.com";
	final String redirectUrl = "http://localhost";
	final String clientSecreet = "6rjSOWzAEKEdiFqpCDGxirkk";

	final SecureString userName = new SecureString(user.toCharArray());
	final SecureString clientId = new SecureString(ClientID.toCharArray());
	final SecureString redirectURI = new SecureString(redirectUrl.toCharArray());
	final SecureString clientSecret = new SecureString(clientSecreet.toCharArray());

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeClass
	public void setup() throws IOException, GeneralSecurityException {
		launchOAuthConsentScreen(userName, clientId, clientSecret, redirectURI);
		openSheets = new OpenSheets();
		openSheets.setSessionMap(sessionMap);
	}

//	@Test
//	public void openSheetBYName() {
//		openSheets.execute(userName, session, "BYNAME", "Demo", null, null, false, null, false);
//	}
	@Test
	public void openSheetBYID() {
		openSheets.execute(userName, "session", "BYURL", "Demo", "https://docs.google.com/spreadsheets/d/11aknRAD_VcEiDcirTr7EZWnz56aQiFmGhY354-IP-Dc/edit#gid=0", null, false, null, false);
	}
	
//	@Test
//	public void openSheetBYURL() {
//		openSheets.execute(userName, "session2", "BYID", "Demo", null,"11aknRAD_VcEiDcirTr7EZWnz56aQiFmGhY354-IP-Dc", false,null, false);
//	}
	
}
