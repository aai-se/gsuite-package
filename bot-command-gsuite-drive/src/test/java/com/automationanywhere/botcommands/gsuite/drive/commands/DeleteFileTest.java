/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommands.gsuite.drive.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

/**
 * Tests delete Google Drive file functionality.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

public class DeleteFileTest {

	public static final SecureString USERNAME = ConstantsTest.getUserName();

	@Spy
	DeleteFile deleteFile;

	@Mock
	GDrive mockGDrive;

	@Mock
	AuthenticationService mockAuthService;

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {
		MockitoAnnotations.initMocks(this);
		deleteFile.setAuthenticationService(mockAuthService);
	}

	@Test
	public void deleteFile_returnsTrueIfDeleted_whenRequiredFileNameProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(deleteFile).getDrive(USERNAME);

			String fileId = "senthil";
			String fileName = "senthil";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			when(mockGDrive.deleteFile(inputFileParamName.toString())).thenReturn(true);
			BooleanValue testId = deleteFile.execute(USERNAME, "BYNAME", fileName, fileId);
			Assert.assertTrue(testId.get());
		} catch (GeneralSecurityException | IOException | JSONException e) {
			Assert.fail();
		}
	}

	@Test
	public void deleteFile_returnsTrueIfDeleted_whenRequiredFileIdProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(deleteFile).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			JSONObject inputFileParamID = new JSONObject();
			inputFileParamID.put("fileId", fileId);

			when(mockGDrive.deleteFile(inputFileParamID.toString())).thenReturn(true);
			BooleanValue testId = deleteFile.execute(USERNAME, "BYID", fileName, fileId);
			Assert.assertTrue(testId.get());
		} catch (GeneralSecurityException | IOException | JSONException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void deleteFile_throwsBotCommandException_whenDeleteFileAPIthrowsIOException() {
		try {
			Mockito.doReturn(mockGDrive).when(deleteFile).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			when(mockGDrive.deleteFile(inputFileParamName.toString())).thenThrow(IOException.class);
			deleteFile.execute(USERNAME, "BYNAME", fileName, fileId);

			Assert.fail();
		} catch (GeneralSecurityException | JSONException e) {
			Assert.fail();
		} catch (IOException e) {
			Assert.assertTrue(true);
		}
	}

	@Test
	public void getDrive_newGDriveImplObjectIsNotNull() {
		try {
			GDrive drive = deleteFile.getDrive(USERNAME);
			assertNotNull(drive);
		} catch (GeneralSecurityException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
	}
}
