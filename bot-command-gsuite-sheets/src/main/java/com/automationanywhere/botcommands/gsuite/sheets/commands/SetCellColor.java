/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.ACTIVECELL;
import static com.automationanywhere.botcommand.gsuite.Constants.CELL;
import static com.automationanywhere.botcommand.gsuite.Constants.COLON;
import static com.automationanywhere.botcommand.gsuite.Constants.DEFAULT_VALUE_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.DESCRIPTION_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_ACTIVECELL;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_MULTIPLECELL;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_SPECIFICCELL;
import static com.automationanywhere.botcommand.gsuite.Constants.RANGE;
import static com.automationanywhere.botcommand.gsuite.Constants.SPECIFICCELL;
import static com.automationanywhere.botcommand.gsuite.Constants.TEXT;
import static com.automationanywhere.botcommand.gsuite.Constants.cellAddressValidator;
import static com.automationanywhere.botcommand.gsuite.Constants.rangeValidator;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Idx.Option;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.MatchesRegex;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

@BotCommand
@CommandPkg(label = "Set cell/text color", name = "setCellColor", description = "Sets the color of specified cells", node_label = "at {{cellOption}} |{{cellAddress}} |{{cellRange}} |in session {{sessionName}}", icon = "excel.svg")
public class SetCellColor {
	private static final Logger LOGGER = LogManager.getLogger(SetCellColor.class);

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public BooleanValue execute(
			@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String session,
			@Idx(index = "2", type = AttributeType.RADIO, options = {
					@Option(index = "2.1", pkg = @Pkg(label = LABEL_ACTIVECELL, value = ACTIVECELL)),
					@Option(index = "2.2", pkg = @Pkg(label = LABEL_SPECIFICCELL, value = SPECIFICCELL)),
					@Option(index = "2.3", pkg = @Pkg(label = LABEL_MULTIPLECELL, value = RANGE)) }) @Pkg(label = "Cell options", default_value_type = DataType.STRING, default_value = ACTIVECELL) @NotEmpty String cellOption,
			@Idx(index = "2.2.1", type = AttributeType.TEXT) @Pkg(label = "Cell name", description = "e.g. A1") @MatchesRegex(cellAddressValidator) @VariableType(value = DataType.STRING) @NotEmpty String cellAddress,
			@Idx(index = "2.3.1", type = AttributeType.TEXT) @Pkg(label = "Cell range", description = "e.g. A1:D10") @MatchesRegex(rangeValidator) @NotEmpty String cellRange,
			@Idx(index = "3", type = AttributeType.RADIO, options = {
					@Option(index = "3.1", pkg = @Pkg(label = "Cell", value = CELL)),
					@Option(index = "3.2", pkg = @Pkg(label = "Text within cell", value = TEXT)) }) @Pkg(label = "Set color to", default_value_type = DataType.STRING, default_value = CELL) @NotEmpty String setColorOption,

			@Idx(index = "4", type = AttributeType.TEXT) @Pkg(label = "Cell color by Name/Code", description = "e.g. Red/#FF0000") @VariableType(value = DataType.STRING) @NotEmpty String color) {

		try {
		GSheets gSheets = (GSheets) sessionMap.get(session);
		ExecutorValidator.getInstance().validate(gSheets);
		if (ACTIVECELL.equals(cellOption)) {
			return new BooleanValue(gSheets.setCellColor(gSheets.getSpreadSheetId(),
					gSheets.getActiveCell(gSheets.getActiveSheetInfo().getSheetName()), setColorOption, color));
		} else if (SPECIFICCELL.equals(cellOption)) {
			boolean result=gSheets.setCellColor(gSheets.getSpreadSheetId(), cellAddress, setColorOption, color);
			// updating active cell.
			gSheets.setActiveCell(gSheets.getActiveSheetInfo().getSheetName(), cellAddress);
			return new BooleanValue(result);
		} else {
			boolean result= gSheets.setCellColor(gSheets.getSpreadSheetId(), cellRange, setColorOption, color);
			// updating active cell.
			gSheets.setActiveCell(gSheets.getActiveSheetInfo().getSheetName(), cellRange.split(COLON)[1]);
			return new BooleanValue(result);
		}
		} catch (IOException exception) {
			LOGGER.error("Exception occurred during execution of Set cell color command.....", exception);
			throw new BotCommandException(exception);
		}
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}
