/*
 * Copyright (c) 2018 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommand.gsuite.validators;

import com.automationanywhere.botcommand.exception.BotCommandException;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class GenericValidator {

    public static <T, U> void validate(BiPredicate<T, U> biPredicate, T firstValue, U secondValue, String validationMessage) {
        if (biPredicate.test(firstValue, secondValue)) {
            throw new BotCommandException(validationMessage);
        }
    }

    public static <T> void validate(Predicate<T> predicate, T firstValue, String validationMessage) {
        if (predicate.test(firstValue)) {
            throw new BotCommandException(validationMessage);
        }
    }
}
