/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.USERNAME;
import static com.automationanywhere.commandsdk.model.AttributeType.RADIO;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.api.impl.GDriveImpl;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommand.gsuite.service.impl.AuthenticationServiceImpl;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import com.google.common.annotations.VisibleForTesting;

/**
 * Uploads a Google Drive file from the local machine location where the Bot
 * Agent is running to the specified location on Google Drive.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

@BotCommand
@CommandPkg(label = "Upload file", name = "UploadFile", description = "Uploads a file from specified folder on Google Drive", node_label = "{{fileLocation}} to destination location |{{fileName}}|folder id {{fileId}}|", return_type = DataType.STRING, return_required = false, return_label = "Returns the id of the uploaded file")

public class UploadFile {
	private static final Logger LOGGER = LogManager.getLogger(UploadFile.class);
	private static final String LOC_DESC = "e.g. C:/User/uploadfile.doc";
	private static final String FOL_NAME = "e.g. <UserHomeDirectory>/GSuite/MyFolder";
	private static final String FOL_ID = "GDrive folder id from document link e.g. sfdu9887FFskjfd899w9";
	private AuthenticationService mAuthenticationService;

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public Value<String> execute(
			@Idx(index = "1", type = AttributeType.CREDENTIAL) @Pkg(label = USERNAME) @NotEmpty SecureString userEmailAddress,
			@Idx(index = "2", type = TEXT) @Pkg(label = "File name (with Location)", description = LOC_DESC) @NotEmpty String fileLocation,
			@Idx(index = "3", type = RADIO, options = {
					@Idx.Option(index = "3.1", pkg = @Pkg(label = "Folder name", value = "BYNAME")),
					@Idx.Option(index = "3.2", pkg = @Pkg(label = "Folder ID", value = "BYID")) }) @Pkg(label = "Upload file", default_value_type = DataType.STRING, default_value = "BYNAME") @NotEmpty String fileOption,
			@Idx(index = "3.1.1", type = TEXT) @Pkg(label = "", description = FOL_NAME, default_value = "", default_value_type = DataType.STRING) @NotEmpty String fileName,
			@Idx(index = "3.2.1", type = TEXT) @Pkg(label = "", description = FOL_ID, default_value = "", default_value_type = DataType.STRING) @NotEmpty String fileId,
			@Idx(index = "4", type = AttributeType.CHECKBOX) @Pkg(label = "Overwrite existing file") Boolean overwriteFile) {
		try {
			JSONObject inputFileParam = new JSONObject();
			if (fileOption.equals("BYNAME")) {
				inputFileParam.put("fileName", fileName);
			} else if (fileOption.equals("BYID")) {
				inputFileParam.put("fileId", fileId);
			}
			return new StringValue(
					getDrive(userEmailAddress).uploadFile(fileLocation, inputFileParam.toString(), overwriteFile));
		} catch (GeneralSecurityException | IOException | JSONException e) {
			LOGGER.error("Exception in uploading Google Drive file: ", e);
			String msg = e.getMessage();
			throw new BotCommandException(msg, e);
		}
	}

	GDrive getDrive(SecureString userEmailAddress) throws GeneralSecurityException, IOException {
		AuthenticationService authenticationService = getAuthenticationService();
		return new GDriveImpl(authenticationService, userEmailAddress.getInsecureString(), sessionMap);
	}

	@VisibleForTesting
	void setAuthenticationService(AuthenticationService mAuthenticationService) {
		this.mAuthenticationService = mAuthenticationService;
	}

	synchronized AuthenticationService getAuthenticationService() {
		if (mAuthenticationService == null) {
			mAuthenticationService = new AuthenticationServiceImpl();
		}
		return mAuthenticationService;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}
