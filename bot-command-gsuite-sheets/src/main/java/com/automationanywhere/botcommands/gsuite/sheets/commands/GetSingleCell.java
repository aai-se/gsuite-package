/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.ACTIVECELL;
import static com.automationanywhere.botcommand.gsuite.Constants.UNFORMATTED_VALUE;
import static com.automationanywhere.commandsdk.model.AttributeType.RADIO;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
import java.io.IOException;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.Constants;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.MatchesRegex;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.google.api.services.sheets.v4.model.ValueRange;

@BotCommand
@CommandPkg(name = "GetSingleCell", label = "Get single cell", node_label = "value of {{cell}} from {{sheetName}} worksheet", description = "Gets the value of a single cell within a worksheet", return_label = "Store cell contents to", return_type = STRING, return_required = true, icon = "excel.svg")
public class GetSingleCell {
	private static final Logger LOGGER = LogManager.getLogger(GetSingleCell.class);

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public Value<?> execute(
			@Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value = "Default", default_value_type = STRING) @NotEmpty String session,
			@Idx(index = "2", type = RADIO, options = {
					@Idx.Option(index = "2.1", pkg = @Pkg(label = "Active cell", value = "ACTIVE_CELL")),
					@Idx.Option(index = "2.2", pkg = @Pkg(label = "Specific cell", value = "SPECIFIC_CELL")) }) @Pkg(label = "Cell option", default_value = "ACTIVE_CELL", default_value_type = STRING) @NotEmpty String cellOption,
			@Idx(index = "2.2.1", type = TEXT) @Pkg(label = "Cell name", description = "e.g. A1") @MatchesRegex("^[A-Z]{1,2}[A-D]?[0-9]{1,7}") @NotEmpty String cell) {
		String cellValue = Constants.EMPTY;
		try {
			GSheets gSheets = (GSheets) sessionMap.get(session);
			ExecutorValidator.getInstance().validate(gSheets);

			String sheetName = gSheets.getActiveSheetInfo().getSheetName();
			ValueRange cellValueRange;
			if (ACTIVECELL.equals(cellOption)) {
				cellValueRange = gSheets.getValue(gSheets.getSpreadSheetId(), sheetName,
						gSheets.getActiveCell(sheetName), UNFORMATTED_VALUE);
			} else
				cellValueRange = gSheets.getValue(gSheets.getSpreadSheetId(), sheetName, cell, UNFORMATTED_VALUE);
			LOGGER.info("Valuerange = " + cellValueRange);

			if (cellValueRange.getValues() != null) {
				cellValue = (String) cellValueRange.getValues().get(0).get(0);
			}
			LOGGER.info("Cellvalue = " + cellValue);
		} catch (IOException exception1) {
			throw new BotCommandException(exception1.getMessage());
		} catch (Exception exception2) {
			throw new BotCommandException(exception2.getMessage());
		}

		return new StringValue(cellValue);
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}
