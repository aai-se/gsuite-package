/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.botcommand.gsuite.Constants.*;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.MatchesRegex;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

@BotCommand
@CommandPkg(label = "Delete range", name = "deleteRange", description = "Deletes the cell range with specified address", node_label = "{{rangeAddress}} |and shift cells {{shiftType}} |in session {{sessionName}}", icon = "Excel_blue.svg")
public class DeleteRange {
	private static final Logger LOGGER = LogManager.getLogger(InsertRange.class);

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public BooleanValue execute(
			@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String sessionName,
			@Idx(index = "2", type = AttributeType.TEXT) @Pkg(label = "Range address", description = "Enter the range address e.g A1:B4") @MatchesRegex(rangeValidator) @VariableType(value = DataType.STRING) @NotEmpty String rangeAddress,
			@Idx(index = "3", type = AttributeType.RADIO, options = {
					@Idx.Option(index = "3.1", pkg = @Pkg(label = "Up", value = ROWS)),
					@Idx.Option(index = "3.2", pkg = @Pkg(label = "Left", value = COLUMNS)) }) @Pkg(label = "Shift type", description = "Select the shift type", default_value = ROWS, default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String shiftType) {
		try {
			GSheets gSheets = (GSheets) sessionMap.get(sessionName);
			ExecutorValidator.getInstance().validate(gSheets);
			return new BooleanValue(gSheets.deleteRange(rangeAddress, shiftType));
		} catch (IOException e) {
			LOGGER.error("Cannot Delete Range from sheet: ", e);
			throw new BotCommandException(e.getMessage());
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
			throw new BotCommandException(ex.getMessage());
		}
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}
