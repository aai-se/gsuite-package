/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommands.gsuite.drive.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.io.IOException;
import java.security.GeneralSecurityException;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

/**
 * Tests move Google Drive file functionality.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

public class MoveFolderTest {

	public static final SecureString USERNAME = ConstantsTest.getUserName();

	@Spy
	MoveFolder moveFolder;

	@Mock
	GDrive mockGDrive;

	@Mock
	AuthenticationService mockAuthService;

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		moveFolder.setAuthenticationService(mockAuthService);
	}

	@Test
	public void moveFolder_returnsMovedFolderId_whenRequiredFolderNameProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(moveFolder).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "PK/PKTest";
			JSONObject inputFolderParamName = new JSONObject();
			inputFolderParamName.put("fileName", fileName);

			String folderId = "trial_file123";
			String folderName = "folderTrial";
			JSONObject outputFolderParamName = new JSONObject();
			outputFolderParamName.put("fileName", folderName);

			when(mockGDrive.moveFolder(inputFolderParamName.toString(), outputFolderParamName.toString()))
					.thenReturn(fileId);
			Value<String> testId = moveFolder.execute(USERNAME, "BYNAME", fileName, fileId, "BYNAME", folderName,
					folderId);
			Assert.assertEquals(fileId, testId.toString());
		} catch (GeneralSecurityException | JSONException | IOException e) {
			Assert.fail();
		}
	}

	@Test
	public void moveFolder_returnsMovedFolderId_whenRequiredFolderIdProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(moveFolder).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			JSONObject inputFolderParamID = new JSONObject();
			inputFolderParamID.put("fileId", fileId);

			String folderId = "trial_file123";
			String folderName = "trial_file.txt";

			JSONObject outputFolderParamID = new JSONObject();
			outputFolderParamID.put("fileId", folderId);

			when(mockGDrive.moveFolder(inputFolderParamID.toString(), outputFolderParamID.toString()))
					.thenReturn(fileId);
			Value<String> testId = moveFolder.execute(USERNAME, "BYID", fileName, fileId, "BYID", folderName, folderId);
			Assert.assertEquals(fileId, testId.toString());
		} catch (GeneralSecurityException | JSONException | IOException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void moveFolder_throwsBotCommandException_whenMoveFolderAPIthrowsIOException() {
		try {
			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			JSONObject inputFolderParamName = new JSONObject();
			inputFolderParamName.put("fileName", fileName);

			String folderId = "trial_file123";
			String folderName = "trial_file.txt";
			JSONObject outputFolderParamName = new JSONObject();
			outputFolderParamName.put("fileName", folderName);

			Mockito.doReturn(mockGDrive).when(moveFolder).getDrive(USERNAME);
			when(mockGDrive.moveFolder(inputFolderParamName.toString(), outputFolderParamName.toString()))
					.thenThrow(IOException.class);
			moveFolder.execute(USERNAME, "BYNAME", fileName, fileId, "BYNAME", folderName, folderId);
			Assert.fail();
		} catch (GeneralSecurityException | JSONException e) {
			Assert.fail();
		} catch (IOException e) {
			Assert.assertTrue(true);
		}

	}

	@Test
	public void getDrive_newGDriveImplObjectIsNotNull() {
		try {
			GDrive drive = moveFolder.getDrive(USERNAME);
			assertNotNull(drive);
		} catch (GeneralSecurityException | IOException e) {
			fail();
		}
	}
}
