/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.api.impl.GDriveImpl;
import com.automationanywhere.botcommand.gsuite.dto.drive.FileDto;
import com.automationanywhere.botcommand.gsuite.dto.sheets.SpreadSheetDto;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.core.security.SecureString;

public class OpenSheetsTest {

	private static final Logger LOGGER = LogManager.getLogger(OpenSheetsTest.class);

	@Spy
	OpenSheets openSheets;

	@Spy
	AuthenticationService authenticationService;

	@Spy
	GSheets gsheet;

	@Mock
	GSheets mockGSheets;

	@Mock
	GDriveImpl mockGDriveImpl;

	@Mock
	FileDto fileDto;

	final String session = "session";
	final String user = "saanchi@aademo.page";
	final SecureString userName = new SecureString(user.toCharArray());
	final String user1 = "arpit@aademo.page";
	final SecureString userName1 = new SecureString(user1.toCharArray());

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeTest
	public void setup() {
		MockitoAnnotations.initMocks(this);
		sessionMap.put(session, mockGSheets);
		openSheets.setSessionMap(sessionMap);
	}

	@Test
	public void openSheetsByURL() {
		try {
			Mockito.doNothing().when(openSheets).validateSessionExists(sessionMap, session);
			Mockito.doReturn(mockGSheets).when(openSheets).getSheets(session, userName);
			SpreadSheetDto spreadSheetDto = new SpreadSheetDto("1baZfKEURJW6zPFZfKEURJWP-ZfKEURJWZg5Xhyzs");

			when(mockGSheets.getSpreadSheetId()).thenReturn("123456");
			when(mockGSheets.openSpreadSheet(mockGSheets.getSpreadSheetId())).thenReturn(spreadSheetDto);

			Assert.assertNull(openSheets.execute(userName, session, "BYURL", null,
					"https://docs.google.com/spreadsheets/d/1baZfKEURJW6zPFZfKEURJWP-ZfKEURJWZg5Xhyzs/edit#gid=0", null,
					true, "Sheet1", false));

		} catch (GeneralSecurityException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
	}

	@Test
	public void openSheetsByName() {
		try {
			Mockito.doNothing().when(openSheets).validateSessionExists(sessionMap, session);
			Mockito.doReturn(mockGSheets).when(openSheets).getSheets(session, userName);
			SpreadSheetDto spreadSheetDto = new SpreadSheetDto("1baZfKEURJW6zPFZfKEURJWP-ZfKEURJWZg5Xhyzs");

			Mockito.doReturn(mockGDriveImpl).when(openSheets).getDrive(userName);
			when(mockGDriveImpl.findFile(eq("Demo"))).thenReturn(fileDto);
			when(mockGSheets.getSpreadSheetId()).thenReturn("123456");
			when(mockGSheets.openSpreadSheet(mockGSheets.getSpreadSheetId())).thenReturn(spreadSheetDto);

			Assert.assertNull(
					openSheets.execute(userName, session, "BYNAME", "Demo", null, null, true, "Sheet1", false));

		} catch (GeneralSecurityException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
	}

	@Test
	public void openSheetsByID() {
		try {
			Mockito.doNothing().when(openSheets).validateSessionExists(sessionMap, session);
			Mockito.doReturn(mockGSheets).when(openSheets).getSheets(session, userName);
			SpreadSheetDto spreadSheetDto = new SpreadSheetDto("1baZfKEURJW6zPFZfKEURJWP-ZfKEURJWZg5Xhyzs");
			when(mockGSheets.getSpreadSheetId()).thenReturn("123456");
			when(mockGSheets.openSpreadSheet(mockGSheets.getSpreadSheetId())).thenReturn(spreadSheetDto);
			Assert.assertNull(openSheets.execute(userName, session, "BYID", null, null,
					"1baZfKEURJW6zPFZfKEURJWP-ZfKEURJWZg5Xhyzs", true, "Sheet1", false));

		} catch (GeneralSecurityException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
	}

	@Test
	public void getSheets() {
		try {
			GSheets sheets = openSheets.getSheets(session, userName);
			Assert.assertNotNull(sheets);
		} catch (GeneralSecurityException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
	}

	@Test
	public void getDrive() {
		try {
			GDrive Ggrive = openSheets.getDrive(userName);
			Assert.assertNotNull(Ggrive);
		} catch (GeneralSecurityException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
	}

	@Test
	public void getAuthenticationService() {
		try {
			AuthenticationService getAuthenticationService = openSheets.getAuthenticationService();
			Assert.assertNotNull(getAuthenticationService);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void getSheetsWithNullSession() {
		try {
			Mockito.doReturn(authenticationService).when(openSheets).getAuthenticationService();
			GSheets sheets = openSheets.getSheets("test", userName);
			Assert.assertNotNull(sheets);
		} catch (GeneralSecurityException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
	}

	@Test(expectedExceptions = { BotCommandException.class })
	public void botCommandExceptionTest() {
		try {
			Mockito.doReturn(mockGSheets).when(openSheets).getSheets(session, userName);
			openSheets.execute(userName, session, "BYURL", "",
					"https://docs.google.com/spreadsheets1baZfKEURJW6zPFZfKEURJWP-ZfKEURJWZg5Xhyzs/edit#gid=0", "",
					true, "Sheet1", false);
		} catch (GeneralSecurityException g) {
			LOGGER.error("GeneralSecurityException");
		} catch (IOException i) {
			LOGGER.error("IOException");
		}
	}

	@Test(expectedExceptions = { BotCommandException.class })
	public void getSheets_invalidurl() {
		try {
			Mockito.doNothing().when(openSheets).validateSessionExists(sessionMap, session);
			Mockito.doReturn(mockGSheets).when(openSheets).getSheets(session, userName);
			openSheets.execute(userName, session, "BYURL", null, "5161", null, true, "Sheet1", false);

		} catch (GeneralSecurityException g) {
			LOGGER.error("GeneralSecurityException");
		} catch (IOException e) {
			Assert.assertTrue(true);
		}
	}

	@Test(expectedExceptions = { BotCommandException.class })
	public void getSheets_GeneralException() {
		try {
			Mockito.doNothing().when(openSheets).validateSessionExists(sessionMap, session);
			Mockito.doThrow(new GeneralSecurityException()).when(openSheets).getSheets(session, userName);
			openSheets.execute(userName, session, "BYURL", null, "5161", null, true, "Sheet1", false);

		} catch (GeneralSecurityException g) {
			LOGGER.error("GeneralSecurityException");
			Assert.assertTrue(true);
		} catch (IOException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = { BotCommandException.class })
	public void illegalArgumentExceptionTest() {
		try {

			Mockito.doNothing().when(openSheets).validateSessionExists(sessionMap, session);
			Mockito.doReturn(mockGSheets).when(openSheets).getSheets(session, userName);
			Mockito.doReturn(mockGDriveImpl).when(openSheets).getDrive(userName);
			when(mockGDriveImpl.findFile(eq("Demo"))).thenReturn(null);
			openSheets.execute(userName, session, "BYNAME", "Demo", null, null, true, "Sheet1", false);

		} catch (IllegalArgumentException i) {
			Assert.assertTrue(true);
		} catch (IOException | GeneralSecurityException g) {
			Assert.fail();
		}
	}

}
