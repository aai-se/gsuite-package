/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;

public class GetCurrentSheetNameTest {
	@Spy
	GetCurrentSheetName getCurrentSheetName;

	@Mock
	AuthenticationService authenticationService;

	@Mock
	GSheets mockGSheets;

	final String session = "session";
	final String newSession = "newsession";
	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		sessionMap.put(session, mockGSheets);
		getCurrentSheetName.setSessionMap(sessionMap);
	}

	@Test
	public void getCurrentWorkSheetName() {
		try {
			when(mockGSheets.getActiveSheetName()).thenReturn("test1234");
			Value<?> returned = getCurrentSheetName.execute(session);
			Assert.assertEquals(mockGSheets.getActiveSheetName(), returned.toString());

		} catch (Exception i) {
		}

	}

	@Test
	public void getCurrentWorkSheetNameExceptions() {
		try {
			Value<?> returned = getCurrentSheetName.execute(newSession);

		} catch (Exception i) {
			assertTrue("Execption Thrown", true);
		}

	}

}