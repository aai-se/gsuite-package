/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.dto.sheets.WorksheetDto;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;

public class DeleteCellTest {
	@Spy
	DeleteCell deleteCell;

	@Mock
	AuthenticationService authenticationService;

	@Mock
	GSheets mockGSheets;

	@Mock
	WorksheetDto activeSheetInfo;

	final String session = "session";

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		sessionMap.put("session", mockGSheets);
		deleteCell.setSessionMap(sessionMap);
	}

	@Test
	public void deleteCellWithShiftLeft() {
		try {
			when(mockGSheets.getActiveCell(anyString())).thenReturn("A1");
			when(mockGSheets.getActiveSheetInfo()).thenReturn(activeSheetInfo);
			when(activeSheetInfo.getSheetName()).thenReturn(new String());
			when(mockGSheets.deleteCell("LEFT", "A1")).thenReturn(true);
			deleteCell.execute(session, "true", "A1", "LEFT");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void deleteCellWithShiftUp() {
		try {
			when(mockGSheets.getActiveCell(anyString())).thenReturn("A1");
			when(mockGSheets.getActiveSheetInfo()).thenReturn(activeSheetInfo);
			when(activeSheetInfo.getSheetName()).thenReturn(new String());
			when(mockGSheets.deleteCell("UP", "A1")).thenReturn(true);
			Boolean response = deleteCell.execute(session, "true", "A1", "UP").get();
			assertTrue(response);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void deleteCellEntireRow() {
		try {
			when(mockGSheets.getActiveCell(anyString())).thenReturn("A1");
			when(mockGSheets.getActiveSheetInfo()).thenReturn(activeSheetInfo);
			when(activeSheetInfo.getSheetName()).thenReturn(new String());
			when(mockGSheets.deleteDimension("A1", "ROWS")).thenReturn(true);
			Boolean response = deleteCell.execute(session, "true", "A1", "ROWS").get();
			assertTrue(response);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void deleteCellEntireColumn() {
		try {
			when(mockGSheets.getActiveCell(anyString())).thenReturn("A1");
			when(mockGSheets.getActiveSheetInfo()).thenReturn(activeSheetInfo);
			when(activeSheetInfo.getSheetName()).thenReturn(new String());
			when(mockGSheets.deleteDimension("A1", "COLUMNS")).thenReturn(true);
			Boolean response = deleteCell.execute(session, "true", "A1", "COLUMNS").get();
			assertTrue(response);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void deleteCellIOException() {
		try {
			when(mockGSheets.getActiveCell(anyString())).thenReturn("A1");
			when(mockGSheets.getActiveSheetInfo()).thenReturn(activeSheetInfo);
			when(activeSheetInfo.getSheetName()).thenReturn(new String());
			when(mockGSheets.deleteDimension("A1", "COLUMNS")).thenThrow(new IOException());
			Boolean response = deleteCell.execute(session, "true", "A1", "COLUMNS").get();
		} catch (IOException e) {
		}
	}

}
