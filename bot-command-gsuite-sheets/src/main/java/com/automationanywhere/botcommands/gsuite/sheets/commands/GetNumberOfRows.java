package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.DEFAULT_VALUE_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.DESCRIPTION_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.NONEMPTYROWS;
import static com.automationanywhere.botcommand.gsuite.Constants.TOTALROWS;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Idx.Option;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

@BotCommand
@CommandPkg(label = "Get number of rows", name = "getNumberOfRows", description = "Gets row count from a google sheet", node_label = "and assign it to {{returnTo}} |in session {{sessionName}}", return_type = DataType.NUMBER, return_required = true, return_label = "Assign output to a variable")
public class GetNumberOfRows extends AbstractAuthUtils {
	private static final Logger LOGGER = LogManager.getLogger(GetNumberOfRows.class);

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public Value<?> execute(
			@Idx(index = "1", type = TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String session,
			@Idx(index = "2", type = AttributeType.RADIO, options = {
					@Option(index = "2.1", pkg = @Pkg(label = "Non-empty rows", value = NONEMPTYROWS)),
					@Option(index = "2.2", pkg = @Pkg(label = "Total rows with data", value = TOTALROWS)) }) @Pkg(label = "", default_value_type = DataType.STRING, default_value = "NONEMPTY") @NotEmpty String fetchRowsType) {

		try {
			validateSessionNotExists(sessionMap, session);
			GSheets gSheets = (GSheets) sessionMap.get(session);
			ExecutorValidator.getInstance().validate(gSheets);
			int numberOfRows = gSheets.getNumberOfRows(gSheets.getSpreadSheetId(), fetchRowsType);
			return new NumberValue(numberOfRows);
		} catch (IOException exception) {
			LOGGER.error("Exception in getting rows count ", exception);
			String msg = exception.getMessage();
			throw new BotCommandException(msg, exception);
		}

	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}
