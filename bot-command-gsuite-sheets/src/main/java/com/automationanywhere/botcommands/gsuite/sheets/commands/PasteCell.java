/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.DEFAULT_VALUE_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.DESCRIPTION_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.FALSE;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_ACTIVECELL;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_SPECIFICCELL;
import static com.automationanywhere.botcommand.gsuite.Constants.TRUE;
import static com.automationanywhere.botcommand.gsuite.Constants.cellAddressValidator;
import java.io.IOException;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Idx.Option;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.MatchesRegex;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

@BotCommand
@CommandPkg(label = "Paste cell", name = "pasteCell", description = "Pastes specified cell content to another cell", node_label = "to active cell | to specific cell {{cellAddress}} | in session {{sessionName}}")
public class PasteCell extends AbstractAuthUtils {
	private static final Logger LOGGER = LogManager.getLogger(PasteCell.class);

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public void execute(
			@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String session,
			@Idx(index = "2", type = AttributeType.RADIO, options = {
					@Option(index = "2.1", pkg = @Pkg(label = LABEL_ACTIVECELL, value = TRUE)),
					@Option(index = "2.2", pkg = @Pkg(label = LABEL_SPECIFICCELL, value = FALSE)) }) @Pkg(label = "Cell options", default_value_type = DataType.STRING, default_value = TRUE) @NotEmpty String cellOption,
			@Idx(index = "2.2.1", type = AttributeType.TEXT) @Pkg(label = "Cell name", description = "e.g. A1") @MatchesRegex(cellAddressValidator) @NotEmpty String cellAddress,
			@Idx(index = "3", type = AttributeType.TEXT) @Pkg(label = "Paste cell address", description = "Enter the cell address to be pasted on. e.g. A1 or B3") @VariableType(value = DataType.STRING) @MatchesRegex(cellAddressValidator) @NotEmpty String pasteCellAddress) {
		try {
			validateSessionNotExists(sessionMap, session);
			GSheets gSheets = (GSheets) sessionMap.get(session);
			ExecutorValidator.getInstance().validate(gSheets);
			if (Boolean.valueOf(cellOption)) {
				gSheets.pasteCell(gSheets.getActiveCell(gSheets.getActiveSheetInfo().getSheetName()), pasteCellAddress);
			} else
				gSheets.pasteCell(cellAddress, pasteCellAddress);

		} catch (IOException exception) {
			LOGGER.error("Exception in paste cell", exception);
			String msg = exception.getMessage();
			throw new BotCommandException(msg, exception);
		}

	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
}
