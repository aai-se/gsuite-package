package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.DEFAULT_VALUE_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.DESCRIPTION_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.SHEET_NAME_VALIDATOR;

import java.io.IOException;
import java.util.Map;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.MatchesRegex;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

@BotCommand
@CommandPkg(label = "Hide worksheet", name = "hideWorksheet", description = "Hides the worksheet with specified name", node_label = "{{worksheetName}} |in session {{sessionName}}", icon = "excel.svg")
public class HideWorksheet {
	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public BooleanValue execute(
			@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String sessionName,
			@Idx(index = "2", type = AttributeType.TEXT) @Pkg(label = "Worksheet name", description = "e.g. Sheet1, sheet name should not exceed 31 characters.") @VariableType(value = DataType.STRING) @MatchesRegex(SHEET_NAME_VALIDATOR) @NotEmpty String worksheetName) {

		try {
			GSheets gSheets = (GSheets) sessionMap.get(sessionName);
			ExecutorValidator.getInstance().validate(gSheets);
			return new BooleanValue(gSheets.hideWorksheet(worksheetName));
		} catch (IOException exception1) {
			throw new BotCommandException(exception1.getMessage());
		} catch (Exception exception2) {
			throw new BotCommandException(exception2.getMessage());
		}
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
}
