package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.gsuite.api.GSheets;

public class AutofitRowsTest {
	private static final Logger LOGGER = LogManager.getLogger(AutofitColumnsTest.class);

	@Spy
	AutofitRows autoFitRows;

	@Mock
	GSheets mockGSheets;

	final String session = "session";
	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		sessionMap.put(session, mockGSheets);
		autoFitRows.setSessionMap(sessionMap);
	}

	@Test
	public void autoFitColumnTest() {
		LOGGER.info("Executing the mock test for AutoFit columns");
		when(mockGSheets.autofitRows()).thenReturn(true);
		LOGGER.info("Executing the AutofitColumns with spy object");
		autoFitRows.execute(session);

	}

	@Test(expectedExceptions = Exception.class)
	public void autoFitColumn_NullException() {
		when(mockGSheets.autofitRows()).thenThrow(IOException.class);
		autoFitRows.execute(null);
		Assert.fail();
	}

	@Test(expectedExceptions = Exception.class)
	public void autoFitColumn_invalidSession() {
		when(mockGSheets.autofitRows()).thenThrow(IOException.class);
		autoFitRows.execute("wrong_session");
		Assert.fail();
	}
}
