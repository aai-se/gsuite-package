/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.*;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import static org.testng.Assert.fail;
import org.testng.Assert;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

public class DeleteWorksheetTest {

    @Spy
    DeleteWorksheet deleteWorksheet;

    @Mock
    GSheets mockGSheets;

    final String session = "session";


    Map<String, Object> sessionMap = new HashMap<>();

    @BeforeMethod
    public void setup() {
        MockitoAnnotations.initMocks(this);
        sessionMap.put("session", mockGSheets);
        deleteWorksheet.setSessionMap(sessionMap);
    }

    @Test
    public void deleteWorksheetByIndexTest() {
        try {
            when(mockGSheets.deleteWorkSheet(anyBoolean(), anyInt(), anyString())).thenReturn(true);
            Assert.assertTrue(deleteWorksheet.execute("session",TRUE, 1, "").get());
        } catch (IOException e){
            fail();
        }

    }
    
    @Test
    public void deleteWorksheetByNameTest() {
        try {
            when(mockGSheets.deleteWorkSheet(anyBoolean(), anyInt(), anyString())).thenReturn(true);
            Assert.assertTrue(deleteWorksheet.execute("session",FALSE, null, "sheet2").get());
        } catch (IOException e){
            fail();
        }

    }
    
    @Test(expectedExceptions = BotCommandException.class)
    public void botCommandExceptionTest(){
        try{
            when(mockGSheets.deleteWorkSheet(anyBoolean(), anyInt(), anyString())).thenThrow(new IOException("Error"));
            deleteWorksheet.execute("session", TRUE, 1, "test123");
        }catch(IOException i){
            Assert.fail();
        }
    }
}
