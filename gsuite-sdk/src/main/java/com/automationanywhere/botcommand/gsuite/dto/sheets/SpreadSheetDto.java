/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommand.gsuite.dto.sheets;

import java.util.Map;

public class SpreadSheetDto {

	private String session;
	private String url;
	private String title;
	private String spreadSheetId;
	private Map<String, WorksheetDto> worksheets;

	public SpreadSheetDto() {
	}

	public SpreadSheetDto(String spreadSheetId) {
		this.spreadSheetId = spreadSheetId;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSpreadSheetId() {
		return spreadSheetId;
	}

	public void setSpreadSheetId(String spreadSheetId) {
		this.spreadSheetId = spreadSheetId;
	}

	public Map<String, WorksheetDto> getWorksheets() {
		return worksheets;
	}

	public void setWorksheets(Map<String, WorksheetDto> worksheets) {
		this.worksheets = worksheets;
	}

	@Override
	public String toString() {
		return "SpreadSheetDto{" + "url='" + url + '\'' + ", title='" + title + '\'' + ", spreadSheetId='"
				+ spreadSheetId + '\'' + '}';
	}
}
