/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommands.gsuite.sheets.commands.GetAllWorkSheetsName;
import com.automationanywhere.botcommands.gsuite.sheets.commands.OpenSheets;
import com.automationanywhere.botcommands.gsuite.sheets.test.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

public class GetAllWorkSheetNameJunit extends AbstractAuthUtils {

	GetAllWorkSheetsName getAllWorkSheetsName;
	OpenSheets openSheets;

	
	final String session = "session";
	private final static SecureString USERNAME  =  ConstantsTest.getUserName();

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {				
		launchOAuthConsentScreen(ConstantsTest.getUserName(), ConstantsTest.getClientid(),
				ConstantsTest.getClientSecret(), ConstantsTest.getRedirectUri());
		getAllWorkSheetsName = new GetAllWorkSheetsName();
		getAllWorkSheetsName.setSessionMap(sessionMap);
		openSheets = new OpenSheets();
		openSheets.setSessionMap(sessionMap);
		openSheets.execute(USERNAME, session, "BYNAME", "Demo111", null, null, false, null, false);
	}

	@Test
	public void getWorksheetName_test() {
		List<?> listOfSheets = (List<?>) getAllWorkSheetsName.execute(session).get();
		System.out.println("\n\n"+listOfSheets+"\n\n");
	}
}