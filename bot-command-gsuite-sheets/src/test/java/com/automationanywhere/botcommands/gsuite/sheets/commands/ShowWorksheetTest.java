/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static org.mockito.Mockito.when;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.gsuite.api.GSheets;

public class ShowWorksheetTest {

	@Spy
	ShowWorksheet showWorksheet;

	@Mock
	GSheets mockGSheets;

	final String session = "session";
	final String sheetName = "sheetname";
	final String spreadSheetId = "spreadsheetId";
	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		sessionMap.put(session, mockGSheets);
		showWorksheet.setSessionMap(sessionMap);
	}

	@Test
	public void ValidInputsTest() {
		try {
			when(mockGSheets.getSpreadSheetId()).thenReturn(spreadSheetId);
			when(mockGSheets.showWorksheet(spreadSheetId, sheetName)).thenReturn(true);
			Boolean returnedValue = showWorksheet.execute(session, sheetName).get();
			Assert.assertTrue(returnedValue);
		} catch (IOException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = Exception.class)
	public void InvalidSheetNameTest() {
		when(mockGSheets.getSpreadSheetId()).thenReturn(spreadSheetId);
		try {
			when(mockGSheets.showWorksheet(spreadSheetId, "Wrong sheet")).thenThrow(IOException.class);
			showWorksheet.execute(session, "Wrong sheet");
			Assert.fail();
		} catch (IOException e) {
			Assert.fail();
		}
	}
}
