/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */

package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.*;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Idx.Option;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.MatchesRegex;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

@BotCommand
@CommandPkg(label = "Delete cell", name = "deleteCell", description = "Deletes value of a specific cell", node_label = "|which is {{cellOption}} |{{cellAddress}} |and {{deleteOption}} |in session{{sessionName}}")
public class DeleteCell {
	private static final Logger LOGGER = LogManager.getLogger(DeleteCell.class);
	private static final String SESSION_EXAMPLE = "e.g. Session1 or S1";

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public BooleanValue execute(
			@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String sessionName,
			@Idx(index = "2", type = AttributeType.RADIO, options = {
					@Option(index = "2.1", pkg = @Pkg(label = LABEL_ACTIVECELL, value = TRUE)),
					@Option(index = "2.2", pkg = @Pkg(label = LABEL_SPECIFICCELL, value = FALSE)) }) @Pkg(label = "Cell options", default_value_type = DataType.STRING, default_value = TRUE) @NotEmpty String cellOption,
			@Idx(index = "2.2.1", type = AttributeType.TEXT) @Pkg(label = "Cell name", description = DESCRIPTION_CELL_ADDRESS) @MatchesRegex(cellAddressValidator) @NotEmpty String cellAddress,
			@Idx(index = "3", type = AttributeType.RADIO, options = {
					@Idx.Option(index = "3.1", pkg = @Pkg(label = "Shift cells left", value = LEFT)),
					@Idx.Option(index = "3.2", pkg = @Pkg(label = "Shift cells up", value = UP)),
					@Idx.Option(index = "3.3", pkg = @Pkg(label = "Entire row", value = ROWS)),
					@Idx.Option(index = "3.4", pkg = @Pkg(label = "Entire column", value = COLUMNS)) }) @Pkg(label = "Delete options", default_value = "Left", default_value_type = DataType.STRING) String deleteOption) {

		try {
			GSheets gSheets = (GSheets) sessionMap.get(sessionName);
			ExecutorValidator.getInstance().validate(gSheets);
			if(TRUE.equals(cellOption))
				cellAddress = gSheets.getActiveCell(gSheets.getActiveSheetInfo().getSheetName());
			if (LEFT.equals(deleteOption) || UP.equals(deleteOption)) {
				return new BooleanValue(
						gSheets.deleteCell(deleteOption, cellAddress.toUpperCase()));
			} else {
				return new BooleanValue(
						gSheets.deleteDimension(cellAddress.toUpperCase(), deleteOption));
			}
		} catch (IOException e) {
			LOGGER.error("exception in deleting cell: ", e);
			String msg = e.getMessage();
			throw new BotCommandException(msg, e);
		}
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}