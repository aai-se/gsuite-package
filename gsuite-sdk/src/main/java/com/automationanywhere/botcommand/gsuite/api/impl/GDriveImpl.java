/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommand.gsuite.api.impl;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.gsuite.DriveConstants;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.dto.drive.FileDto;
import com.automationanywhere.botcommand.gsuite.enums.FileType;
import com.automationanywhere.botcommand.gsuite.service.AESEncrypter;
import com.automationanywhere.botcommand.gsuite.service.AuthDto;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommand.gsuite.utils.Utils;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.Change;
import com.google.api.services.drive.model.ChangeList;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Permission;
import com.google.api.services.drive.model.PermissionList;
import com.google.api.services.drive.model.Revision;
import com.google.api.services.drive.model.RevisionList;
import com.google.api.services.drive.model.StartPageToken;
import com.google.common.annotations.VisibleForTesting;

public class GDriveImpl implements GDrive {

	@Sessions
	private Map<String, Object> sessionMap;

	private static final Logger LOGGER = LogManager.getLogger(GDriveImpl.class);
	private final HttpTransport httpTransport;

	private AuthenticationService authService;
	private Drive driveService;
	private File fileGlobal;
	private String userName;

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public GDriveImpl(AuthenticationService authService, String user, Map<String, Object> sessionMap)
			throws GeneralSecurityException, IOException {
		this.authService = authService;
		this.userName = user;
		setSessionMap(sessionMap);
		httpTransport = GoogleNetHttpTransport.newTrustedTransport();
	}

	public GDriveImpl(AuthenticationService authService) throws GeneralSecurityException, IOException {
		this.authService = authService;
		httpTransport = GoogleNetHttpTransport.newTrustedTransport();
	}

	@VisibleForTesting
	public GDriveImpl() throws GeneralSecurityException, IOException {
		httpTransport = GoogleNetHttpTransport.newTrustedTransport();
	}

	@VisibleForTesting
	void setDrive(Drive drive) {
		this.driveService = drive;
	}

	@VisibleForTesting
	void setAuthService(AuthenticationService authService) {
		this.authService = authService;
	}

	@Override
	public FileDto findFile(String name) throws IOException {
		setupAuthorization();

		FileDto fileDto = null;
		FileList fileList = driveService.files().list().setQ(DriveConstants.SET_NAME + name + DriveConstants.SET_TRASH)
				.setFields(DriveConstants.FIND_FILE_FIELDS).execute();
		LOGGER.info("File List = " + fileList.toString());
		List<File> files = fileList.getFiles();
		for (File file : files) {
			LOGGER.info("Files = " + file);
			LOGGER.info("File name : " + file.getName() + " , id = " + file.getId());
			fileDto = new FileDto(file.getId(), file.getName());

		}
		return fileDto;
	}

	List<File> recurseFileList(String parentFolderId, HashMap<String, List<File>> fileParentMap)
			throws IOException, JSONException {

		// Get list of folders in the parent folder
		Set<File> filesOutput = new HashSet<>();
		FileList folderList = driveService.files().list()
				.setQ(DriveConstants.QUOTE + parentFolderId + DriveConstants.SET_PARENT_FOLDER_RECURSE_FILE)
				.setFields(DriveConstants.RECURSE_FILE_FIELDS).execute();
		List<File> folders = folderList.getFiles();
		// If folder id is present in the Map, add all its files in the output list
		// then, recurse through all folder ids to find matching keys in the Map
		for (File folder : folders) {
			if (fileParentMap.containsKey(folder.getId())) {
				filesOutput.addAll(fileParentMap.get(folder.getId()));
			}
			filesOutput.addAll(recurseFileList(folder.getId(), fileParentMap));
		}
		return new ArrayList<>(filesOutput);
	}

	@Override

	public List<File> findFileReturnList(String fileLocation, String name, String searchType)
			throws IOException, JSONException {
		// Instantiate Google Drive Service object
		// Instantiate Google Drive Service object
		setupAuthorization();

		// Get valid parent folder file resource from fileLocation
		List<File> fList = parseFilePath(fileLocation, "found");
		File f = null;
		if (fList == null || fList.isEmpty())
			throw new IOException(fileLocation + " does not exist");
		else
			f = fList.get(0);

		String parentFolderId = f.getId();

		FileList fileList = null;

		// TODO: Add handling for END and BEG searchType

		// Get list of files matching search string constraints according to searchType
		if (searchType.equals("MAT")) {
			fileList = driveService.files().list()
					.setQ(DriveConstants.SET_NAME + name + DriveConstants.SET_TRASH_NOT_FOLDER_FIND_FILE_RETURN_LIST)
					.setFields(DriveConstants.FIND_FILE_RETURN_FIELDS).execute();
		} else if (searchType.equals("CON")) {
			fileList = driveService.files().list()
					.setQ(DriveConstants.SET_NAME_CONTAINS + name
							+ DriveConstants.SET_TRASH_NOT_FOLDER_FIND_FILE_RETURN_LIST)
					.setFields(DriveConstants.FIND_FILE_RETURN_FIELDS).execute();
		}

		// Store (parentId, files present for that parentId) as key-value pairs
		List<File> files = fileList.getFiles();
		HashMap<String, List<File>> fileParentMap = new HashMap<>();
		for (File fEle : files) {
			List<String> parents = fEle.getParents();
			for (String pId : parents) {
				List<File> fileEle;
				if (fileParentMap.containsKey(pId)) {
					fileEle = fileParentMap.get(pId);
				} else {
					fileEle = new ArrayList<>();
				}
				fileEle.add(fEle);
				fileParentMap.put(pId, fileEle);
			}
		}

		// find list of files rcursivly in all subfolders of the parent folder
		List<File> filesOutput = recurseFileList(parentFolderId, fileParentMap);
		if (fileParentMap.containsKey(parentFolderId)) {
			filesOutput.addAll(fileParentMap.get(parentFolderId));
		}
		LOGGER.info("File List = " + filesOutput);

		// if no files found, throw exception
		if (filesOutput.size() == 0) {
			throw new IOException("No files found having name " + name);
		}
		return filesOutput;
	}

	@Override

	public File findFileType(String name) throws IOException, JSONException {
		setupAuthorization();

		FileList fileList = driveService.files().list().setQ(DriveConstants.SET_NAME + name + DriveConstants.SET_TRASH)
				.setFields(DriveConstants.FIND_FILE_TYPE_FIELDS).execute();
		LOGGER.info("File List = " + fileList.toString());
		List<File> files = fileList.getFiles();
		if (files.isEmpty()) {
			throw new IOException("No files found having name " + name);
		}
		File newf = null;
		for (File file : files) {
			LOGGER.info("Files = " + file);
			LOGGER.info("File name : " + file.getName() + " , id = " + file.getId());
			newf = file;
		}
		return newf;
	}

	private String getFileParentPath(String fileName) {
		String[] filePath = fileName.split(DriveConstants.FORWARD_SLASH);
		String parentPath = "";
		for (int itr = 0; itr < filePath.length - 1; itr++) {
			parentPath = parentPath + filePath[itr];
			if (itr < filePath.length - 2) {
				parentPath = parentPath + DriveConstants.FORWARD_SLASH;
			}
		}
		return parentPath;
	}

	String getLatestFilenameFromDirectory(String destFolder, String name) throws IOException, JSONException {

		String[] fileNameList = null;
		String fileName = null;
		if (name.contains(".")) {
			fileNameList = name.split("\\.");
			fileName = fileNameList[0];
		} else {
			fileName = name;
		}
		Boolean copiedFileExistsFlag = true;
		if (!fileName.contains("_copy")) {
			final String tempFileName = name;
			try (Stream<Path> paths = Files.walk(Paths.get(destFolder))) {
				List<String> resultCopy = paths.filter(Files::isRegularFile).map(x -> x.toString())
						.filter(f -> f.contains(tempFileName)).collect(Collectors.toList());
				if (resultCopy.size() == 0) {
					copiedFileExistsFlag = false;
					fileName = name;
				} else {
					fileName = fileName + "_copy";
				}
			} catch (NoSuchFileException e) {
				throw new IOException("File couldn’t be downloaded as " + destFolder
						+ " doesn’t exist. Please check for typo and try again.");
			}
		}

		final String fileNameFinal = fileName;
		Integer version = -1;
		try (Stream<Path> paths = Files.walk(Paths.get(destFolder))) {
			List<String> resultCopy = paths.filter(Files::isRegularFile).map(x -> x.toString())
					.filter(f -> f.startsWith(fileNameFinal)).collect(Collectors.toList());

			resultCopy.forEach(System.out::println);
			TreeMap<Long, String> sortedFileList = new TreeMap<>();
			for (String file : resultCopy) {
				java.io.File temp = new java.io.File(file);
				sortedFileList.put(-temp.lastModified(), file);
			}

			if (sortedFileList.size() != 0) {
				Map.Entry<Long, String> entry = sortedFileList.firstEntry();
				String[] fileNameList2 = entry.getValue().split(fileName);
				if (fileNameList2.length > 1) {
					String tempStr = fileNameList2[1].trim();
					if (tempStr.startsWith("(")) {
						version = Integer.parseInt(tempStr.substring(tempStr.indexOf("(") + 1, tempStr.indexOf(")")));
					}
				}
			}
		}
		if (copiedFileExistsFlag) {
			if (version > 0) {
				version++;
			} else {
				version = 1;
			}
			if (name.contains(".")) {
				fileName = fileNameList[0] + "_copy(" + version + ")." + fileNameList[1];
			} else {
				fileName = name + " (" + version + ")";
			}
		}
		return fileName;
	}

	@Override
	public List<FileDto> getAllFiles(boolean isDeleted) throws IOException, JSONException {
		setupAuthorization();
		return null;
	}

	@Override
	public List<FileDto> getAllFolders() throws IOException, JSONException {
		setupAuthorization();
		return null;
	}

	@Override
	public String saveAs(String fileId, String name) throws IOException, JSONException {
		setupAuthorization();

		File file = new File();
		file.setName(name);
		File newFile = driveService.files().copy(fileId, file).setFields(DriveConstants.SAVE_AS_FIELDS).execute();
		LOGGER.info("save as command, new Id = " + newFile.getId());
		return newFile.getId();
	}

	private String extractFilename(String fullPath) {
		// Given an entire file path like C:/Documents/MyFile.docx
		// return MyFile.docx, ie, the file name
		String[] paths = fullPath.split(DriveConstants.FORWARD_SLASH);
		return paths[paths.length - 1];
	}

	Boolean fileExistsOnDevice(String filePath) {
		java.io.File file = new java.io.File(filePath);
		return file.exists();

	}

	@Override
	public String downloadFile(String inputFileParam, String destFolder, Boolean overwriteFile, String outputFileParam)

			throws IOException, JSONException, JSONException {
		// Initialize Google Drive service
		setupAuthorization();

		// Get file resource using the GET API call by passing file name or file ID to
		// it
		File file = getFileFromJson(inputFileParam, "downloaded", DriveConstants.FILE);
		String fileName = file.getName(), fileId = file.getId();

		String renamedFileName = "";
		JSONObject outputFileJson = new JSONObject(outputFileParam);
		// Set up new file name of the downloaded file if the rename file option has
		// been selected
		if (outputFileJson.has(DriveConstants.NEW_FILE_NAME)) {
			String newFileName = outputFileJson.getString(DriveConstants.NEW_FILE_NAME);
			if (fileExistsOnDevice(destFolder + DriveConstants.FORWARD_SLASH + extractFilename(newFileName))
					&& !overwriteFile) {
				throw new IOException(
						"File couldn’t be downloaded as " + extractFilename(newFileName) + " already exists in "
								+ destFolder + " and override option is not selected. Please validate and try again.");
			}
			renamedFileName = newFileName;
		} else {
			if (fileExistsOnDevice(destFolder + DriveConstants.FORWARD_SLASH + extractFilename(fileName))
					&& !overwriteFile) {
				throw new IOException(
						"File couldn’t be downloaded as " + extractFilename(fileName) + " already exists in "
								+ destFolder + " and override option is not selected. Please validate and try again.");
			}
			renamedFileName = extractFilename(fileName);
		}

		// Set up file output stream to download file on to the local machine
		OutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(destFolder + DriveConstants.FORWARD_SLASH + renamedFileName + "."
					+ Utils.getFileExtension(file.getMimeType()), true);
		} catch (FileNotFoundException e) {
			throw new IOException("File couldn’t be downloaded as " + destFolder
					+ " doesn’t exist. Please check for " + "typo and try again.");
		}

		if (Utils.isGoogleDoc(file.getMimeType())) {
			// If file is a native Google Drive document (Google Docs, Google Sheets, Google
			// Slides), first export it to
			// a local file type
			driveService.files().export(fileId, Utils.getDownloadMimetype(file.getMimeType()))
					.executeMediaAndDownloadTo(outputStream);
		} else {
			driveService.files().get(fileId).executeMediaAndDownloadTo(outputStream);
		}
		return fileId;
	}

	@Override
	public String uploadFile(String fileLocation, String inputFileParam, Boolean overwriteFile)

			throws IOException, JSONException, JSONException {
		// Initialize Google Drive service object
		setupAuthorization();

		String fileName, fileId = "";

		// Get local file object of file to be uploaded
		String[] pathList = fileLocation.split(DriveConstants.FORWARD_SLASH);
		fileName = pathList[pathList.length - 1];
		java.io.File filePath = new java.io.File(fileLocation);

		// Get Google Drive file resource of upload folder location
		File destFile = getFileFromJson(inputFileParam, "uploaded", DriveConstants.FOLDER);
		String destFolder = destFile.getName();

		// Check destination folder to be of type Google Drive folder
		if (!destFile.getMimeType().equals(DriveConstants.MIME_TYPE_FOLDER)) {
			throw new IOException("File couldn't be uploaded as destination folder " + destFolder + " is not a folder. "
					+ "Please validate and try again.");
		}

		List<File> fList = parseFilePath(destFolder, "uploaded");
		File uploadParentFile = null;
		if (fList.size() > 0)
			uploadParentFile = fList.get(0);
		else if (fList.size() == 0)
			throw new IOException(destFolder + " does not exist");

		// Get Google Drive file resource of existing file having same location and name
		// as uploaded file
		fList = parseFilePath(destFolder + DriveConstants.FORWARD_SLASH + fileName, "uploaded");
		File uploadFile = null;
		if (fList.size() > 0) {
			// If file exists, create new file version if overwrite flag false
			uploadFile = fList.get(0);
			if (overwriteFile) {
				// Create new version in existing file - same file id
				File fileNew = new File();
				fileNew.setName(fileName);
				FileContent mediaContent = new FileContent(Utils.getContentType(filePath), filePath);
				File file = driveService.files().update(uploadFile.getId(), fileNew, mediaContent)
						.setFields(DriveConstants.ID).setAddParents(uploadParentFile.getId()).execute();
			} else {
				throw new IOException("File couldn’t be uploaded as " + fileLocation
						+ " already exist in Google drive and override option is not selected. Please validate and try again");
			}
			fileId = uploadFile.getId();
		} else if (fList.size() == 0) {
			// If file doesn't exist, create new file
			File fileMetadata = new File();
			fileMetadata.setName(fileName);
			List<String> parents = new ArrayList<>();
			parents.add(uploadParentFile.getId());
			fileMetadata.setParents(parents);
			FileContent mediaContent = new FileContent(Utils.getContentType(filePath), filePath);
			File file = driveService.files().create(fileMetadata, mediaContent).setFields(DriveConstants.ID).execute();
			LOGGER.info("File ID: " + file.getId());
			fileId = file.getId();
		}
		return fileId;
	}

	@Override

	public Boolean openFile(String inputFileParam) throws IOException, JSONException, JSONException {
		// Instantiate Google Drive service object
		setupAuthorization();

		JSONObject inputJson = new JSONObject(inputFileParam);
		File file = getFileFromJson(inputFileParam, "opened", inputJson.getString(DriveConstants.FILETYPE));

		// Set file variable to current file resource
		this.fileGlobal = file;

		return true;
	}

	@Override

	public String getFileId() throws IOException, JSONException {

		if (fileGlobal == null) {
			LOGGER.error("This object has not been initialized yet. Returning null.");
			return null;
		} else {
			return fileGlobal.getId();
		}
	}

	@Override

	public List<Value> getFilePermission(String inputFileParam) throws IOException, JSONException, JSONException {
		// Instantiate Google Drive service object
		setupAuthorization();

		File file = getFileFromJson(inputFileParam, "found", DriveConstants.FILE);
		String fileName = file.getName(), fileId = file.getId();

		PermissionList permissions = driveService.permissions().list(fileId)
				.setFields(DriveConstants.GET_FILE_PERMISSIONS_FIELDS).execute();
		// Get list of permissions from permission resource
		List<Permission> pList = permissions.getPermissions();
		// Transform permission list to a list of values for Bot Agent to read
		List<Value> listVal = new ArrayList<>();
		for (Permission p : pList) {
			listVal.add(new StringValue("" + p));
		}
		return listVal;
	}

	@Override

	public Boolean deletePermission(String inputFileParam, String permissionId) throws IOException, JSONException {
		// Instantiate Google Drive service object
		setupAuthorization();

		File file = getFileFromJson(inputFileParam, "found", DriveConstants.FILE);
		String fileId = file.getId();

		// Call delete permission API
		driveService.permissions().delete(fileId, permissionId).execute();
		return true;
	}

	@Override

	public String updatePermission(String inputFileParam, String extraParam, String permissionId)
			throws IOException, JSONException {
		// Instantiate Google Drive service object
		setupAuthorization();

		File file = getFileFromJson(inputFileParam, "found", DriveConstants.FILE);
		String fileId = file.getId();

		JSONObject extraParamJson = new JSONObject(extraParam);
		// Create new Permission resource to set the updated role
		Permission permission = new Permission();
		permission.setRole(extraParamJson.getString(DriveConstants.ROLE));

		Drive.Permissions.Update updatePerm = driveService.permissions().update(fileId, permissionId, permission);
		// Set transferOwnership flag if role is "owner"
		if (extraParamJson.has(DriveConstants.TRANSFER_OWNERSHIP)) {
			updatePerm.setTransferOwnership(extraParamJson.getBoolean(DriveConstants.TRANSFER_OWNERSHIP));
		}
		Permission outputPerm = updatePerm.execute();
		return outputPerm.toString();
	}

	@Override

	public Map<String, Value> getFileInformation(String inputFileParam) throws IOException, JSONException {
		// Instantiate Google Drive service object
		setupAuthorization();

		File file = getFileFromJson(inputFileParam, "found", DriveConstants.FILE);

		JSONObject inputFileJson = new JSONObject(inputFileParam);
		String fileParentLocation = "";

		if (inputFileJson.has(DriveConstants.FILENAME)) {
			fileParentLocation = getFileParentPath(inputFileJson.getString(DriveConstants.FILENAME));
		}

		Map<String, Value> outputMap = new HashMap<>();
		outputMap.put("fileId", new StringValue(file.getId()));
		outputMap.put("fileName", new StringValue(file.getName()));
		outputMap.put("fileOwners", new StringValue(file.getOwners().toString()));
		outputMap.put("fileCreationDate", new StringValue(file.getCreatedTime().toString()));
		outputMap.put("fileModifiedBy", new StringValue(file.getLastModifyingUser().toString()));
		outputMap.put("fileModifiedTime", new StringValue(file.getModifiedTime().toString()));
		outputMap.put("fileParentFolderPath", new StringValue(fileParentLocation));
		outputMap.put("fileParentId", new StringValue(file.getParents().toString()));
		outputMap.put("fileVersion", new StringValue(file.getVersion().toString()));

		return outputMap;
	}

	@Override
	public Boolean deleteFile(String inputFileParam) throws IOException, JSONException {
		// Instantiate Google Drive service object
		setupAuthorization();

		File file = getFileFromJson(inputFileParam, "deleted", DriveConstants.FILE);
		String fileName = file.getName(), fileId = file.getId();

		File fileNew = new File();
		fileNew.setTrashed(true);

		driveService.files().update(file.getId(), fileNew).setFields(DriveConstants.ID).execute();
		return true;
	}

	@Override
	public String copyFile(String inputFileParam, String outputFileParam) throws IOException, JSONException {
		// Instantiate Google Drive Service object
		setupAuthorization();

		File file = getFileFromJson(inputFileParam, "copied", DriveConstants.FILE);
		String fileName = file.getName(), fileId = file.getId();

		File destFolder = getFileFromJson(outputFileParam, "copied", DriveConstants.FOLDER);
		String folderName = destFolder.getName(), folderId = destFolder.getId();

		if (!destFolder.getMimeType().equals(DriveConstants.MIME_TYPE_FOLDER)) {
			throw new IOException("File couldn't be copied as destination folder " + folderName + " is not a folder. "
					+ "Please validate and try again.");
		}

		File fileNew = new File();
		List<String> parents = new ArrayList<>();
		parents.add(folderId);
		fileNew.setParents(parents);

		JSONObject outputFileJson = new JSONObject(outputFileParam);
		if (outputFileJson.has(DriveConstants.NEW_FILE_NAME)) {
			fileNew.setName(outputFileJson.getString(DriveConstants.NEW_FILE_NAME));
		} else {
			fileNew.setName(extractFilename(fileName));
		}

		File outputFile = driveService.files().copy(fileId, fileNew).execute();
		return outputFile.getId();
	}

	@Override
	public String moveFile(String inputFileParam, String outputFileParam) throws IOException, JSONException {
		// Instantiate Google Drive service object
		setupAuthorization();

		File file = getFileFromJson(inputFileParam, "moved", DriveConstants.FILE);
		String fileName = file.getName(), fileId = file.getId();

		File destFolder = getFileFromJson(outputFileParam, "moved", DriveConstants.FOLDER);
		String folderName = destFolder.getName(), folderId = destFolder.getId();

		if (!destFolder.getMimeType().equals(DriveConstants.MIME_TYPE_FOLDER)) {
			throw new IOException("File couldn't be moved as destination folder " + folderName
					+ " is not a folder. Please validate and try again.");
		}

		// Get existing file object along with the parents field
		File fileObj = driveService.files().get(fileId).setFields("parents").execute();
		// Generate comma separated string of parents of the file
		StringBuilder previousParents = new StringBuilder();
		for (String parent : fileObj.getParents()) {
			previousParents.append(parent);
			previousParents.append(',');
		}
		// Remove the previous parents and add the new destination folder as parents
		fileObj = driveService.files().update(fileId, null).setAddParents(folderId)
				.setRemoveParents(previousParents.toString()).setFields(DriveConstants.MOVE_FILE_FIELDS).execute();

		return fileObj.getId();
	}

	@Override

	public String renameFile(String inputFileParam, String newFileName) throws IOException, JSONException {
		System.out.println("\n\n\nInto Rename File sub-method...");

		// Instantiate Google Drive service object
		setupAuthorization();

		JSONObject inputJson = new JSONObject(inputFileParam);
		File file = getFileFromJson(inputFileParam, "renamed", inputJson.getString(DriveConstants.FILETYPE));

		// Create new file object with new file name
		File fileNew = new File();
		fileNew.setName(newFileName);

		// fileNew.setPermissions(file.getPermissions());
		driveService.files().update(file.getId(), fileNew).setFields(DriveConstants.ID).execute();

		driveService.files().update(file.getId(), fileNew).setFields(DriveConstants.ID).execute();

		return file.getId();
	}

	@Override

	public String createPermission(String inputFileParam, String otherParam) throws IOException, JSONException {

		File file = getFileFromJson(inputFileParam, "found", DriveConstants.FILE);
		String fileName = file.getName(), fileId = file.getId();
		JSONObject paramJson = new JSONObject(otherParam);

		// Create new permission resource by adding information from JSONObjects
		Permission permission = new Permission();
		permission.setKind(DriveConstants.DRIVE_PERMISSION_KIND);
		permission.setRole(paramJson.getString(DriveConstants.ROLE));
		permission.setType(paramJson.getString(DriveConstants.TYPE));
		if (paramJson.has(DriveConstants.DOMAIN)) {
			permission.setDomain(paramJson.getString(DriveConstants.DOMAIN));
		}
		if (paramJson.has(DriveConstants.EMAIL_ADDRESS)) {
			permission.setEmailAddress(paramJson.getString(DriveConstants.EMAIL_ADDRESS));
		}

		// Call create permission API
		Drive.Permissions.Create createPerm = driveService.permissions().create(fileId, permission);
		if (paramJson.has(DriveConstants.TRANSFER_OWNERSHIP)
				&& paramJson.getBoolean(DriveConstants.TRANSFER_OWNERSHIP)) {
			createPerm.setTransferOwnership(true);
		}
		Permission outputPermission = createPerm.execute();
		return outputPermission.toString();
	}

	@Override
	public String retrieveChanges(String inputFileParam) throws IOException, JSONException {
		setupAuthorization();

		File file = getFileFromJson(inputFileParam, "found", DriveConstants.FILE);
		String fileName = file.getName(), fileId = file.getId();

		StartPageToken response = driveService.changes().getStartPageToken().execute();

		String savedStartPageToken = "1605";// response.getStartPageToken();
		String pageToken = "1605";// response.getStartPageToken();
		while (pageToken != null) {
			ChangeList changes = driveService.changes().list(pageToken).execute();
			for (Change change : changes.getChanges()) {
				// Process change
			}
			if (changes.getNewStartPageToken() != null) {
				// Last page, save this token for the next polling interval
				savedStartPageToken = changes.getNewStartPageToken();
			}
			pageToken = changes.getNextPageToken();
		}
		return null;
	}

	@Override

	public List<Value> manageRevisions(String inputFileParam) throws IOException, JSONException {
		setupAuthorization();

		File file = getFileFromJson(inputFileParam, "found", DriveConstants.FILE);
		String fileId = file.getId();
		RevisionList revisions = driveService.revisions().list(fileId).execute();

		List<Revision> revisionList = revisions.getRevisions();
		List<Value> revisionIdList = new ArrayList<>();
		for (Revision r : revisionList) {
			revisionIdList.add(new StringValue(r.getId()));
		}
		return revisionIdList;
	}

	@Override

	public String createFile(String name, String filePath, FileType type) throws IOException {
		setupAuthorization();

		File fileNew = new File();
		fileNew.setName(name);
		List<String> parents = new ArrayList<>();
		parents.add(filePath);
		fileNew.setParents(parents);
		switch (type) {
		case EXCEL:
			fileNew.setMimeType(DriveConstants.MIME_TYPE_SPREADSHEET);
			break;
		case WORD:
			fileNew.setMimeType(DriveConstants.MIME_TYPE_DOCUMENT);
			break;
		case PDF:
			fileNew.setMimeType(DriveConstants.MIME_TYPE_PDF);
			break;
		case PRESENTATION:
			fileNew.setMimeType(DriveConstants.MIME_TYPE_PPT);
			break;
		case TEXT:
			fileNew.setMimeType(DriveConstants.MIME_TYPE_PLAIN);
			break;
		case CSV:
			fileNew.setMimeType(DriveConstants.MIME_TYPE_CSV);
			break;
		case UNSPECIFIED:
		default:
			break;
		}

		Drive.Files.Create create = driveService.files().create(fileNew).setFields(DriveConstants.CREATE_FILE_FIELDS);
		File file = create.execute();

		return file.getId();
	}

	@Override

	public boolean fileExists(String name, String path) throws IOException {
		setupAuthorization();

		FileList fileList = driveService.files().list().setQ(DriveConstants.getFileExistsQuery(name, path))
				.setFields(DriveConstants.FILE_EXISTS_FIELDS).execute();
		LOGGER.info("File List = " + fileList.toString());
		if (fileList.getFiles().size() > 0)
			return true;

		return false;
	}

	// =============================================================================================================================================================================

	@Override
	public String exportAsPdf(String inputFileParam, String outputFolderParam) throws IOException, JSONException {

		// Instantiate Google Drive service object
		setupAuthorization();

		File sourceFile = getFileFromJson(inputFileParam, DriveConstants.EXPORTASPDF, DriveConstants.FILE);
		JSONObject outputParams = new JSONObject(outputFolderParam);
		String acceptedExtensions = DriveConstants.EXPORT_ACCEPTED_FORMATS;

		if (sourceFile.getFileExtension() == null || acceptedExtensions.contains(sourceFile.getFileExtension())) {

			File destinationFolder = getFileFromJson(outputFolderParam, DriveConstants.EXPORTASPDF,
					DriveConstants.FOLDER);
			if (!outputParams.getBoolean(DriveConstants.ISOVERRIDE)) {
				// to get all the childs
				List<File> files = getChildList(destinationFolder).getFiles();
				for (File file : files) {
					if (sourceFile.getName().equals(file.getName())) {
						throw new IOException("Could not export " + sourceFile.getName() + " with ID as "
								+ sourceFile.getId() + " to pdf since a file with same name exists in "
								+ destinationFolder.getName() + " on Google drive and Override option is not selected, "
								+ "please select override option and try again");
					}
				}
			}

			// if override is also selected or no file present with the same name as of
			// source file, so now we can delete the current file and create a new file with
			// .pdf extension
			File retID = driveService.files().get(sourceFile.getId()).execute()
					.setMimeType(DriveConstants.MIME_TYPE_PDF);
			return retID.getName();
		} else {
			throw new IOException("File format not supported.");
		}
	}

	@Override
	public Boolean createFolder(String inputFolderParams) throws IOException, JSONException {

		// Instantiate Google Drive service object
		setupAuthorization();
		File fileNew = new File();
		List<String> parents = new ArrayList<>();

		// adding parent as parentFile.
		parents.add(getFileFromJson(inputFolderParams, DriveConstants.CREATEFOLDER, DriveConstants.FOLDER).getId());
		fileNew.setParents(parents);
		fileNew.setName(new JSONObject(inputFolderParams).getString(DriveConstants.CHILDNAME));
		fileNew.setMimeType(DriveConstants.MIME_TYPE_FOLDER);
		driveService.files().create(fileNew).setFields(DriveConstants.CREATE_FOLDER_FIELDS).execute();
		return true;
	}

	@Override
	public Boolean deleteFolder(String inputFolderParam) throws IOException, JSONException {

		// Instantiate Google Drive service object
		setupAuthorization();

		File file = getFileFromJson(inputFolderParam, DriveConstants.DELETEFODLER, DriveConstants.FOLDER);

		File fileNew = new File();
		fileNew.setTrashed(true);

		driveService.files().update(file.getId(), fileNew).setFields(DriveConstants.ID).execute();
		return true;
	}

	@Override
	public Boolean checkPermissions(String inputParam, String fileType) throws IOException, JSONException {

		// Instantiate Google Drive service object
		setupAuthorization();

		File file = getFileFromJson(inputParam, DriveConstants.CHECKPERMISSIONS, fileType);
		PermissionList permissions = driveService.permissions().list(file.getId())
				.setFields(DriveConstants.CHECKPERMISSIONS_FIELDS).execute();
		// Get list of permissions from permission resource
		List<Permission> pList = permissions.getPermissions();

		// Transform permission list to list of permissions to get the role
		for (Permission p : pList)
			if (validatePermission(p.getRole(), inputParam))
				return true;

		return false;
	}

	/**
	 * Validates if the opted Permission fall under the permission role for that
	 * file or not
	 * 
	 * @param fileRole   Role alloted to the file
	 * @param inputParam Role opted to check by the user
	 * @return true if present
	 * @throws JSONException
	 */
	private boolean validatePermission(String fileRole, String inputParam) throws JSONException {

		JSONObject inputJson = new JSONObject(inputParam);
		String choiceRole = inputJson.getString(DriveConstants.ROLE);

		if (fileRole.equals(DriveConstants.FILE_OWNER_ROLE))
			return true;
		if (!choiceRole.equals(DriveConstants.CHOICE_DELETE) && (fileRole.equals(DriveConstants.ORGANIZER_ROLE)
				|| fileRole.equals(DriveConstants.FILE_ORGANIZER_ROLE)))
			return true;
		if (choiceRole.equals(DriveConstants.CHOICE_WRITER) && fileRole.equals(DriveConstants.FILE_WRITER_ROLE))
			return true;
		if (choiceRole.equals(DriveConstants.CHOICE_READ) && (fileRole.equals(DriveConstants.FILE_READ_ROLE)
				|| fileRole.equals(DriveConstants.FILE_COMMENTER_ROLE)))
			return true;

		return false;
	}

	@Override
	public String moveFolder(String inputFileParam, String outputFileParam) throws IOException, JSONException {
		// Instantiate Google Drive service object
		setupAuthorization();

		File sourceFolder = getFileFromJson(inputFileParam, "moved", DriveConstants.FOLDER);
		String sourceFolderId = sourceFolder.getId();

		File destFolder = getFileFromJson(outputFileParam, "moved", DriveConstants.FOLDER);
		String destinationFolderId = destFolder.getId();

		if (!destFolder.getMimeType().equals(DriveConstants.MIME_TYPE_FOLDER)) {
			throw new IOException("File couldn't be moved as source folder " + sourceFolder.getName()
					+ " is not a folder. Please validate and try again.");
		}

		if (!destFolder.getMimeType().equals(DriveConstants.MIME_TYPE_FOLDER)) {
			throw new IOException("File couldn't be moved as destination folder " + destFolder.getName()
					+ " is not a folder. Please validate and try again.");
		}

		// Get existing file object along with the parents field
		File fileObj = driveService.files().get(sourceFolderId).setFields(DriveConstants.MOVE_FOLDER_FIELDS).execute();
		// Generate comma separated string of parents of the file
		StringBuilder previousParents = new StringBuilder();
		for (String parent : fileObj.getParents()) {
			previousParents.append(parent);
			previousParents.append(',');
		}
		// Remove the previous parents and add the new destination folder as parents
		fileObj = driveService.files().update(sourceFolderId, null).setAddParents(destinationFolderId)
				.setRemoveParents(previousParents.toString()).setFields(DriveConstants.MOVE_FOLDER_FIELDS).execute();

		return fileObj.getId();
	}

	@Override
	public String copyFolder(String inputFileParam, String outputFileParam) throws IOException, JSONException {

		System.out.println("Copy Folder started..");
		// Instantiate Google Drive Service object
		setupAuthorization();
		System.out.println("\nSetup Completed");

		File sourceFolder = getFileFromJson(inputFileParam, "copied", DriveConstants.FOLDER);

		System.out.println("SourceFolder retrieved");

		File destFolder = getFileFromJson(outputFileParam, "copied", DriveConstants.FOLDER);

		validateFolder(sourceFolder);
		validateFolder(destFolder);

		System.out.println("\n\nFolders " + sourceFolder.getName() + " and " + destFolder.getName()
				+ " retrieved successfully....");
		File fileNew = new File();
		List<String> parents = new ArrayList<>();
		parents.add(destFolder.getId());
		fileNew.setParents(parents);
		fileNew.setName(sourceFolder.getName());
		fileNew.setMimeType(DriveConstants.MIME_TYPE_FOLDER);
		File fileCreated = driveService.files().create(fileNew).setFields(DriveConstants.COPY_FOLDER_FIELDS).execute();

		// folder cannot be copied directly...
		// so we have to iterate all over the childrens of that folder and make a copy
		// of it to the new folder we created ...
		copyFolderChildrens(sourceFolder, fileCreated);
		return sourceFolder.getId();
	}

	public void validateFolder(File sourceFolder) throws IOException {
		if (!sourceFolder.getMimeType().equals(DriveConstants.MIME_TYPE_FOLDER)) {
			throw new IOException("File couldn't be copied as source folder " + sourceFolder.getName()
					+ " is not a folder. " + "Please validate and try again.");
		}
	}

	/**
	 * Copies all sub (Files/Folders) of a given source folder to required folder.
	 * 
	 * @param sourceFolder      where files/folders are actually present
	 * @param destinationFolder where files/folders need to be copied
	 * @throws IOException
	 * @throws JSONException
	 */
	public void copyFolderChildrens(File sourceFolder, File destinationFolder) throws IOException, JSONException {

		// get all childs
		FileList fileList = getChildList(sourceFolder);

		List<File> files = fileList.getFiles();
		LOGGER.info("Files retrieved = \n" + files);

		for (int i = 0; i < files.size(); i++) {

			JSONObject source = new JSONObject().put(DriveConstants.FILEID, files.get(i).getId());
			JSONObject destination = new JSONObject().put(DriveConstants.FILEID, destinationFolder.getId());

			if (files.get(i).getMimeType().equals(DriveConstants.MIME_TYPE_FOLDER)) {
				copyFolder(source.toString(), destination.toString());
			} else {
				copyFile(source.toString(), destination.toString());
			}
		}
	}

	/**
	 * Returns all the sub (Files/Folders) for the sourceFolder
	 * 
	 * @param sourceFolder
	 * @return list of childs
	 * @throws IOException
	 */
	private FileList getChildList(File sourceFolder) throws IOException {
		return driveService.files().list()
				.setQ(DriveConstants.SET_PARENTS + sourceFolder.getId() + DriveConstants.QUOTE)
				.setFields(DriveConstants.FILE_LIST_FIELDS).execute();
	}

	/**
	 * Retrieves the file after parsing through the file path.
	 * 
	 * @param inputFileParam
	 * @param callingFunction
	 * @param fileType
	 * @return Drive.Model.File object if the path is correct
	 * @throws IOException
	 * @throws JSONException
	 */
	public File getFileFromJson(String inputFileParam, String callingFunction, String fileType)
			throws IOException, JSONException {
		//System.out.println("Entered here..." + inputFileParam);
		String fileName = "", fileId = "";
		File file = null;
		JSONObject inputFileJson = new JSONObject(inputFileParam);
		if (inputFileJson.has(DriveConstants.FILENAME)) {

			// if root folder found...
			if (inputFileJson.getString(DriveConstants.FILENAME).equalsIgnoreCase(DriveConstants.ROOT)) {
				return driveService.files().get(DriveConstants.ROOT_ID).execute();
			}

			fileName = inputFileJson.getString(DriveConstants.FILENAME);
			List<File> fList = parseFilePath(fileName, callingFunction);
			if (fList.size() == 1)
				file = fList.get(0);
			else if (fList.size() == 0)
				throw new IOException("File couldn’t be " + callingFunction + " as " + fileName
						+ " doesn’t exist. Please check for typo and try again.");
			else if (fList.size() > 1)
				throw new IOException("File couldn’t be " + callingFunction + " as more than one " + fileType
						+ " with name " + fileName + " exist in Google drive. Please validate " + fileType
						+ " name and try again.");
		} else if (inputFileJson.has(DriveConstants.FILEID)) {
			System.out.println("In FieldID");
			fileId = inputFileJson.getString(DriveConstants.FILEID);
			try {
				file = getFileById(fileId);
			} catch (IOException e) {
				throw new IOException("File couldn’t be " + callingFunction + " as " + fileId
						+ " doesn’t exist. Please check for typo and try again.");
			}
		}
		return file;
	}

	@Override
	public List<File> parseFilePath(String filePath, String callingFunction) throws IOException, JSONException {

		setupAuthorization();
		// Recurses through the file path and returns list of file resources matching
		// the exact file path
		// Most commands that use this method throw an exception if this method returns
		// more than
		// one file in the returned list

		// Trim starting and ending slash
		System.out.println("\n\n filepath == " + filePath);

		if (filePath.startsWith(DriveConstants.FORWARD_SLASH)) {
			filePath = filePath.substring(1, filePath.length());
		}

		if (filePath.endsWith(DriveConstants.FORWARD_SLASH)) {
			filePath = filePath.substring(0, filePath.length() - 1);
		}

		// Start by taking "My Drive" as parent folder and 1st folder in the file path
		// as child

		String[] fileComponents = filePath.split(DriveConstants.PATH_SEPERATOR);
		String parentName = DriveConstants.DEFAULT_DRIVE_PARENT;

		String parentId = getFileById(DriveConstants.ROOT).getId();
		String folderName = fileComponents[0];
		// Get the child folder file resource
		List<File> fileList = getChildFolderId(parentName, parentId, folderName, callingFunction);
		if (fileList.size() == 0) {
			throw new IOException("The filepath entered is : " + filePath
					+ ". Please validate if this is the full file path and try again.");
		}

		// Use the following id as parent id in the next getChildFolderId iteration
		parentId = fileList.get(0).getId();
		for (int itr = 0; itr < fileComponents.length - 1; itr++) {
			parentName = fileComponents[itr];
			folderName = fileComponents[itr + 1];
			// Recursively return child folder until the entire path is traversed by
			// updating the child as parent
			// in every iteration
			fileList = getChildFolderId(parentName, parentId, folderName, callingFunction);
			if (fileList.size() == 0)
				break;
			parentId = fileList.get(0).getId();
		}
		return fileList;
	}

	/**
	 * validates for the child folder
	 * 
	 * @param parentName      Parent folder name
	 * @param parentId        Parent fodler ID
	 * @param folderName      child folder Name
	 * @param callingFunction function which needs it..
	 * @return List of childs with the same name
	 * @throws IOException
	 * @throws JSONException
	 */
	public List<File> getChildFolderId(String parentName, String parentId, String folderName, String callingFunction)
			throws IOException, JSONException {

		FileList fileList = driveService.files().list()
				.setQ(DriveConstants.SET_NAME + folderName + DriveConstants.SET_TRASH)
				.setFields(DriveConstants.GET_CHILD_FIELDS).execute();
		List<File> files = fileList.getFiles();
		if (files.size() == 0) {
			throw new IOException("File couldn’t be " + callingFunction + " as " + folderName + " doesn’t exist in "
					+ parentName + ". Please check for typo and try again.");
		}
		List<File> eligibleFiles = new ArrayList<>();
		for (File file : files) {
			List<String> parents = file.getParents();
			if (parents == null) {
				continue;
			}
			for (String parent : parents) {
				if (parent.equals(parentId)) {
					eligibleFiles.add(file);
				}
			}
		}
		return eligibleFiles;
	}

	/**
	 * Returns Drive.Model.File object if the file is present.
	 * 
	 * @param fileId
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 */
	public File getFileById(String fileId) throws IOException, JSONException {

		File file = driveService.files().get(fileId).setFields(DriveConstants.GET_FILE_FIELDS).execute();
		System.out.println("\n\ndriveService = " + file);
		return driveService.files().get(fileId).setFields(DriveConstants.GET_FILE_FIELDS).execute();
	}

	/**
	 * Initialize Authentication
	 * 
	 * @throws IOException
	 */

	public void setupAuthorization() throws IOException {
		LOGGER.info("SETUP Authorization called");

		AuthDto authDto = authService.getAuthDto(userName);

		Credential credential = authService.getGoogleCredential(userName, getAuthCredentials(authDto.getClientId()),
				getAuthCredentials(authDto.getClientSecret()), getAuthCredentials(authDto.getRedirectURL()));

		LOGGER.info("Credential Fetched");
		// initialize driveService
		if (driveService == null)
			driveService = getDriveService(credential);
	}

	public String getAuthCredentials(String text) {
		System.out.println("Text = " + text);
		return AESEncrypter.decrypt(text);
	}

	Drive getDriveService(Credential credential) {
		return new Drive.Builder(httpTransport, JacksonFactory.getDefaultInstance(), credential)
				.setApplicationName("AAE Hopper bot").build();
	}

}
