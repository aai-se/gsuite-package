/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.commandsdk.model.AttributeType.NUMBER;
import static com.automationanywhere.botcommand.gsuite.Constants.*;
import static com.automationanywhere.commandsdk.model.AttributeType.RADIO;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.GreaterThanEqualTo;
import com.automationanywhere.commandsdk.annotations.rules.MatchesRegex;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.NumberInteger;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

@BotCommand
@CommandPkg(label = "Delete sheet", name = "DeleteWorksheet", description = "Deletes specific sheet", node_label = "with name {{sheetName}}||at index {{sheetIndex}} |in session {{sessionName}}", icon = "excel.svg")
public class DeleteWorksheet {

	private static final Logger LOGGER = LogManager.getLogger(DeleteWorksheet.class);

	@Sessions
	private Map<String, Object> sessionMap;

	private static final String SHEET_BY_INDEX_EXAMPLE = "e.g. 1 or 3";

	private static final String SHEET_BY_NAME_EXAMPLE = "e.g. Final P&L";

	@Execute
	public BooleanValue execute(

			@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String sessionName,

			@Idx(index = "2", type = RADIO, options = {
					@Idx.Option(index = "2.1", pkg = @Pkg(label = "Index", value = TRUE)),
					@Idx.Option(index = "2.2", pkg = @Pkg(label = "Name", value = FALSE)) }) @Pkg(label = "Delete sheet by", default_value_type = DataType.STRING, default_value = TRUE) @NotEmpty String sheetOption,
			@Idx(index = "2.1.1", type = NUMBER) @Pkg(label = "", description = SHEET_BY_INDEX_EXAMPLE, default_value = "1", default_value_type = DataType.NUMBER) @NotEmpty @GreaterThanEqualTo("1") @NumberInteger Number sheetIndex,
			@Idx(index = "2.2.1", type = TEXT) @Pkg(label = "", description = SHEET_BY_NAME_EXAMPLE) @NotEmpty @MatchesRegex(SHEET_NAME_VALIDATOR) String sheetName) {

		try {
			GSheets gSheets = (GSheets) sessionMap.get(sessionName);
			ExecutorValidator.getInstance().validate(gSheets);

			int sheetIndexInt = sheetIndex == null ? 0 : sheetIndex.intValue();
			boolean retVal = gSheets.deleteWorkSheet(Boolean.valueOf(sheetOption), sheetIndexInt, sheetName);
			return new BooleanValue(retVal);
			
		} catch (IOException e) {
			LOGGER.error("Exception in delete worksheet: ", e);
			String msg = e.getMessage();
			throw new BotCommandException(msg, e);
		}

	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}
