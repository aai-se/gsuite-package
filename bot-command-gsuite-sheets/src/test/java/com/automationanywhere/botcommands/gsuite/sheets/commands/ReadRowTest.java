/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.*;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.dto.sheets.WorksheetDto;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.google.api.services.sheets.v4.model.ValueRange;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.when;
import static org.testng.Assert.fail;

public class ReadRowTest {
	@Spy
	ReadRow readRow;

	@Mock
	GSheets mockGSheets;

	@Mock
	WorksheetDto worksheetDto;

	@Mock
	ListValue<StringValue> rowList;

	final String spreadSheetId = "spreadsheetId";
	final String session = "session";

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);

		sessionMap.put("session", mockGSheets);
		readRow.setSessionMap(sessionMap);
		when(mockGSheets.getSpreadSheetId()).thenReturn(spreadSheetId);

	}

	@Test
	public void readRowByActiveCell() {
		try {

			when(mockGSheets.getActiveSheetInfo()).thenReturn(worksheetDto);
			when(worksheetDto.getSheetName()).thenReturn("Sheet1");
			when(mockGSheets.getActiveCell(anyString())).thenReturn("A1");

			when(mockGSheets.readRow(anyString(), anyBoolean())).thenReturn(rowList);
			Assert.assertEquals(readRow.execute(session, ACTIVECELL, null, true), rowList);
		} catch (IOException e) {
			fail();
		}
	}

	@Test
	public void readRowBySpecificCell() {
		try {
			when(mockGSheets.readRow(anyString(), anyBoolean())).thenReturn(rowList);
			Assert.assertEquals(readRow.execute(session, SPECIFICCELL, "A1", true), rowList);
		} catch (IOException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void botCommandExceptionTest() {
			try {
				when(mockGSheets.readRow(anyString(), anyBoolean())).thenThrow(new IOException());
				readRow.execute(session, SPECIFICCELL, "A1", true);
				} catch (IOException e) {
				Assert.fail();
			}
	}

}
