/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.ACTIVECELL;
import static com.automationanywhere.botcommand.gsuite.Constants.DEFAULT_VALUE_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.DESCRIPTION_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_ACTIVECELL;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_MULTIPLECELL;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_SPECIFICCELL;
import static com.automationanywhere.botcommand.gsuite.Constants.MULTIPLE;
import static com.automationanywhere.botcommand.gsuite.Constants.NONE;
import static com.automationanywhere.botcommand.gsuite.Constants.SPECIFICCELL;
import static com.automationanywhere.botcommand.gsuite.Constants.cellAddressValidator;
import static com.automationanywhere.botcommand.gsuite.Constants.rangeValidator;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Idx.Option;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.GreaterThanEqualTo;
import com.automationanywhere.commandsdk.annotations.rules.LessThanEqualTo;
import com.automationanywhere.commandsdk.annotations.rules.MatchesRegex;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

@BotCommand
@CommandPkg(label = "Format cells", name = "formatCells", description = "Formats specified cells", node_label = "|{{cellAddress}} |{{cellRange}} |by applying |{{isFont}} font |{{isAlignment}} Alignment |{{isMerge}} Merge |in session {{sessionName}}")
public class FormatCell extends AbstractAuthUtils {
	private static final Logger LOGGER = LogManager.getLogger(FormatCell.class);

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public Value<String> execute(
			@Idx(index = "1", type = TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String session,
			@Idx(index = "2", type = AttributeType.RADIO, options = {
					@Option(index = "2.1", pkg = @Pkg(label = LABEL_ACTIVECELL, value = ACTIVECELL)),
					@Option(index = "2.2", pkg = @Pkg(label = LABEL_SPECIFICCELL, value = SPECIFICCELL)),
					@Option(index = "2.3", pkg = @Pkg(label = LABEL_MULTIPLECELL, value = MULTIPLE)) }) @Pkg(label = "Cell options", default_value_type = DataType.STRING, default_value = ACTIVECELL) @NotEmpty String cellOption,
			@Idx(index = "2.2.1", type = AttributeType.TEXT) @Pkg(label = "Cell Name", description = "e.g. A1") @MatchesRegex(cellAddressValidator) @VariableType(value = DataType.STRING) @NotEmpty String cellAddress,
			@Idx(index = "2.3.1", type = AttributeType.TEXT) @Pkg(label = "Cell range", description = "e.g. A1:D10") @MatchesRegex(rangeValidator) @VariableType(value = DataType.STRING) @NotEmpty String cellRange,

			@Idx(index = "3", type = AttributeType.CHECKBOX) @Pkg(label = "Font", default_value = "false", default_value_type = DataType.BOOLEAN) @VariableType(value = DataType.BOOLEAN) @NotEmpty Boolean isFont,
			@Idx(index = "3.1", type = AttributeType.TEXT) @Pkg(label = "Font Name", description = "e.g. Arial, Calibri, Times New Roman, etc") @VariableType(value = DataType.STRING) String fontName,
			@Idx(index = "3.2", type = AttributeType.NUMBER) @Pkg(label = "Font Size", description = "Can be from 8 to 72") @LessThanEqualTo("72") @GreaterThanEqualTo("8") @VariableType(value = DataType.NUMBER) Number fontSize,
			@Idx(index = "3.3", type = AttributeType.CHECKBOX) @Pkg(label = "Bold", description = "", default_value = "false", default_value_type = DataType.BOOLEAN) @VariableType(value = DataType.BOOLEAN) @NotEmpty Boolean isBold,
			@Idx(index = "3.4", type = AttributeType.CHECKBOX) @Pkg(label = "Italic", description = "", default_value = "false", default_value_type = DataType.BOOLEAN) @VariableType(value = DataType.BOOLEAN) @NotEmpty Boolean isItalic,
			@Idx(index = "3.5", type = AttributeType.CHECKBOX) @Pkg(label = "Underline", description = "", default_value = "false", default_value_type = DataType.BOOLEAN) @VariableType(value = DataType.BOOLEAN) @NotEmpty Boolean isUnderline,
			@Idx(index = "3.6", type = AttributeType.CHECKBOX) @Pkg(label = "Strike Through", description = "", default_value = "false", default_value_type = DataType.BOOLEAN) @VariableType(value = DataType.BOOLEAN) @NotEmpty Boolean isStrikethrough,
			@Idx(index = "3.7", type = AttributeType.TEXT) @Pkg(label = "Font color", description = "e.g. red") @VariableType(value = DataType.STRING) String fontColor,
			@Idx(index = "4", type = AttributeType.CHECKBOX) @Pkg(label = "Alignment", default_value = "false", default_value_type = DataType.BOOLEAN) @VariableType(value = DataType.BOOLEAN) @NotEmpty Boolean isAlignment,
			@Idx(index = "4.1", type = AttributeType.SELECT, options = {
					@Idx.Option(index = "4.1.1", pkg = @Pkg(label = "None", value = NONE)),
					@Idx.Option(index = "4.1.2", pkg = @Pkg(label = "Top align", value = "TOP")),
					@Idx.Option(index = "4.1.3", pkg = @Pkg(label = "Middle align", value = "MIDDLE")),
					@Idx.Option(index = "4.1.4", pkg = @Pkg(label = "Bottom align", value = "BOTTOM")) }) @Pkg(label = "Vertical Text alignment", description = "Select the vertical alignment type", default_value = "NONE", default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String verticalAlignment,
			@Idx(index = "4.2", type = AttributeType.SELECT, options = {
					@Idx.Option(index = "4.2.1", pkg = @Pkg(label = "None", value = NONE)),
					@Idx.Option(index = "4.2.2", pkg = @Pkg(label = "Align Text Left", value = "LEFT")),
					@Idx.Option(index = "4.2.3", pkg = @Pkg(label = "Align Text Center", value = "CENTER")),
					@Idx.Option(index = "4.2.4", pkg = @Pkg(label = "Align Text Right", value = "RIGHT")) }) @Pkg(label = "Horizontal Text alignment", description = "Select the horizontal alignment type", default_value = "NONE", default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String horizontalAlignment,
			@Idx(index = "5", type = AttributeType.CHECKBOX) @Pkg(label = "Wrap Text", default_value = "false", default_value_type = DataType.BOOLEAN) @VariableType(value = DataType.BOOLEAN) @NotEmpty Boolean isWrapText,
			@Idx(index = "5.1", type = AttributeType.SELECT, options = {
					@Idx.Option(index = "5.1.1", pkg = @Pkg(label = "Overflow", value = "OVERFLOW_CELL")),
					@Idx.Option(index = "5.1.2", pkg = @Pkg(label = "Legacy", value = "LEGACY_WRAP")),
					@Idx.Option(index = "5.1.3", pkg = @Pkg(label = "Clip", value = "CLIP")),
					@Idx.Option(index = "5.1.4", pkg = @Pkg(label = "Wrap", value = "WRAP")) }) @Pkg(label = "Text Wrap Options", description = "Select the text wrap type", default_value = "OVERFLOW_CELL", default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String wrapText,
			@Idx(index = "6", type = AttributeType.CHECKBOX) @Pkg(label = "Merge Type", default_value = "false", default_value_type = DataType.BOOLEAN) @VariableType(value = DataType.BOOLEAN) @NotEmpty Boolean isMerge,
			@Idx(index = "6.1", type = AttributeType.SELECT, options = {
					@Idx.Option(index = "6.1.1", pkg = @Pkg(label = "Merge Cells", value = "MERGE_ALL")),
					@Idx.Option(index = "6.1.2", pkg = @Pkg(label = "Merge Columns", value = "MERGE_COLUMNS")),
					@Idx.Option(index = "6.1.3", pkg = @Pkg(label = "Merge Rows", value = "MERGE_ROWS")),
					@Idx.Option(index = "6.1.4", pkg = @Pkg(label = "Unmerge Cells", value = "UNMERGE")) }) @Pkg(label = "", description = "Select the merge type", default_value = "Merge & Center", default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String mergeType) {
		try {
			validateSessionNotExists(sessionMap, session);
			GSheets gSheets = (GSheets) sessionMap.get(session);
			ExecutorValidator.getInstance().validate(gSheets);
			String spredSheetId = gSheets.getSpreadSheetId();
			if (ACTIVECELL.equals(cellOption))
				cellAddress = gSheets.getActiveCell(gSheets.getActiveSheetInfo().getSheetName());
			
			Boolean format = gSheets.formatCell(spredSheetId, cellOption, cellAddress, cellRange, isFont, fontName,
					fontSize, isBold, isItalic, isUnderline, isStrikethrough, fontColor, isAlignment, verticalAlignment,
					horizontalAlignment, isWrapText, wrapText, isMerge, mergeType);
			
			return new StringValue(spredSheetId);
		} catch (IOException e) {
			LOGGER.error("exception in inserting row/ column: ", e);
			String msg = e.getMessage();
			throw new BotCommandException(msg, e);
		} catch (Exception ex) {
			throw new BotCommandException(ex.getMessage(), ex);
		}
		
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}
