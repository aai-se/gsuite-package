/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static org.testng.Assert.*;
import static com.automationanywhere.botcommand.gsuite.Constants.*;
import static com.automationanywhere.botcommand.gsuite.Constants.SPECIFICCELL;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.dto.sheets.WorksheetDto;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;

public class DeleteRowColumnTest {
	@Spy
	DeleteRowColumn deleteRowColumn;

	@Mock
	AuthenticationService authenticationService;

	@Mock
	GSheets mockGSheets;

	@Mock
	WorksheetDto activeSheetInfo;

	final String session = "session";

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		sessionMap.put("session", mockGSheets);
		deleteRowColumn.setSessionMap(sessionMap);
	}

	@Test
	public void deleteRowAtSpecificIndex() {
		try {
			when(mockGSheets.deleteRowColumn(ROWS, SPECIFICCELL, "1")).thenReturn(true);
			deleteRowColumn.execute(session, "ROWOPERATION", "DELETEROWAT", "1", null, null, null, null, null, null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void deleteRowByActiveCell() {
		try {
			when(mockGSheets.deleteRowColumn(ROWS, ACTIVECELL, EMPTY)).thenReturn(true);
			deleteRowColumn.execute(session, "ROWOPERATION", "DELETEROWBY", null, "ACTIVECELL", null, null, null, null, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void deleteRowRange() {
		try {
			when(mockGSheets.deleteRowColumn(ROWS, RANGE, "5:7")).thenReturn(true);
			deleteRowColumn.execute(session, "ROWOPERATION", "DELETEROWBY", null, "RANGE", "5:7", null, null, null, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void deleteColumnAtSpecificIndex() {
		try {
			when(mockGSheets.deleteRowColumn(COLUMNS, SPECIFICCELL, "1")).thenReturn(true);
			deleteRowColumn.execute(session, "COLUMNOPERATION", null, null, null, null, "DELETECOLUMNAT", "1", null, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void deleteColumnByActiveCell() {
		try {
			when(mockGSheets.deleteRowColumn(COLUMNS, ACTIVECELL, "1")).thenReturn(true);
			deleteRowColumn.execute(session, "COLUMNOPERATION", null, null, null, null, "DELETECOLUMNBY", null, "ACTIVECELL", null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void deleteColumnByRange() {
		try {
			when(mockGSheets.deleteRowColumn(COLUMNS, RANGE, "B:E")).thenReturn(true);
			deleteRowColumn.execute(session, "COLUMNOPERATION", null, null, null, null, "DELETECOLUMNBY", null, "RANGE", "B:E");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	@Test(expectedExceptions = BotCommandException.class)
	public void deleteRowColumnIOException() {
		try {
			when(mockGSheets.deleteRowColumn(ROWS, SPECIFICCELL, "1")).thenThrow(new IOException());
			deleteRowColumn.execute(session, "ROWOPERATION", "DELETEROWAT", "1", null, null, null, null, null, null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

}
