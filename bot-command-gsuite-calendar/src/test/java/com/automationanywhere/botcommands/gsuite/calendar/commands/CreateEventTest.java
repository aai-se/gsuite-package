/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */

package com.automationanywhere.botcommands.gsuite.calendar.commands;

import com.automationanywhere.botcommand.gsuite.api.GCalendar;
import com.automationanywhere.botcommand.gsuite.dto.calendar.EventDTO;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class CreateEventTest {

    @Spy
    CreateEvent createEvent;

    @Mock
    GCalendar gCalendar;

    Map<String,Object> sessionMap;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        sessionMap = new HashMap<>();
    }

    @Test
    public void createEventWithDefaultValues() throws GeneralSecurityException, IOException, ParseException {

            doReturn(gCalendar).when(createEvent).getCalendar();
            createEvent.setSessionMap(sessionMap);

            EventDTO eventDTOInput = createEvent.getEventDTO("TestEvent",null,null,"2019-07-30","2019-07-30","ALL_DAY",null,null, "SYSTEM_TIMEZONE",null,null,false,"PUBLIC","Event desc");
            EventDTO eventDTOOut = createEvent.getEventDTO("TestEvent",null,null,"2019-07-30","2019-07-30","ALL_DAY",null,null, "SYSTEM_TIMEZONE",null,null,false,"PUBLIC","Event desc");

            when(gCalendar.createEvent(eq(eventDTOInput))).thenReturn(eventDTOOut);

            createEvent.execute("session","TestEvent",null,null,"2019-07-30","2019-07-30","ALL_DAY",null,null, "SYSTEM_TIMEZONE",null,null,false,"PUBLIC","Event desc");
            assertEquals(eventDTOOut,eventDTOInput);
            verify(gCalendar,times(1)).createEvent(eq(eventDTOInput));
    }

    @Test
    public void createEventWithSpecificTimes() throws GeneralSecurityException, IOException, ParseException {

            doReturn(gCalendar).when(createEvent).getCalendar();
            createEvent.setSessionMap(sessionMap);

            EventDTO eventDTOInput = createEvent.getEventDTO("TestEvent",null,"test@univ.org","2019-07-30","2019-07-30","SPECIFY_TIME","11:00:00","12:00:00", "SYSTEM_TIMEZONE",null,null,false,"PUBLIC","Event desc");
            EventDTO eventDTOOut = createEvent.getEventDTO("TestEvent",null,"test@univ.org","2019-07-30","2019-07-30","SPECIFY_TIME","11:00:00","12:00:00", "SYSTEM_TIMEZONE",null,null,false,"PUBLIC","Event desc");

            when(gCalendar.createEvent(eq(eventDTOInput))).thenReturn(eventDTOOut);

            createEvent.execute("session","TestEvent",null,"test@univ.org","2019-07-30","2019-07-30","SPECIFY_TIME","11:00:00","12:00:00", "SYSTEM_TIMEZONE",null,null,false,"PUBLIC","Event desc");
            assertEquals(eventDTOOut,eventDTOInput);
            verify(gCalendar,times(1)).createEvent(eq(eventDTOInput));

    }


    @Test
    public void createEventWithSpecificTimeZone() throws GeneralSecurityException, IOException, ParseException {

            doReturn(gCalendar).when(createEvent).getCalendar();
            createEvent.setSessionMap(sessionMap);

            EventDTO eventDTOInput = createEvent.getEventDTO("TestEvent",null,"john@company.com,test@univ.org","2019-07-30","2019-07-30","SPECIFY_TIME","11:00:00","12:00:00", "SPECIFY_TIMEZONE","America/Los_Angeles","America/Los_Angeles",false,"PUBLIC","Event desc");
            EventDTO eventDTOOut = createEvent.getEventDTO("TestEvent",null,"john@company.com,test@univ.org","2019-07-30","2019-07-30","SPECIFY_TIME","11:00:00","12:00:00", "SPECIFY_TIMEZONE","America/Los_Angeles","America/Los_Angeles",false,"PUBLIC","Event desc");

            when(gCalendar.createEvent(eq(eventDTOInput))).thenReturn(eventDTOOut);

            createEvent.execute("session","TestEvent",null,"john@company.com,test@univ.org","2019-07-30","2019-07-30","SPECIFY_TIME","11:00:00","12:00:00", "SPECIFY_TIMEZONE","America/Los_Angeles","America/Los_Angeles",false,"PUBLIC","Event desc");
            assertEquals(eventDTOOut,eventDTOInput);
            verify(gCalendar,times(1)).createEvent(eq(eventDTOInput));

    }


    @Test
    public void createEventWithRecurrance() throws GeneralSecurityException, IOException, ParseException {

            doReturn(gCalendar).when(createEvent).getCalendar();
            createEvent.setSessionMap(sessionMap);
            List<String> recurrance = new ArrayList<>();
            recurrance.add("RDATE;VALUE=DATE:19970101,19970120");
            createEvent.setRecurringRule(recurrance);

            EventDTO eventDTOInput = createEvent.getEventDTO("TestEvent",null,"john@company.com,test@univ.org","2019-07-30","2019-07-30","SPECIFY_TIME","11:00:00","12:00:00", "SPECIFY_TIMEZONE","America/Los_Angeles","America/Los_Angeles",true,"PUBLIC","Event desc");
            EventDTO eventDTOOut = createEvent.getEventDTO("TestEvent",null,"john@company.com,test@univ.org","2019-07-30","2019-07-30","SPECIFY_TIME","11:00:00","12:00:00", "SPECIFY_TIMEZONE","America/Los_Angeles","America/Los_Angeles",true,"PUBLIC","Event desc");

            when(gCalendar.createEvent(eq(eventDTOInput))).thenReturn(eventDTOOut);

            createEvent.execute("session","TestEvent",null,"john@company.com,test@univ.org","2019-07-30","2019-07-30","SPECIFY_TIME","11:00:00","12:00:00", "SPECIFY_TIMEZONE","America/Los_Angeles","America/Los_Angeles",true,"PUBLIC","Event desc");
            assertEquals(eventDTOOut,eventDTOInput);
            assertTrue(Arrays.equals(eventDTOOut.getRecurrence(),eventDTOInput.getRecurrence()));

            verify(gCalendar,times(1)).createEvent(eq(eventDTOInput));

    }
}
