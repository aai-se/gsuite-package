package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.DEFAULT_VALUE_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.DESCRIPTION_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.cellAddressValidator;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.MatchesRegex;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.AttributeType;

@BotCommand
@CommandPkg(label = "Go to cell", name = "GoToCell", node_label = ": {{cellOption}} |{{cell}}|", description = "Goes to a specified cell", icon = "excel.svg")

public class GoToCell {
	private static final Logger LOGGER = LogManager.getLogger(GoToCell.class);

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public BooleanValue execute(
			@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = STRING) @NotEmpty String sessionName,
			@Idx(index = "2", type = AttributeType.RADIO, options = {
					@Idx.Option(index = "2.1", pkg = @Pkg(label = "Specific cell", value = "specificCell")),
					@Idx.Option(index = "2.2", pkg = @Pkg(label = "One cell to the left", value = "OneCellLeft")),
					@Idx.Option(index = "2.3", pkg = @Pkg(label = "One cell to the right", value = "OneCellRight")),
					@Idx.Option(index = "2.4", pkg = @Pkg(label = "One cell above", value = "OneCellUp")),
					@Idx.Option(index = "2.5", pkg = @Pkg(label ="One cell below", value = "OneCellDown")),
					@Idx.Option(index = "2.6", pkg = @Pkg(label = "Beginning of the row", value = "BeginningOfRow")),
					@Idx.Option(index = "2.7", pkg = @Pkg(label = "End of the row", value = "EndOfRow")),
					@Idx.Option(index = "2.8", pkg = @Pkg(label = "Beginning of the column", value = "BeginningOfColumn")),
					@Idx.Option(index = "2.9", pkg = @Pkg(label = "End of the column", value = "EndOfColumn")) }) @Pkg(label = "Cell options", default_value = "specificCell", default_value_type = STRING) String cellOption,
			@Idx(index = "2.1.1", type = AttributeType.TEXT) @Pkg(label = "Cell name", description = "e.g. B1") @MatchesRegex(cellAddressValidator) @VariableType(value = STRING) @NotEmpty String cellAddress) throws JSONException {
		try {
			  GSheets gSheets = (GSheets) sessionMap.get(sessionName);
		        ExecutorValidator.getInstance().validate(gSheets);     
		        return new BooleanValue(gSheets.goToCell(cellOption, cellAddress));
			
		} catch (IOException e) {
			LOGGER.error("exception in getting single cell data from the sheet: ", e);
			String msg = e.getMessage();
			throw new BotCommandException(msg, e);
		}
		
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}
