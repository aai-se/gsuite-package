/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
package com.automationanywhere.botcommands.gsuite.authorization.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.automationanywhere.botcommands.gsuite.authorization.utilities.AuthTestSetUp;
import com.automationanywhere.botcommands.gsuite.authorization.utilities.ConstantsTest;

/***
 * Tests on the methods found in DeleteCell
 */
public class ConnectTest extends AuthTestSetUp {

	private static final Logger LOGGER = LogManager.getLogger(ConnectTest.class);

	/***
	 * Do initial Setup
	 */
	@BeforeClass
	public static void doSetup() {
		doInitialSetup();

	}

	/***
	 * Connect with respective inputs parameters.
	 */
	@Test
	public void connectTestMethod1() {
		LOGGER.info("Connect Test Method started..");
		connect.execute(ConstantsTest.getUserName(), ConstantsTest.getClientid(), ConstantsTest.getRedirectUri(),
				ConstantsTest.getClientSecret());
		LOGGER.info("Connect Test Method end..");
	}

//	/***
//	 * Check method with invalid parameters. It will return BotCommandException.
//	 */
//	@Test(expected = BotCommandException.class)
//	public void connectTestMethod2() {
//		connect.execute(ConstantsTest.getUserName(), ConstantsTest.getClientid(), ConstantsTest.getRedirectUri(),
//				ConstantsTest.getClientSecret());
//	}
//
//	/***
//	 * Check method with invalid parameters. It will return BotCommandException.
//	 */
//	@Test(expected = Exception.class)
//	public void connectTestMethod4() {
//		connect.setSessionMap(null);
//		connect.execute(ConstantsTest.getUserName(), ConstantsTest.getClientid(), ConstantsTest.getRedirectUri(),
//				ConstantsTest.getClientSecret());
//	}
//
//	@Test(expected = BotCommandException.class)
//	public void connectTestMethod3() {
//		connect.execute(ConstantsTest.getUserName(), ConstantsTest.getClientid(), ConstantsTest.getRedirectUri(),
//				ConstantsTest.getClientSecret());
//	}

	@AfterClass
	public static void doCleanUp() {
		disConnect.execute(ConstantsTest.getUserName(), true);
	}

}
