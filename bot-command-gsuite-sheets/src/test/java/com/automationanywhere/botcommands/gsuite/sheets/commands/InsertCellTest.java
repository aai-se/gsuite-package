package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.dto.sheets.WorksheetDto;
import com.automationanywhere.botcommands.gsuite.sheets.constants.Constants;

public class InsertCellTest {
	@Spy
	InsertCell insertCell;

	@Mock
	GSheets mockGSheets;

	final String session = "session";

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		sessionMap.put("session", mockGSheets);
		insertCell.setSessionMap(sessionMap);
	}

	@Test
	public void insertSpecificCellTest() throws IOException {
		when(mockGSheets.insertCell(anyString(), anyString())).thenReturn(true);
		Boolean response = insertCell.execute(session, Constants.SPECIFICCELL, "A1", Constants.ROWS).get();
		Assert.assertTrue(response);
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void InvalidShiftTest() throws IOException {
		when(mockGSheets.insertCell("A1", "Left")).thenThrow(new IOException());
		insertCell.execute(session, Constants.SPECIFICCELL, "A1", "Left").get();
	}
	
	@Test(expectedExceptions = BotCommandException.class)
	public void InvalidCellInput() throws IOException {
		when(mockGSheets.insertCell("A100000", Constants.ROWS)).thenThrow(new IOException());
		insertCell.execute(session, Constants.SPECIFICCELL, "A100000", Constants.ROWS).get();
	}

	@Test
	public void insertActiveCellTest() throws IOException {
		WorksheetDto workSheet = mock(WorksheetDto.class);
		when(mockGSheets.getActiveSheetInfo()).thenReturn(workSheet);
		when(workSheet.getSheetName()).thenReturn("MyworkSheet");
		when(mockGSheets.getActiveCell(workSheet.getSheetName())).thenReturn("A1");
		when(mockGSheets.insertCell(anyString(), anyString())).thenReturn(true);
		Boolean response = insertCell.execute(session, Constants.ACTIVECELL, "", Constants.ROWS).get();
		Assert.assertTrue(response);
	}

	
	@Test(expectedExceptions = BotCommandException.class)
	public void InvalidSessionTest() throws IOException {
		insertCell.execute("default", Constants.SPECIFICCELL, "A1", "Left").get();

	}

}
