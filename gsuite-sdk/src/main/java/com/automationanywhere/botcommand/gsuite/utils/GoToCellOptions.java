package com.automationanywhere.botcommand.gsuite.utils;

import static com.automationanywhere.botcommand.gsuite.Constants.MESSAGES;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.impl.GSheetsImpl;

public enum GoToCellOptions {
	SpecificCell {

		@Override
		public void goToCell(GSheetsImpl gsheetsImpl, String cellName) {
			gsheetsImpl.setActiveCell(gsheetsImpl.getActiveSheetInfo().getSheetName(),cellName);
		}
	},
	OneCellLeft {
		@Override
		public void goToCell(GSheetsImpl gsheetsImpl, String cellName) {
			String sheetName = gsheetsImpl.getActiveSheetInfo().getSheetName();
			String newActiveCell = GoToCellUtilities.oneCellLeft(gsheetsImpl.getActiveCell(sheetName));
			if (newActiveCell.equals(gsheetsImpl.getActiveCell(sheetName)))
				throw new BotCommandException(MESSAGES.getString(gotoCellOperationException, "OneCellLeft"));
			gsheetsImpl.setActiveCell(gsheetsImpl.getActiveSheetInfo().getSheetName(),newActiveCell);
		}
	},
	OneCellRight {
		@Override
		public void goToCell(GSheetsImpl gsheetsImpl, String cellName) {
			String sheetName = gsheetsImpl.getActiveSheetInfo().getSheetName();
			String newActiveCell = GoToCellUtilities.oneCellRight(gsheetsImpl.getActiveCell(sheetName));
			if (newActiveCell.equals(gsheetsImpl.getActiveCell(sheetName)))
				throw new BotCommandException(MESSAGES.getString(gotoCellOperationException, "OneCellRight"));
			gsheetsImpl.setActiveCell(gsheetsImpl.getActiveSheetInfo().getSheetName(),newActiveCell);
		}
	},
	OneCellUp {
		@Override
		public void goToCell(GSheetsImpl gsheetsImpl, String cellName) {
			String sheetName = gsheetsImpl.getActiveSheetInfo().getSheetName();
			String newActiveCell = GoToCellUtilities.oneCellUp(gsheetsImpl.getActiveCell(sheetName));
			if (newActiveCell.equals(gsheetsImpl.getActiveCell(sheetName)))
				throw new BotCommandException(MESSAGES.getString(gotoCellOperationException, "OneCellUp"));
			gsheetsImpl.setActiveCell(gsheetsImpl.getActiveSheetInfo().getSheetName(),newActiveCell);
		}
	},
	OneCellDown {
		@Override
		public void goToCell(GSheetsImpl gsheetsImpl, String cellName) {
			String sheetName = gsheetsImpl.getActiveSheetInfo().getSheetName();
			String newActiveCell = GoToCellUtilities.oneCellDown(gsheetsImpl.getActiveCell(sheetName));
			if (newActiveCell.equals(gsheetsImpl.getActiveCell(sheetName)))
				throw new BotCommandException(MESSAGES.getString(gotoCellOperationException, "OneCellDown"));
			gsheetsImpl.setActiveCell(gsheetsImpl.getActiveSheetInfo().getSheetName(),newActiveCell);
		}
	},
	BeginningOfRow {
		@Override
		public void goToCell(GSheetsImpl gsheetsImpl, String cellName) {
			String sheetName = gsheetsImpl.getActiveSheetInfo().getSheetName();
			String newActiveCell = GoToCellUtilities.beginningOfRow(gsheetsImpl.getActiveCell(sheetName));
			gsheetsImpl.setActiveCell(gsheetsImpl.getActiveSheetInfo().getSheetName(),newActiveCell);
		}
	},
	EndOfRow {
		@Override
		public void goToCell(GSheetsImpl gsheetsImpl, String cellName) {
			String sheetName = gsheetsImpl.getActiveSheetInfo().getSheetName();
			String newActiveCell = GoToCellUtilities.endOfRow(gsheetsImpl, gsheetsImpl.getActiveCell(sheetName));
			gsheetsImpl.setActiveCell(gsheetsImpl.getActiveSheetInfo().getSheetName(),newActiveCell);
		}
	},
	BeginningOfColumn {
		@Override
		public void goToCell(GSheetsImpl gsheetsImpl, String cellName) {
			String sheetName = gsheetsImpl.getActiveSheetInfo().getSheetName();
			String newActiveCell = GoToCellUtilities.beginningOfColumn(gsheetsImpl.getActiveCell(sheetName));
			gsheetsImpl.setActiveCell(gsheetsImpl.getActiveSheetInfo().getSheetName(),newActiveCell);
		}
	},
	EndOfColumn {
		@Override
		public void goToCell(GSheetsImpl gsheetsImpl, String cellName) {
			String sheetName = gsheetsImpl.getActiveSheetInfo().getSheetName();
			String newActiveCell = GoToCellUtilities.endOfColumn(gsheetsImpl, gsheetsImpl.getActiveCell(sheetName));
			gsheetsImpl.setActiveCell(gsheetsImpl.getActiveSheetInfo().getSheetName(),newActiveCell);
		}
	};

	public abstract void goToCell(GSheetsImpl gsheetsImpl, String cellName);

	private static final String gotoCellOperationException = "OnlineExcel.Command.GotoCellOperationException";
}
