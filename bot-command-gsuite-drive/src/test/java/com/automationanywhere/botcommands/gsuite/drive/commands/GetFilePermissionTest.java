/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommands.gsuite.drive.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

/**
 * Tests get Google Drive file permission functionality.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

public class GetFilePermissionTest {

	public static final SecureString USERNAME = ConstantsTest.getUserName();

	@Spy
	GetFilePermission getFilePermission;

	@Mock
	GDrive mockGDrive;

	@Mock
	AuthenticationService mockAuthService;

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {
		MockitoAnnotations.initMocks(this);
		getFilePermission.setAuthenticationService(mockAuthService);
	}

	@Test
	public void getFilePermission_returnsFilePermissionList_whenRequiredFileNameProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(getFilePermission).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			List<Value> permissionList = new ArrayList<>();
			permissionList.add(new StringValue("{\"deleted\":false,\"displayName\":\"Saanchi Muthya\","
					+ "\"emailAddress\":\"saanchi@aademo.page\",\"id\":\"03243532392755356977\","
					+ "\"kind\":\"drive#permission\",\"role\":\"owner\",\"type\":\"user\"}"));
			ListValue<?> expectedOutput = new ListValue<>();
			expectedOutput.set(permissionList);

			when(mockGDrive.getFilePermission(inputFileParamName.toString())).thenReturn(permissionList);
			Value<List<Value>> testId = getFilePermission.execute(USERNAME, "BYNAME", fileName, fileId);
			Assert.assertEquals(expectedOutput.get(), testId.get());
		} catch (GeneralSecurityException | IOException | JSONException e) {
			Assert.fail();
		}
	}

	@Test
	public void getFilePermission_returnsFilePermissionList_whenRequiredFileIdProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(getFilePermission).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			JSONObject inputFileParamID = new JSONObject();
			inputFileParamID.put("fileId", fileId);

			List<Value> permissionList = new ArrayList<>();
			permissionList.add(new StringValue("{\"deleted\":false,\"displayName\":\"Saanchi Muthya\","
					+ "\"emailAddress\":\"saanchi@aademo.page\",\"id\":\"03243532392755356977\","
					+ "\"kind\":\"drive#permission\",\"role\":\"owner\",\"type\":\"user\"}"));
			ListValue<?> expectedOutput = new ListValue<>();
			expectedOutput.set(permissionList);

			when(mockGDrive.getFilePermission(inputFileParamID.toString())).thenReturn(permissionList);
			Value<List<Value>> testId = getFilePermission.execute(USERNAME, "BYID", fileName, fileId);
			Assert.assertEquals(expectedOutput.get(), testId.get());
		} catch (GeneralSecurityException | IOException | JSONException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void getFilePermission_throwsBotCommandException_whenGetPermissionAPIthrowsIOException() {
		try {
			Mockito.doReturn(mockGDrive).when(getFilePermission).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			when(mockGDrive.getFilePermission(inputFileParamName.toString())).thenThrow(IOException.class);
			getFilePermission.execute(USERNAME, "BYNAME", fileName, fileId);

			Assert.fail();
		} catch (GeneralSecurityException | JSONException e) {
			Assert.fail();
		} catch (IOException e) {
			Assert.assertTrue(true);
		}
	}

	@Test
	public void getDrive_newGDriveImplObjectIsNotNull() {
		try {
			GDrive drive = getFilePermission.getDrive(USERNAME);
			assertNotNull(drive);
		} catch (GeneralSecurityException | IOException e) {
			fail();
		}
	}
}
