package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;

public class DeleteRangeTest {

	@Spy
	DeleteRange deleteRange;
	@Mock
	GSheets mockGSheets;
	
	final String session = "session";
	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() throws IOException {
		MockitoAnnotations.initMocks(this);
		sessionMap.put(session, mockGSheets);
		deleteRange.setSessionMap(sessionMap);
	}
	
	

	@Test
	public void deleteRangeTest() throws IOException  {
		when(mockGSheets.deleteRange(anyString(), anyString())).thenReturn(true);
		Boolean response = deleteRange.execute(session, "A1:B4", "ROWS").get();
		Assert.assertTrue(response);
	}
 
	@Test(expectedExceptions = BotCommandException.class)
	public void deleteRangeSessionTest() {
		deleteRange.execute("default", "A1:B4", "left");
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void InvalidShiftTypeTest() throws IOException   {
		when(mockGSheets.deleteRange(anyString(), anyString())).thenThrow(new IOException());
		deleteRange.execute(session, "A1:B4", "left");
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void InvalidRangeTest() throws IOException  {
		
		when(mockGSheets.deleteRange(anyString(), anyString())).thenThrow(new IOException());
		deleteRange.execute(session, "B4:A1", "COLUMNS");
	}

}
