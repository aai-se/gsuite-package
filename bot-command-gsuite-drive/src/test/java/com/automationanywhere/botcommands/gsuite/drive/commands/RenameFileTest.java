/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommands.gsuite.drive.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

/**
 * Tests rename Google Drive file functionality.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

public class RenameFileTest {

	public static final SecureString USERNAME = ConstantsTest.getUserName();

	@Spy
	RenameFile renameFile;

	@Mock
	GDrive mockGDrive;

	@Mock
	AuthenticationService mockAuthService;

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		renameFile.setAuthenticationService(mockAuthService);
	}

	@Test
	public void renameFile_returnsRenamedFileId_whenRequiredFileNameProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(renameFile).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			String newFileName = "newtrial_file.txt";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);
			inputFileParamName.put("fileType", "file");

			when(mockGDrive.renameFile(inputFileParamName.toString(), newFileName)).thenReturn(fileId);
			Value<String> testId = renameFile.execute(USERNAME, "BYNAME", fileName, fileId, newFileName);
			Assert.assertEquals(fileId, testId.toString());
		} catch (GeneralSecurityException | JSONException | IOException e) {
			Assert.fail();
		}
	}

	@Test
	public void renameFile_returnsRenamedFileId_whenRequiredFileIdProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(renameFile).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trialTest.txt";
			String newFileName = "trail	Test.txt";
			JSONObject inputFileParamID = new JSONObject();
			inputFileParamID.put("fileId", fileId);
			inputFileParamID.put("fileType", "file");

			when(mockGDrive.renameFile(inputFileParamID.toString(), newFileName)).thenReturn(fileId);
			Value<String> testId = renameFile.execute(USERNAME, "BYID", fileName, fileId, newFileName);
			Assert.assertEquals(fileId, testId.toString());
		} catch (GeneralSecurityException | JSONException | IOException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void renameFile_throwsBotCommandException_whenRenameFileAPIthrowsIOException() {
		try {
			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			String newFileName = "newtrial_file.txt";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);
			inputFileParamName.put("fileType", "file");

			Mockito.doReturn(mockGDrive).when(renameFile).getDrive(USERNAME);
			when(mockGDrive.renameFile(inputFileParamName.toString(), newFileName)).thenThrow(IOException.class);
			renameFile.execute(USERNAME, "BYNAME", fileName, fileId, newFileName);
			Assert.fail();
		} catch (GeneralSecurityException | JSONException e) {
			Assert.fail();
		} catch (IOException e) {
			Assert.assertTrue(true);
		}
	}

	@Test
	public void getDrive_newGDriveImplObjectIsNotNull() {
		try {
			GDrive drive = renameFile.getDrive(USERNAME);
			assertNotNull(drive);
		} catch (GeneralSecurityException | IOException e) {
			Assert.fail();
		}
	}
}
