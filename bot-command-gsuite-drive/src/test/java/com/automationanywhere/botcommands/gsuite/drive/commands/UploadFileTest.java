/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommands.gsuite.drive.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

/**
 * Tests upload Google Drive file functionality.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

public class UploadFileTest {

	public static final SecureString USERNAME = ConstantsTest.getUserName();

	@Spy
	UploadFile uploadFile;

	@Mock
	AuthenticationService mockAuthService;

	@Mock
	GDrive mockGDrive;

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		uploadFile.setAuthenticationService(mockAuthService);
	}

	@Test
	public void uploadFile_returnsUploadedFileId_whenRequiredFileNameProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(uploadFile).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			when(mockGDrive.uploadFile("C:/Users/user/Downloads/trialtext.txt", inputFileParamName.toString(), false))
					.thenReturn(fileId);
			Value<String> testId = uploadFile.execute(USERNAME, "C:/Users/user/Downloads/trialtext.txt", "BYNAME",
					fileName, fileId, false);
			Assert.assertEquals(fileId, testId.toString());
		} catch (JSONException | IOException | GeneralSecurityException e) {
			Assert.fail();
		}
	}

	@Test
	public void uploadFile_returnsUploadedFileId_whenRequiredFileIdProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(uploadFile).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";

			JSONObject inputFileParamID = new JSONObject();
			inputFileParamID.put("fileId", fileId);

			when(mockGDrive.uploadFile("C:/Users/user/Downloads/trialtext.txt", inputFileParamID.toString(), false))
					.thenReturn(fileId);
			Value<String> testId = uploadFile.execute(USERNAME, "C:/Users/user/Downloads/trialtext.txt", "BYID",
					fileName, fileId, false);
			Assert.assertEquals(fileId, testId.toString());
		} catch (JSONException | IOException | GeneralSecurityException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void uploadFile_throwsBotCommandException_whenUploadFileAPIthrowsIOException() {
		try {
			Mockito.doReturn(mockGDrive).when(uploadFile).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			String newFileName = "newtrial_file.txt";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			JSONObject outputFileParam = new JSONObject();
			outputFileParam.put("newFileName", newFileName);
			when(mockGDrive.uploadFile("C:/Users/user/Downloads/trialtext.txt", inputFileParamName.toString(), false))
					.thenThrow(IOException.class);
			uploadFile.execute(USERNAME, "C:/Users/user/Downloads/trialtext.txt", "BYNAME", fileName, fileId, false);

			Assert.fail();
		} catch (JSONException | GeneralSecurityException e) {
			Assert.fail();
		} catch (IOException e) {
			Assert.assertTrue(true);
		}
	}

	@Test
	public void getDrive_newGDriveImplObjectIsNotNull() {
		try {
			GDrive drive = uploadFile.getDrive(USERNAME);
			assertNotNull(drive);
		} catch (GeneralSecurityException | IOException e) {
			fail();
		}
	}
}
