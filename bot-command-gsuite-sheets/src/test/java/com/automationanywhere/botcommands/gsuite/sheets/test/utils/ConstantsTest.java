package com.automationanywhere.botcommands.gsuite.sheets.test.utils;

import java.io.IOException;
import java.security.GeneralSecurityException;

import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.core.security.SecureString;

public class ConstantsTest extends AbstractAuthUtils {

	public static final String CLIENTID = "1045113049707-u3fhdtroech2uch19jogs52fg98t3lpu.apps.googleusercontent.com";
	public static final String REDIRECT_URI = "http://localhost";
	public static final String CLIENTSECRET = "6rjSOWzAEKEdiFqpCDGxirkk";
	public static final String USERNAME = "saanchi@aademo.page";
	
	public static final String APPCLIENTID = "420153753071-0qm5s4teg4nu390b5auv6ilcjk4fu845.apps.googleusercontent.com";
	public static final String APPREDIRECT_URI = "urn:ietf:wg:oauth:2.0:oob";
	public static final String APPCLIENTSECRET = "2rMX-W6sXRMxJLoj4YRzcw2B";
	public static final String APPUSERNAME = "appperfectdemo@gmail.com";
	
	
	/*
	 * Saanchi's  ACCount
	 */
	public static SecureString getClientid() {
		return new SecureString(CLIENTID.toCharArray());
	}

	public static SecureString getRedirectUri() {
		return new SecureString(REDIRECT_URI.toCharArray());
	}

	public static SecureString getClientSecret() {
		return new SecureString(CLIENTSECRET.toCharArray());
	}

	public static SecureString getUserName() {
		return new SecureString(USERNAME.toCharArray());
	}
	/*
	 * AppPerfect ACCount
	 */
//	public static SecureString getAppClientid() {
//		return new SecureString(APPCLIENTID.toCharArray());
//	}
//
//	public static SecureString getAppRedirectUri() {
//		return new SecureString(APPREDIRECT_URI.toCharArray());
//	}
//
//	public static SecureString getAppClientSecret() {
//		return new SecureString(APPCLIENTSECRET.toCharArray());
//	}
//
//	public static SecureString getAppUserName() {
//		return new SecureString(APPUSERNAME.toCharArray());
//	}
}
