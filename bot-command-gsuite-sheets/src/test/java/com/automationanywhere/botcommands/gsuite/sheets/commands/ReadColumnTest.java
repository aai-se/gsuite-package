/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.ACTIVECELL;
import static com.automationanywhere.botcommand.gsuite.Constants.SPECIFICCELL;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.dto.sheets.WorksheetDto;

public class ReadColumnTest {
	@Spy
	ReadColumn readColumn;

	@Mock
	GSheets mockGSheets;

	@Mock
	WorksheetDto worksheetDto;

	@Mock
	ListValue<StringValue> columnList;

	final String spreadSheetId = "spreadsheetId";
	final String session = "session";

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);

		sessionMap.put("session", mockGSheets);
		readColumn.setSessionMap(sessionMap);
		when(mockGSheets.getSpreadSheetId()).thenReturn(spreadSheetId);
	}

	@Test
	public void readColumnByActiveCell() {
		try {
			when(mockGSheets.getActiveSheetInfo()).thenReturn(worksheetDto);
			when(worksheetDto.getSheetName()).thenReturn("Sheet1");
			when(mockGSheets.getActiveCell(anyString())).thenReturn("A1");

			when(mockGSheets.readColumn(anyString(), anyBoolean())).thenReturn(columnList);
			Assert.assertEquals(readColumn.execute(session, ACTIVECELL, null, true), columnList);
		} catch (IOException e) {
			fail();
		}
	}

	@Test
	public void readRowBySpecificCell() {
		try {
			when(mockGSheets.readColumn(anyString(), anyBoolean())).thenReturn(columnList);
			Assert.assertEquals(readColumn.execute(session, SPECIFICCELL, "A1", true), columnList);
		} catch (IOException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void botCommandExceptionTest() {
		try {
			when(mockGSheets.readColumn(anyString(), anyBoolean())).thenThrow(new IOException());
			readColumn.execute(session, SPECIFICCELL, "A1", true);
		} catch (IOException e) {
			Assert.fail();
		}
	}
}
