///*
// * Copyright (c) 2019 Automation Anywhere.
// * All rights reserved.
// *
// * This software is the proprietary information of Automation Anywhere.
// * You shall use it only in accordance with the terms of the license agreement
// * you entered into with Automation Anywhere.
// *
// */
//package com.automationanywhere.botcommands.gsuite.sheets.commands;
//
//import static org.mockito.Mockito.when;
//import static org.testng.Assert.fail;
//
//import java.io.IOException;
//import java.security.GeneralSecurityException;
//import java.util.HashMap;
//import java.util.Map;
//
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.MockitoAnnotations;
//import org.mockito.Spy;
//import org.testng.Assert;
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.Test;
//
//import com.automationanywhere.botcommand.exception.BotCommandException;
//import com.automationanywhere.botcommand.gsuite.api.GDrive;
//import com.automationanywhere.botcommand.gsuite.api.GSheets;
//import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
//
//public class SaveAsSheetTest {
//	@Spy
//	SaveAsSheet saveAs;
//
//	@Mock
//	AuthenticationService authenticationService;
//
//	@Mock
//	GSheets mockGSheets;
//
//	@Mock
//	GDrive mockGDrive;
//
//	final String session = "session";
//
//	Map<String, Object> sessionMap = new HashMap<>();
//
//	@BeforeMethod
//	public void setup() {
//		MockitoAnnotations.initMocks(this);
//		sessionMap.put("session", mockGSheets);
//		saveAs.setSessionMap(sessionMap);
//		saveAs.setAuthenticationService(authenticationService);
//	}
//
//	@Test
//	public void saveAs() {
//		try {
//			Mockito.doReturn(mockGDrive).when(saveAs).getDrive();
//			when(mockGSheets.getSpreadSheetId()).thenReturn("1");
//			when(mockGDrive.saveAs("1", "test123")).thenReturn("2");
//			when(mockGSheets.saveAs("1", "2", "test123")).thenReturn("2");
//			Assert.assertTrue(saveAs.execute("session", "test123").get());
//		} catch (IOException e) {
//			fail();
//		} catch (GeneralSecurityException m) {
//			fail();
//		}
//	}
//
//	@Test(expectedExceptions = BotCommandException.class)
//	public void botCommandExceptionTest() {
//		try {
//			Mockito.doReturn(mockGDrive).when(saveAs).getDrive();
//			when(mockGSheets.getSpreadSheetId()).thenReturn("1");
//			when(mockGDrive.saveAs("1", "test123")).thenThrow(IOException.class);
//
//			saveAs.execute("session", "test123");
//		} catch (GeneralSecurityException | IOException e) {
//			fail();
//		}
//	}
//
//	@Test
//	public void getSheets() {
//		try {
//			GDrive drive = saveAs.getDrive();
//			Assert.assertNotNull(drive);
//		} catch (GeneralSecurityException e) {
//			fail();
//		} catch (IOException e) {
//			fail();
//		}
//	}
//}
