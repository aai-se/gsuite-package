package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;

public class AutofitColumnsTest {

	@Spy
	AutofitColumns autoFitColumns;

	@Mock
	GSheets mockGSheets;

	final String session = "session";
	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		sessionMap.put(session, mockGSheets);
		autoFitColumns.setSessionMap(sessionMap);

	}

	@Test
	public void autoFitColumnTest() {
		when(mockGSheets.autofitColumns()).thenReturn(true);
		autoFitColumns.execute(session);
	}

	@Test(expectedExceptions = Exception.class)
	public void autoFitColumn_NullException() {
		when(mockGSheets.autofitColumns()).thenThrow(IOException.class);
		autoFitColumns.execute(null);
		Assert.fail();
	}

	@Test(expectedExceptions = Exception.class)
	public void autoFitColumn_invalidSession() {
		when(mockGSheets.autofitColumns()).thenThrow(IOException.class);
		autoFitColumns.execute("wrong_session");
		Assert.fail();
	}
}
