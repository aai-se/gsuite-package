/*
* Copyright (c) 2019 Automation Anywhere.
* All rights reserved.
*
* This software is the proprietary information of Automation Anywhere.
* You shall use it only in accordance with the terms of the license agreement
* you entered into with Automation Anywhere.
*
*/
package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.ACTIVECELL;
import static com.automationanywhere.botcommand.gsuite.Constants.SPECIFICCELL;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommands.gsuite.sheets.commands.OpenSheets;
import com.automationanywhere.botcommands.gsuite.sheets.commands.SetSingleCell;
import com.automationanywhere.core.security.SecureString;

public class SetSingleCellJunit extends AbstractAuthUtils {
	SetSingleCell setSingleCell;
	OpenSheets openSheets;

	final String session = "session";
	final String user = "saanchi@aademo.page";
	final String ClientID = "1045113049707-u3fhdtroech2uch19jogs52fg98t3lpu.apps.googleusercontent.com";
	final String redirectUrl = "http://localhost";
	final String clientSecreet = "6rjSOWzAEKEdiFqpCDGxirkk";

	final SecureString userName = new SecureString(user.toCharArray());
	final SecureString clientId = new SecureString(ClientID.toCharArray());
	final SecureString redirectURI = new SecureString(redirectUrl.toCharArray());
	final SecureString clientSecret = new SecureString(clientSecreet.toCharArray());

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {
		launchOAuthConsentScreen(userName, clientId, clientSecret, redirectURI);

		openSheets = new OpenSheets();
		openSheets.setSessionMap(sessionMap);
		openSheets.execute(userName, session, "BYNAME", "Demo", null, null, true, "Megha", false);
		setSingleCell = new SetSingleCell();
		setSingleCell.setSessionMap(sessionMap);
	}

	@Test
	public void setSingleCellTest() {
		setSingleCell.execute(session, ACTIVECELL, null, "Testing there");

		setSingleCell.execute(session, SPECIFICCELL, "K1", "Testing here");
		setSingleCell.execute(session, ACTIVECELL, null, "Testing there");
	}
}