/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.google.api.services.sheets.v4.model.ValueRange;

import org.json.JSONException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.mockito.Mockito.*;

public class GetNumberofRowsTest {
	@Spy
	GetNumberOfRows getNumberOfRows;

	@Mock
	AuthenticationService authenticationService;

	@Mock
	GSheets mockGSheets;

	final String session = "session";

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		sessionMap.put("session", mockGSheets);
		getNumberOfRows.setSessionMap(sessionMap);
	}

	@Test
	public void getNumberofRows_TOTALROWS_Test() {
		try {
			when(mockGSheets.getSpreadSheetId()).thenReturn("123456");
			when(mockGSheets.getNumberOfRows("123456", "TOTALROWS")).thenReturn(10);

			Value<?> numberOfRows = getNumberOfRows.execute(session, "TOTALROWS");
			Assert.assertEquals(new NumberValue(10).toString(), numberOfRows.toString());
			verify(mockGSheets, times(1)).getNumberOfRows(mockGSheets.getSpreadSheetId(), "TOTALROWS");

		} catch (Exception i) {

		}
	}

	@Test
	public void getNumberofRows_NONEMPTYROWS_Test() {
		try {
			when(mockGSheets.getSpreadSheetId()).thenReturn("123456");
			when(mockGSheets.getNumberOfRows("123456", "NONEMPTYROWS")).thenReturn(10);

			Value<?> numberOfRows = getNumberOfRows.execute(session, "NONEMPTYROWS");
			Assert.assertEquals(new NumberValue(10).toString(), numberOfRows.toString());
			verify(mockGSheets, times(1)).getNumberOfRows(mockGSheets.getSpreadSheetId(), "TOTALROWS");

		} catch (Exception i) {

		}
	}

	@Test
	public void getNumberofRows_EmptySheet_Test() {
		try {
			when(mockGSheets.getSpreadSheetId()).thenReturn("123456");
			when(mockGSheets.getNumberOfRows("123456", "NONEMPTYROWS")).thenReturn(0);
			Value<?> numberOfRows = getNumberOfRows.execute(session, "NONEMPTYROWS");
			Assert.assertEquals(new NumberValue(0).toString(), numberOfRows.toString());
			verify(mockGSheets, times(1)).getNumberOfRows(mockGSheets.getSpreadSheetId(), "TOTALROWS");

		} catch (Exception i) {

		}
	}

	@SuppressWarnings("unchecked")
	@Test(expectedExceptions = BotCommandException.class)
	public void getNumberofRows_IOEXCEPTION() {
		try {
			when(mockGSheets.getSpreadSheetId()).thenReturn("123456");
			when(mockGSheets.getNumberOfRows("123456", "TOTALROWS")).thenThrow(IOException.class);
			getNumberOfRows.execute(session, "TOTALROWS");
			verify(mockGSheets, times(1)).getNumberOfRows(mockGSheets.getSpreadSheetId(), "TOTALROWS");
			Assert.fail();
		} catch (IOException e) {
			Assert.assertTrue(true);
		}
	}

}