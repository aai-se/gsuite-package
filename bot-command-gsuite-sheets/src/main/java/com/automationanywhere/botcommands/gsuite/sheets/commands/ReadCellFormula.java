/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.*;
import static com.automationanywhere.botcommands.gsuite.sheets.utils.Utils.getException;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Idx.Option;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.MatchesRegex;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.sheets.v4.model.ValueRange;

@BotCommand
@CommandPkg(label = "Read cell formula", name = "readCellFormula", description = "Returns formula (if present) for a specified cell", node_label = "from {{cellOption}} |{{cellAddress}} |and assign value to {{returnTo}} |in session {{sessionName}}", return_type = DataType.STRING, return_required = true, return_label = "Assign output to a variable", return_description = "Returns blank if no formula is present", icon = "Excel_blue.svg")
public class ReadCellFormula {
	private static final Logger LOGGER = LogManager.getLogger(InsertRange.class);

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public Value<String> execute(
			@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String sessionName,
			@Idx(index = "2", type = AttributeType.RADIO, options = {
					@Option(index = "2.1", pkg = @Pkg(label = LABEL_ACTIVECELL, value = ACTIVECELL)),
					@Option(index = "2.2", pkg = @Pkg(label = LABEL_SPECIFICCELL, value = SPECIFICCELL)) }) @Pkg(label = "Cell options", default_value_type = DataType.STRING, default_value = TRUE) @NotEmpty String cellOption,
			@Idx(index = "2.2.1", type = AttributeType.TEXT) @Pkg(label = "Cell name", description = "e.g. A1") @MatchesRegex(cellAddressValidator) @VariableType(value = DataType.STRING) @NotEmpty String cellAddress) {

		try {
			LOGGER.info("Performing Read cell formula Command.. \n");
			GSheets gSheets = (GSheets) sessionMap.get(sessionName);
			ExecutorValidator.getInstance().validate(gSheets);
			String sheetName = gSheets.getActiveSheetInfo().getSheetName();
			ValueRange cellValueRange;
			String cellValue = EMPTY;
			if (ACTIVECELL.equals(cellOption))
				cellValueRange = gSheets.getValue(gSheets.getSpreadSheetId(), sheetName,
						gSheets.getActiveCell(sheetName), FORMULA);
			else
				cellValueRange = gSheets.getValue(gSheets.getSpreadSheetId(), sheetName, cellAddress, FORMULA);
			if (cellValueRange.getValues() != null) {
				cellValue = (String) cellValueRange.getValues().get(0).get(0);
			}
			if (!cellValue.startsWith(EQUALS))
				cellValue = EMPTY;
			return new StringValue(cellValue);
		} catch (IOException exception) {
			LOGGER.error("Exception occurred during execution of Read cell formula .....", exception);
//			String msg = getException((GoogleJsonResponseException) exception);
			throw new BotCommandException(exception);
		}

	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}
