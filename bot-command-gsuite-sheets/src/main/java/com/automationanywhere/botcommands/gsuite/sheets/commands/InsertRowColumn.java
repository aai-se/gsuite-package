/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */

package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.ACTIVECELL;
import static com.automationanywhere.botcommand.gsuite.Constants.COLUMNS;
import static com.automationanywhere.botcommand.gsuite.Constants.DEFAULT_VALUE_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.DESCRIPTION_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.EMPTY;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_ACTIVECELL;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.RANGE;
import static com.automationanywhere.botcommand.gsuite.Constants.ROWS;
import static com.automationanywhere.botcommand.gsuite.Constants.SPECIFICCELL;
import static com.automationanywhere.botcommand.gsuite.Constants.columnIndexValidator;
import static com.automationanywhere.botcommand.gsuite.Constants.columnRangeValidator;
import static com.automationanywhere.botcommand.gsuite.Constants.rowIndexValidator;
import static com.automationanywhere.botcommand.gsuite.Constants.rowRangeValidator;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.utils.Utils;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Idx.Option;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.MatchesRegex;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

@BotCommand
@CommandPkg(name = "InsertRowColumn", label = "Insert Row/Column", node_label = ": {{insertRowOption}} |{{insertColumnOption}} |{{rowIndex}} |{{columnIndex}} |{{insertRowByOption}} |{{insertColumnByOption}} |{{rowRange}} |{{columnRange}} |into current worksheet |in session {{sessionName}}", description = "Inserts row or column in worksheet")
public class InsertRowColumn {
	private static final Logger LOGGER = LogManager.getLogger(InsertRowColumn.class);
	private static final String ROWOPERATION = "ROWOPERATION";
	private static final String COLUMNOPERATION = "COLUMNOPERATION";
	private static final String INSERTROWAT = "INSERTROWAT";
	private static final String INSERTROWBY = "INSERTROWBY";
	private static final String INSERTCOLUMNAT = "INSERTCOLUMNAT";
	private static final String INSERTCOLUMNBY = "INSERTCOLUMNBY";

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public void execute(
			@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String sessionName,
			@Idx(index = "2", type = AttributeType.RADIO, options = {
					@Option(index = "2.1", pkg = @Pkg(label = "Row Operations", value = ROWOPERATION)),
					@Option(index = "2.2", pkg = @Pkg(label = "Column Operations", value = COLUMNOPERATION)) }) @Pkg(label = "", default_value_type = DataType.STRING, default_value = ROWOPERATION) @NotEmpty String operationTypeOption,
			@Idx(index = "2.1.1", type = AttributeType.RADIO, options = {
					@Option(index = "2.1.1.1", pkg = @Pkg(label = "Insert Row(s) at", value = INSERTROWAT)),
					@Option(index = "2.1.1.2", pkg = @Pkg(label = "Insert row by", value = INSERTROWBY)) }) @Pkg(label = "", default_value_type = DataType.STRING, default_value = INSERTROWAT) @NotEmpty String insertRowOption,
			@Idx(index = "2.1.1.1.1", type = AttributeType.TEXT) @Pkg(label = "", description = "Enter the row index") @MatchesRegex(rowIndexValidator) @VariableType(value = DataType.STRING) @NotEmpty String rowIndex,
			@Idx(index = "2.1.1.2.1", type = AttributeType.RADIO, options = {
					@Option(index = "2.1.1.2.1.1", pkg = @Pkg(label = LABEL_ACTIVECELL, value = ACTIVECELL)),
					@Option(index = "2.1.1.2.1.2", pkg = @Pkg(label = "Range", value = RANGE)) }) @Pkg(label = "", default_value_type = DataType.STRING, default_value = ACTIVECELL) @NotEmpty String insertRowByOption,
			@Idx(index = "2.1.1.2.1.2.1", type = AttributeType.TEXT) @Pkg(label = "", description = "For single row e.g. 10 and for multiple rows e.g. 1:10") @MatchesRegex(rowRangeValidator) @VariableType(value = DataType.STRING) @NotEmpty String rowRange,

			@Idx(index = "2.2.1", type = AttributeType.RADIO, options = {
					@Option(index = "2.2.1.1", pkg = @Pkg(label = "Insert Column(s) at", value = INSERTCOLUMNAT)),
					@Option(index = "2.2.1.2", pkg = @Pkg(label = "Insert column by", value = INSERTCOLUMNBY)) }) @Pkg(label = "", default_value_type = DataType.STRING, default_value = INSERTCOLUMNAT) @NotEmpty String insertColumnOption,
			@Idx(index = "2.2.1.1.1", type = AttributeType.TEXT) @Pkg(label = "", description = "Enter the column address") @MatchesRegex(columnIndexValidator) @VariableType(value = DataType.STRING) @NotEmpty String columnIndex,
			@Idx(index = "2.2.1.2.1", type = AttributeType.RADIO, options = {
					@Option(index = "2.2.1.2.1.1", pkg = @Pkg(label = LABEL_ACTIVECELL, value = ACTIVECELL)),
					@Option(index = "2.2.1.2.1.2", pkg = @Pkg(label = "Range", value = RANGE)) }) @Pkg(label = "", default_value_type = DataType.STRING, default_value = ACTIVECELL) @NotEmpty String insertColumnByOption,
			@Idx(index = "2.2.1.2.1.2.1", type = AttributeType.TEXT) @Pkg(label = "", description = "For single column e.g. B and for multiple columns e.g. B:D") @MatchesRegex(columnRangeValidator) @VariableType(value = DataType.STRING) @NotEmpty String columnRange) {
		try {
			GSheets gSheets = (GSheets) sessionMap.get(sessionName);
			ExecutorValidator.getInstance().validate(gSheets);
			if (ROWOPERATION.equals(operationTypeOption)) {
				if (INSERTROWAT.equals(insertRowOption)) {
					gSheets.insertRowColumn(gSheets.getSpreadSheetId(), ROWS, SPECIFICCELL, rowIndex);
				} else {
					if (ACTIVECELL.equals(insertRowByOption)) {
						gSheets.insertRowColumn(gSheets.getSpreadSheetId(), ROWS, ACTIVECELL, EMPTY);
					} else {
						gSheets.insertRowColumn(gSheets.getSpreadSheetId(), ROWS, RANGE, rowRange);
					}
				}
				
			} else {
				if (INSERTCOLUMNAT.equals(insertColumnOption)) {
					gSheets.insertRowColumn(gSheets.getSpreadSheetId(), COLUMNS, SPECIFICCELL, String.valueOf(Utils.getColIndex(columnIndex)));
				} else {
					if (ACTIVECELL.equals(insertColumnByOption)) {
						gSheets.insertRowColumn(gSheets.getSpreadSheetId(), COLUMNS, ACTIVECELL, EMPTY);
					} else {
						gSheets.insertRowColumn(gSheets.getSpreadSheetId(), COLUMNS, RANGE, columnRange);
					}
				}
			}

		} catch (IOException e) {
            LOGGER.error("exception in inserting row/ column: ", e);
            String msg = e.getMessage();
            throw new BotCommandException(msg, e);
        }catch (Exception ex) {
			throw new BotCommandException(ex.getMessage(), ex);
		}
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}