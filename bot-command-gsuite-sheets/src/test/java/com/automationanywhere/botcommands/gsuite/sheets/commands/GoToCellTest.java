/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.junit.Assert;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.api.impl.GDriveImpl;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.core.security.SecureString;

public class GoToCellTest {
	private static final Logger LOGGER = LogManager.getLogger(ActiveSheetTest.class);

	@Spy
	GoToCell goToCell;

	@Spy
	AuthenticationService authenticationService;

	@Spy
	GSheets gsheet;

	@Mock
	GSheets mockGSheets;

	@Mock
	GDriveImpl mockGDriveImpl;

	@Mock
	OpenSheets openSheets;

	final String session = "session";
	final String sheetName = "sheetname";
	final String user = "saanchi@aademo.page";
	final SecureString userName = new SecureString(user.toCharArray());
	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {

		MockitoAnnotations.initMocks(this);
		sessionMap.put(session, mockGSheets);
		goToCell.setSessionMap(sessionMap);

	}

	@Test
	public void goToCell() {
		try {
			when(mockGSheets.goToCell("specificCell", "A10")).thenReturn(true);
			Boolean response = goToCell.execute(session, "specificCell", "A10").get();
			assertTrue(response);
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	@Test(expectedExceptions = BotCommandException.class)
	public void goToCellIOException() throws IOException, JSONException {
			when(mockGSheets.goToCell("OneCellLeft", "A1")).thenThrow(new IOException());
			Boolean response = goToCell.execute(session, "OneCellLeft", "A1").get();
			Assert.fail();
	}

}
