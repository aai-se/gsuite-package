/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommand.gsuite.service;

import java.io.IOException;

import com.google.api.client.auth.oauth2.Credential;

public interface AuthenticationService {

	/**
	 * Returns the authDto (used for encyption)
	 * 
	 * @return
	 */
	public AuthDto getAuthDto(String userName) throws IOException;

	/**
	 * Triggers the OAuth flow with the provided client credentials
	 * 
	 * @param clientId
	 * @param clientSecret
	 * @param redirectURI
	 * @return
	 * @throws IOException
	 */
	public Credential getGoogleCredential(String userEmailAddress, String clientId, String clientSecret,
			String redirectURI) throws IOException;

	/**
	 * Returns Credential if found
	 * 
	 * @return
	 */
	Credential getCredential();

}
