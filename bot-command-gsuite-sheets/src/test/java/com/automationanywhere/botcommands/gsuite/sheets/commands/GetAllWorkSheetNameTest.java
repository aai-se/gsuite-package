/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

public class GetAllWorkSheetNameTest {
	@Spy
	GetAllWorkSheetsName getAllWorkSheetsName;


	@Mock
	GSheets mockGSheets;

	final String session = "session";
	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		sessionMap.put(session, mockGSheets);
		getAllWorkSheetsName.setSessionMap(sessionMap);

	}
	@Test
	public void getWorksheetName_test() throws IOException { 
		ListValue<StringValue[]> value = new ListValue<StringValue[]>();
		when((ListValue<StringValue[]>)mockGSheets.getAllWorkSheet()).thenReturn(value);
		assertEquals(value, getAllWorkSheetsName.execute(session));
		
	}
	
	@Test(expectedExceptions = BotCommandException.class)
	public void getWorksheetNameSession() {
		getAllWorkSheetsName.execute("default");
		
	}
}