/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommand.gsuite.api.impl;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.gsuite.Constants;
import com.automationanywhere.botcommand.gsuite.api.GCalendar;
import com.automationanywhere.botcommand.gsuite.dto.calendar.EventDTO;
import com.automationanywhere.botcommand.gsuite.service.AESEncrypter;
import com.automationanywhere.botcommand.gsuite.service.AuthDto;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.CalendarList;
import com.google.api.services.calendar.model.CalendarListEntry;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttendee;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.common.annotations.VisibleForTesting;

public class GCalendarImpl implements GCalendar {
	private static final Logger LOGGER = LogManager.getLogger(GCalendarImpl.class);

	private AuthenticationService authService;
	private Calendar calService;
	private final HttpTransport httpTransport;
	private EventDTO eventDTO;
	private String primaryCalId;
	private String userName;

	// setter userName
	public void setUserName(String userName) {
		this.userName = userName;
	}

	@VisibleForTesting
	public GCalendarImpl() throws GeneralSecurityException, IOException {
		httpTransport = GoogleNetHttpTransport.newTrustedTransport();
	}

	public GCalendarImpl(AuthenticationService authService) throws GeneralSecurityException, IOException {
		this.authService = authService;
		httpTransport = GoogleNetHttpTransport.newTrustedTransport();
	}

	void setupAuthorization() throws IOException {
		LOGGER.info("SETUP Authorization called");
		AuthDto authDto = authService.getAuthDto(userName);
		Credential credential = authService.getCredential();
		if (credential == null) {
			credential = authService.getGoogleCredential(userName, AESEncrypter.decrypt(authDto.getClientId()),
					AESEncrypter.decrypt(authDto.getClientSecret()), AESEncrypter.decrypt(authDto.getRedirectURL()));
			LOGGER.debug("Able to fetch credentials1");
		}

		calService = getCalendarService(credential);
		LOGGER.info(calService.toString());
	}

	Calendar getCalendarService(Credential credential) {
		return new Calendar.Builder(httpTransport, JacksonFactory.getDefaultInstance(), credential)
				.setApplicationName(Constants.APPLICATION_NAME).build();
	}

	@Override
	public EventDTO createEvent(EventDTO eventDTO) throws IOException {

		setupAuthorization();

		// gGet Primary Calendar for the user:
		primaryCalId = getPrimaryCalendar();
		Event event = convertToEvent(eventDTO);
		Calendar.Events calEvents = calService.events();
		Calendar.Events.Insert calInsert = calEvents.insert(primaryCalId, event);
		calInsert = calInsert.setFields("id");

		Event calEvent = calInsert.execute();
		LOGGER.info("Event created == " + calEvent.toString());
//        eventDTO.setUrl(calEvent.getSource().getUrl());
		eventDTO.setId(calEvent.getId());

		LOGGER.trace("Event created == " + eventDTO.toString());
		this.eventDTO = eventDTO;

		return eventDTO;
	}

	@Override
	public boolean deleteEvent(EventDTO eventDTO) throws IOException {

		setupAuthorization();

		// gGet Primary Calendar for the user:
		try {
			primaryCalId = getPrimaryCalendar();

			Calendar.Events calEvents = calService.events();
			Calendar.Events.Delete calDel = calEvents.delete(primaryCalId, eventDTO.getId());
			calDel.execute();
		} catch (IOException ex) {
			LOGGER.error("Failed to delete event : " + eventDTO.getId());
			return false;
		}
		LOGGER.info("Successfully deleted event.");
		return true;
	}

	@Override
	public String getEventId() {
		if (eventDTO == null) {
			LOGGER.error("No event has been initialized. Returning null");
			return null;
		} else {
			return eventDTO.getId();
		}
	}

	Event convertToEvent(EventDTO eventDTO) {
		Event event = new Event();
		event.setSummary(eventDTO.getSummary());
		event.setLocation(eventDTO.getLocation());
		event.setDescription(eventDTO.getDescription());

		if (eventDTO.getRecurrence() != null)
			event.setRecurrence(Arrays.asList(eventDTO.getRecurrence()));

		if (eventDTO.getAttendees() != null) {
			List<EventAttendee> eventAttendees = new ArrayList<>(eventDTO.getAttendees().length);
			for (int i = 0; i < eventDTO.getAttendees().length; i++) {
				EventAttendee attendee = new EventAttendee();
				attendee.setEmail(eventDTO.getAttendees()[i]);
				eventAttendees.add(attendee);
			}
			event.setAttendees(eventAttendees);
		}
		event.setVisibility(eventDTO.getVisibility().getName());
		EventDateTime start = new EventDateTime();
		if (eventDTO.getStart().getDate() != null)
			start.setDate(eventDTO.getStart().getDate());
		else
			start.setDate(null);

		if (eventDTO.getStart().getDateTime() != null)
			start.setDateTime(eventDTO.getStart().getDateTime());
		else
			start.setDateTime(null);

		if (eventDTO.getStart().getTimeZone() != null)
			start.setTimeZone(eventDTO.getStart().getTimeZone());
		else {
			start.setTimeZone(TimeZone.getDefault().getID());
		}
		event.setStart(start);

		EventDateTime end = new EventDateTime();
		if (eventDTO.getEnd().getDate() != null)
			end.setDate(eventDTO.getEnd().getDate());
		else
			end.setDate(null);

		if (eventDTO.getEnd().getDateTime() != null)
			end.setDateTime(eventDTO.getEnd().getDateTime());
		else
			end.setDateTime(null);

		if (eventDTO.getEnd().getTimeZone() != null)
			end.setTimeZone(eventDTO.getEnd().getTimeZone());
		else {
			end.setTimeZone(TimeZone.getDefault().getID());
		}

		event.setEnd(end);

		LOGGER.trace("Event data == " + event.toString());
		return event;
	}

	String getPrimaryCalendar() throws IOException {
		String primaryCalId = null;
		Calendar.CalendarList calendarList1 = calService.calendarList();
		Calendar.CalendarList.List calendarList2 = calendarList1.list();
		calendarList2 = calendarList2.setFields("items(id,primary)");
		CalendarList calendarList = calendarList2.execute();
		
		//CalendarList calendarList = calService.calendarList().list().setFields("items(id,primary)").execute(); 
		
		
		for (CalendarListEntry calendarListEntry : calendarList.getItems()) {
			if (calendarListEntry.isPrimary()) {
				primaryCalId = calendarListEntry.getId();
				this.primaryCalId = primaryCalId;
			}
		}
		return primaryCalId;
	}

	@VisibleForTesting
	void setAuthService(AuthenticationService authService) {
		this.authService = authService;
	}

	@VisibleForTesting
	void setCalendar(Calendar calendar) {
		this.calService = calendar;
	}

	@VisibleForTesting
	void setEventDTO(EventDTO eventDTO) {
		this.eventDTO = eventDTO;
	}
}
