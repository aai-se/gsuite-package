/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
package com.automationanywhere.botcommands.gsuite.authorization.utilities;

import java.util.HashMap;
import java.util.Map;

import com.automationanywhere.botcommands.gsuite.authorization.commands.Connect;
import com.automationanywhere.botcommands.gsuite.authorization.commands.Disconnect;

public class AuthTestSetUp {

	protected static Map<String, Object> authSessionMap = new HashMap<String, Object>();
	protected static String notExsisteduserRefrence = "Rais";
	protected static Connect connect;
	protected static Disconnect disConnect;

	public static void doInitialSetup() {
		if (connect == null) {
			connect = new Connect();
		}
		connect.setSessionMap(authSessionMap);
		if (disConnect == null) {
			disConnect = new Disconnect();
		}
		disConnect.setSessionMap(authSessionMap);
	}
}