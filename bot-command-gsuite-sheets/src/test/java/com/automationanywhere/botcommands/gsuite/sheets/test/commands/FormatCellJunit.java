/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommands.gsuite.sheets.commands.FormatCell;
import com.automationanywhere.botcommands.gsuite.sheets.commands.OpenSheets;
import com.automationanywhere.botcommands.gsuite.sheets.test.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

public class FormatCellJunit extends AbstractAuthUtils {

	FormatCell formatCell;

	OpenSheets openSheets;

	public final static SecureString USERNAME = ConstantsTest.getUserName();
	final String session = "session";
	final String newSession = "newsession";
	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {
		launchOAuthConsentScreen(ConstantsTest.getUserName(), ConstantsTest.getClientid(),
				ConstantsTest.getClientSecret(), ConstantsTest.getRedirectUri());

		openSheets = new OpenSheets();
		openSheets.setSessionMap(sessionMap);
		openSheets.execute(ConstantsTest.getUserName(), session, "BYNAME", "Demo", null, null, true, "ArpitTest",
				false);
		formatCell = new FormatCell();
		formatCell.setSessionMap(sessionMap);

	}

	@Test
	public void formateCellTest() {

		// formatCell.execute(sessionName, cellOption, cellAddress, cellRange,
		//isFont,
		//   fontName, fontSize, isBold, isItalic, isUnderline, isStrikethrough,fontColor,
		//isAlignment, 
		//   verticalAlignment, horizontalAlignment, 
		//isWrapText,
		//   wrapText, 
		//isMerge, 
		 //    mergeType);
		formatCell.execute(session, "MULTIPLE", "A1", "A1:A7", 
				true, "Calibri", 25, true,true,true,true, "brown",
				false, "MIDDLE", "CENTER", 
				false, "OVERFLOW_CELL", 
				true, "MERGE_ALL");
	}

}
