/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;

import java.util.Map;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.DataType;

@BotCommand
@CommandPkg(label = "Close", name = "CloseSpreadsheet", description = "Closes the current spreadsheet", node_label = "", icon = "excel.svg")
public class CloseSheet {

	private static final String SESSION_EXAMPLE = "e.g. Session1 or S1";

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public BooleanValue execute(

			@Idx(index = "1", type = TEXT) @Pkg(label = "Session name", description = SESSION_EXAMPLE, default_value_type = DataType.STRING, default_value = "Default") @NotEmpty String session) {

		try {
			GSheets gSheets = (GSheets) sessionMap.get(session);
			ExecutorValidator.getInstance().validate(gSheets);
			return new BooleanValue(gSheets.closeSpreadSheet().toString());
		} catch (Exception ex) {
			throw new BotCommandException(ex.getMessage(), ex);
		} 

	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}
