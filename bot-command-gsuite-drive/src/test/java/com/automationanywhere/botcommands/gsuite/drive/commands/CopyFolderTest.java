/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommands.gsuite.drive.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

/**
 * Tests copy Google Drive file functionality.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

public class CopyFolderTest {

	public static final SecureString USERNAME = ConstantsTest.getUserName();

	@Spy
	CopyFolder copyFolder;

	@Mock
	GDrive mockGDrive;

	@Mock
	AuthenticationService mockAuthService;

//
	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		copyFolder.setAuthenticationService(mockAuthService);
	}

	@Test
	public void copyFolder_returnsCopiedFolderId_whenRequiredFolderNameProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(copyFolder).getDrive(USERNAME);

			String sourceFolderId = "trial_file123";
			String sourceFolderName = "PKTest";
			JSONObject inputFolderParamName = new JSONObject();
			inputFolderParamName.put("fileName", sourceFolderName);

			String destinationFolderId = "trial_file123";
			String destinationFolderName = "PK";
			JSONObject outputFolderParamName = new JSONObject();
			outputFolderParamName.put("fileName", destinationFolderName);

			when(mockGDrive.copyFolder(inputFolderParamName.toString(), outputFolderParamName.toString()))
					.thenReturn(sourceFolderId);
			Value<?> testId = copyFolder.execute(USERNAME, "BYNAME", sourceFolderName, sourceFolderId, "BYNAME",
					destinationFolderName, destinationFolderId);
			Assert.assertEquals(sourceFolderId, testId.toString());
		} catch (JSONException | IOException | GeneralSecurityException e) {
			Assert.fail();
		}
	}

	@Test
	public void copyFolder_returnsCopiedFolderId_whenRequiredFolderIdProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(copyFolder).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			JSONObject inputFolderParamID = new JSONObject();
			inputFolderParamID.put("fileId", fileId);

			String folderId = "trial_file123";
			String folderName = "trial_file.txt";
			JSONObject outputFolderParamID = new JSONObject();
			outputFolderParamID.put("fileId", folderId);

			when(mockGDrive.copyFolder(inputFolderParamID.toString(), outputFolderParamID.toString()))
					.thenReturn(fileId);
			Value<String> testId = copyFolder.execute(USERNAME, "BYID", fileName, fileId, "BYID", folderName, folderId);
			Assert.assertEquals(fileId, testId.toString());
		} catch (JSONException | IOException | GeneralSecurityException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void copyFolder_throwsBotCommandException_whenCopyFolderAPIthrowsIOException() {
		try {
			Mockito.doReturn(mockGDrive).when(copyFolder).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			String folderId = "trial_file123";
			String folderName = "trial_file.txt";
			JSONObject outputFileParamName = new JSONObject();
			outputFileParamName.put("fileName", folderName);

			when(mockGDrive.copyFolder(inputFileParamName.toString(), outputFileParamName.toString()))
					.thenThrow(IOException.class);
			copyFolder.execute(USERNAME, "BYNAME", fileName, fileId, "BYNAME", folderName, folderId);
			Assert.fail();
		} catch (JSONException | GeneralSecurityException e) {
			Assert.fail();
		} catch (IOException e) {
			Assert.assertTrue(true);
		}
	}

	@Test
	public void getDrive_newGDriveImplObjectIsNotNull() {
		try {
			GDrive drive = copyFolder.getDrive(USERNAME);
			assertNotNull(drive);
		} catch (GeneralSecurityException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
	}
}
