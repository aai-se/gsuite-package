/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.MIME_TYPE_DOCUMENT;
import static com.automationanywhere.botcommand.gsuite.Constants.MIME_TYPE_FOLDER;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.api.impl.GDriveImpl;
import com.automationanywhere.botcommand.gsuite.api.impl.GSheetsImpl;
import com.automationanywhere.botcommand.gsuite.dto.sheets.SpreadSheetDto;
import com.automationanywhere.botcommand.gsuite.enums.FileType;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.core.security.SecureString;
import com.google.api.services.drive.model.File;

public class CreateWorkBookTest {

	@Spy
	CreateWorkbook createWorkbook;

	@Mock
	AuthenticationService authenticationService;

	@Mock
	GSheets mockGSheets;

	@Mock
	GDrive mockGDrive;

	@Mock
	GSheetsImpl mockGSheetsImpl;

	final String session = "session";
	Map<String, Object> sessionMap = new HashMap<>();
	final String user = "saanchi@aademo.page";
	final SecureString userName = new SecureString(user.toCharArray());
	Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.messages.messages");

	@BeforeMethod
	public void setup() {

		MockitoAnnotations.initMocks(this);
		sessionMap.put(session, mockGSheets);
		createWorkbook.setSessionMap(sessionMap);
		createWorkbook.setAuthenticationService(authenticationService);
	}

	@Test
	public void createSpreadSheet() {
		String title = "test";
		try {
			String sheetId = "12345";

			Mockito.doReturn(mockGSheets).when(createWorkbook).getSheets(session, userName);
			Mockito.doReturn(mockGDrive).when(createWorkbook).getDrive(userName);

			File file = new File();
			file.setId("123abc");
			file.setMimeType(MIME_TYPE_FOLDER);
			file.setName("Dummy");

			List<File> listFile = new ArrayList<>();
			listFile.add(file);
			when(mockGDrive.parseFilePath(eq("/Dummy"), eq("create"))).thenReturn(listFile);
			when(mockGDrive.createFile(eq("test"), eq("123abc"), eq(FileType.EXCEL))).thenReturn(sheetId);

			when(mockGSheets.getSpreadSheetId()).thenReturn(sheetId);
			Value<String> testId = createWorkbook.execute(userName, "session", title, "true", "/Dummy", null);
			Assert.assertEquals(sheetId, testId.get());

			// Invoked 1 time
			// verify(mockGDrive, times(1)).parseFilePath(eq("/Dummy"), eq("create"));
			// verify(mockGDrive, times(1)).createFile(eq("test"), eq("123abc"),
			// eq(FileType.EXCEL));
			// verify(mockGSheets, times(1)).getSpreadSheet(any(SpreadSheetDto.class));

		} catch (GeneralSecurityException | JSONException | IOException e) {
			Assert.fail();
		}
	}
	@Test
	public void createSpreadSheetByVariable() {
		String title = "test";
		try {
			String sheetId = "12345";

			Mockito.doReturn(mockGSheets).when(createWorkbook).getSheets(session, userName);
			Mockito.doReturn(mockGDrive).when(createWorkbook).getDrive(userName);

			File file = new File();
			file.setId("123abc");
			file.setMimeType(MIME_TYPE_FOLDER);
			file.setName("Dummy");

			List<File> listFile = new ArrayList<>();
			listFile.add(file);
			when(mockGDrive.parseFilePath(eq("/Dummy"), eq("create"))).thenReturn(listFile);
			when(mockGDrive.createFile(eq("test"), eq("123abc"), eq(FileType.EXCEL))).thenReturn(sheetId);

			when(mockGSheets.getSpreadSheetId()).thenReturn(sheetId);
			Value<String> testIdnew = createWorkbook.execute(userName, "session", title, "false", null, "/Dummy");
			Assert.assertEquals(sheetId, testIdnew.get());

			// Invoked 1 time
			// verify(mockGDrive, times(1)).parseFilePath(eq("/Dummy"), eq("create"));
			// verify(mockGDrive, times(1)).createFile(eq("test"), eq("123abc"),
			// eq(FileType.EXCEL));
			// verify(mockGSheets, times(1)).getSpreadSheet(any(SpreadSheetDto.class));

		} catch (GeneralSecurityException | JSONException | IOException e) {
			Assert.fail();
		}
	}


	@Test
	public void createSpreadSheetAtRoot() {
		String title = "test";
		try {
			String sheetId = "12345";

			Mockito.doReturn(mockGSheets).when(createWorkbook).getSheets(session, userName);
			Mockito.doReturn(mockGDrive).when(createWorkbook).getDrive(userName);
			when(mockGDrive.createFile(eq("test"), eq("root"), eq(FileType.EXCEL))).thenReturn(sheetId);

			Mockito.doReturn(true).when(createWorkbook).checkAndCreate(anyString(), anyObject(), anyObject(),
					anyString());

			when(mockGSheets.getSpreadSheetId()).thenReturn(sheetId);
			Value<String> testId = createWorkbook.execute(userName, session, "title", "true", "", null);
			Assert.assertEquals(sheetId, testId.get());
			// Invoked 0 times
			// verify(mockGDrive, times(0)).parseFilePath(eq(""), eq("create"));

			// Invoked 1 time
			// verify(mockGDrive, times(1)).createFile(eq("test"), eq("root"),
			// eq(FileType.EXCEL));
		} catch (GeneralSecurityException | JSONException | IOException e) {
			Assert.fail();
		}
	}

	@Test
	public void createDuplicateSpreadSheetThrowsException() {
		String title = "test";
		try {
			String sheetId = "12345";

			Mockito.doReturn(mockGSheets).when(createWorkbook).getSheets(session, userName);
			Mockito.doReturn(mockGDrive).when(createWorkbook).getDrive(userName);
			File file = new File();
			file.setId("123abc");
			file.setMimeType(MIME_TYPE_FOLDER);
			file.setName("Dummy");

			List<File> listFile = new ArrayList<>();
			listFile.add(file);
			when(mockGDrive.parseFilePath(eq("/Dummy"), eq("create"))).thenReturn(listFile);
			when(mockGDrive.createFile(eq("test"), eq("123abc"), eq(FileType.EXCEL)))
					.thenThrow(new BotCommandException(MESSAGES.getString("create.duplicate.filename", title)));

			when(mockGSheets.getSpreadSheetId()).thenReturn(sheetId);
			Value<String> testId = createWorkbook.execute(userName, session, title, "true", "/Dummy", null);
			Assert.fail();
		} catch (BotCommandException | JSONException e) {
			Assert.assertTrue(e.getMessage().startsWith("Unable to create Google Sheets with name"));
			try {
				verify(mockGDrive, times(1)).parseFilePath(eq("/Dummy"), eq("create"));

				verify(mockGDrive, times(1)).createFile(eq("test"), eq("123abc"), eq(FileType.EXCEL));
			} catch (IOException | JSONException ex) {
				Assert.fail();
			}

		} catch (GeneralSecurityException | IOException e) {
			Assert.fail();
		}
	}

	@Test
	public void createSpreadSheetAtWrongPath() {
		String title = "test";
		try {
			String sheetId = "12345";

			Mockito.doReturn(mockGSheets).when(createWorkbook).getSheets(session, userName);
			Mockito.doReturn(mockGDrive).when(createWorkbook).getDrive(userName);
			File file = new File();
			file.setId("123abc");
			file.setMimeType(MIME_TYPE_DOCUMENT);
			file.setName("DummyDocument");

			List<File> listFile = new ArrayList<>();
			listFile.add(file);
			when(mockGDrive.parseFilePath(eq("/Dummy"), eq("create"))).thenReturn(listFile);
			when(mockGDrive.createFile(eq("test"), eq("123abc"), eq(FileType.EXCEL))).thenReturn(sheetId);

			when(mockGSheets.getSpreadSheetId()).thenReturn(sheetId);
			Value<String> testId = createWorkbook.execute(userName, session, title, "true", "/Dummy", null);
			Assert.assertEquals(sheetId, testId.get());

			// Invoked 1 time
			verify(mockGDrive, times(1)).parseFilePath(eq("/Dummy"), eq("create"));

			// Invoked 0 time
			verify(mockGDrive, times(0)).createFile(eq("test"), eq("123abc"), eq(FileType.EXCEL));
			verify(mockGSheets, times(0)).getSpreadSheetId();

		} catch (BotCommandException | JSONException e) {
			Assert.assertTrue(e.getMessage().startsWith("Sorry, we couldn't find"));
		} catch (GeneralSecurityException e) {
			Assert.fail();
		} catch (IOException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void executeTest_receivingIOException_expectingBotCommandException() {
		try {
			SpreadSheetDto spreadSheetDto = new SpreadSheetDto();
			spreadSheetDto.setSession("session");
			spreadSheetDto.setTitle("name");
			Mockito.doReturn(mockGSheets).when(createWorkbook).getSheets(session, userName);
			Mockito.doReturn(mockGDrive).when(createWorkbook).getDrive(userName);

			when(mockGDrive.parseFilePath(anyString(), anyString())).thenThrow(IOException.class);
			createWorkbook.execute(userName, session, "name", "true", "/Dummy/new", null);

			Assert.fail();
		} catch (GeneralSecurityException | JSONException i) {
			Assert.fail();
		} catch (IOException e) {
			Assert.assertTrue(true);
		}
	}

	@Test
	public void getSheetsTest_checkNotNull() {
		try {
			GSheets tester = createWorkbook.getSheets(session, userName);
			Assert.assertNotNull(tester);
		} catch (GeneralSecurityException | IOException i) {

		}
	}

	@Test
	public void getSheetsTest_checkNull() {
		try {
			sessionMap.put(null, null);
			GSheets tester = createWorkbook.getSheets("sessions", userName);
			Assert.assertNotNull(tester);
		} catch (GeneralSecurityException | IOException i) {

		}
	}

	@Test
	public void getDriveTest_checkNotNull() {
		try {
			GDrive tester = createWorkbook.getDrive(userName);
			Assert.assertNotNull(tester);
		} catch (GeneralSecurityException | IOException i) {

		}
	}

	@Test
	public void getAuthenticateTest() {
		try {
			createWorkbook.setAuthenticationService(null);
			AuthenticationService authentication = createWorkbook.getAuthenticationService();
			Assert.assertNotNull(authentication);
		} catch (Exception i) {

		}
	}


	@Test(expectedExceptions = BotCommandException.class)
	public void checkCreate() {
		String title = "test";
		try {
			String sheetId = "12345";

			Mockito.doReturn(mockGSheets).when(createWorkbook).getSheets(session, userName);
			Mockito.doReturn(mockGDrive).when(createWorkbook).getDrive(userName);

			File file = new File();
			file.setId("123abc");
			file.setMimeType(MIME_TYPE_FOLDER);
			file.setName("Dummy");

			List<File> listFile = new ArrayList<>();
			listFile.add(file);
			when(mockGDrive.parseFilePath(eq("/Dummy"), eq("create"))).thenReturn(listFile);
			when(mockGDrive.createFile(eq("test"), eq("123abc"), eq(FileType.EXCEL))).thenReturn(sheetId);
			when(mockGSheets.getSpreadSheetId()).thenReturn(sheetId);
			when(mockGDrive.fileExists(anyString(), anyString())).thenReturn(true);
			Value<String> testId = createWorkbook.execute(userName, "session", title, "true", "/Dummy", null);
			
		} catch (GeneralSecurityException | JSONException  e) {
			Assert.fail();
		}catch (IOException e) {
			Assert.assertTrue(true);
		}
	}

}
