/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */

package com.automationanywhere.botcommands.gsuite.calendar.commands;


import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GCalendar;
import com.automationanywhere.botcommand.gsuite.api.impl.GCalendarImpl;
import com.automationanywhere.botcommand.gsuite.dto.calendar.EventDTO;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommand.gsuite.service.impl.AuthenticationServiceImpl;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.DataType;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Map;

import static com.automationanywhere.botcommands.gsuite.calendar.utils.Utils.getException;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;

@BotCommand
@CommandPkg(label = "Deletes a calendar event", name = "Delete", description = "Deletes an existing event",
        node_label = "with the name {{eventName}}| from {{session}}",
        return_type = DataType.STRING)
public class DeleteEvent {

    private static final Logger LOGGER = LogManager.getLogger(DeleteEvent.class);
    private static final String SESSION_EXAMPLE = "e.g. Session1 or S1";
    private static final String EVENT_EXAMPLE = "e.g. 41ejdgwsvmuftfv4cq3o3ne7bpmg";

    @Sessions
    private Map<String, Object> sessionMap;

    public void setSessionMap(Map<String,Object> sessionMap){
        this.sessionMap = sessionMap;
    }

    @Execute
    public void execute(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", description = SESSION_EXAMPLE, default_value = "Default", default_value_type = DataType.STRING) @NotEmpty String session,
            @Idx(index = "2", type = TEXT) @Pkg(label = "Event Id", description = EVENT_EXAMPLE, default_value = "Default", default_value_type = DataType.STRING)  String eventId
    ){
        try {
            GCalendar gCalendar = (GCalendar) sessionMap.get(session);
            if (gCalendar == null) {
                gCalendar = getCalendar();
            } else {
                eventId = gCalendar.getEventId();
            }

            EventDTO eventDTO = new EventDTO();
            eventDTO.setId(eventId);
            LOGGER.info("Deleting calendar event : " + eventId);
            gCalendar.deleteEvent(eventDTO);
        }catch (GeneralSecurityException | IOException e){
            LOGGER.error("exception in user authorization: ",e);
            String msg;
            if(e instanceof GoogleJsonResponseException )
                msg = getException((GoogleJsonResponseException) e);
            else
                msg = e.getMessage();
            throw new BotCommandException(msg, e);
        }
    }

    GCalendar getCalendar() throws GeneralSecurityException, IOException {
        AuthenticationService authenticationService = new AuthenticationServiceImpl();

        return new GCalendarImpl(authenticationService);
    }

}
