/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
package com.automationanywhere.botcommand.gsuite.service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.Constants;
import com.automationanywhere.botcommand.gsuite.messages.PropertiesReader;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommand.gsuite.service.impl.AuthenticationServiceImpl;
import com.automationanywhere.botcommand.gsuite.utils.Utils;
import com.automationanywhere.core.security.SecureString;
import com.google.api.client.auth.oauth2.Credential;

/**
 * The AbstractAuthUtils class implements methods to validate/remove user
 * sessions, dump user credentials in a file with AES encryption for later use.
 */
public abstract class AbstractAuthUtils {

	private static final Logger LOGGER = LogManager.getLogger(AbstractAuthUtils.class);

	/**
	 * This method validates user session based on key 'user email address'.
	 * 
	 * @param sessionMap       Package sessionMap.
	 * @param userEmailAddress User email address.
	 */
	protected void setUserSession(Map<String, Object> sessionMap, String userEmailAddress) {
		if (!sessionMap.containsKey(Constants.AUTH_USER + userEmailAddress))
			sessionMap.put(Constants.AUTH_USER + userEmailAddress, userEmailAddress);
	}

	/**
	 * This method validates user session based on key 'user email address' and
	 * remove user reference from session map.
	 * 
	 * @param sessionMap Package sessionMap.
	 * @param user       User email address.
	 */
	protected void validateAndRemoveUserSession(Map<String, Object> sessionMap, String userEmailAddress) {
		if (!sessionMap.containsKey(Constants.AUTH_USER + userEmailAddress)) {
			throw new BotCommandException(PropertiesReader.getValue("GSuiteAuth.User.NotExists"));
		} else {
			sessionMap.remove(Constants.AUTH_USER + userEmailAddress);
			try {
				Files.delete(Paths.get(Utils.getCredentialFilePathAsString(userEmailAddress)));
			} catch (IOException exception) {
				LOGGER.debug("Unable to delete user credentials file", exception);
			}
		}
	}

	/**
	 * This method launches OAuth Consent Screen and if successful then creates an
	 * AuthDto object with encrypted parameters and calls dumpJSONFile method to
	 * dump that dto in a json file at path
	 * C:\ProgramData\AutomationAnywhere\GSuiteAuth
	 * 
	 * @param sessionMap       Package SessionMap.
	 * @param clientId         Client id.
	 * @param userEmailAddress User email address.
	 * @param password         password.
	 * @param tenantId         tenantId.
	 * @param clientSecret     clientSecret
	 */
	protected Credential launchOAuthConsentScreen(SecureString userEmailAddress, SecureString clientId,
			SecureString clientSecret, SecureString redirectURI) throws IOException, GeneralSecurityException {

		AuthenticationService authImpl = getAuthenticationService();
		Credential credentials = authImpl.getGoogleCredential(userEmailAddress.getInsecureString(),
				clientId.getInsecureString(), clientSecret.getInsecureString(), redirectURI.getInsecureString());

		AuthDto authDto = new AuthDto();
		try {
			authDto.setClientId(clientId.getInsecureString());
			authDto.setClientSecret(clientSecret.getInsecureString());
			authDto.setRedirectURL(redirectURI.getInsecureString());
			dumpJSONFile(userEmailAddress, authDto);
		} catch (Exception e) {
			// TODO : throw proper exception
		}
		return credentials;
	}

	AuthenticationService getAuthenticationService() {
		return new AuthenticationServiceImpl();
	}

	/**
	 * This method dumps AuthDto object in a json file at path
	 * C:\ProgramData\AutomationAnywhere\office365Auth.
	 * 
	 * @param sessionMap       Package SessionMap.
	 * @param userEmailAddress User email address.
	 */
	private boolean dumpJSONFile(SecureString userEmailAddress, AuthDto authDto) throws IOException {
		LOGGER.trace("Creating a temporary directory for username..." + userEmailAddress.getInsecureString());
		Utils.createTempDirectory(AESEncrypter.encrypt(userEmailAddress.getInsecureString()));

		try (FileOutputStream file = new FileOutputStream(
				Utils.getCredentialFilePathAsString(userEmailAddress.getInsecureString()), false);
				ObjectOutputStream out = new ObjectOutputStream(file)) {
			out.writeObject(authDto);
		}
		return true;
	}

	/**
	 * This method validates workbook session based on sessionName identifier.
	 * 
	 * @param sessionMap  Package sessionMap.
	 * @param sessionName Workbook session identifier.
	 */
	public void validateSessionExists(Map<String, Object> sessionMap, String sessionName) {
		if (sessionMap.containsKey(sessionName))
			throw new BotCommandException(
					"Google sheet session is already open with " + sessionName + ".Please enter another session name");
	}

	public void validateSessionNotExists(Map<String, Object> sessionMap, String sessionName) {

		if (!sessionMap.containsKey(sessionName))
			throw new BotCommandException("No google sheet online session found with " + sessionName);
	}

}
