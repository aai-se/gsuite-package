/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommand.gsuite.messages;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class PropertiesReader {

  private static final String BUNDLE_NAME = "com.automationanywhere.botcommand.gsuite.messages.messages";

  private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

  private PropertiesReader() {
  }

  public static String getValue(String key) {
    try {
      return RESOURCE_BUNDLE.getString(key);
    } catch (MissingResourceException e) {
      return '!' + key + '!';
    }
  }
}
