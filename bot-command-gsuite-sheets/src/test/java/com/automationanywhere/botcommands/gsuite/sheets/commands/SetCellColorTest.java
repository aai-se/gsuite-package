/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.ACTIVECELL;
import static com.automationanywhere.botcommand.gsuite.Constants.CELL;
import static com.automationanywhere.botcommand.gsuite.Constants.RANGE;
import static com.automationanywhere.botcommand.gsuite.Constants.SPECIFICCELL;
import static com.automationanywhere.botcommand.gsuite.Constants.TEXT;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.dto.sheets.WorksheetDto;

public class SetCellColorTest {

	@Spy
	SetCellColor setCellColor;

	@Mock
	GSheets mockGSheets;

	@Mock
	WorksheetDto worksheetDto;

	final String session = "session";
	final String spreadSheetId = "spreadsheetId";

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		sessionMap.put("session", mockGSheets);
		setCellColor.setSessionMap(sessionMap);
		when(mockGSheets.getSpreadSheetId()).thenReturn(spreadSheetId);
		when(mockGSheets.getActiveSheetInfo()).thenReturn(worksheetDto);
		when(worksheetDto.getSheetName()).thenReturn("Sheet1");
		when(mockGSheets.getActiveCell(anyString())).thenReturn("A1");
	}

	@Test
	public void setFormulaForActiveCellTest() {
		try {
			when(mockGSheets.setCellColor(spreadSheetId, "A1", CELL, "Red")).thenReturn(true);
			Assert.assertTrue(setCellColor.execute(session, ACTIVECELL, null,null, CELL, "Red").get());
		} catch (IOException e) {
			Assert.fail();
		}
	}

	@Test
	public void setFormulaForSpecificCellTest() {
		try {
			when(mockGSheets.setCellColor(spreadSheetId, "A1", CELL, "#FF0000")).thenReturn(true);
			Assert.assertTrue(setCellColor.execute(session, SPECIFICCELL, "A1",null, CELL, "#FF0000").get());
			verify(mockGSheets, times(1)).setActiveCell(anyString(),anyString());
		} catch (IOException e) {
			Assert.fail();
		}
	}
	
	@Test
	public void setFormulaForRangeTest() {
		try {
			when(mockGSheets.setCellColor(spreadSheetId, "A1:B5", TEXT, "Red")).thenReturn(true);
			Assert.assertTrue(setCellColor.execute(session, RANGE, null, "A1:B5",  TEXT, "Red").get());
			verify(mockGSheets, times(1)).setActiveCell(anyString(),anyString());
		} catch (IOException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = Exception.class)
	public void botCommandExceptionTest() {
			try {
				when(mockGSheets.getSpreadSheetId()).thenReturn("worng ID");
				when(mockGSheets.setCellColor(anyString(), anyString(), anyString(), anyString())).thenThrow(new IOException());
				setCellColor.execute(session, SPECIFICCELL, "A1", null, TEXT, "#FF0000");
			} catch (IOException e) {
				Assert.fail();
			}
			
	}

}
