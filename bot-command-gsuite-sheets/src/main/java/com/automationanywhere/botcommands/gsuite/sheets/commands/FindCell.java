///*
// * Copyright (c) 2019 Automation Anywhere.
// * All rights reserved.
// *
// * This software is the proprietary information of Automation Anywhere.
// * You shall use it only in accordance with the terms of the license agreement
// * you entered into with Automation Anywhere.
// */
//
//package com.automationanywhere.botcommands.gsuite.sheets.commands;
//
//import static com.automationanywhere.botcommand.gsuite.Constants.ACTIVECELL;
//import static com.automationanywhere.botcommand.gsuite.Constants.DEFAULT_VALUE_SESSION;
//import static com.automationanywhere.botcommand.gsuite.Constants.DESCRIPTION_SESSION;
//import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_ACTIVECELL;
//import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_SESSION;
//import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_SPECIFICCELL;
//import static com.automationanywhere.botcommand.gsuite.Constants.SPECIFICCELL;
//import static com.automationanywhere.botcommand.gsuite.Constants.UNFORMATTED_VALUE;
//import static com.automationanywhere.botcommand.gsuite.Constants.cellAddressValidator;
//import static com.automationanywhere.botcommands.gsuite.sheets.utils.Utils.getException;
//import static com.automationanywhere.commandsdk.model.AttributeType.CHECKBOX;
//import static com.automationanywhere.commandsdk.model.AttributeType.RADIO;
//import static com.automationanywhere.commandsdk.model.AttributeType.SELECT;
//import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
//import static com.automationanywhere.commandsdk.model.DataType.LIST;
//import static com.automationanywhere.commandsdk.model.DataType.STRING;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//
//import com.automationanywhere.botcommand.data.impl.ListValue;
//import com.automationanywhere.botcommand.data.impl.StringValue;
//import com.automationanywhere.botcommand.exception.BotCommandException;
//import com.automationanywhere.botcommand.gsuite.api.GSheets;
//import com.automationanywhere.botcommand.gsuite.helper.sheets.CellSplitter;
//import com.automationanywhere.botcommand.gsuite.utils.Utils;
//import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
//import com.automationanywhere.commandsdk.annotations.BotCommand;
//import com.automationanywhere.commandsdk.annotations.CommandPkg;
//import com.automationanywhere.commandsdk.annotations.Execute;
//import com.automationanywhere.commandsdk.annotations.Idx;
//import com.automationanywhere.commandsdk.annotations.Idx.Option;
//import com.automationanywhere.commandsdk.annotations.Pkg;
//import com.automationanywhere.commandsdk.annotations.Sessions;
//import com.automationanywhere.commandsdk.annotations.rules.MatchesRegex;
//import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
//import com.automationanywhere.commandsdk.model.DataType;
//import com.google.api.client.googleapis.json.GoogleJsonResponseException;
//import com.google.api.services.sheets.v4.model.ValueRange;
//
//@BotCommand
//@CommandPkg(label = "Find", name = "findCell", description = "Finds or replaces content from a google sheet", node_label = "{{findText}} |from {{from}} |{{!fromSpecificCellText}} |to {{till}} |{{!tillSpecificCellText}} |in google sheet |replace it with {{replaceText}} |and assign to {{returnTo}} |in session {{sessionName}}", return_label = "Assign cell addresses to a list variable", return_required = true, return_type = LIST, return_sub_type = STRING, icon = "excel.svg")
//
//public class FindCell {
//	private static final Logger LOGGER = LogManager.getLogger(GetMultipleCell.class);
//	Utils util = new Utils();
//	CellSplitter cellSplitter = new CellSplitter();
//
//	@Sessions
//	private Map<String, Object> sessionMap;
//
//	@Execute
//	public ListValue<StringValue> execute(
//			@Idx(index = "1", type = TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @NotEmpty String sessionName,
//
//			@Idx(index = "2", type = SELECT, options = {
//					@Option(index = "2.1", pkg = @Pkg(label = "Beginning", value = "BEGINNING")),
//					@Option(index = "2.2", pkg = @Pkg(label = "End", value = "END")),
//					@Option(index = "2.3", pkg = @Pkg(label = LABEL_ACTIVECELL, value = ACTIVECELL)),
//					@Option(index = "2.4", pkg = @Pkg(label = LABEL_SPECIFICCELL, value = SPECIFICCELL)) }) @Pkg(label = "From", default_value = "BEGINNING", description = "e.g. A5 or B10", default_value_type = DataType.STRING) @NotEmpty String from,
//			@Idx(index = "2.4.1", type = TEXT) @Pkg(label = "Cell name", default_value = "") @MatchesRegex(cellAddressValidator) @NotEmpty String fromSpecificCellText,
//			@Idx(index = "3", type = SELECT, options = {
//					@Option(index = "3.1", pkg = @Pkg(label = "Beginning", value = "BEGINNING")),
//					@Option(index = "3.2", pkg = @Pkg(label = "End", value = "END")),
//					@Option(index = "3.3", pkg = @Pkg(label = LABEL_ACTIVECELL, value = ACTIVECELL)),
//					@Option(index = "3.4", pkg = @Pkg(label = LABEL_SPECIFICCELL, value = SPECIFICCELL)) }) @Pkg(label = "Till", default_value = "END", default_value_type = DataType.STRING) @NotEmpty String till,
//			@Idx(index = "3.4.1", type = TEXT) @Pkg(label = "Cell name", default_value = "") @MatchesRegex(cellAddressValidator) @NotEmpty String tillSpecificCellText,
//			@Idx(index = "4", type = TEXT) @Pkg(label = "Find", default_value = "") @NotEmpty String findText,
//			@Idx(index = "5", type = RADIO, options = {
//					@Option(index = "5.1", pkg = @Pkg(label = "By rows", value = "BYROWS")),
//					@Option(index = "5.2", pkg = @Pkg(label = "By columns", value = "BYCOLUMNS")) }) @Pkg(label = "Search options", default_value_type = DataType.STRING, default_value = "BYROWS") @NotEmpty String searchOptions,
//
//			@Idx(index = "6", type = CHECKBOX) @Pkg(label = "Match case", default_value = "false", default_value_type = DataType.BOOLEAN) @NotEmpty Boolean matchCase,
//
//			@Idx(index = "7", type = CHECKBOX) @Pkg(label = "Match entire cell contents", default_value = "false", default_value_type = DataType.BOOLEAN) @NotEmpty Boolean matchEntireCell,
//
//			@Idx(index = "8", type = CHECKBOX) @Pkg(label = "Replace with", default_value = "false", default_value_type = DataType.BOOLEAN) @NotEmpty Boolean replaceWith,
//			@Idx(index = "8.1", type = TEXT) @Pkg(label = "", default_value = "") @NotEmpty String replaceText) {
//
//		
//		GSheets gSheets = (GSheets) sessionMap.get(sessionName);
//		ExecutorValidator.getInstance().validate(gSheets);
//		
//		String cellAddress = "";
//		try {
//			ValueRange cellValues;
//			String range = "";
//
//			if (from.equals("BEGINNING")) {
//				range += "A1";
//			} else {
//				range += fromSpecificCellText;
//			}
//			range += ":";
//			if (till.equals("END")) {
//				range += gSheets.getValuesRange(gSheets.getSpreadSheetId(), sheetName);
//			} else {
//				range += tillSpecificCellText;
//			}
//
//			cellValues = gSheets.getValue(gSheets.getSpreadSheetId(), sheetName, range, UNFORMATTED_VALUE);
//
//			List<List<Object>> sheetCells = cellValues.getValues();
//			ArrayList<StringValue> foundCells = new ArrayList<>();
//
//			if (searchOption.equals("BYROWS")) {
//				for (int i = 0; i < sheetCells.size(); i++) {
//					for (int j = 0; j < sheetCells.get(i).size(); j++) {
//						String cellContents = sheetCells.get(i).get(j).toString();
//						if (cellContents.equals(findText) || (matchCase && cellContents.contains(findText))) {
//							String foundCell = makeCell(i, j, from, fromSpecificCellText);
//							LOGGER.info("found cell: " + foundCell);
//							foundCells.add(new StringValue(foundCell));
//						}
//					}
//				}
//			} else {
//				int i = 0;
//				for (int j = 0; j < sheetCells.get(i).size(); j++) {
//					for (i = 0; i < sheetCells.size(); i++) {
//						String cellContents = sheetCells.get(i).get(j).toString();
//						if (cellContents.equals(findText) || (matchCase && cellContents.contains(findText))) {
//							String foundCell = makeCell(i, j, from, fromSpecificCellText);
//							LOGGER.info("found cell: " + foundCell);
//							foundCells.add(new StringValue(foundCell));
//						}
//					}
//				}
//			}
//
//			LOGGER.info(gSheets.getValuesRange(gSheets.getSpreadSheetId(), sheetName));
//			ListValue output = new ListValue();
//			output.set(foundCells);
//			return output;
//
//		} catch (IOException i) {
//			LOGGER.error("exception in getting data from sheet: ", i);
//			String msg = getException((GoogleJsonResponseException) i);
//			throw new BotCommandException(msg, i);
//		}
//	}
//
//	public void setSessionMap(Map<String, Object> sessionMap) {
//		this.sessionMap = sessionMap;
//	}
//
//	public String makeCell(int row, int column, String from, String fromCell) {
//		String output = "";
//		if (!(from.equals("BEGINNING"))) {
//			cellSplitter.splitCell(fromCell);
//			column += util.getColIndex(cellSplitter.getColumnName()) - 1;
//			row += Integer.parseInt(cellSplitter.getRowName()) - 1;
//		}
//
//		String columnName = util.getColName(column + 1);
//		output += columnName;
//		output += (row + 1);
//		return output;
//	}
//}
