/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommand.gsuite.api.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.gsuite.dto.calendar.EventDTO;
import com.automationanywhere.botcommand.gsuite.dto.calendar.ScheduleDTO;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.CalendarList;
import com.google.api.services.calendar.model.Event;

public class GCalendarTest {

	@Mock
	AuthenticationService authService;

	@Mock
	Credential credential;

	@Mock
	Calendar calService;

	@Spy
	GCalendarImpl gCalendar;

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);

	}

	public void init() {
		Calendar.CalendarList mockCalList = mock(Calendar.CalendarList.class);
		CalendarList mockCalModel = new CalendarList();
		mockCalModel.setItems(new ArrayList<>());
		Calendar.CalendarList.List mockList = mock(Calendar.CalendarList.List.class);
		try {
			gCalendar.setAuthService(authService);
			gCalendar.setCalendar(calService);
			gCalendar.setEventDTO(null);
			doReturn(calService).when(gCalendar).getCalendarService(credential);
			doNothing().when(gCalendar).setupAuthorization();

			when(calService.calendarList()).thenReturn(mockCalList);
			when(mockCalList.list()).thenReturn(mockList);
			when(mockList.setFields(anyString())).thenReturn(mockList);
			when(mockList.execute()).thenReturn(mockCalModel);

		} catch (IOException e) {
			Assert.fail();
		}

	}

	@Test
	public void createEventWithNullEventDTO() {
		try {
			init();
			gCalendar.createEvent(null);
			 Assert.fail("Should have come here but in a nullpointer block");
		} catch (NullPointerException e) {
			Assert.assertTrue(true, "Nullpointer was thrown");
		} catch (IOException e) {
			Assert.fail("IOException should not have been thrown.");
		}
	}

    @Test
    public void createEventWithEmptyEventDTO(){
        try {
            init();
            Calendar.Events calEvents = mock(Calendar.Events.class);
            Calendar.Events.Insert calInsert = mock(Calendar.Events.Insert.class);

            Event event = new Event();
            event.setId("12345");

            when(calService.events()).thenReturn(calEvents);
            when(calEvents.insert(eq(null),any(Event.class))).thenReturn(calInsert);
            when(calInsert.setFields(anyString())).thenReturn(calInsert);
            when(calInsert.execute()).thenReturn(event);
            
            EventDTO eventDTO = gCalendar.createEvent(new EventDTO());
            
            assertEquals(event.getId(),eventDTO.getId());
        } catch (NullPointerException e) {
            e.printStackTrace();
            fail("NullPointerException should not have been thrown.");
        } catch (IOException e) {
            e.printStackTrace();
            fail("IOException should not have been thrown.");
        }
    }


    @Test
    public void createEventWithValidEventDTO(){
        try {
            init();
            Calendar.Events calEvents = mock(Calendar.Events.class);
            Calendar.Events.Insert calInsert = mock(Calendar.Events.Insert.class);
            EventDTO eventDTO = new EventDTO();
            eventDTO.setSummary("Test Event");
            eventDTO.setLocation("633 River Oaks Pwky,San Jose");

            Event event = gCalendar.convertToEvent(eventDTO);
            event.setId("12345");

            when(calService.events()).thenReturn(calEvents);
            when(calEvents.insert(eq(null),any(Event.class))).thenReturn(calInsert);
            when(calInsert.setFields(anyString())).thenReturn(calInsert);
            when(calInsert.execute()).thenReturn(event);
            

            EventDTO retEventDTO = gCalendar.createEvent(eventDTO);

            assertEquals(event.getId(),retEventDTO.getId());
            assertEquals(event.getSummary(),retEventDTO.getSummary());

        } catch (NullPointerException e) {
            e.printStackTrace();
            fail("NullPointerException should not have been thrown.");
        } catch (IOException e) {
            e.printStackTrace();
            fail("IOException should not have been thrown.");
        }
    }

    @Test
    public void createEventWithValidEventDTOAndAttendeeList(){
        try {
            init();
            Calendar.Events calEvents = mock(Calendar.Events.class);
            Calendar.Events.Insert calInsert = mock(Calendar.Events.Insert.class);
            EventDTO eventDTO = new EventDTO();
            eventDTO.setSummary("Test Event");
            eventDTO.setLocation("633 River Oaks Pwky,San Jose");
            eventDTO.setAttendees(new String[]{"test@aa.com","test2@aa.com"});

            Event event = gCalendar.convertToEvent(eventDTO);
            event.setId("12345");

            when(calService.events()).thenReturn(calEvents);
            when(calEvents.insert(eq(null),any(Event.class))).thenReturn(calInsert);
            when(calInsert.setFields(anyString())).thenReturn(calInsert);
            when(calInsert.execute()).thenReturn(event);

            EventDTO retEventDTO = gCalendar.createEvent(eventDTO);

            assertEquals(event.getId(),retEventDTO.getId());
            assertEquals(event.getSummary(),retEventDTO.getSummary());
            assertEquals(2,event.getAttendees().size());

        } catch (NullPointerException e) {
            e.printStackTrace();
            fail("NullPointerException should not have been thrown.");
        } catch (IOException e) {
            e.printStackTrace();
            fail("IOException should not have been thrown.");
        }
    }

    @Test
    public void createEventWithSpecificDatesForAllDayEvent() throws ParseException {
        try {
            init();
            Calendar.Events calEvents = mock(Calendar.Events.class);
            Calendar.Events.Insert calInsert = mock(Calendar.Events.Insert.class);
            EventDTO eventDTO = new EventDTO();
            eventDTO.setSummary("Test Event");
            eventDTO.setLocation("633 River Oaks Pwky,San Jose");
            eventDTO.setAllDay(true);

            TimeZone timeZone = TimeZone.getTimeZone("GMT");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf.setTimeZone(timeZone);

            Date d = sdf.parse("2019-07-30T1:00:00");
            DateTime t = new DateTime(true,d.getTime(),0);

            ScheduleDTO start = new ScheduleDTO(t,null,"GMT");

            ScheduleDTO end = new ScheduleDTO(new DateTime("2019-07-30T12:00:00"),null,"GMT");
            eventDTO.setStart(start);
            eventDTO.setEnd(end);

            Event event = gCalendar.convertToEvent(eventDTO);
            event.setId("12345");

            when(calService.events()).thenReturn(calEvents);
            when(calEvents.insert(eq(null),any(Event.class))).thenReturn(calInsert);
            when(calInsert.setFields(anyString())).thenReturn(calInsert);
            when(calInsert.execute()).thenReturn(event);

            EventDTO retEventDTO = gCalendar.createEvent(eventDTO);
            assertEquals("GMT",event.getStart().getTimeZone());
            assertEquals("2019-07-30",event.getStart().getDate().toStringRfc3339());
            assertEquals("2019-07-30",event.getStart().getDate().toString());
            assertEquals(event.getId(),retEventDTO.getId());
            assertEquals(event.getSummary(),retEventDTO.getSummary());

        } catch (NullPointerException e) {
            e.printStackTrace();
            fail("NullPointerException should not have been thrown.");
        } catch (IOException e) {
            e.printStackTrace();
            fail("IOException should not have been thrown.");
        }
    }

    @Test
    public void createEventWithSpecificDates() throws ParseException {
        try {
            init();
            Calendar.Events calEvents = mock(Calendar.Events.class);
            Calendar.Events.Insert calInsert = mock(Calendar.Events.Insert.class);
            EventDTO eventDTO = new EventDTO();
            eventDTO.setSummary("Test Event");
            eventDTO.setLocation("633 River Oaks Pwky,San Jose");
            eventDTO.setAllDay(false);
            TimeZone timeZone = TimeZone.getTimeZone("GMT");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            sdf.setTimeZone(timeZone);
            DateTime t = new DateTime(sdf.parse("2019-07-30T01:00:00"),timeZone);

            ScheduleDTO start = new ScheduleDTO(null,t,"GMT");

            ScheduleDTO end = new ScheduleDTO(null,new DateTime(sdf.parse("2019-07-30T12:00:00"),timeZone),"GMT");
            eventDTO.setStart(start);
            eventDTO.setEnd(end);

            Event event = gCalendar.convertToEvent(eventDTO);
            event.setId("12345");

            when(calService.events()).thenReturn(calEvents);
            when(calEvents.insert(eq(null),any(Event.class))).thenReturn(calInsert);
            when(calInsert.setFields(anyString())).thenReturn(calInsert);
            when(calInsert.execute()).thenReturn(event);

            EventDTO retEventDTO = gCalendar.createEvent(eventDTO);
            assertEquals("GMT",event.getStart().getTimeZone());
            assertEquals("2019-07-30T01:00:00.000Z",event.getStart().getDateTime().toStringRfc3339());
            assertEquals("2019-07-30T01:00:00.000Z",event.getStart().getDateTime().toString());
            assertEquals(event.getId(),retEventDTO.getId());
            assertEquals(event.getSummary(),retEventDTO.getSummary());

        } catch (NullPointerException e) {
            e.printStackTrace();
            fail("NullPointerException should not have been thrown.");
        } catch (IOException e) {
            e.printStackTrace();
            fail("IOException should not have been thrown.");
        }
    }


    @Test
    public void createEventWithSpecificDatesAndTimeZone() throws ParseException {
        try {
            init();
            Calendar.Events calEvents = mock(Calendar.Events.class);
            Calendar.Events.Insert calInsert = mock(Calendar.Events.Insert.class);
            EventDTO eventDTO = new EventDTO();
            eventDTO.setSummary("Test Event");
            eventDTO.setLocation("633 River Oaks Pwky,San Jose");
            eventDTO.setAllDay(false);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            TimeZone timeZone = TimeZone.getTimeZone("America/Los_Angeles");
            sdf.setTimeZone(timeZone);
            DateTime t = new DateTime(sdf.parse("2019-07-30T11:00:00"),timeZone);

            ScheduleDTO start = new ScheduleDTO(null,t,"America/Los_Angeles");

            ScheduleDTO end = new ScheduleDTO(null,new DateTime(sdf.parse("2019-07-30T12:00:00"),timeZone),"America/Los_Angeles");

            eventDTO.setStart(start);
            eventDTO.setEnd(end);

            Event event = gCalendar.convertToEvent(eventDTO);
            event.setId("12345");

            when(calService.events()).thenReturn(calEvents);
            when(calEvents.insert(eq(null),any(Event.class))).thenReturn(calInsert);
            when(calInsert.setFields(anyString())).thenReturn(calInsert);
            when(calInsert.execute()).thenReturn(event);

            EventDTO retEventDTO = gCalendar.createEvent(eventDTO);
            assertEquals("America/Los_Angeles",event.getStart().getTimeZone());
            assertEquals("2019-07-30T11:00:00.000-07:00",event.getStart().getDateTime().toStringRfc3339());
            assertEquals("2019-07-30T12:00:00.000-07:00",event.getEnd().getDateTime().toString());
            assertEquals(event.getId(),retEventDTO.getId());
            assertEquals(event.getSummary(),retEventDTO.getSummary());

        } catch (NullPointerException e) {
            e.printStackTrace();
            fail("NullPointerException should not have been thrown.");
        } catch (IOException e) {
            e.printStackTrace();
            fail("IOException should not have been thrown.");
        }
    }


    @Test
    public void deleteEvent(){
        init();
        Calendar.Events calEvents = mock(Calendar.Events.class);
        Calendar.Events.Delete mockDelete = mock(Calendar.Events.Delete.class);
        Void event = null;
        EventDTO eventDTO = new EventDTO();
        eventDTO.setId("abcde");

        when(calService.events()).thenReturn(calEvents);
        try {
            when(calEvents.delete(eq(null),eq("abcde"))).thenReturn(mockDelete);

            doAnswer(invocation -> {
                assertTrue(true);
                return null;
            }).when(mockDelete).execute();
            boolean isSuccess = gCalendar.deleteEvent(eventDTO);
            assertTrue(isSuccess);
            verify(calEvents,times(1)).delete(eq(null),eq("abcde"));
            verify(mockDelete,times(1)).execute();
        } catch (IOException e) {
            e.printStackTrace();
            fail("IOException should not have been thrown.");
        }
    }

    @Test
    public void deleteEventThrowsIOException(){
        init();
        Calendar.Events calEvents = mock(Calendar.Events.class);
        Calendar.Events.Delete mockDelete = mock(Calendar.Events.Delete.class);
        Void event = null;
        EventDTO eventDTO = new EventDTO();
        eventDTO.setId("abcde");

        when(calService.events()).thenReturn(calEvents);
        try {
            when(calEvents.delete(eq(null),eq("abcde"))).thenThrow(new IOException("Failure"));

            doAnswer(invocation -> {
                assertTrue(true);
                return null;
            }).when(mockDelete).execute();
            boolean isSuccess = gCalendar.deleteEvent(eventDTO);
            assertFalse(isSuccess);
            verify(calEvents,times(1)).delete(eq(null),eq("abcde"));
            verify(mockDelete,times(0)).execute();
        } catch (IOException e) {
            e.printStackTrace();
         assertTrue(e.getMessage().equals("Failure"),"IOException has been thrown.");
        }
    }

    @Test
    public void getEventIdWithEventDTOBeingNull(){
        init();

        String id = gCalendar.getEventId();
        assertNull(id);
    }

    @Test
    public void getEventIdWithEventDTOBeingNotNull(){
        init();
    try{
        Calendar.Events calEvents = mock(Calendar.Events.class);
        Calendar.Events.Insert calInsert = mock(Calendar.Events.Insert.class);
        EventDTO eventDTO = new EventDTO();
        eventDTO.setSummary("Test Event");
        eventDTO.setLocation("633 River Oaks Pwky,San Jose");

        Event event = gCalendar.convertToEvent(eventDTO);
        event.setId("12345");

        when(calService.events()).thenReturn(calEvents);
        when(calEvents.insert(eq(null),any(Event.class))).thenReturn(calInsert);
        when(calInsert.setFields(anyString())).thenReturn(calInsert);
        when(calInsert.execute()).thenReturn(event);

        EventDTO retEventDTO = gCalendar.createEvent(eventDTO);

        assertEquals(event.getId(),retEventDTO.getId());
        assertEquals(event.getSummary(),retEventDTO.getSummary());

        String id = gCalendar.getEventId();
        assertEquals("12345",id);

    } catch (NullPointerException e) {
        e.printStackTrace();
        fail("NullPointerException should not have been thrown.");
    } catch (IOException e) {
        e.printStackTrace();
        fail("IOException should not have been thrown.");
    }


    }

}
