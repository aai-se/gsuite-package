package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.*;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommands.gsuite.sheets.commands.OpenSheets;
import com.automationanywhere.botcommands.gsuite.sheets.commands.RenameSheet;
import com.automationanywhere.botcommands.gsuite.sheets.commands.SetSingleCell;
import com.automationanywhere.core.security.SecureString;

public class RenameSheetJunit extends AbstractAuthUtils {

	RenameSheet renameSheet;
	OpenSheets openSheets;
	SetSingleCell setSingleCell;
	
	final String session = "session";
	final String user = "saanchi@aademo.page";
	final String ClientID = "1045113049707-u3fhdtroech2uch19jogs52fg98t3lpu.apps.googleusercontent.com";
	final String redirectUrl = "http://localhost";
	final String clientSecreet = "6rjSOWzAEKEdiFqpCDGxirkk";

	final SecureString userName = new SecureString(user.toCharArray());
	final SecureString clientId = new SecureString(ClientID.toCharArray());
	final SecureString redirectURI = new SecureString(redirectUrl.toCharArray());
	final SecureString clientSecret = new SecureString(clientSecreet.toCharArray());

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {
		launchOAuthConsentScreen(userName, clientId, clientSecret, redirectURI);

		openSheets = new OpenSheets();
		openSheets.setSessionMap(sessionMap);
		openSheets.execute(userName, session, "BYNAME", "Demo", null, null, false,null, false);
	
		renameSheet = new RenameSheet();
		renameSheet.setSessionMap(sessionMap);
		
		setSingleCell= new  SetSingleCell();
		setSingleCell.setSessionMap(sessionMap);
	}

	@Test
	public void renameSheetTest() {
		// by index
			// valid input
//			renameSheet.execute(session, "true", 2, null, "SheetTestNew");
			// info given doesn't exist
//			renameSheet.execute(session, "true", 10, null, "SheetTest2");
			// trying to rename sheet with name which is already exist.								not working
//			renameSheet.execute(session, "true", 1, null, "Megha");
			//  trying to rename sheet with name which is already exist, checking for case sensitivity.			not working
//			renameSheet.execute(session, "true", 1, null, "megha");										

		//by name
			// valid input
//			renameSheet.execute(session, "false", null, "new", "NewSheet");
			// info given doesn't exist
//			renameSheet.execute(session, "false", null, "Mnb", "NewSheet2");
			// trying to rename sheet with name which is already exist.									
//			renameSheet.execute(session, "false", null, "NewSheet", "Megha");
		//  trying to rename sheet with name which is already exist, checking for case sensitivity.		
//			renameSheet.execute(session, "false", null, "NewSheet", "megha");
			
		// try to rename activated sheet
//			renameSheet.execute(session, "false", null, "megha22", "Megha23");
//			setSingleCell.execute(session, ACTIVECELL, null, "decker");
			
		//try to rename a hidden worksheet
			renameSheet.execute(session, "false", null, "DepositsRenamed", "Deposits");
	}

}
