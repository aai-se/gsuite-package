/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.Spy;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommand.gsuite.service.impl.AuthenticationServiceImpl;
import com.automationanywhere.botcommands.gsuite.sheets.commands.AutofitColumns;
import com.automationanywhere.botcommands.gsuite.sheets.commands.OpenSheets;
import com.automationanywhere.botcommands.gsuite.sheets.test.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;


//Done by Vipin


public class AutofitColumnsJunit extends AbstractAuthUtils {
	
	AutofitColumns autofitColumns;

	OpenSheets openSheets;
	final String session = "session";

	public static final SecureString USERNAME =  ConstantsTest.getUserName();
	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {
        launchOAuthConsentScreen(ConstantsTest.getUserName(),ConstantsTest.getClientid(), ConstantsTest.getClientSecret(), ConstantsTest.getRedirectUri());
		autofitColumns = new AutofitColumns();
		autofitColumns.setSessionMap(sessionMap);
		openSheets = new OpenSheets();
		openSheets.setSessionMap(sessionMap);
		openSheets.execute(ConstantsTest.getUserName(), session, "BYNAME", "Demo", null, null, true, "Megha", false);
	}

	@Test
	public void autofitColumnsTest() {
			 autofitColumns.execute(session);	
	}
	
	@Test(expectedExceptions = BotCommandException.class)
	public void autofitColumnsTest_NullSession() {
		autofitColumns.execute(null);
	}
	
	@Test(expectedExceptions = BotCommandException.class)
	public void autofitColumnsTest_WrongSession() {
		autofitColumns.execute("wrong_session");
	}
}

