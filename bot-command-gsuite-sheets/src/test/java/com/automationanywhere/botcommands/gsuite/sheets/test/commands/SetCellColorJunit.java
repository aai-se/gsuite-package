/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static com.automationanywhere.botcommand.gsuite.Constants.*;

import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommands.gsuite.sheets.commands.OpenSheets;
import com.automationanywhere.botcommands.gsuite.sheets.commands.SetCellColor;
import com.automationanywhere.core.security.SecureString;

public class SetCellColorJunit extends AbstractAuthUtils{
	SetCellColor setCellColor;
	OpenSheets openSheets;

	final String session = "session";
	final String user = "saanchi@aademo.page";
	final String ClientID = "1045113049707-u3fhdtroech2uch19jogs52fg98t3lpu.apps.googleusercontent.com";
	final String redirectUrl = "http://localhost";
	final String clientSecreet = "6rjSOWzAEKEdiFqpCDGxirkk";

	final SecureString userName = new SecureString(user.toCharArray());
	final SecureString clientId = new SecureString(ClientID.toCharArray());
	final SecureString redirectURI = new SecureString(redirectUrl.toCharArray());
	final SecureString clientSecret = new SecureString(clientSecreet.toCharArray());

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {
		launchOAuthConsentScreen(userName, clientId, clientSecret, redirectURI);

		openSheets = new OpenSheets();
		openSheets.setSessionMap(sessionMap);
		openSheets.execute(userName, session, "BYNAME", "Demo", null, null, true, "Megha", false);
		setCellColor = new SetCellColor();
		setCellColor.setSessionMap(sessionMap);
	}

	@Test
	public void setCellColorTest() {
		setCellColor.execute(session, SPECIFICCELL, "E8", null, CELL, "#baaAAF");
		setCellColor.execute(session, RANGE, null,"E1:E10", CELL, "#EAAaaf");
		setCellColor.execute(session, RANGE, null,"A3:B5", TEXT, "#fbbcde");
		setCellColor.execute(session, RANGE, null,"B1:B1", TEXT, "#ecdbef");
		setCellColor.execute(session, RANGE, null,"B7:A6", TEXT, "red");
		setCellColor.execute(session, RANGE, null,"B3:A10", CELL, "#ecdbef");
		setCellColor.execute(session, ACTIVECELL, null,null, CELL, "blue");

	}
}
