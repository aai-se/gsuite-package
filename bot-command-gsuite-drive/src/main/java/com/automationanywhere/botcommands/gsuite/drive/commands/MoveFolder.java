/**
 * Moveright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.USERNAME;
import static com.automationanywhere.commandsdk.model.AttributeType.RADIO;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.api.impl.GDriveImpl;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommand.gsuite.service.impl.AuthenticationServiceImpl;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import com.google.common.annotations.VisibleForTesting;

/**
 * Copies a Google Drive folder from a Google Drive location to another Google
 * Drive location.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

@BotCommand
@CommandPkg(label = "Move folder", name = "MoveFolder", description = "Moves a folder from one location to another", node_label = "|with name {{folderName}}|with id {{folderId}}| to folder {{folderName}}| to folder id {{folderId}}")
public class MoveFolder {
	private static final Logger LOGGER = LogManager.getLogger(MoveFolder.class);
	private static final String NAME_DESC = "e.g. MyFolder";
	private static final String ID_DESC = "e.g. 17dIAwvrEA4JeLysfFky9oZL0qAzesY0IdbOu-AvHB1U from document link https://docs.google.com/spreadsheets/d/17dIAwvrEA4JeLysfFky9oZL0qAzesY0IdbOu-AvHB1U/edit#gid=0";
	private static final String LOC_DESC = "e.g. MyFolder2/MyFolder3";
	private AuthenticationService mAuthenticationService;

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public Value<String> execute(
			@Idx(index = "1", type = AttributeType.CREDENTIAL) @Pkg(label = USERNAME) @NotEmpty SecureString userEmailAddress,
			@Idx(index = "2", type = RADIO, options = {
					@Idx.Option(index = "2.1", pkg = @Pkg(label = "Folder name (with location)", value = "BYNAME")),
					@Idx.Option(index = "2.2", pkg = @Pkg(label = "Folder ID", value = "BYID")) }) @Pkg(label = "Source", default_value_type = DataType.STRING, default_value = "BYNAME") @NotEmpty String sourceFolderOption,
			@Idx(index = "2.1.1", type = TEXT) @Pkg(label = "", description = NAME_DESC, default_value = "", default_value_type = DataType.STRING) @NotEmpty String sourceFolderName,
			@Idx(index = "2.2.1", type = TEXT) @Pkg(label = "", description = ID_DESC, default_value = "", default_value_type = DataType.STRING) @NotEmpty String sourceFolderId,
			@Idx(index = "3", type = RADIO, options = {
					@Idx.Option(index = "3.1", pkg = @Pkg(label = "Folder location", value = "BYNAME")),
					@Idx.Option(index = "3.2", pkg = @Pkg(label = "Folder ID", value = "BYID")) }) @Pkg(label = "Destination", default_value_type = DataType.STRING, default_value = "BYNAME") @NotEmpty String destFolderOption,
			@Idx(index = "3.1.1", type = TEXT) @Pkg(label = "", description = LOC_DESC, default_value = "", default_value_type = DataType.STRING) @NotEmpty String destinationFolderName,
			@Idx(index = "3.2.1", type = TEXT) @Pkg(label = "", description = ID_DESC, default_value = "", default_value_type = DataType.STRING) @NotEmpty String destinationFolderId) {
		try {

			JSONObject inputFolderParam = new JSONObject();
			if (sourceFolderOption.equals("BYNAME")) {
				inputFolderParam.put("fileName", sourceFolderName.trim());
			} else if (sourceFolderOption.equals("BYID")) {
				inputFolderParam.put("fileId", sourceFolderId.trim());
			}

			JSONObject outputFolderParam = new JSONObject();

			if (destFolderOption.equals("BYNAME")) {
				outputFolderParam.put("fileName", destinationFolderName.trim());
			} else if (destFolderOption.equals("BYID")) {
				outputFolderParam.put("fileId", destinationFolderId.trim());
			}

			return new StringValue(
					getDrive(userEmailAddress).moveFolder(inputFolderParam.toString(), outputFolderParam.toString()));
		} catch (GeneralSecurityException | IOException | JSONException e) {
			LOGGER.error("Exception in moveing Google Drive folder: ", e);
			throw new BotCommandException(e.getMessage(), e);
		}
	}

	GDrive getDrive(SecureString userEmailAddress) throws GeneralSecurityException, IOException {
		AuthenticationService authenticationService = getAuthenticationService();
		return new GDriveImpl(authenticationService, userEmailAddress.getInsecureString(), sessionMap);
	}

	@VisibleForTesting
	void setAuthenticationService(AuthenticationService mAuthenticationService) {
		this.mAuthenticationService = mAuthenticationService;
	}

	public AuthenticationService getAuthenticationService() {
		if (mAuthenticationService == null) {
			mAuthenticationService = new AuthenticationServiceImpl();
		}
		return mAuthenticationService;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
}
