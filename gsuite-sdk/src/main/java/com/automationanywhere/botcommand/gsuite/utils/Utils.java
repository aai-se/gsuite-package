/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommand.gsuite.utils;

import static com.automationanywhere.botcommand.gsuite.Constants.EMPTY;
import static com.automationanywhere.botcommand.gsuite.Constants.MIME_TYPE_DOCUMENT;
import static com.automationanywhere.botcommand.gsuite.Constants.MIME_TYPE_IMAGE;
import static com.automationanywhere.botcommand.gsuite.Constants.MIME_TYPE_PPT;
import static com.automationanywhere.botcommand.gsuite.Constants.MIME_TYPE_SPREADSHEET;
import static com.automationanywhere.botcommand.gsuite.Constants.ROWS;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import javax.activation.MimeType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.helper.sheets.CellSplitter;
import com.automationanywhere.botcommand.gsuite.service.AESEncrypter;
import com.google.api.services.sheets.v4.model.GridRange;

import eu.medsea.mimeutil.MimeUtil;

public class Utils {

	private static final String GSUITEAUTH = "G-SuiteAuth";
	private static final String CREDENTIAL_FILE_NAME = "credentials.json";
	private static final Logger LOGGER = LogManager.getLogger(Utils.class);

	public static final String NATIVE_FOLDER_PATH = System.getenv("PROGRAMDATA").concat(File.separator)
			.concat("AutomationAnywhere");

	public static File createTempDirectory(String userName) throws IOException {
		File aaFile = new File(NATIVE_FOLDER_PATH);
		if (!aaFile.exists()) {
			aaFile.mkdir();
			LOGGER.trace("AA folder created:{}", aaFile.exists());
		} else {
			LOGGER.debug("AA folder already exists:{}", aaFile.exists());
		}
		File generatedDir = new File(NATIVE_FOLDER_PATH, GSUITEAUTH);

		if (!generatedDir.mkdir()) {
			LOGGER.debug("package gsuite folder already exists");
		}
		generatedDir = new File(NATIVE_FOLDER_PATH+File.separator+GSUITEAUTH, userName);

		if (!generatedDir.mkdir()) {
			LOGGER.debug("package email folder already exists");
		}
		return generatedDir;
	}

	/**
	 * This method getCredentialFilePathAsString returns user credentials file path
	 * as a string. * @param userEmailAddress user email address.
	 * 
	 * @return path of user credentials file. eg-
	 *         "C:\ProgramData\AutomationAnywhere\gsuiteAuth\ABCcredentials.json".
	 */
	public static String getCredentialFilePathAsString(String userEmailAddress) {
		return new StringBuilder().append(NATIVE_FOLDER_PATH).append(File.separator + GSUITEAUTH).append(File.separator)
				.append(AESEncrypter.encrypt(userEmailAddress)).append(File.separator).append(CREDENTIAL_FILE_NAME)
				.toString();
	}

	public static String getDownloadMimetype(String inputType) {
		if (inputType.equals(MIME_TYPE_DOCUMENT)) {
			return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
		} else if (inputType.equals(MIME_TYPE_SPREADSHEET)) {
			return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		} else if (inputType.equals(MIME_TYPE_IMAGE)) {
			return "image/jpeg";
		} else if (inputType.equals(MIME_TYPE_PPT)) {
			return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
		} else {
			return inputType;
		}
	}

	public static String getFileExtension(String mimeType) {
		if (mimeType.equals(MIME_TYPE_DOCUMENT)) {
			return "docx";
		} else if (mimeType.equals(MIME_TYPE_SPREADSHEET)) {
			return "xlsx";
		} else if (mimeType.equals(MIME_TYPE_IMAGE)) {
			return "jpeg";
		} else if (mimeType.equals(MIME_TYPE_PPT)) {
			return "pptx";
		} else {
			return "";
		}
	}

	public static String getContentType(File file) {
		Collection mimeTypes = MimeUtil.getMimeTypes(file);
		MimeType mt = null;
		for (Object o : mimeTypes) {
			mt = (MimeType) o;
			break;
		}
		// TODO: find generic mimetype to return as default
		// Maybe : application/vnd.google-apps.unknown
		return mt != null ? mt.toString() : "xxx";
	}

	public static Boolean isGoogleDoc(String inputType) {
		if (inputType.equals(MIME_TYPE_DOCUMENT) || inputType.equals(MIME_TYPE_SPREADSHEET)
				|| inputType.equals(MIME_TYPE_IMAGE) || inputType.equals(MIME_TYPE_PPT)) {
			return true;
		}
		return false;
	}

	public static int getColIndex(String colName) {
		int colIndex = 0;

		for (int i = 0; i < colName.length(); i++) {
			colIndex = colIndex * 26 + (colName.charAt(i) - ('A' - 1));
		}

		return colIndex;
	}

	public static String getColName(Integer colIndex) {
		try {
			StringBuilder columnName = new StringBuilder();
			while (colIndex > 0) {
				// Find remainder
				int rem = colIndex % 26;
				// If remainder is 0, then a
				// 'Z' must be there in output
				if (rem == 0) {
					columnName.append("Z");
					colIndex = (colIndex / 26) - 1;
				} else // If remainder is non-zero
				{
					columnName.append((char) ((rem - 1) + 'A'));
					colIndex = colIndex / 26;
				}
			}
			return columnName.reverse().toString();
		} catch (Exception ex) {
			throw new BotCommandException(ex.getMessage(), ex);
		}
	}

	public static String getCellRowColumn(String cellUsed, String dimension) {
		try {
			StringBuilder column = new StringBuilder(EMPTY);
			StringBuilder row = new StringBuilder(EMPTY);
			for (int i = 0; i < cellUsed.length(); i++) {
				if (cellUsed.charAt(i) >= 'A' && cellUsed.charAt(i) <= 'Z')
					column.append(String.valueOf(cellUsed.charAt(i)));
				if (cellUsed.charAt(i) >= '0' && cellUsed.charAt(i) <= '9')
					row.append(String.valueOf(cellUsed.charAt(i)));
			}
			if (ROWS.equals(dimension))
				return row.toString();
			else
				return column.toString();
		} catch (Exception ex) {
			throw new BotCommandException(ex.getMessage(), ex);
		}
	}

	public static GridRange convertToGridRange(String cellAddress, int sheetId) {
		GridRange gridRange = new GridRange();
		gridRange.setSheetId(sheetId);
		String[] addresses = cellAddress.split(":");
		CellSplitter cellSplitter = new CellSplitter();
		
		if (addresses.length == 1) {
			cellSplitter.splitCell(addresses[0]);
			int column=Utils.getColIndex(cellSplitter.getColumnName());
			int row=Integer.parseInt(cellSplitter.getRowName());
			gridRange.setStartColumnIndex(column-1);
			gridRange.setStartRowIndex(row-1);
			gridRange.setEndColumnIndex(column);
			gridRange.setEndRowIndex(row);
		} else {
			cellSplitter.splitCell(addresses[0]);
			gridRange.setStartColumnIndex(Utils.getColIndex(cellSplitter.getColumnName())-1);
			gridRange.setStartRowIndex(Integer.parseInt(cellSplitter.getRowName())-1);
			cellSplitter.splitCell(addresses[1]);
			gridRange.setEndColumnIndex(Utils.getColIndex(cellSplitter.getColumnName()));
			gridRange.setEndRowIndex(Integer.parseInt(cellSplitter.getRowName()));
		}
		
		//if reverse range is provided as cellAddress
		if(gridRange.getEndColumnIndex() <= gridRange.getStartColumnIndex())
		{
			int temp= gridRange.getStartColumnIndex();
			gridRange.setStartColumnIndex(gridRange.getEndColumnIndex() - 1);
			gridRange.setEndColumnIndex(temp + 1);
		}
		if(gridRange.getEndRowIndex() <= gridRange.getStartRowIndex())
		{
			int temp= gridRange.getStartRowIndex(); 
			gridRange.setStartRowIndex(gridRange.getEndRowIndex()-1);
			gridRange.setEndRowIndex(temp + 1);
		}
		return gridRange;
	}
}
		
		
//		CellSplitter cellSplitter = new CellSplitter();
//		if (addresses.length == 1) {
//			cellSplitter.splitCell(addresses[0]);
//			gridRange.setStartColumnIndex(Utils.getColIndex(cellSplitter.getColumnName()));
//			gridRange.setStartRowIndex(Integer.parseInt(cellSplitter.getRowName()));
//			gridRange.setEndColumnIndex(gridRange.getStartColumnIndex() + 1);
//			gridRange.setEndRowIndex(gridRange.getStartRowIndex() + 1);
//		} else {
//			cellSplitter.splitCell(addresses[0]);
//			gridRange.setStartColumnIndex(Utils.getColIndex(cellSplitter.getColumnName()));
//			gridRange.setStartRowIndex(Integer.parseInt(cellSplitter.getRowName()));
//			cellSplitter.splitCell(addresses[1]);
//			gridRange.setEndColumnIndex(Utils.getColIndex(cellSplitter.getColumnName()));
//			gridRange.setEndRowIndex(Integer.parseInt(cellSplitter.getRowName()));
//		}
//		return gridRange;
//	}
//
//}
