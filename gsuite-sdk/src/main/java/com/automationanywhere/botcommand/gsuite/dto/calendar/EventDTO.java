/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommand.gsuite.dto.calendar;

import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

public class EventDTO {
    String id;
    Date created;
    Date updated;
    String summary;     // title
    String description;
    String location;
    String url;
    PersonDTO creator;
    PersonDTO organizer;

    ScheduleDTO start;
    ScheduleDTO end;
    boolean endTimeUnspecified = false;  //Whether the end time is actually unspecified.
    boolean isRecurring = false;
    boolean isAllDay = false;
    String[] recurrence;
    String recurringEventId;

    String[] attendees;
    Visibility visibility = Visibility.DEFAULT;
    Status eventStatus = Status.TENTATIVE;

    public EventDTO() {
        start = new ScheduleDTO();
        end = new ScheduleDTO();
        creator = new PersonDTO();
        organizer = new PersonDTO();
        attendees = new String[0];
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isRecurring() {
        return isRecurring;
    }

    public void setRecurring(boolean recurring) {
        isRecurring = recurring;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public PersonDTO getCreator() {
        return creator;
    }

    public void setCreator(PersonDTO creator) {
        this.creator = creator;
    }

    public PersonDTO getOrganizer() {
        return organizer;
    }

    public void setOrganizer(PersonDTO organizer) {
        this.organizer = organizer;
    }

    public ScheduleDTO getStart() {
        return start;
    }

    public void setStart(ScheduleDTO start) {
        this.start = start;
    }

    public ScheduleDTO getEnd() {
        return end;
    }

    public void setEnd(ScheduleDTO end) {
        this.end = end;
    }

    public boolean isEndTimeUnspecified() {
        return endTimeUnspecified;
    }

    public void setEndTimeUnspecified(boolean endTimeUnspecified) {
        this.endTimeUnspecified = endTimeUnspecified;
    }

    public String[] getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(String[] recurrence) {
        this.recurrence = recurrence;
    }

    public String getRecurringEventId() {
        return recurringEventId;
    }

    public void setRecurringEventId(String recurringEventId) {
        this.recurringEventId = recurringEventId;
    }

    public String[] getAttendees() {
        return attendees;
    }

    public void setAttendees(String[] attendees) {
        this.attendees = attendees;
    }

    public Visibility getVisibility() {
        return visibility;
    }

    public void setVisibility(Visibility visibility) {
        this.visibility = visibility;
    }

    public Status getEventStatus() {
        return eventStatus;
    }

    public void setEventStatus(Status eventStatus) {
        this.eventStatus = eventStatus;
    }

    public boolean isAllDay() {
        return isAllDay;
    }

    public void setAllDay(boolean allDay) {
        isAllDay = allDay;
    }

    @Override
    public String toString() {
        return "EventDTO{" +
                "id='" + id + '\'' +
                ", created=" + created +
                ", updated=" + updated +
                ", summary='" + summary + '\'' +
                ", description='" + description + '\'' +
                ", location='" + location + '\'' +
                ", creator=" + creator +
                ", organizer=" + organizer +
                ", Event starts at =" + start.toString() +
                ", Event ends at =" + end.toString() +
                ", endTimeUnspecified=" + endTimeUnspecified +
                ", isRecurring=" + isRecurring +
                ", recurrence=" + Arrays.toString(recurrence) +
                ", recurringEventId='" + recurringEventId + '\'' +
                ", attendees=" + Arrays.toString(attendees) +
                ", visibility=" + visibility +
                ", eventStatus=" + eventStatus +
                ", url =" + url +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventDTO eventDTO = (EventDTO) o;
        return endTimeUnspecified == eventDTO.endTimeUnspecified &&
                isRecurring == eventDTO.isRecurring &&
                isAllDay == eventDTO.isAllDay &&
                Objects.equals(id, eventDTO.id) &&
                Objects.equals(created, eventDTO.created) &&
                Objects.equals(updated, eventDTO.updated) &&
                summary.equals(eventDTO.summary) &&
                Objects.equals(description, eventDTO.description) &&
                Objects.equals(location, eventDTO.location) &&
                Objects.equals(url, eventDTO.url) &&
                Objects.equals(creator, eventDTO.creator) &&
                Objects.equals(organizer, eventDTO.organizer) &&
                start.equals(eventDTO.start) &&
                end.equals(eventDTO.end) &&
                Arrays.equals(recurrence, eventDTO.recurrence) &&
                Objects.equals(recurringEventId, eventDTO.recurringEventId) &&
                Arrays.equals(attendees, eventDTO.attendees) &&
                visibility == eventDTO.visibility &&
                eventStatus == eventDTO.eventStatus;
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, created, updated, summary, description, location, url, creator, organizer, start, end, endTimeUnspecified, isRecurring, isAllDay, recurringEventId, visibility, eventStatus);
        result = 31 * result + Arrays.hashCode(recurrence);
        result = 31 * result + Arrays.hashCode(attendees);
        return result;
    }
}
