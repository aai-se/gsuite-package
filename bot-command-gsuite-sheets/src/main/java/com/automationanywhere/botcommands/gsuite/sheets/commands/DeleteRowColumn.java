/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */

package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.*;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.utils.Utils;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Idx.Option;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.MatchesRegex;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Map;

@BotCommand
@CommandPkg(name = "DeleteRowColumn", label = "Delete Row/Column", node_label = ": {{deleteRowOption}} |{{deleteColumnOption}} |{{rowIndex}} |{{columnIndex}} |{{deleteRowByOption}} |{{deleteColumnByOption}} |{{rowRange}} |{{columnRange}} |into current worksheet |in session {{sessionName}}", description = "Deletes row or column in worksheet")
public class DeleteRowColumn {
	private static final Logger LOGGER = LogManager.getLogger(DeleteRowColumn.class);

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public BooleanValue execute(
			@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String sessionName,
			@Idx(index = "2", type = AttributeType.RADIO, options = {
					@Option(index = "2.1", pkg = @Pkg(label = "Row Operations", value = ROWOPERATION)),
					@Option(index = "2.2", pkg = @Pkg(label = "Column Operations", value = COLUMNOPERATION)) }) @Pkg(label = "", default_value_type = DataType.STRING, default_value = ROWOPERATION) @NotEmpty String operationTypeOption,
			@Idx(index = "2.1.1", type = AttributeType.RADIO, options = {
					@Option(index = "2.1.1.1", pkg = @Pkg(label = "Delete Row(s) at", value = DELETEROWAT)),
					@Option(index = "2.1.1.2", pkg = @Pkg(label = "Delete row by", value = DELETEROWBY)) }) @Pkg(label = "", default_value_type = DataType.STRING, default_value = DELETEROWAT) @NotEmpty String deleteRowOption,
			@Idx(index = "2.1.1.1.1", type = AttributeType.TEXT) @Pkg(label = "", description = "Enter the row index") @MatchesRegex(rowIndexValidator) @VariableType(value = DataType.STRING) @NotEmpty String rowIndex,
			@Idx(index = "2.1.1.2.1", type = AttributeType.RADIO, options = {
					@Option(index = "2.1.1.2.1.1", pkg = @Pkg(label = LABEL_ACTIVECELL, value = ACTIVECELL)),
					@Option(index = "2.1.1.2.1.2", pkg = @Pkg(label = "Range", value = RANGE)) }) @Pkg(label = "", default_value_type = DataType.STRING, default_value = ACTIVECELL) @NotEmpty String deleteRowByOption,
			@Idx(index = "2.1.1.2.1.2.1", type = AttributeType.TEXT) @Pkg(label = "", description = "For single row e.g. 10 and for multiple rows e.g. 1:10") @MatchesRegex(rowRangeValidator) @VariableType(value = DataType.STRING) @NotEmpty String rowRange,

			@Idx(index = "2.2.1", type = AttributeType.RADIO, options = {
					@Option(index = "2.2.1.1", pkg = @Pkg(label = "Delete Column(s) at", value = DELETECOLUMNAT)),
					@Option(index = "2.2.1.2", pkg = @Pkg(label = "Delete column by", value = DELETECOLUMNBY)) }) @Pkg(label = "", default_value_type = DataType.STRING, default_value = DELETECOLUMNAT) @NotEmpty String deleteColumnOption,
			@Idx(index = "2.2.1.1.1", type = AttributeType.TEXT) @Pkg(label = "", description = "Enter the column address") @MatchesRegex(columnIndexValidator) @VariableType(value = DataType.STRING) @NotEmpty String columnIndex,
			@Idx(index = "2.2.1.2.1", type = AttributeType.RADIO, options = {
					@Option(index = "2.2.1.2.1.1", pkg = @Pkg(label = LABEL_ACTIVECELL, value = ACTIVECELL)),
					@Option(index = "2.2.1.2.1.2", pkg = @Pkg(label = "Range", value = RANGE)) }) @Pkg(label = "", default_value_type = DataType.STRING, default_value = ACTIVECELL) @NotEmpty String deleteColumnByOption,
			@Idx(index = "2.2.1.2.1.2.1", type = AttributeType.TEXT) @Pkg(label = "", description = "For single column e.g. B and for multiple columns e.g. B:D") @MatchesRegex(columnRangeValidator) @VariableType(value = DataType.STRING) @NotEmpty String columnRange) {
		try {
			GSheets gSheets = (GSheets) sessionMap.get(sessionName);
			ExecutorValidator.getInstance().validate(gSheets);
			if (ROWOPERATION.equals(operationTypeOption)) {
				if (DELETEROWAT.equals(deleteRowOption)) {
					return new BooleanValue(gSheets.deleteRowColumn(ROWS, SPECIFICCELL, rowIndex));
				} else {
					if (ACTIVECELL.equals(deleteRowByOption)) {
						return new BooleanValue(gSheets.deleteRowColumn(ROWS, ACTIVECELL, EMPTY));
					} else {
						return new BooleanValue(gSheets.deleteRowColumn(ROWS, RANGE, rowRange));
					}
				}
			} else {
				if (DELETECOLUMNAT.equals(deleteColumnOption)) {
					return new BooleanValue(gSheets.deleteRowColumn(COLUMNS, SPECIFICCELL,
							String.valueOf(Utils.getColIndex(columnIndex))));
				} else {
					if (ACTIVECELL.equals(deleteColumnByOption)) {
						return new BooleanValue(gSheets.deleteRowColumn(COLUMNS, ACTIVECELL, EMPTY));
					} else {
						return new BooleanValue(gSheets.deleteRowColumn(COLUMNS, RANGE, columnRange));
					}
				}
			}
		} catch (IOException e) {
			LOGGER.error("exception in deleting row/ column: ", e);
			String msg = e.getMessage();
			throw new BotCommandException(msg, e);
		} 
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}