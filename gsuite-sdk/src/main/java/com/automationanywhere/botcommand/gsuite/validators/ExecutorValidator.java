/*
 * Copyright (c) 2018 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommand.gsuite.validators;

import com.automationanywhere.botcommand.gsuite.api.GoogleSuite;
import com.automationanywhere.botcommand.gsuite.messages.PropertiesReader;

import java.util.Objects;
import java.util.function.Predicate;

public class ExecutorValidator implements Validator<Object> {

    private static final Validator<Object> INSTANCE = new ExecutorValidator();

    private ExecutorValidator() {
        super();
    }

    public static Validator<Object> getInstance() {
        return INSTANCE;
    }


    @Override
    public void validate(Object gSheets) {
        Predicate<Object> predicateNotNull = Objects::nonNull;
        GenericValidator.validate(predicateNotNull.negate(), gSheets, PropertiesReader.getValue("sheet.notfound.in.session"));
        Predicate<Object> predicate = object -> object instanceof GoogleSuite;
        GenericValidator.validate(predicate.negate(), gSheets, PropertiesReader.getValue("sheet.notfound.in.session"));
    }
}
