/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static com.automationanywhere.botcommand.gsuite.Constants.COLUMNS;
import static com.automationanywhere.botcommand.gsuite.Constants.ROWS;
import static com.automationanywhere.botcommand.gsuite.Constants.SPECIFICCELL;
import static com.automationanywhere.botcommand.gsuite.Constants.ACTIVECELL;

import com.automationanywhere.botcommand.gsuite.Constants;
import com.automationanywhere.botcommand.gsuite.api.impl.GSheetsImpl;
import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommands.gsuite.sheets.commands.InsertCell;
import com.automationanywhere.botcommands.gsuite.sheets.commands.OpenSheets;
import com.automationanywhere.botcommands.gsuite.sheets.test.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

public class InsertCellJunit extends AbstractAuthUtils {
	InsertCell insertCell ;
	OpenSheets openSheets;
	GSheetsImpl gSheets;

	final String session = "session";
	public final static SecureString USERNAME =  ConstantsTest.getUserName();

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {
		launchOAuthConsentScreen(ConstantsTest.getUserName(), ConstantsTest.getClientid(),
				ConstantsTest.getClientSecret(), ConstantsTest.getRedirectUri());
		openSheets = new OpenSheets();
		insertCell = new InsertCell();
		openSheets.setSessionMap(sessionMap);
		insertCell.setSessionMap(sessionMap);
		openSheets.execute(USERNAME, session, "BYNAME", "Demo111", null, null, false, null, false);
		
	}
	

	@Test
	public void insertCellTest() throws IOException {
//		insertCell.execute(session, SPECIFICCELL,"A10", ROWS);
	
		insertCell.execute(session, SPECIFICCELL,"A10", COLUMNS);
		
		insertCell.execute(session, ACTIVECELL,"", ROWS);
		
		
		
		
		
//		insertCell.execute(session, ACTIVECELL,"", ROWS);

//		insertCell.execute(session, SPECIFICCELL,"A10", "left");
		
//		insertCell.execute(session, SPECIFICCELL,"A10000", "left");
	}
}
