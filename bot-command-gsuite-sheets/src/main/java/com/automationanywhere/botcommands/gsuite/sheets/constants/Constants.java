package com.automationanywhere.botcommands.gsuite.sheets.constants;

public class Constants {
	public static final String LABEL_ACTIVECELL = "Active cell";
	public static final String LABEL_SPECIFICCELL = "Specific cell";
	public static final String ROWS = "ROWS";
	public static final String COLUMNS = "COLUMNS";
	public static final String LABEL_SESSION = "Session name";
	public static final String DESCRIPTION_SESSION = "e.g. Session1 or S1";
	public static final String DESCRIPTION_CELL_ADDRESS = "e.g. A1";
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	public static final String ACTIVECELL = "ACTIVECELL";
	public static final String RANGE = "RANGE";
	public static final String DEFAULT_VALUE_SESSION = "Default";
	public static final String SPECIFICCELL = "SPECIFICCELL";
	public static final int MAX_COLUMN_VALUE = 16384; // XFD
	public static final int MAX_ROW_VALUE = 1048576;
	public static final String rowIndexValidator = "^((10[0-4][0-8][0-5][0-7][0-6])|([1-9][0-9]{1,5})|([1-9]))$";
	public static final String rowRangeValidator = "^((10[0-4][0-8][0-5][0-7][0-6])|([1-9][0-9]{1,5})|([1-9])):((10[0-4][0-8][0-5][0-7][0-6])|([1-9][0-9]{1,5})|([1-9]))$";
	public static final String columnRangeValidator = "^(([A-W][A-Z]{1,2})|(X[A-E][A-Z])|(XF[A-D])|([A-Z]{1,2})):(([A-W][A-Z]{1,2})|(X[A-E][A-Z])|(XF[A-D])|([A-Z]{1,2}))$";
	public static final String columnIndexValidator = "^(([A-W][A-Z]{1,2})|(X[A-E][A-Z])|(XF[A-D])|([A-Z]{1,2}))$";
	public static final String cellAddressValidator = "^(([A-W][A-Z]{1,2})|(X[A-E][A-Z])|(XF[A-D])|([A-Z]{1,2}))((10[0-4][0-8][0-5][0-7][0-6])|([1-9][0-9]{1,5})|([1-9]))$";
	public static final String rangeValidator = "^(([A-W][A-Z]{1,2})|(X[A-E][A-Z])|(XF[A-D])|([A-Z]{1,2}))((10[0-4][0-8][0-5][0-7][0-6])|([1-9][0-9]{1,5})|([1-9])):(([A-W][A-Z]{1,2})|(X[A-E][A-Z])|(XF[A-D])|([A-Z]{1,2}))((10[0-4][0-8][0-5][0-7][0-6])|([1-9][0-9]{1,5})|([1-9]))$";
	public static final String SHEET_NAME_VALIDATOR = "^[^\\\\\\/\\?\\*\\[\\]]{1,31}$";
	public static final String workbookPathValidator = "^([a-zA-Z0-9 ]+[/])*([a-zA-Z0-9 ])+[.](xls[xm]?)$";
	public static final String NONEMPTYROWS="NONEMPTYROWS";
	public static final String TOTALROWS="TOTALROWS";
	public static final String EMPTY = "";
	public static final String HIDDEN = "hidden";
	public static final String NAME_DESC = "Name of the spreadsheet";
	public static final String PATH_DESC = "Path where to create the spreadsheet.If empty, file will be created under My-Drive.";
}
