package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.*;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommands.gsuite.sheets.commands.DeleteWorksheet;
import com.automationanywhere.botcommands.gsuite.sheets.commands.OpenSheets;
import com.automationanywhere.botcommands.gsuite.sheets.test.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

public class DeleteWorksheetJunit extends AbstractAuthUtils {
	DeleteWorksheet deleteWorksheet;
	OpenSheets openSheets;
	final String session = "session";
	public static final SecureString USERNAME = ConstantsTest.getUserName();
	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {
		launchOAuthConsentScreen(ConstantsTest.getUserName(), ConstantsTest.getClientid(),
				ConstantsTest.getClientSecret(), ConstantsTest.getRedirectUri());
		deleteWorksheet = new DeleteWorksheet();
		deleteWorksheet.setSessionMap(sessionMap);
		openSheets = new OpenSheets();
		openSheets.setSessionMap(sessionMap);
//		openSheets.execute(USERNAME, session, "BYNAME", "Demo", null, null, true, "Sheet42", false);
		openSheets.execute(USERNAME, session, "BYNAME", "Demo", null, null, false, null, false);
	}

	@Test
	public void DeleteWorksheetTest()  {
//		deleteWorksheet.execute(session, FALSE, null, "ShEEt44");
//		deleteWorksheet.execute(session, TRUE, 19, null);
	}

}
