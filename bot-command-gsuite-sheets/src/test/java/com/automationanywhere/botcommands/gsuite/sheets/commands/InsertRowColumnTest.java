//package com.automationanywhere.botcommands.gsuite.sheets.commands;
//
//import static org.mockito.Mockito.when;
//
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.Map;
//
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.mockito.Spy;
//import org.testng.Assert;
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.Test;
//
//import com.automationanywhere.botcommand.exception.BotCommandException;
//import com.automationanywhere.botcommand.gsuite.api.GSheets;
//import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
//
//public class InsertRowColumnTest {
//	 @Spy
//     InsertRowColumn insertRowColumn;
//
//     @Mock
//     AuthenticationService authenticationService;
//
//     @Mock
//     GSheets mockGSheets;
//
//
//     final String session = "session";
//
//
//     Map<String, Object> sessionMap = new HashMap<>();
//
//     @BeforeMethod
//     public void setup() {
//         MockitoAnnotations.initMocks(this);
//         sessionMap.put("session", mockGSheets);
//         insertRowColumn.setSessionMap(sessionMap);
//     }
//
//     @Test
//     public void deleteCellShiftLeft_true() {
//         try {
//             when(mockGSheets.getSpreadSheetId()).thenReturn("1");
//             when(mockGSheets.insertRowColumn(mockGSheets.getSpreadSheetId(), "ROWS", "ACTIVECELL", ""))
//                     .thenReturn(true);
//             Assert.assertTrue(insertRowColumn.execute(mockGSheets.getSpreadSheetId(), "ROWS", "ACTIVECELL", ""));
//         } catch (IOException e) {
//             Assert.fail();
//         }
//     }
//
//     @Test
//     public void deleteDimensionRows_true() {
//         try {
//             when(mockGSheets.getSpreadSheetId()).thenReturn("1");
//             when(mockGSheets.deleteDimension("1", "B1", "ROWS"))
//                     .thenReturn(true);
//         } catch (IOException e) {
//             Assert.fail();
//         }
//     }
//
//     @Test(expectedExceptions = BotCommandException.class)
//     public void throwIOException_BotCommandException(){
//         try {
//             when(mockGSheets.getSpreadSheetId()).thenReturn("1");
//             when(mockGSheets.deleteDimension("1", "A1","ROWS"))
//                     .thenThrow(new IOException());
//             deleteCell.execute("session", "false", "A1", "ROWS");
//         } catch(IOException e) {
//             Assert.fail();
//         }
//     }
//}
