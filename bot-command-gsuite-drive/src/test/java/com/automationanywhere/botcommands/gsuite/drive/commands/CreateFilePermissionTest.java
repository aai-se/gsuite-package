/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommands.gsuite.drive.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

/**
 * Tests create Google Drive file permission functionality.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

public class CreateFilePermissionTest  {

	public static final SecureString USERNAME = ConstantsTest.getUserName();

	@Spy
	CreateFilePermission createFilePermission;

	@Mock
	GDrive mockGDrive;

	@Mock
	AuthenticationService mockAuthService;

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		createFilePermission.setAuthenticationService(mockAuthService);
	}

	@Test
	public void createsPermission_returnsNewPermission_forWriterRole_UserGrantee() {
		try {
			Mockito.doReturn(mockGDrive).when(createFilePermission).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			String role = "writer";
			String type = "user";
			String permissionId = "test_permission123";
			String emailAddress = "abc@aademo.page";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			JSONObject inputFileParamID = new JSONObject();
			inputFileParamID.put("fileId", fileId);

			JSONObject otherParam = new JSONObject();
			otherParam.put("role", role);
			otherParam.put("type", type);
			otherParam.put("emailAddress", emailAddress);

			String expectedPermission = "{\"deleted\":false,\"displayName\":\"Saanchi Muthya\",\"emailAddress\":\"abc@aademo.page\",\"id\":"
					+ permissionId + ",\"kind\":\"drive#permission\",\"role\":" + role + ",\"type\":" + type + "}";
			when(mockGDrive.createPermission(inputFileParamName.toString(), otherParam.toString()))
					.thenReturn(expectedPermission);
			StringValue actualPermission = createFilePermission.execute(USERNAME, "BYNAME", fileName, fileId, role,
					false, type, emailAddress, emailAddress, "");
			Assert.assertEquals(actualPermission.toString(), expectedPermission);

			when(mockGDrive.createPermission(inputFileParamID.toString(), otherParam.toString()))
					.thenReturn(expectedPermission);
			actualPermission = createFilePermission.execute(USERNAME, "BYID", fileName, fileId, role, false, type,
					emailAddress, emailAddress, "");
			Assert.assertEquals(actualPermission.toString(), expectedPermission);
		} catch (GeneralSecurityException | IOException | JSONException e) {
			Assert.fail();
		}
	}

	@Test
	public void createsPermission_returnsNewPermission_forOwnerRole_GroupGrantee() {
		try {
			Mockito.doReturn(mockGDrive).when(createFilePermission).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			String role = "owner";
			String type = "group";
			String permissionId = "test_permission123";
			String emailAddress = "abc@aademo.page";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			JSONObject otherParam = new JSONObject();
			otherParam.put("role", role);
			otherParam.put("type", type);
			otherParam.put("emailAddress", emailAddress);
			otherParam.put("transferOwnership", true);

			String expectedPermission = "{\"deleted\":false,\"displayName\":\"Saanchi Muthya\",\"emailAddress\":\"abc@aademo.page\",\"id\":"
					+ permissionId + ",\"kind\":\"drive#permission\",\"role\":" + role + ",\"type\":" + type + "}";
			when(mockGDrive.createPermission(inputFileParamName.toString(), otherParam.toString()))
					.thenReturn(expectedPermission);
			StringValue actualPermission = createFilePermission.execute(USERNAME, "BYNAME", fileName, fileId, role,
					true, type, emailAddress, emailAddress, "");
			Assert.assertEquals(actualPermission.toString(), expectedPermission);
		} catch (GeneralSecurityException | IOException | JSONException e) {
			Assert.fail();
		}
	}

	@Test
	public void createsPermission_returnsNewPermission_forReaderRole_DomainGrantee() {
		try {
			Mockito.doReturn(mockGDrive).when(createFilePermission).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			String role = "reader";
			String type = "domain";
			String domain = "aademo.page";
			String permissionId = "test_permission123";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			JSONObject otherParam = new JSONObject();
			otherParam.put("role", role);
			otherParam.put("type", type);
			otherParam.put("domain", domain);

			String expectedPermission = "{\"deleted\":false,\"displayName\":\"Saanchi Muthya\",\"domain\":" + domain
					+ ",\"id\":" + permissionId + ",\"kind\":\"drive#permission\",\"role\":" + role + ",\"type\":"
					+ type + "}";
			when(mockGDrive.createPermission(inputFileParamName.toString(), otherParam.toString()))
					.thenReturn(expectedPermission);
			StringValue actualPermission = createFilePermission.execute(USERNAME, "BYNAME", fileName, fileId, role,
					false, type, "", "", domain);
			Assert.assertEquals(actualPermission.toString(), expectedPermission);
		} catch (GeneralSecurityException | IOException | JSONException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void createsPermission_throwsBotCommandException_whenCreatePermissionAPIthrowsIOException() {
		try {
			Mockito.doReturn(mockGDrive).when(createFilePermission).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			String role = "writer";
			String type = "user";
			String permissionId = "test_permission123";
			String emailAddress = "abc@aademo.page";

			JSONObject otherParam = new JSONObject();
			otherParam.put("role", role);
			otherParam.put("type", type);
			otherParam.put("emailAddress", emailAddress);

			when(mockGDrive.createPermission(inputFileParamName.toString(), otherParam.toString()))
					.thenThrow(IOException.class);
			createFilePermission.execute(USERNAME, "BYNAME", fileName, fileId, role, false, type, emailAddress,
					emailAddress, "");

			Assert.fail();
		} catch (GeneralSecurityException | JSONException e) {
			Assert.fail();
		} catch (IOException e) {
			Assert.assertTrue(true);
		}
	}

	@Test
	public void getDrive_newGDriveImplObjectIsNotNull() {
		try {
			GDrive drive = createFilePermission.getDrive(USERNAME);
			assertNotNull(drive);
		} catch (GeneralSecurityException | IOException e) {
			fail();
		}
	}
}
