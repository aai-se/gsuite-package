package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;

public class HideWorkSheetTest {

	@Spy
	HideWorksheet hideWorksheet;

	@Mock
	GSheets mockGSheets;

	final String session = "session";
	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		sessionMap.put(session, mockGSheets);
		hideWorksheet.setSessionMap(sessionMap);

	}

	@Test
	public void hideWorkSheetTest() throws IOException {
		when(mockGSheets.hideWorksheet(anyString())).thenReturn(true);
		Boolean response = hideWorksheet.execute(session, "MyWorkSheet").get();
		Assert.assertTrue(response);

	}

	@Test(expectedExceptions = BotCommandException.class)
	public void hideWorkSheetInvalidSession() throws IOException {
		when(mockGSheets.hideWorksheet(anyString())).thenReturn(true);
		Boolean response = hideWorksheet.execute("default", "MyWorkSheet").get();
		Assert.assertTrue(response);
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void hideWorkSheetInvalidSheet() throws IOException {
		when(mockGSheets.hideWorksheet("MyWorkSheet")).thenThrow(new IOException());
		Boolean response = hideWorksheet.execute(session, "MyWorkSheet").get();
		Assert.assertTrue(response);
	}
}
