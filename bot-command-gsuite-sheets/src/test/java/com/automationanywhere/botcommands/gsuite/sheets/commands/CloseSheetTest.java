/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */

package com.automationanywhere.botcommands.gsuite.sheets.commands;

import com.automationanywhere.bot.service.Bot;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;


public class CloseSheetTest {
    @Spy
    CloseSheet closeSheet;
  //  ReadRow readRow;

    @Mock
    AuthenticationService authenticationService;

    @Mock
    GSheets mockGSheets;


    final String session = "session";


    Map<String, Object> sessionMap = new HashMap<>();

    @BeforeMethod
    public void setup() {
        MockitoAnnotations.initMocks(this);
        sessionMap.put("session", mockGSheets);
        closeSheet.setSessionMap(sessionMap);
    }


    @Test
    public void testCloseSheet(){
            when(mockGSheets.closeSpreadSheet()).thenReturn(true); //mock gsheets.closespreadsheet, then return true
            BooleanValue testId = closeSheet.execute(session);  //type booleanvalue or boolean?
            Assert.assertTrue(testId.get());                 //true, boolean value testid
    }
    @Test(expectedExceptions = BotCommandException.class)
    public void invalidSessionTest() {
    	closeSheet.execute("default");
    }

}
