package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.impl.GSheetsImpl;
import com.automationanywhere.botcommand.gsuite.helper.sheets.CellSplitter;
import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommand.gsuite.utils.Utils;
import com.automationanywhere.botcommands.gsuite.sheets.commands.ActivateSheet;
import com.automationanywhere.botcommands.gsuite.sheets.commands.OpenSheets;
import com.automationanywhere.botcommands.gsuite.sheets.test.utils.ConstantsTest;

public class ActivateSheetJunit extends AbstractAuthUtils {
	ActivateSheet activateSheet;
	OpenSheets openSheets;
	String session = "Default";
	GSheetsImpl gsheet;

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {

		launchOAuthConsentScreen(ConstantsTest.getUserName(), ConstantsTest.getClientid(),
				ConstantsTest.getClientSecret(), ConstantsTest.getRedirectUri());

		openSheets = new OpenSheets();
		openSheets.setSessionMap(sessionMap);
		openSheets.execute(ConstantsTest.getUserName(), session, "BYNAME", "Demo", null, null, true, "Megha", false);
		activateSheet = new ActivateSheet();
		activateSheet.setSessionMap(sessionMap);
		gsheet = new GSheetsImpl();
		gsheet.setSessionMap(sessionMap);

	}

	@Test
	public void activateSheetByIndex() {
		System.out
				.println("---------------------------------------- Activating Sheet----------------------------------");
		activateSheet.execute(session, "true", 1, null);
	}

	@Test
	public void activateSheetByName() {

		System.out
				.println("---------------------------------------- Activating Sheet----------------------------------");
		activateSheet.execute(session, "false", 1, "Sheet24");

	}

	@Test(expectedExceptions = BotCommandException.class)
	public void activateSheetByWrongIndex() {
		System.out
				.println("---------------------------------------- Activating Sheet----------------------------------");
		activateSheet.execute(session, "true", 111, null);
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void activateSheetByWrongName() {
		System.out
				.println("---------------------------------------- Activating Sheet----------------------------------");
		activateSheet.execute(session, "false", 1, "Sheet2as4");
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void activateSheetByNullIndex() {
		System.out
				.println("---------------------------------------- Activating Sheet----------------------------------");
		activateSheet.execute(session, "false", null, "Sheet2as4");
	}

	@Test(expectedExceptions = NullPointerException.class)
	public void activateSheetByNullName() {
		System.out
				.println("---------------------------------------- Activating Sheet----------------------------------");
		activateSheet.execute(session, "false", 1, null);
	}

//	@Test
//	public void zCellSplitterTest() {
//		String[] address = { "A1", "B32000", "AA1", "AB2","ZZZ1" };
//		for (int i = 0; i < address.length; i++) {
//			CellSplitter cellSplitter = new CellSplitter();
//			cellSplitter.splitCell(address[i]);
//			int row = Integer.parseInt(cellSplitter.getRowName());
//			String columnName = cellSplitter.getColumnName();
//			int column = Utils.getColIndex(cellSplitter.getColumnName());
//			String columnName2 = Utils.getColName(column);
//			System.out.println(
//					"-----------------------------------------Address-------------------------------------------------:"
//							+ address[i]);
//
//			System.out.println(
//					"-----------------------------------------Row-------------------------------------------------:"
//							+ row);
//			System.out.println(
//					"-----------------------------------------Column name-------------------------------------------------:"
//							+ columnName);
//			System.out.println(
//					"-----------------------------------------Column-------------------------------------------------:"
//							+ column);
//			System.out.println(
//					"-----------------------------------------Column name 2-------------------------------------------------:"
//							+ columnName2);
//			System.out.println(
//					"------------------------------------------------------------------------------------------");
//
//		}
//	}
}
