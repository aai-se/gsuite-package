package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.gsuite.api.impl.GSheetsImpl;
import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommands.gsuite.sheets.commands.DeleteCell;
import com.automationanywhere.botcommands.gsuite.sheets.commands.OpenSheets;
import com.automationanywhere.botcommands.gsuite.sheets.test.utils.ConstantsTest;

public class DeleteCellJUnit extends AbstractAuthUtils {
	DeleteCell deleteCell;
	OpenSheets openSheets;
	String session = "Default";
	GSheetsImpl gsheet;

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {

		launchOAuthConsentScreen(ConstantsTest.getUserName(), ConstantsTest.getClientid(),
				ConstantsTest.getClientSecret(), ConstantsTest.getRedirectUri());

		openSheets = new OpenSheets();
		openSheets.setSessionMap(sessionMap);
		openSheets.execute(ConstantsTest.getUserName(), session, "BYNAME", "Demo", null, null, true, "Megha", false);
		deleteCell = new DeleteCell();
		deleteCell.setSessionMap(sessionMap);
		gsheet = new GSheetsImpl();
		gsheet.setSessionMap(sessionMap);

	}

	@Test
	public void deleteCellSpecificShift() {
		deleteCell.execute(session, "true", "C3", "UP");
		deleteCell.execute(session, "false", "C3", "UP");
		deleteCell.execute(session, "false", "D5", "LEFT");
		deleteCell.execute(session, "false", "C3", "ROWS");
		deleteCell.execute(session, "false", "C3", "COLUMNS");
	}
	

}