/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.USERNAME;
import static com.automationanywhere.commandsdk.model.AttributeType.CHECKBOX;
import static com.automationanywhere.commandsdk.model.AttributeType.RADIO;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.api.impl.GDriveImpl;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommand.gsuite.service.impl.AuthenticationServiceImpl;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import com.google.common.annotations.VisibleForTesting;

/**
 * Creates permission for a Google Drive file.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

@BotCommand
@CommandPkg(label = "Create file permission", name = "CreateFilePermission", description = "Create a new permission for a file", node_label = "for file |with name {{fileName}}|with id {{fileId}}|having role as {{role}} and grantee type as {{granteeType}}", return_type = STRING, return_required = false, return_label = "Returns the newly created permission")
public class CreateFilePermission {
	private static final Logger LOGGER = LogManager.getLogger(CreateFilePermission.class);
	private static final String NAME_DESC = "e.g. MyFolder/MyFile.doc";
	private static final String ID_DESC = "e.g. 17dIAwvrEA4JeLysfFky9oZL0qAzesY0IdbOu-AvHB1U from document "
			+ "link https://docs.google.com/spreadsheets/d/17dIAwvrEA4JeLysfFky9oZL0qAzesY0IdbOu-AvHB1U/edit#gid=0";
	private AuthenticationService mAuthenticationService;

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public StringValue execute(
			@Idx(index = "1", type = AttributeType.CREDENTIAL) @Pkg(label = USERNAME) @NotEmpty SecureString userEmailAddress,
			@Idx(index = "2", type = RADIO, options = {
					@Idx.Option(index = "2.1", pkg = @Pkg(label = "File name (with location)", value = "BYNAME")),
					@Idx.Option(index = "2.2", pkg = @Pkg(label = "File ID", value = "BYID")) }) @Pkg(label = "Create permission for file", default_value_type = DataType.STRING, default_value = "BYNAME") @NotEmpty String fileOption,
			@Idx(index = "2.1.1", type = TEXT) @Pkg(label = "", description = NAME_DESC, default_value = "", default_value_type = DataType.STRING) @NotEmpty String fileName,
			@Idx(index = "2.2.1", type = TEXT) @Pkg(label = "", description = ID_DESC, default_value = "", default_value_type = DataType.STRING) @NotEmpty String fileId,
			@Idx(index = "3", type = AttributeType.SELECT, options = {
					@Idx.Option(index = "3.1", pkg = @Pkg(label = "Owner", value = "owner")),
					@Idx.Option(index = "3.2", pkg = @Pkg(label = "Organizer", value = "organizer")),
					@Idx.Option(index = "3.3", pkg = @Pkg(label = "File organizer", value = "fileOrganizer")),
					@Idx.Option(index = "3.4", pkg = @Pkg(label = "Writer", value = "writer")),
					@Idx.Option(index = "3.5", pkg = @Pkg(label = "Commenter", value = "commenter")),
					@Idx.Option(index = "3.6", pkg = @Pkg(label = "Reader", value = "reader")) }) @Pkg(label = "Roles", default_value_type = STRING, default_value = "reader") @NotEmpty String role,
			@Idx(index = "3.1.1", type = CHECKBOX) @Pkg(label = "Transfer ownership") @NotEmpty Boolean transferOwnership,
			@Idx(index = "4", type = AttributeType.SELECT, options = {
					@Idx.Option(index = "4.1", pkg = @Pkg(label = "User", value = "user")),
					@Idx.Option(index = "4.2", pkg = @Pkg(label = "Group", value = "group")),
					@Idx.Option(index = "4.3", pkg = @Pkg(label = "Domain", value = "domain")),
					@Idx.Option(index = "4.4", pkg = @Pkg(label = "Anyone", value = "anyone")) }) @Pkg(label = "Grantee type", default_value_type = STRING, default_value = "user") @NotEmpty String granteeType,
			@Idx(index = "4.1.1", type = TEXT) @Pkg(label = "Email address") @NotEmpty String userEmail,
			@Idx(index = "4.2.1", type = TEXT) @Pkg(label = "Email address") @NotEmpty String groupEmail,
			@Idx(index = "4.3.1", type = TEXT) @Pkg(label = "Domain name") @NotEmpty String domain) {
		try {
			JSONObject inputFileParam = new JSONObject();
			if (fileOption.equals("BYNAME")) {
				inputFileParam.put("fileName", fileName);
			} else if (fileOption.equals("BYID")) {
				inputFileParam.put("fileId", fileId);
			}
			JSONObject otherParam = new JSONObject();
			otherParam.put("role", role);
			if (role.equals("owner")) {
				otherParam.put("transferOwnership", transferOwnership);
			}
			otherParam.put("type", granteeType);
			if (granteeType.equals("group")) {
				otherParam.put("emailAddress", groupEmail);
			} else if (granteeType.equals("domain")) {
				otherParam.put("domain", domain);
			} else {
				otherParam.put("emailAddress", userEmail);
			}

			return new StringValue(
					getDrive(userEmailAddress).createPermission(inputFileParam.toString(), otherParam.toString()));
		} catch (GeneralSecurityException | IOException | JSONException e) {
			LOGGER.error("Exception in finding Google Drive file: ", e);
			String msg = e.getMessage();
			throw new BotCommandException(msg, e);
		}
	}

	GDrive getDrive(SecureString userEmailAddress) throws GeneralSecurityException, IOException {
		AuthenticationService authenticationService = getAuthenticationService();
		return new GDriveImpl(authenticationService, userEmailAddress.getInsecureString(), sessionMap);
	}

	@VisibleForTesting
	void setAuthenticationService(AuthenticationService mAuthenticationService) {
		this.mAuthenticationService = mAuthenticationService;
	}

	synchronized AuthenticationService getAuthenticationService() {
		if (mAuthenticationService == null) {
			mAuthenticationService = new AuthenticationServiceImpl();
		}
		return mAuthenticationService;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
}
