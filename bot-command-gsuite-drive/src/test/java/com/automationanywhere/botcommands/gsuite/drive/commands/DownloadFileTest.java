/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommands.gsuite.drive.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

/**
 * Tests download Google Drive file functionality.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

public class DownloadFileTest {

	public static final SecureString USERNAME = ConstantsTest.getUserName();

	@Spy
	DownloadFile downloadFile;

	@Mock
	GDrive mockGDrive;

	@Mock
	AuthenticationService mockAuthService;

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {
		MockitoAnnotations.initMocks(this);
		downloadFile.setAuthenticationService(mockAuthService);
	}

	@Test
	public void downloadFile_returnsDownloadedFileId_whenRequiredFileNameProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(downloadFile).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			JSONObject outputFileParam = new JSONObject();
			when(mockGDrive.downloadFile(inputFileParamName.toString(), "C:/Users/user/Downloads", false,
					outputFileParam.toString())).thenReturn(fileId);
			Value<String> testId = downloadFile.execute(USERNAME, "BYNAME", fileName, fileId, "C:/Users/user/Downloads",
					false, false, "newtrial_file.txt");
			Assert.assertEquals(fileId, testId.toString());
		} catch (GeneralSecurityException | IOException | JSONException e) {
			Assert.fail();
		}
	}

	@Test
	public void downloadFile_returnsDownloadedFileId_whenRequiredFileIdProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(downloadFile).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			JSONObject inputFileParamID = new JSONObject();
			inputFileParamID.put("fileId", fileId);

			JSONObject outputFileParam = new JSONObject();
			when(mockGDrive.downloadFile(inputFileParamID.toString(), "C:/Users/user/Downloads", false,
					outputFileParam.toString())).thenReturn(fileId);
			Value<String> testId = downloadFile.execute(USERNAME, "BYID", fileName, fileId, "C:/Users/user/Downloads",
					false, false, "newtrial_file.txt");
			Assert.assertEquals(fileId, testId.toString());
		} catch (GeneralSecurityException | IOException | JSONException e) {
			Assert.fail();
		}
	}

	@Test
	public void downloadFile_returnsDownloadedFileId_renameDownloadedFileDuringDownload() {
		try {
			Mockito.doReturn(mockGDrive).when(downloadFile).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			String newFileName = "newtrial_file.txt";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			JSONObject outputFileParam = new JSONObject();
			outputFileParam.put("newFileName", newFileName);
			when(mockGDrive.downloadFile(inputFileParamName.toString(), "C:/Users/user/Downloads", false,
					outputFileParam.toString())).thenReturn(fileId);
			Value<String> testId = downloadFile.execute(USERNAME, "BYNAME", fileName, fileId, "C:/Users/user/Downloads",
					false, true, newFileName);
			Assert.assertEquals(fileId, testId.toString());
		} catch (GeneralSecurityException | IOException | JSONException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void downloadFile_throwsBotCommandException_whenDownloadFileAPIthrowsIOException() {
		try {
			Mockito.doReturn(mockGDrive).when(downloadFile).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			String newFileName = "newtrial_file.txt";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			JSONObject outputFileParam = new JSONObject();
			outputFileParam.put("newFileName", newFileName);
			when(mockGDrive.downloadFile(inputFileParamName.toString(), "C:/Users/user/Downloads", false,
					outputFileParam.toString())).thenThrow(IOException.class);
			downloadFile.execute(USERNAME, "BYNAME", fileName, fileId, "C:/Users/user/Downloads", false, true,
					newFileName);

			Assert.fail();
		} catch (GeneralSecurityException | JSONException e) {
			Assert.fail();
		} catch (IOException e) {
			Assert.assertTrue(true);
		}
	}

	@Test
	public void getDrive_newGDriveImplObjectIsNotNull() {
		try {
			GDrive drive = downloadFile.getDrive(USERNAME);
			assertNotNull(drive);
		} catch (GeneralSecurityException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
	}
}
