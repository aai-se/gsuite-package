/* Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.gsuite.api.GSheets;

public class RenameSheetTest {

	@Spy
	RenameSheet renameSheet;
	
	@Mock
	GSheets mockGSheets;

	final String session = "session";

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		sessionMap.put(session, mockGSheets);
		renameSheet = new RenameSheet();
		renameSheet.setSessionMap(sessionMap);
		when(mockGSheets.getSpreadSheetId()).thenReturn("2");
	}

	@Test
	public void reanmeWorksheetByIndexTest() {
		try {
			when(mockGSheets.renameSheet(anyBoolean(), anyInt(), anyString(), anyString())).thenReturn(true);
			Assert.assertTrue(renameSheet.execute(session,TRUE,10, "","Sheettest").get());  
		} catch (Exception i) {
			Assert.fail();
		}
	}
	
	@Test
	public void reanmeWorksheetByNameTest() {
		try {
			when(mockGSheets.renameSheet(anyBoolean(), anyInt(), anyString(), anyString())).thenReturn(true);
			Assert.assertTrue(renameSheet.execute(session,FALSE,null, "","Sheettest").get());  
		} catch (Exception i) {
			Assert.fail();
		}
	}

    @Test(expectedExceptions = Exception.class)
    public void botCommandExceptionTest(){
        try{
        	
        	when(mockGSheets.renameSheet(anyBoolean(), anyInt(), anyString(), anyString())).thenThrow(new IOException());
        	renameSheet.execute(session,"TRUE",10, "Sheet1","Sheettest");
        	}catch(IOException i){
            Assert.fail();
        }
    }
}
