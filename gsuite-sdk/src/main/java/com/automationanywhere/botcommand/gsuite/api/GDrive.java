/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommand.gsuite.api;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.json.JSONException;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.gsuite.dto.drive.FileDto;
import com.automationanywhere.botcommand.gsuite.enums.FileType;
import com.google.api.services.drive.model.File;

public interface GDrive extends GoogleSuite {

	List<File> parseFilePath(String filePath, String callingFunction) throws IOException, JSONException;

	/**
	 * Downloads the file and stores it in the destination folder on the local
	 * device determined by destFolder param The name of the file remains the same
	 * 
	 * @param inputFileParam : Contains file name or file Id of the the file to be
	 *                       downloaded
	 * @param destFolder     : Destination local device location
	 * @return downloaded file id
	 * @throws IOException,  JSONException
	 * @throws JSONException
	 */
	String downloadFile(String inputFileParam, String destFolder, Boolean overwriteFile, String outputFileParam)
			throws IOException, JSONException, JSONException;

	/**
	 * Uploads the file to the drive location determined by fileLocation If there
	 * exists a file by the same name, this method throws an IOException
	 * 
	 * @param fileLocation : Drive upload location
	 * @param toCreate     : Boolean flag indicates whether file needs to be newly
	 *                     created in the drive
	 * @return uploaded file id
	 * @throws IOException,  JSONException
	 * @throws JSONException
	 */
	String uploadFile(String fileLocation, String inputFileParam, Boolean toCreate)
			throws IOException, JSONException, JSONException;

	/**
	 * Lists file resources by searching for file name in the fileLocation path
	 * There can be multiple searches - exact string match to name (MAT), begins
	 * with name (BEG), ends with name (END), contains name in the file name (CON).
	 * 
	 * @param fileLocation : File path search location
	 * @param name         : String to be searched in fileLocation, this may be the
	 *                     file name or part of the file name
	 * @param searchType   : MAT, BEG, END, CON
	 * @return List of files matching search string
	 * @throws IOException, JSONException
	 */
	List<File> findFileReturnList(String fileLocation, String name, String searchType)
			throws IOException, JSONException;

	/**
	 * Creates a copy of the file and stores it in the same drive location. The new
	 * name of the copied file is determined by copiedName param If there are
	 * multiple files by the same name, this method will return the last iterated
	 * entry.
	 * 
	 * @param inputFileParam  : Contains file name or file Id of the the file to be
	 *                        copied
	 * @param outputFileParam : Contains folder name or folder Id of the the
	 *                        destination folder
	 * @return file id of copied file
	 * @throws IOException, JSONException
	 */
	String copyFile(String inputFileParam, String outputFileParam) throws IOException, JSONException;

	/**
	 * Creates a copy of the folder and stores it in the same drive location. The
	 * new name of the copied folder is determined by copiedName param. If there are
	 * multiple folders by the same name, this method will return the last iterated
	 * entry.
	 * 
	 * @param inputFileParam  : Contains folder name or folder Id of the the folder
	 *                        to be copied
	 * @param outputFileParam : Contains folder name or folder Id of the the
	 *                        destination folder
	 * @return folder id of copied folder
	 * @throws IOException, JSONException
	 */
	String copyFolder(String inputFileParam, String outputFileParam) throws IOException, JSONException;

	/**
	 * Moves the file and stores it in the destination drive location determined by
	 * destFolder param The name of the file remains the same
	 * 
	 * @param inputFileParam  : Contains file name or file Id of the the file to be
	 *                        moved
	 * @param outputFileParam : Contains folder name or folder Id of the the
	 *                        destination folder
	 * @return file id of moved file
	 * @throws IOException, JSONException
	 */
	String moveFile(String inputFileParam, String outputFileParam) throws IOException, JSONException;

	/**
	 * Moves the folder and stores it in the destination drive location determined
	 * by destFolder param The name of the folder remains the same
	 * 
	 * @param inputFolderParam  : Contains folder name or folder Id of the the file
	 *                          to be moved
	 * @param outputFolderParam : Contains folder name or folder Id of the the
	 *                          destination folder
	 * @return folder id of moved folder
	 * @throws IOException, JSONException
	 */
	String moveFolder(String inputFolderParam, String outputFolderParam) throws IOException, JSONException;

	/**
	 * Renames the file specified by a Google Drive location
	 * 
	 * @param inputFileParam : Contains file name or file Id of the the file to be
	 *                       renamed
	 * @param newFileName    : New file name of the file
	 * @return file id of renamed file
	 * @throws IOException, JSONException
	 */
	String renameFile(String inputFileParam, String newFileName) throws IOException, JSONException;

	/**
	 * Gets file information like - name, id, created by, creation date, modified
	 * by, modification date, parent folder path, parent folder id, version
	 * 
	 * @param inputFileParam : Contains file name or file Id of the the file to get
	 *                       information
	 * @return file information in key value pairs
	 * @throws IOException, JSONException
	 */
	Map<String, Value> getFileInformation(String inputFileParam) throws IOException, JSONException;

	/**
	 * Deletes file specified by file name or id
	 * 
	 * @param inputFileParam : Contains file name or file Id of the the file to be
	 *                       deleted
	 * @return true if deleted
	 * @throws IOException, JSONException
	 */
	Boolean deleteFile(String inputFileParam) throws IOException, JSONException;

	/**
	 * Deletes folder specified by folder name or id
	 * 
	 * @param inputFolderParam : Contains folder name or folder Id of the the folder
	 *                         to be deleted
	 * @return true if deleted
	 * @throws IOException, JSONException
	 */
	Boolean deleteFolder(String inputFolderParam) throws IOException, JSONException;

	/**
	 * Creates folder specified by folder name or id
	 * 
	 * @param inputFolderParam : Contains folder name and parent folder name to be
	 *                         created
	 * @return true if created
	 * @throws IOException, JSONException
	 */
	Boolean createFolder(String inputFolderParams) throws IOException, JSONException;

	/**
	 * Gets the list of file permissions.
	 * 
	 * @param inputFileParam : Contains file name or file Id of the the file to get
	 *                       information
	 * @return file permissions as a list
	 * @throws IOException, JSONException
	 */
	List<Value> getFilePermission(String inputFileParam) throws IOException, JSONException;

	/**
	 * Returns true if any permission read/write/delete found for the file/folder
	 * name
	 * 
	 * @param inputParam : Contains file or folder name
	 * @return true/false as per permissions found.
	 * @throws IOException, JSONException
	 */
	Boolean checkPermissions(String inputParam, String fileType) throws IOException, JSONException;

	/**
	 * Creates permission for Google Drive file.
	 * 
	 * @param inputFileParam : Contains file name or file Id of the the file to get
	 *                       information
	 * @param otherParam     : Contains role, (transferOwnership flag), granteeType,
	 *                       emailAddress (user or group)
	 * @return created permission resource as a string
	 * @throws IOException, JSONException
	 */
	String createPermission(String inputFileParam, String otherParam) throws IOException, JSONException;

	/**
	 * Updates permission of Google Drive file.
	 * 
	 * @param inputFileParam : Contains file name or file Id of the the file to get
	 *                       information
	 * @param extraParam     : Contains role, (transferOwnership flag)
	 * @param permissionId   : Permission id of the Google Drive Permission which is
	 *                       to be updated
	 * @return updated permission resource as a string
	 * @throws IOException, JSONException
	 */
	String updatePermission(String inputFileParam, String extraParam, String permissionId)
			throws IOException, JSONException;

	/**
	 * Deletes an existing permission of a Google Drive file.
	 * 
	 * @param inputFileParam : Contains file name or file Id of the the file to get
	 *                       information
	 * @param permissionId   : Permission id of the existing permission
	 * @return true if permission was successfully deleted
	 * @throws IOException, JSONException
	 */
	Boolean deletePermission(String inputFileParam, String permissionId) throws IOException, JSONException;

	/**
	 * Opens an existing Google Drive file/folder.
	 * 
	 * @param inputFileParam : Contains file name or file Id of the the file to get
	 *                       information
	 * @return true if file/folder was successfully opened
	 * @throws IOException,  JSONException
	 * @throws JSONException
	 */
	Boolean openFile(String inputFileParam) throws IOException, JSONException, JSONException;

	/**
	 * Returns the file information by its name. If there are multiple files by the
	 * same name, this method will return the last iterated entry.
	 * 
	 * @param name
	 * @return FileDto
	 * @throws IOException, JSONException
	 */
	FileDto findFile(String name) throws IOException, JSONException;

	/**
	 * Returns the file datatype by its name. If there are multiple files by the
	 * same name, this method will return the last iterated entry.
	 * 
	 * @param name
	 * @return File
	 * @throws IOException, JSONException
	 */
	File findFileType(String name) throws IOException, JSONException;

	/**
	 * Returns a list of all files on the drive.
	 * 
	 * @param isDeleted : If true, the list contains even the deleted files.
	 * @return
	 */
	List<FileDto> getAllFiles(boolean isDeleted) throws IOException, JSONException;

	/**
	 * Returns a list of all folders present on the drive.
	 * 
	 * @return
	 */
	List<FileDto> getAllFolders() throws IOException, JSONException;

	/**
	 * Creates a copy of the current file and returns the new id
	 * 
	 * @param fileId
	 * @param name
	 * @return
	 */
	String saveAs(String fileId, String name) throws IOException, JSONException;

	String getFileId() throws IOException, JSONException;

	String retrieveChanges(String inputFileParam) throws IOException, JSONException;

	List<Value> manageRevisions(String inputFileParam) throws IOException, JSONException;

	/**
	 * Creates a file in Google Drive.
	 * 
	 * @param name
	 * @param filePath
	 * @param type
	 * @return : Id of the file created
	 */
	String createFile(String name, String filePath, FileType type) throws IOException, JSONException;

	/**
	 * Checks if the file already exists at the specified path.
	 * 
	 * @param name : name of the file
	 * @param path : id of the folder where to check. This is assuming that the
	 *             complete path name has been already validated by the
	 *             {@code parseFilePath()} method.
	 * @return
	 */
	boolean fileExists(String name, String path) throws IOException, JSONException;

	/**
	 * Export supported file to PDF at a given folder
	 * 
	 * @param inputFileParam : Contains file name or file Id of the the file to get
	 *                       information
	 * @param otherParam     : Contains destination folder path as well as
	 *                       permission to override file if present
	 * @return created file ID
	 * @throws IOException, JSONException
	 */
	String exportAsPdf(String inputFileParam, String outputFolderParam) throws IOException, JSONException;

}
