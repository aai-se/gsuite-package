package com.automationanywhere.botcommands.gsuite.drive.utils;

import com.automationanywhere.core.security.SecureString;

public class ConstantsTest {

	public static final String CLIENTID = "1045113049707-u3fhdtroech2uch19jogs52fg98t3lpu.apps.googleusercontent.com";
	public static final String REDIRECT_URI = "urn:ietf:wg:oauth:2.0:oob";
	public static final String CLIENTSECRET = "6rjSOWzAEKEdiFqpCDGxirkk";
	public static final String USERNAME = "saanchi@aademo.page";

	public static SecureString getClientid() {
		return new SecureString(CLIENTID.toCharArray());
	}

	public static SecureString getRedirectUri() {
		return new SecureString(REDIRECT_URI.toCharArray());
	}

	public static SecureString getClientSecret() {
		return new SecureString(CLIENTSECRET.toCharArray());
	}

	public static SecureString getUserName() {
		return new SecureString(USERNAME.toCharArray());
	}
}