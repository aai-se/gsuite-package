/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommand.gsuite.api;

import com.automationanywhere.botcommand.gsuite.dto.calendar.EventDTO;

import java.io.IOException;

public interface GCalendar extends GoogleSuite {

    EventDTO createEvent(EventDTO event) throws IOException;

    boolean deleteEvent(EventDTO eventDTO) throws IOException;

    String getEventId();

}
