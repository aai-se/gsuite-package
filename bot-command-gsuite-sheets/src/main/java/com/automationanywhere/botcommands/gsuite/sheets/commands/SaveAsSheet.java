///*
// * Copyright (c) 2019 Automation Anywhere.
// * All rights reserved.
// *
// * This software is the proprietary information of Automation Anywhere.
// * You shall use it only in accordance with the terms of the license agreement
// * you entered into with Automation Anywhere.
// */
//
//package com.automationanywhere.botcommands.gsuite.sheets.commands;
//
//import com.automationanywhere.botcommand.data.impl.BooleanValue;
//import com.automationanywhere.botcommand.exception.BotCommandException;
//import com.automationanywhere.botcommand.gsuite.api.GDrive;
//import com.automationanywhere.botcommand.gsuite.api.GSheets;
//import com.automationanywhere.botcommand.gsuite.api.impl.GDriveImpl;
//import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
//import com.automationanywhere.botcommand.gsuite.service.impl.AuthenticationServiceImpl;
//import com.automationanywhere.commandsdk.annotations.*;
//import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
//import com.automationanywhere.commandsdk.model.DataType;
//import com.google.common.annotations.VisibleForTesting;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.json.JSONException;
//
//import java.io.IOException;
//import java.security.GeneralSecurityException;
//import java.util.Map;
//
//import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
//
////
////@BotCommand
////@CommandPkg(label = "Save As",
////        name = "SaveSpreadSheet",
////        description = "Saves a spreadsheet with a different name",
////        node_label = "{{spreadsheetName}}",
////        icon = "excel.svg")
//public class SaveAsSheet {
//	private static final Logger LOGGER = LogManager.getLogger(SaveAsSheet.class);
//
//	private static final String SESSION_EXAMPLE = "e.g. Session1 or S1";
//
//	private AuthenticationService mAuthenticationService = null;
//
//	@Sessions
//	private Map<String, Object> sessionMap;
//
////  @Execute
//	public BooleanValue execute(
//
//			@Idx(index = "1", type = TEXT) @Pkg(label = "Session name", description = SESSION_EXAMPLE, default_value_type = DataType.STRING, default_value = "Default") @NotEmpty String session,
//			@Idx(index = "2", type = TEXT) @Pkg(label = "New name", description = "e.g. Final Invoice") @NotEmpty String spreadsheetName) {
//		try {
//			GSheets gSheets = (GSheets) sessionMap.get(session);
//
//			GDrive drive = getDrive();
//			String spreadSheetId = drive.saveAs(gSheets.getSpreadSheetId(), spreadsheetName);
//			gSheets.saveAs(gSheets.getSpreadSheetId(), spreadSheetId, spreadsheetName);
//
//			// Update the session with the new sheet Id
//			sessionMap.put(session, gSheets);
//		} catch (GeneralSecurityException | IOException | JSONException e) {
//			LOGGER.error("exception in adding new sheet: ", e);
//			String msg = e.getMessage();
//			throw new BotCommandException(msg, e);
//		}
//		return new BooleanValue(Boolean.toString(true));
//	}
//
//	GDrive getDrive() throws GeneralSecurityException, IOException {
//		AuthenticationService authenticationService = getAuthenticationService();
//		return new GDriveImpl(authenticationService);
//	}
//
//	AuthenticationService getAuthenticationService() {
//		if (mAuthenticationService == null) {
//			mAuthenticationService = new AuthenticationServiceImpl();
//		}
//		return mAuthenticationService;
//	}
//
//	@VisibleForTesting
//	void setAuthenticationService(AuthenticationService mAuthenticationService) {
//		this.mAuthenticationService = mAuthenticationService;
//	}
//
//	public void setSessionMap(Map<String, Object> sessionMap) {
//		this.sessionMap = sessionMap;
//	}
//
//}