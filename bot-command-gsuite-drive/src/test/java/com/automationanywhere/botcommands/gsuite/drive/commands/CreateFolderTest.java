/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommands.gsuite.drive.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

/**
 * Tests create Google Drive file functionality.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

public class CreateFolderTest {

	public static final SecureString USERNAME = ConstantsTest.getUserName();

	@Spy
	CreateFolder createFolder;

	@Mock
	GDrive mockGDrive;

	@Mock
	AuthenticationService mockAuthService;

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		createFolder.setAuthenticationService(mockAuthService);
	}

	@Test
	public void createFolder_returnsTrueIfCreated_whenRequiredFolderNameProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(createFolder).getDrive(USERNAME);

			String parentFolderName = "root";
			String childFolderName = "NewFolder";
			JSONObject inputFolderParams = new JSONObject();
			inputFolderParams.put("fileName", parentFolderName);
			inputFolderParams.put("childName", childFolderName);

			when(mockGDrive.createFolder(inputFolderParams.toString())).thenReturn(true);
			BooleanValue testId = createFolder.execute(USERNAME, parentFolderName, childFolderName);
			Assert.assertTrue(testId.get());
		} catch (JSONException | IOException | GeneralSecurityException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void createFolder_throwsBotCommandException_whenCreateFolderAPIthrowsIOException() {
		try {
			Mockito.doReturn(mockGDrive).when(createFolder).getDrive(USERNAME);

			String parentFolderName = "trial_file123";
			String childFolderName = "trial_file.txt";
			JSONObject inputFolderParams = new JSONObject();
			inputFolderParams.put("fileName", parentFolderName);
			inputFolderParams.put("childName", childFolderName);

			when(mockGDrive.createFolder(inputFolderParams.toString())).thenThrow(IOException.class);
			createFolder.execute(USERNAME, parentFolderName, childFolderName);

			Assert.fail();
		} catch (GeneralSecurityException | JSONException e) {
			Assert.fail();
		} catch (IOException e) {
			Assert.assertTrue(true);
		}
	}

	@Test
	public void getDrive_newGDriveImplObjectIsNotNull() {
		try {
			GDrive drive = createFolder.getDrive(USERNAME);
			assertNotNull(drive);
		} catch (GeneralSecurityException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
	}
}
