/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommands.gsuite.sheets.commands.CreateWorkbook;
import com.automationanywhere.botcommands.gsuite.sheets.test.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

public class CreateWorkBookJunit extends AbstractAuthUtils {

	CreateWorkbook createWorkbook;

	public final static SecureString USERNAME = ConstantsTest.getUserName();
	final String session = "session";
	//final String newSession = "newsession";
	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeClass
	public void setup() throws IOException, GeneralSecurityException {
		launchOAuthConsentScreen(ConstantsTest.getUserName(), ConstantsTest.getClientid(),
				ConstantsTest.getClientSecret(), ConstantsTest.getRedirectUri());
		createWorkbook = new CreateWorkbook();
		createWorkbook.setSessionMap(sessionMap);

		// launchOAuthConsentScreen(ConstantsTest.getAppUserName(),
		// ConstantsTest.getAppClientid(), ConstantsTest.getAppClientSecret(),
		// ConstantsTest.getAppRedirectUri());

	}

	@Test
	public void createWorkBook() {

		createWorkbook.execute(USERNAME, session, "Arpitjunit1", "true", "PK/PKTest/PKNestedTest", null);
	}
	
	
//	@Test
//	public void createWorkBookNullSession() {
//
//		createWorkbook.execute(USERNAME, "as", "ArpitTest1", "true", "", null);
//	}

}
