/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.COLUMNS;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommands.gsuite.sheets.commands.DeleteRange;
import com.automationanywhere.botcommands.gsuite.sheets.commands.OpenSheets;
import com.automationanywhere.botcommands.gsuite.sheets.test.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

public class DeleteRangeJunit extends AbstractAuthUtils {
	private final static Logger LOGGER = LogManager.getLogger(DeleteRangeJunit.class);
	DeleteRange deleteRange;
	OpenSheets openSheets;
	final String session = "session";
	public static final SecureString USERNAME = ConstantsTest.getUserName();
	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {
		launchOAuthConsentScreen(ConstantsTest.getUserName(), ConstantsTest.getClientid(),
				ConstantsTest.getClientSecret(), ConstantsTest.getRedirectUri());
		deleteRange = new DeleteRange();
		deleteRange.setSessionMap(sessionMap);
		openSheets = new OpenSheets();
		openSheets.setSessionMap(sessionMap);
		openSheets.execute(USERNAME, session, "BYNAME", "Demo111", null, null, false, null, false);
	}

	@Test
	public void DeleteRangeTest() {
		deleteRange.execute(session, "A1:H7", COLUMNS);
	}
}
