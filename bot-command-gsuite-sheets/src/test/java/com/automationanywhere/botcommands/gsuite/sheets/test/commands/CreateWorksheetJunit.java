/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommands.gsuite.sheets.commands.CreateSheet;
import com.automationanywhere.botcommands.gsuite.sheets.commands.OpenSheets;
import com.automationanywhere.botcommands.gsuite.sheets.test.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

public class CreateWorksheetJunit extends AbstractAuthUtils {

	CreateSheet createWorksheet;

	OpenSheets openSheets;

	public final static SecureString USERNAME = ConstantsTest.getUserName();
	final String session = "session";
	final String newSession = "newsession";
	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeClass
	public void setup() throws IOException, GeneralSecurityException {
		launchOAuthConsentScreen(ConstantsTest.getUserName(), ConstantsTest.getClientid(),
				ConstantsTest.getClientSecret(), ConstantsTest.getRedirectUri());

		openSheets = new OpenSheets();
		openSheets.setSessionMap(sessionMap);
		openSheets.execute(ConstantsTest.getUserName(), session, "BYNAME", "CreateSheet", null, null, false, null, false);
		createWorksheet = new CreateSheet();
		createWorksheet.setSessionMap(sessionMap);
	}

	@Test
	public void createWorksheet_BYINDEX() {

		BooleanValue result = createWorksheet.execute(session, "BYINDEX", 1, null);

	}
//	@Test
//	public void createWorksheet_BYName() {
//
//		BooleanValue result = createWorksheet.execute(session, "BYNAME", null, "new2");
//
//	}


}
