package com.automationanywhere.botcommand.gsuite.api.impl;

import static org.mockito.Mockito.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.gsuite.DriveConstants;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files.Create;
import com.google.api.services.drive.model.File;

public class GDriveImplTest {

	@Mock
	AuthenticationService authService;

	@Mock
	Credential credential;

	@Mock
	Drive driveService;

	@Mock
	GDriveImpl mockGDrive;

	@Spy
	GDriveImpl gDrive;

	public void setup() {
		MockitoAnnotations.initMocks(this);
		try {
			// initialize gDriveImpl
			gDrive.setAuthService(authService);
			gDrive.setDrive(driveService);

			// initialize setupAuthorization
			doReturn(driveService).when(gDrive).getDriveService(credential);
			doNothing().when(gDrive).setupAuthorization();

		} catch (IOException e) {
			Assert.fail();
		}
	}

//	@Test
//	public void copyFolderTest1() {
//		try {
//			Drive.Files driveFiles = mock(Drive.Files.class);
//			Drive.Files.Create driveFilesCreate = mock(Drive.Files.Create.class);
//
//			JSONObject inputParam = new JSONObject();
//			inputParam.put("fileId", "spreadSheetID");
//			JSONObject outputParam = new JSONObject();
//			outputParam.put("fileName", "copyFolderName");
//
//			// flow for the method
//			String retVal = "spreadSheetID";
//			File sourceFolder = new File();
//			sourceFolder.setId(retVal);
//			File destFolder = new File();
//
//			List<File> listOfFile = new ArrayList<File>();
//			File fileByID = new File();
//			listOfFile.add(fileByID);
//			doReturn(fileByID).when(mockGDrive).getFileById(anyString());
//
//			doReturn(sourceFolder).when(mockGDrive).getFileFromJson(anyString(), anyString(), anyString());
//			doNothing().when(mockGDrive).validateFolder(sourceFolder);
//
//			// api call
//			File fileCreated = new File();
//			when(driveService.files()).thenReturn(driveFiles);
//			when(driveFiles.create(new File())).thenReturn(driveFilesCreate);
//			when(driveFilesCreate.setFields(anyString())).thenReturn(driveFilesCreate);
//			when(driveFilesCreate.execute()).thenReturn(fileCreated);
//
//			// actual calling of the method to return value accordingly we set above
//			String val = gDrive.copyFolder(inputParam.toString(), outputParam.toString());
//
//			Assert.assertEquals(retVal, val);
//		} catch (IOException | JSONException e) {
//			e.printStackTrace();
//		}
//	}

	@Test
	public void copyFolderTest2() {
		try {
			setup();
			Drive.Files driveFiles = mock(Drive.Files.class);
			Drive.Files.Create driveFilesCreate = mock(Drive.Files.Create.class);

			JSONObject inputParam = new JSONObject();
			inputParam.put("fileId", "spreadSheetID");
			JSONObject outputParam = new JSONObject();
			outputParam.put("fileName", "copyFolderName");
			String retVal = "spreadSheetID";
			File sourceFolder = new File();
			sourceFolder.setId(retVal);
			sourceFolder.setName("Arpit");
			File destFolder = new File();
			destFolder.setId("spreadSheetID");
			destFolder.setName("Arpit2");
			driveFiles.create(sourceFolder);

			Mockito.doReturn(sourceFolder).when(gDrive).getFileFromJson(anyString(), anyString(), anyString());
			Mockito.doReturn(destFolder).when(gDrive).getFileFromJson(anyString(), anyString(), anyString());
			Mockito.doNothing().when(gDrive).validateFolder(anyObject());
			Mockito.doNothing().when(gDrive).copyFolderChildrens(anyObject(), anyObject());

			File file = new File();
			when(driveService.files()).thenReturn(driveFiles);
			when(driveFiles.create(anyObject())).thenReturn(driveFilesCreate);
			when(driveFilesCreate.setFields(anyString())).thenReturn(driveFilesCreate);
			when(driveFilesCreate.execute()).thenReturn(file);

			String val = gDrive.copyFolder(inputParam.toString(), outputParam.toString());

			Assert.assertEquals(retVal, val);
		} catch (IOException | JSONException e) {
			e.printStackTrace();
		}
	}
}
