package com.automationanywhere.botcommand.gsuite.service;

import java.io.Serializable;

/**
 * 
 * 
 * @param clientId         Client id.
 * @param userEmailAddress User email address.
 * @param redirectURL      RedirectURL
 * @param clientSecret     ClientSecret
 */
public class AuthDto implements Serializable {

	private static final long serialVersionUID = 134285725L;
	private String clientId;
	private String redirectURL;
	private String clientSecret;

	public String getClientId() {
		return clientId;
	}

	public String getRedirectURL() {
		return redirectURL;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setRedirectURL(String redirectURL) {
		this.redirectURL = AESEncrypter.encrypt(redirectURL);
	}

	public void setClientId(String clientId) throws Exception {
		this.clientId = AESEncrypter.encrypt(clientId);
	}

	public void setClientSecret(String clientSecret) throws Exception {
		this.clientSecret = AESEncrypter.encrypt(clientSecret);
	}

}
