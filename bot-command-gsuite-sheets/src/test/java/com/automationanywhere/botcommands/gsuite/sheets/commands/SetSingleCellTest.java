/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.ACTIVECELL;
import static com.automationanywhere.botcommand.gsuite.Constants.SPECIFICCELL;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.dto.sheets.WorksheetDto;

public class SetSingleCellTest {

	@Spy
	SetSingleCell singleCell;

	@Mock
	GSheets mockGSheets;

	@Mock
	WorksheetDto worksheetDto;

	final String session = "session";
	final String spreadSheetId = "spreadsheetId";

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		sessionMap.put("session", mockGSheets);
		singleCell.setSessionMap(sessionMap);
	}

	@Test
	public void setActiveCellTest() {
		try {
			when(mockGSheets.getSpreadSheetId()).thenReturn(spreadSheetId);
			when(mockGSheets.getActiveSheetInfo()).thenReturn(worksheetDto);
			when(worksheetDto.getSheetName()).thenReturn("Sheet1");
			when(mockGSheets.getActiveCell(anyString())).thenReturn("A1");
			when(mockGSheets.setSingleCell( "A1", "new")).thenReturn(spreadSheetId);
			Assert.assertEquals(singleCell.execute(session, ACTIVECELL, null, "new").get(), spreadSheetId);
		} catch (IOException e) {
			Assert.fail();
		}
	}

	@Test
	public void setSpecificCellTest() {
		try {
			when(mockGSheets.getSpreadSheetId()).thenReturn(spreadSheetId);
			when(mockGSheets.setSingleCell("A1", "new")).thenReturn(spreadSheetId);
			Assert.assertEquals(singleCell.execute(session, SPECIFICCELL, "A1", "new").get(), spreadSheetId);
		} catch (IOException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void botCommandExceptionTest() {
		try {
			when(mockGSheets.getSpreadSheetId()).thenReturn("wrong id");
			when(mockGSheets.setSingleCell("A1", "new")).thenThrow(new IOException());
			singleCell.execute(session, SPECIFICCELL, "A1", "new");
		} catch (IOException e) {
			Assert.fail();
		}
	}

}
