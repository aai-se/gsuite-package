/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommand.gsuite.helper.sheets;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CellSplitter {

    private static final String CELL_SPLIT_REGEX = "([A-Za-z]+)([0-9]+)";

    private String rowName;

    private String columnName;

    public static boolean validateCell(String cellAddress) {

        if(StringUtils.isBlank(cellAddress)){
            return false;
        }

        Matcher matcher = getMatcherForCellValue(cellAddress);
        return matcher.matches() && matcher.groupCount() > 1;
    }

    public void splitCell(String cellValue) {
        Matcher matcher = getMatcherForCellValue(cellValue);
        if (matcher.matches() && matcher.groupCount() > 1) {
            columnName = matcher.group(1);
            rowName = matcher.group(2);
        }
    }

    private static Matcher getMatcherForCellValue(String cellValue) {
        Pattern pattern = Pattern.compile(CELL_SPLIT_REGEX);
        return pattern.matcher(cellValue);
    }

    public String getRowName() {
        return rowName;
    }

    public void setRowName(String rowName) {
        this.rowName = rowName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }
}
