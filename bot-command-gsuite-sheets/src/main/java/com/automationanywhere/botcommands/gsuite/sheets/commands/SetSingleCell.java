/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */

package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.*;
import static com.automationanywhere.commandsdk.model.AttributeType.RADIO;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Idx.Option;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.MatchesRegex;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

@BotCommand
@CommandPkg(name = "SetSingleCell", label = "Set cell", description = "Sets the value of a cell in google sheet", node_label = "to {{value}} |in session {{sessionName}}", icon = "excel.svg")
public class SetSingleCell {
	private static final Logger LOGGER = LogManager.getLogger(SetSingleCell.class);

	@Sessions
	private Map<String, Object> sessionMap;

	private static final String CELL_EXAMPLE = "e.g. A1";

	private static final String VALUE_TO_SET_EXAMPLE = "e.g. San Jose";

	@Execute
	public StringValue execute(
			@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String sessionName,
			@Idx(index = "2", type = RADIO, options = {
					@Option(index = "2.1", pkg = @Pkg(label = LABEL_ACTIVECELL, value = ACTIVECELL)),
					@Option(index = "2.2", pkg = @Pkg(label = LABEL_SPECIFICCELL, value = SPECIFICCELL)) }) @Pkg(label = "Cell options", default_value = ACTIVECELL, default_value_type = STRING) String cellOption,
			@Idx(index = "2.2.1", type = TEXT) @Pkg(label = "Cell name", description = CELL_EXAMPLE) @MatchesRegex(cellAddressValidator) @NotEmpty String cellAddress,
			@Idx(index = "3", type = TEXT) @Pkg(label = "Cell value", description = VALUE_TO_SET_EXAMPLE) @NotEmpty String value) {

		String spreadsheetId = null;
		try {
			GSheets gSheets = (GSheets) sessionMap.get(sessionName);
			ExecutorValidator.getInstance().validate(gSheets);

			if (ACTIVECELL.equals(cellOption)) {
				spreadsheetId = gSheets
						.setSingleCell(gSheets.getActiveCell(gSheets.getActiveSheetInfo().getSheetName()), value);
			} else {
				spreadsheetId = gSheets.setSingleCell(cellAddress, value);
			}

		} catch (IOException e) {
			LOGGER.error("Exception in getting single cell data from the sheet: ", e);
			String msg = e.getMessage();
			throw new BotCommandException(msg, e);
		}

		return new StringValue(spreadsheetId);
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}