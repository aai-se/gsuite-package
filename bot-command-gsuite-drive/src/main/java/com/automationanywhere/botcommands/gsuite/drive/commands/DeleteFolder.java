/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.api.impl.GDriveImpl;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommand.gsuite.service.impl.AuthenticationServiceImpl;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import com.google.common.annotations.VisibleForTesting;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Map;

import static com.automationanywhere.botcommand.gsuite.Constants.USERNAME;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;

/**
 * Deletes a Google Drive file from a Google Drive location and moves the file
 * to Trash folder.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

@BotCommand
@CommandPkg(label = "Delete file", name = "DeleteFolder", description = "Deletes a file on Google Drive", node_label = "with name {{fileName}}|with id {{fileId}}", return_type = DataType.BOOLEAN, return_required = false, return_label = "Returns the file delete status")
public class DeleteFolder {
	private static final Logger LOGGER = LogManager.getLogger(DeleteFolder.class);
	private static final String NAME_DESC = "e.g. MyFolder/MyFolder.doc";
	private static final String ID_DESC = "e.g. 17dIAwvrEA4JeLysfFky9oZL0qAzesY0IdbOu-AvHB1U from document "
			+ "link https://docs.google.com/spreadsheets/d/17dIAwvrEA4JeLysfFky9oZL0qAzesY0IdbOu-AvHB1U/edit#gid=0";
	private AuthenticationService mAuthenticationService;

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public BooleanValue execute(
			@Idx(index = "1", type = AttributeType.CREDENTIAL) @Pkg(label = USERNAME) @NotEmpty SecureString userEmailAddress,
			@Idx(index = "2", type = AttributeType.RADIO, options = {
					@Idx.Option(index = "2.1", pkg = @Pkg(label = "Folder name (with location)", value = "BYNAME")),
					@Idx.Option(index = "2.2", pkg = @Pkg(label = "Folder id", value = "BYID")) }) @Pkg(label = "Delete by", value = "Open type", default_value_type = DataType.STRING, default_value = "BYNAME") String folderOption,
			@Idx(index = "2.1.1", type = TEXT) @Pkg(label = "", description = NAME_DESC) @NotEmpty String fileName,
			@Idx(index = "2.2.1", type = TEXT) @Pkg(label = "", description = ID_DESC) @NotEmpty String fileId) {
		try {
			JSONObject inputFolderParam = new JSONObject();
			if (folderOption.equals("BYNAME")) {
				inputFolderParam.put("fileName", fileName);
			} else if (folderOption.equals("BYID")) {
				inputFolderParam.put("fileId", fileId);
			}
			return new BooleanValue("" + getDrive(userEmailAddress).deleteFolder(inputFolderParam.toString()));
		} catch (GeneralSecurityException | IOException | JSONException e) {
			LOGGER.error("Exception in deleting Google Drive file: ", e);
			String msg = e.getMessage();
			throw new BotCommandException(msg, e);
		}
	}

	GDrive getDrive(SecureString userEmailAddress) throws GeneralSecurityException, IOException {
		AuthenticationService authenticationService = getAuthenticationService();
		return new GDriveImpl(authenticationService, userEmailAddress.getInsecureString(), sessionMap);
	}

	@VisibleForTesting
	void setAuthenticationService(AuthenticationService mAuthenticationService) {
		this.mAuthenticationService = mAuthenticationService;
	}

	synchronized AuthenticationService getAuthenticationService() {
		if (mAuthenticationService == null) {
			mAuthenticationService = new AuthenticationServiceImpl();
		}
		return mAuthenticationService;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
}
