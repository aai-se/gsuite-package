package com.automationanywhere.botcommands.gsuite.drive.utils;

import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;

public class Utils {

    public static String getException(GoogleJsonResponseException e) {
        GoogleJsonError error = e.getDetails();
        String errorMsg = error.getMessage();
        return errorMsg;
    }
}
