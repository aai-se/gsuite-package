/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommand.gsuite.api.impl;

import static com.automationanywhere.botcommand.gsuite.Constants.ACTIVECELL;
import static com.automationanywhere.botcommand.gsuite.Constants.APPLICATION_NAME;
import static com.automationanywhere.botcommand.gsuite.Constants.BYINDEX;
import static com.automationanywhere.botcommand.gsuite.Constants.BYNAME;
import static com.automationanywhere.botcommand.gsuite.Constants.CELL;
import static com.automationanywhere.botcommand.gsuite.Constants.COLON;
import static com.automationanywhere.botcommand.gsuite.Constants.COLUMNS;
import static com.automationanywhere.botcommand.gsuite.Constants.EMPTY;
import static com.automationanywhere.botcommand.gsuite.Constants.EXCLAMATION;
import static com.automationanywhere.botcommand.gsuite.Constants.HIDDEN;
import static com.automationanywhere.botcommand.gsuite.Constants.LEFT;
import static com.automationanywhere.botcommand.gsuite.Constants.MULTIPLE;
import static com.automationanywhere.botcommand.gsuite.Constants.NEGATIVEONE;
import static com.automationanywhere.botcommand.gsuite.Constants.NONE;
import static com.automationanywhere.botcommand.gsuite.Constants.NONEMPTYROWS;
import static com.automationanywhere.botcommand.gsuite.Constants.ROWS;
import static com.automationanywhere.botcommand.gsuite.Constants.SPECIFICCELL;
import static com.automationanywhere.botcommand.gsuite.Constants.STARTCELLADDRESS;
import static com.automationanywhere.botcommand.gsuite.Constants.TEXT;
import static com.automationanywhere.botcommand.gsuite.Constants.TOTALROWS;
import static com.automationanywhere.botcommand.gsuite.Constants.UNFORMATTED_VALUE;
import static com.automationanywhere.botcommand.gsuite.Constants.USER_ENTERED;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.dto.sheets.SpreadSheetDto;
import com.automationanywhere.botcommand.gsuite.dto.sheets.WorksheetDto;
import com.automationanywhere.botcommand.gsuite.helper.sheets.CellSplitter;
import com.automationanywhere.botcommand.gsuite.helper.sheets.ColorUtils;
import com.automationanywhere.botcommand.gsuite.service.AESEncrypter;
import com.automationanywhere.botcommand.gsuite.service.AuthDto;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommand.gsuite.utils.GoToCellOptions;
import com.automationanywhere.botcommand.gsuite.utils.Utils;
import com.automationanywhere.botcommand.sdk.command.utils.EnumParser;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.AddSheetRequest;
import com.google.api.services.sheets.v4.model.AutoResizeDimensionsRequest;
import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetRequest;
import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetResponse;
import com.google.api.services.sheets.v4.model.BatchUpdateValuesRequest;
import com.google.api.services.sheets.v4.model.CellData;
import com.google.api.services.sheets.v4.model.CellFormat;
import com.google.api.services.sheets.v4.model.Color;
import com.google.api.services.sheets.v4.model.CopyPasteRequest;
import com.google.api.services.sheets.v4.model.DataFilter;
import com.google.api.services.sheets.v4.model.DeleteDimensionRequest;
import com.google.api.services.sheets.v4.model.DeleteRangeRequest;
import com.google.api.services.sheets.v4.model.DeleteSheetRequest;
import com.google.api.services.sheets.v4.model.DimensionRange;
import com.google.api.services.sheets.v4.model.GetSpreadsheetByDataFilterRequest;
import com.google.api.services.sheets.v4.model.GridRange;
import com.google.api.services.sheets.v4.model.InsertDimensionRequest;
import com.google.api.services.sheets.v4.model.InsertRangeRequest;
import com.google.api.services.sheets.v4.model.MergeCellsRequest;
import com.google.api.services.sheets.v4.model.RepeatCellRequest;
import com.google.api.services.sheets.v4.model.Request;
import com.google.api.services.sheets.v4.model.Sheet;
import com.google.api.services.sheets.v4.model.SheetProperties;
import com.google.api.services.sheets.v4.model.Spreadsheet;
import com.google.api.services.sheets.v4.model.SpreadsheetProperties;
import com.google.api.services.sheets.v4.model.TextFormat;
import com.google.api.services.sheets.v4.model.UnmergeCellsRequest;
import com.google.api.services.sheets.v4.model.UpdateSheetPropertiesRequest;
import com.google.api.services.sheets.v4.model.UpdateSpreadsheetPropertiesRequest;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.google.common.annotations.VisibleForTesting;

public class GSheetsImpl implements GSheets {

	private static final Logger LOGGER = LogManager.getLogger(GSheetsImpl.class);

	@Sessions
	private Map<String, Object> sessionMap;

	private AuthenticationService authService;
	private Sheets sheetService;
	private SpreadSheetDto spreadSheetDto;
	private String userName;
	private String sessionName;
	private WorksheetDto activeSheetInfo;

	@VisibleForTesting
	void setWorksheetDto(WorksheetDto worksheetDto) {
		this.activeSheetInfo = worksheetDto;
	}

	// added dec activeSheetName
	private String activeSheetName;
	private int activeSheetId;

	public WorksheetDto getActiveSheetInfo() {
		return activeSheetInfo;
	}

	// added dec activeCellMap
	private Map<String, String> activeCellMap = new HashMap<>();

	// getter activeSheetName
	public String getActiveSheetName() {
		LOGGER.info("Active Sheet Nane  = " + this.activeSheetInfo.getSheetName().toString());
		if (this.activeSheetInfo.getSheetName() != null)
			return this.activeSheetInfo.getSheetName();
		else
			throw new BotCommandException(
					"Either you are running this command before open command or some internal error occurred.");
	}

	@Override
	public void setActiveSheetName(String activeSheetName) {
		this.activeSheetName = activeSheetName;

	}

	// setter userName
	public void setUserName(String userName) {
		this.userName = userName;
	}

	// setter.
	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	// setter
	public void setActiveSheetId(int activeSheetId) {
		this.activeSheetId = activeSheetId;
	}

	// setter
	public void setActiveSheetInfo(WorksheetDto activeSheetInfo) {
		this.activeSheetInfo = activeSheetInfo;
	}

	// getter
	public int getActiveSheetId() {
		LOGGER.info("Active Sheet Nane  = " + this.activeSheetInfo.getSheetId().toString());
		if (this.activeSheetInfo.getSheetId() != null)
			return this.activeSheetInfo.getSheetId();
		else
			throw new BotCommandException(
					"Either you are running this command before open command or some internal error occurred.");
	}

	// setter activeSheetName
	public void setActiveSheetName(String activeSheetName, int activeSheetId) {
		LOGGER.info("Setting Active sheet to " + activeSheetName);
		this.activeSheetName = activeSheetName;
		this.activeSheetId = activeSheetId;
		if (!activeCellMap.containsKey(activeSheetName))
			setActiveCell(activeSheetName, "A1");
	}

	// added getter activeCellMap
	public String getActiveCell(String sheetName) {
		return activeCellMap.get(sheetName);
	}

	// added setter activeCellMap
	public void setActiveCell(String sheetName, String activeCell) {
		LOGGER.info("Active cell set to " + activeCell + " for sheet " + sheetName);
		activeCellMap.put(sheetName, activeCell); // this.activeSheetName
	}

	private final HttpTransport httpTransport;

	@VisibleForTesting
	public GSheetsImpl() throws GeneralSecurityException, IOException {
		httpTransport = GoogleNetHttpTransport.newTrustedTransport();
	}

	@VisibleForTesting
	void setAuthService(AuthenticationService authService) {
		this.authService = authService;
	}

	@VisibleForTesting
	void setSpredSheetDto(SpreadSheetDto spreadSheetDto) {
		this.spreadSheetDto = spreadSheetDto;
	}

	@VisibleForTesting
	void setSheetService(Sheets sheets) {
		this.sheetService = sheets;
	}

	public GSheetsImpl(AuthenticationService authService, String session, Map<String, Object> sessionMap,
			String userEmailAddress) throws GeneralSecurityException, IOException {
		this.authService = authService;
		this.sessionName = session;
		this.setSessionMap(sessionMap);
		this.setUserName(userEmailAddress);
		this.httpTransport = GoogleNetHttpTransport.newTrustedTransport();
	}

	public GSheetsImpl(AuthenticationService authService) throws GeneralSecurityException, IOException {
		this.authService = authService;
		httpTransport = GoogleNetHttpTransport.newTrustedTransport();
	}

	@Override
	public void insertRowColumn(String spreadsheetId, String dimension, String cellOption, String cellAddress)
			throws IOException {
		setupAuthorization();
		int startIndex = -1;
		int endIndex = -1;
		if (ACTIVECELL.equals(cellOption)) {
			String cellValue = getActiveCell(getActiveSheetName());
			if (ROWS.equals(dimension)) {
				startIndex = Integer.parseInt(Utils.getCellRowColumn(cellValue, dimension));
				endIndex = startIndex + 1;
			} else {
				startIndex = Utils.getColIndex((Utils.getCellRowColumn(cellValue, dimension)));
				endIndex = startIndex + 1;
			}
		} else if (SPECIFICCELL.equals(cellOption)) {
			startIndex = Integer.parseInt(cellAddress);
			endIndex = startIndex + 1;
		} else {
			String[] range = cellAddress.split(":");
			if (ROWS.equals(dimension)) {
				startIndex = Integer.parseInt(range[0]);
				endIndex = Integer.parseInt(range[1]);
			} else {
				startIndex = Utils.getColIndex(range[0]);
				endIndex = Utils.getColIndex(range[1]);
			}

		}
		InsertDimensionRequest insertRowColumn = new InsertDimensionRequest();
		insertRowColumn
				.setRange(new DimensionRange().setDimension(dimension).setStartIndex(startIndex).setEndIndex(endIndex));
		BatchUpdateSpreadsheetRequest request = new BatchUpdateSpreadsheetRequest()
				.setRequests(Arrays.asList(new Request().setInsertDimension(insertRowColumn)));
		sheetService.spreadsheets().batchUpdate(spreadsheetId, request).execute();
	}

	@Override
	public Boolean deleteRowColumn(String dimension, String cellOption, String cellAddress) throws IOException {
		setupAuthorization();
		int startIndex = -1;
		int endIndex = -1;
		if (ACTIVECELL.equals(cellOption)) {
			String cellValue = getActiveCell(getActiveSheetName());
			if (ROWS.equals(dimension)) {
				startIndex = Integer.parseInt(Utils.getCellRowColumn(cellValue, dimension)) - 1;
				endIndex = startIndex + 1;
			} else {
				startIndex = Utils.getColIndex((Utils.getCellRowColumn(cellValue, dimension))) - 1;
				endIndex = startIndex + 1;
			}
		} else if (SPECIFICCELL.equals(cellOption)) {
			startIndex = Integer.parseInt(cellAddress) - 1;
			endIndex = startIndex + 1;
		} else {
			String[] range = cellAddress.split(":");
			if (ROWS.equals(dimension)) {
				startIndex = Integer.parseInt(range[0]) - 1;
				endIndex = Integer.parseInt(range[1]);
			} else {
				startIndex = Utils.getColIndex(range[0]) - 1;
				endIndex = Utils.getColIndex(range[1]);
			}
		}
		DeleteDimensionRequest deleteDimensionRequest = new DeleteDimensionRequest();
		deleteDimensionRequest.setRange(new DimensionRange().setDimension(dimension).setStartIndex(startIndex)
				.setEndIndex(endIndex).setSheetId(this.getActiveSheetInfo().getSheetId()));
		BatchUpdateSpreadsheetRequest request = new BatchUpdateSpreadsheetRequest()
				.setRequests(Arrays.asList(new Request().setDeleteDimension(deleteDimensionRequest)));
		sheetService.spreadsheets().batchUpdate(this.getSpreadSheetId(), request).execute();
		return true;
	}

	@Override
	public SpreadSheetDto openSpreadSheet(String spreadSheetId) throws IOException {

		setupAuthorization();

		Spreadsheet spreadsheet = sheetService.spreadsheets().get(spreadSheetId)
				.setFields("spreadsheetId,spreadsheetUrl,sheets,properties").execute();

		LOGGER.info("Spreadsheet id = " + spreadsheet.getSpreadsheetId());
		String title = spreadsheet.getProperties().getTitle();
		LOGGER.info("sheets title  = " + title);
		this.spreadSheetDto = new SpreadSheetDto();
		this.spreadSheetDto.setSpreadSheetId(spreadsheet.getSpreadsheetId());
		this.spreadSheetDto.setTitle(spreadsheet.getProperties().getTitle());
		this.spreadSheetDto.setSession(this.sessionName);
		this.spreadSheetDto.setUrl(spreadsheet.getSpreadsheetUrl());
		return this.spreadSheetDto;
	}

	@Override
	public Value<?> getAllWorkSheet() throws IOException {
		LOGGER.info("Authorization");
		String spreadSheetID = this.getSpreadSheetId();
		setupAuthorization();
		int index = 0;
		Spreadsheet spreadSheet = sheetService.spreadsheets().get(spreadSheetID).execute();
		StringValue sheetNames[] = new StringValue[spreadSheet.size()];
		for (Sheet sheet : spreadSheet.getSheets()) {
			SheetProperties properties = sheet.getProperties();
			sheetNames[index++] = new StringValue(properties.getTitle());
		}

		LOGGER.info("Returning the List Sheetnames");
		return new ListValue<>(sheetNames);
	}


	@Override
	public Boolean createWorkSheet(String sheetOption, Number index, String sheetName) throws IOException {

		setupAuthorization();
		LOGGER.info("Performing Create Worksheet command.");
		List<Request> requests = new ArrayList<>();
		SheetProperties sheetProperties = null;

		if (index.intValue() > retrieveSheetCount(false)) {
			throw new BotCommandException("Index is too high, please enter valid index.!");
		}
		if (BYINDEX.equals(sheetOption))
			sheetProperties = new SheetProperties().setIndex(index.intValue() - 1);
		else
			sheetProperties = new SheetProperties().setTitle(sheetName);

		LOGGER.info("Sheet Properties " + sheetProperties);
		requests.add(new Request().setAddSheet(new AddSheetRequest().setProperties(sheetProperties)));
		BatchUpdateSpreadsheetRequest body = new BatchUpdateSpreadsheetRequest().setRequests(requests);
		LOGGER.info("Creating new sheet");
		LOGGER.trace("new sheet request = " + body);
		BatchUpdateSpreadsheetResponse response = sheetService.spreadsheets()
				.batchUpdate(this.spreadSheetDto.getSpreadSheetId(), body).execute();
		LOGGER.trace("new sheet response = " + response);
		return true;
	}

	@Override
	public boolean renameSheet(boolean byIndex, int sheetIndex, String sheetName, String newSheetName)
			throws IOException {
		setupAuthorization();
		LOGGER.info("Performing Rename Sheet command.");

		List<String> listOfWorksheets = getWorksheetNames(true);

		if (byIndex) {
			if (sheetIndex <= listOfWorksheets.size())
				sheetName = listOfWorksheets.get(sheetIndex - 1);
			else
				throw new BotCommandException("Cannot find worksheet at index " + sheetIndex
						+ ". Please ensure the following: The sheet index is valid. The sheet at given index is not hidden.");
		} else {
			if (indexOf(listOfWorksheets, sheetName) == -1)
				throw new BotCommandException("Cannot find worksheet with name " + sheetName
						+ ". Please ensure the following: The sheet name is valid. The sheet with given name is not hidden.");
			// due to case insensitivity of sheet name
			sheetName = listOfWorksheets.get(indexOf(listOfWorksheets, sheetName));
		}
		// if sheet with given new sheet name, already exist
		if (indexOf(listOfWorksheets, newSheetName) != -1) {
			throw new BotCommandException(
					"Sheet with name " + newSheetName + " already exist in workbook. Please Enter a different name.");
		}
		int sheetId = getWorksheetIdByName(sheetName, true);
		List<Request> requests = new ArrayList<>();

		// add sheet id in SheetProperties object
		SheetProperties sheetProperties = new SheetProperties();
		sheetProperties.setTitle(newSheetName);
		sheetProperties.setSheetId(sheetId);
		requests.add(new Request().setUpdateSheetProperties(
				new UpdateSheetPropertiesRequest().setProperties(sheetProperties).setFields("title")));

		BatchUpdateSpreadsheetRequest batchUpdateSpreadsheetRequest = new BatchUpdateSpreadsheetRequest();
		batchUpdateSpreadsheetRequest.setRequests(requests);

		sheetService.spreadsheets().batchUpdate(this.getSpreadSheetId(), batchUpdateSpreadsheetRequest).execute();
		// if active sheet will rename
		String activeSheetName = activeSheetInfo.getSheetName();
		if (activeSheetName.equalsIgnoreCase(sheetName)) {
			setActiveSheetInfo(BYNAME, NEGATIVEONE, newSheetName);
//			activeSheetInfo.setSheetName(newSheetName);
		}
		// update active cell map
		if (activeCellMap.containsKey(sheetName)) {
			activeCellMap.put(newSheetName, activeCellMap.get(sheetName));
			activeCellMap.remove(sheetName);
		}
		return true;
	}

	@Override
	public Boolean insertCell(String cellAddress, String shiftType) throws IOException {
		setupAuthorization();
		String spreadsheetId = this.getSpreadSheetId();
		InsertRangeRequest insertRangeRequest = new InsertRangeRequest();
		insertRangeRequest.setRange(Utils.convertToGridRange(cellAddress, this.getActiveSheetInfo().getSheetId()))
				.setShiftDimension(shiftType);
		BatchUpdateSpreadsheetRequest request = new BatchUpdateSpreadsheetRequest()
				.setRequests(Arrays.asList(new Request().setInsertRange(insertRangeRequest)));
		LOGGER.info("Sending the request to perform operation>>");
		sheetService.spreadsheets().batchUpdate(spreadsheetId, request).execute();
		return true;
	}

	@Override
	public void insertRange(String spreadsheetId, String rangeAddress, String shiftType) throws IOException {

		setupAuthorization();
		LOGGER.info("Inserting the cell>>");
		DeleteRangeRequest deleteRangeRequest = new DeleteRangeRequest();
		deleteRangeRequest.setRange(Utils.convertToGridRange(rangeAddress, this.activeSheetInfo.getSheetId()))
				.setShiftDimension(shiftType);
		BatchUpdateSpreadsheetRequest request = new BatchUpdateSpreadsheetRequest()
				.setRequests(Arrays.asList(new Request().setDeleteRange(deleteRangeRequest)));
		LOGGER.info("Sending the request to perform operation>>");
		sheetService.spreadsheets().batchUpdate(spreadsheetId, request).execute();
	}

	@Override
	public Boolean deleteRange(String rangeAddress, String shiftType) throws IOException {
		String spreadSheetId = this.getSpreadSheetId();
		DeleteRangeRequest deleteRangeRequest = new DeleteRangeRequest();
		deleteRangeRequest.setRange(Utils.convertToGridRange(rangeAddress, this.activeSheetInfo.getSheetId()));
		BatchUpdateSpreadsheetRequest request = new BatchUpdateSpreadsheetRequest()
				.setRequests(Arrays.asList(new Request().setDeleteRange(deleteRangeRequest)));
		sheetService.spreadsheets().batchUpdate(spreadSheetId, request).execute();
		return true;
	}

	@Override
	public Boolean pasteCell(String cellAddress, String pasteCellAddress) throws IOException {
		int sheetId = this.getActiveSheetInfo().getSheetId();
		CopyPasteRequest copyPasteRequest = new CopyPasteRequest();

		copyPasteRequest.setSource(Utils.convertToGridRange(cellAddress, sheetId))
				.setDestination(Utils.convertToGridRange(pasteCellAddress, sheetId)).setPasteOrientation("NORMAL")
				.setPasteType("PASTE_NORMAL");
		LOGGER.info(" Copy Paste Request Done ");
		BatchUpdateSpreadsheetRequest request = new BatchUpdateSpreadsheetRequest()
				.setRequests(Arrays.asList(new Request().setCopyPaste(copyPasteRequest)));
		sheetService.spreadsheets().batchUpdate(this.getSpreadSheetId(), request).execute();
		LOGGER.info(" Update Request Done ");
		setActiveCell(this.getActiveSheetInfo().getSheetName(), pasteCellAddress);
		return true;
	}

	void setupAuthorization() throws IOException {
		LOGGER.info("SETUP Authorization called");
		AuthDto authDto = authService.getAuthDto(userName);
		Credential credential = authService.getGoogleCredential(userName, getAuthCredentials(authDto.getClientId()),
				getAuthCredentials(authDto.getClientSecret()), getAuthCredentials(authDto.getRedirectURL()));
		LOGGER.debug("Able to fetch credentials1");
		if (sheetService == null)
			sheetService = getSheetServices(credential);
		LOGGER.info(sheetService.toString());
	}

	@Override
	public String getAuthCredentials(String text) {
		return AESEncrypter.decrypt(text);
	}

	Sheets getSheetServices(Credential credential) {
		return new Sheets.Builder(httpTransport, JacksonFactory.getDefaultInstance(), credential)
				.setApplicationName(APPLICATION_NAME).build();
	}

	@Override
	public boolean deleteWorkSheet(boolean byIndex, int sheetIndex, String sheetName) throws IOException {

		LOGGER.info("Performing Delete sheet Command...");
		setupAuthorization();
		List<String> listOfWorksheets = getWorksheetNames(true);
		// if there is only one worksheet is visible in workbook
		if (listOfWorksheets.size() == 1)
			throw new BotCommandException("Cannot delete only sheet within the workbook.");

		if (byIndex) {
			if (sheetIndex <= listOfWorksheets.size())
				sheetName = listOfWorksheets.get(sheetIndex - 1);
			else
				throw new BotCommandException("Cannot find worksheet at index " + sheetIndex
						+ ". Please ensure the following: The sheet index is valid. The sheet at given index is not hidden.");
		} else {
			if (indexOf(listOfWorksheets, sheetName) == -1)
				throw new BotCommandException("Cannot find worksheet with name " + sheetName
						+ ". Please ensure the following: The sheet name is valid. The sheet with given name is not hidden.");
			// due to case insensitivity of sheet name
			sheetName = listOfWorksheets.get(indexOf(listOfWorksheets, sheetName));
		}

		int sheetId = getWorksheetIdByName(sheetName, true);
		List<Request> requests = new ArrayList<>();
		requests.add(new Request().setDeleteSheet(new DeleteSheetRequest().setSheetId(sheetId)));
		BatchUpdateSpreadsheetRequest body = new BatchUpdateSpreadsheetRequest().setRequests(requests);
		sheetService.spreadsheets().batchUpdate(this.getSpreadSheetId(), body).execute();
		LOGGER.info("Worksheet with name " + sheetName + " has successfully deleted.");

		// change activeSheetInfo if active sheet will be hidden
		String activeSheet = this.activeSheetInfo.getSheetName();
		if (activeSheet.equalsIgnoreCase(sheetName)) {
			int activeSheetIndex = indexOf(listOfWorksheets, activeSheet);
			// if active sheet is on 0th index
			if (activeSheetIndex == 0) {
				setActiveSheetInfo(BYNAME, -1, listOfWorksheets.get(1));
			} else {
				setActiveSheetInfo(BYNAME, -1, listOfWorksheets.get(activeSheetIndex - 1));
			}
		}

		// update active cell map
		if (activeCellMap.containsKey(sheetName))
			activeCellMap.remove(sheetName);

		return true;
	}

	@Override
	public ValueRange getValue(String spreadSheetId, String sheetName, String cellAddress, String formatType)
			throws IOException {
		setupAuthorization();
		StringBuilder range = new StringBuilder("'").append(sheetName).append("'");
		if (cellAddress != null) { // Incase we want to return all cells in the sheet.
			range.append("!").append(cellAddress);
		}
		ValueRange result = sheetService.spreadsheets().values().get(spreadSheetId, range.toString())
				.setValueRenderOption(formatType).execute();
		int numRows = result.getValues() != null ? result.getValues().size() : 0;
		LOGGER.debug(numRows + " rows retrieved." + result.getValues());
		return result;
	}

	@Override
	public Boolean activateSheet(boolean sheetOptionIsIndex, Number sheetIndex, String sheetName) {

		List<String> listOfWorksheets = getWorksheetNames(true);

		if (sheetOptionIsIndex) {
			if (sheetIndex.intValue() > listOfWorksheets.size())
				throw new BotCommandException("Cannot find worksheet at index " + sheetIndex.intValue()
						+ ". Please ensure the following: The sheet index is valid. The sheet at given index is not hidden.");
			sheetName = listOfWorksheets.get(sheetIndex.intValue() - 1);
			this.setActiveSheetInfo(BYNAME, NEGATIVEONE, sheetName);
		} else {
			if (indexOf(listOfWorksheets, sheetName) == -1) {
				throw new BotCommandException("Cannot find worksheet with name " + sheetName
						+ ". Please ensure the following: The sheet name is valid. The sheet is not hidden.");
			}
			this.setActiveSheetInfo(BYNAME, NEGATIVEONE, sheetName);
		}
		LOGGER.info("Updated Active Sheet Name: " + activeSheetInfo.getSheetName());
		return true;
	}

	@Override
	public void setActiveSheetWhileOpening(boolean isSpecificSheet, String sheetName) {
		if (!isSpecificSheet)
			activateSheet(true, 1, EMPTY);
		else {
			activateSheet(false, NEGATIVEONE, sheetName);
		}
	}

	@Override
	public Boolean goToCell(String cellOption, String cell) throws IOException {
		LOGGER.info("Performing GoToCell Command.. ");
		// initialize GoTOCellOption
		GoToCellOptions enmGoToCellOptions = EnumParser.valueOf(cellOption, GoToCellOptions.SpecificCell);

		// perform seperate functions as per the cell options..
		enmGoToCellOptions.goToCell(this, cell);
		return true;
	}

	@Override
	public String getSpreadSheetId() {
		if (spreadSheetDto == null) {
			LOGGER.error("This object has not been initialized yet. Returning null.");
			return null;
		} else {
			return spreadSheetDto.getSpreadSheetId();
		}
	}

	@Override
	public AuthenticationService getAuthenticationService() {
		return authService;
	}

	@Override
	public ListValue<StringValue> readColumn(String cellAddress, boolean readFullColumn) throws IOException {
		setupAuthorization();
		LOGGER.info("Performing Read column command.");
		String activeSheetName = this.activeSheetInfo.getSheetName();
		ValueRange valueRange = sheetService.spreadsheets().values().get(this.getSpreadSheetId(), activeSheetName)
				.setMajorDimension(COLUMNS).execute();
		// If sheet is empty
		if (valueRange.getValues() == null) {
			LOGGER.info("Returning empty list as sheet is empty.");
			return new ListValue<>();
		}
		// List of columns of worksheet
		List<List<Object>> columnList = valueRange.getValues();

		CellSplitter cellSplitter = new CellSplitter();
		cellSplitter.splitCell(cellAddress);
		int columnIndex = Utils.getColIndex(cellSplitter.getColumnName());
		// column is out of used range
		if (columnIndex > columnList.size()) {
			if (readFullColumn) {
				LOGGER.info("Returning empty list as column is empty.");
				return new ListValue<>();
			} else {
				LOGGER.error("Throwing error as column index is greater than used columns... ");
				throw new BotCommandException(
						"Unable to read values of column as address " + cellAddress + " is out of used range");
			}

		}
		List<Object> column = columnList.get(columnIndex - 1);

		// reading column from specific row
		if (!readFullColumn) {
			int rowIndex = Integer.parseInt(cellSplitter.getRowName());
			// specific row is out of range
			if (rowIndex > column.size()) {
				LOGGER.error("Throwing error as row index is greater than used rows.. ");
				throw new BotCommandException(
						"Unable to read values of column as address " + cellAddress + " is out of used range");
			}
			List<Object> columnTemp = new ArrayList<>();
			for (int i = 0; i < column.size(); i++) {
				if (i >= (rowIndex - 1))
					columnTemp.add(column.get(i));
			}
			column = columnTemp;
		}

		StringValue[] result = new StringValue[column.size()];
		int i = 0;
		for (Object value : column) {
			result[i++] = (new StringValue(value.toString()));
		}
		LOGGER.info("Returning following list of values for row " + columnIndex + " : " + column);
		return new ListValue<>(result);
	}

	@Override
	public ListValue<StringValue> readRow(String cellAddress, boolean readFullRow) throws IOException {

		setupAuthorization();
		LOGGER.info("Performing Read row command.");
		String activeSheetName = this.activeSheetInfo.getSheetName();
		ValueRange valueRange = sheetService.spreadsheets().values().get(this.getSpreadSheetId(), activeSheetName)
				.setMajorDimension(ROWS).execute();
		// If sheet is empty
		if (valueRange.getValues() == null) {
			LOGGER.info("Returning empty list as sheet is empty.");
			return new ListValue<>();
		}

		// List of rows of worksheet
		List<List<Object>> rowsList = valueRange.getValues();

		CellSplitter cellSplitter = new CellSplitter();
		cellSplitter.splitCell(cellAddress);
		int rowIndex = Integer.parseInt(cellSplitter.getRowName());
		// row is out of used range
		if (rowIndex > rowsList.size()) {
			if (readFullRow) {
				LOGGER.info("Returning empty list as row is empty.");
				return new ListValue<>();
			} else {
				LOGGER.error("Throwing error as row index is greater than used rows.. ");
				throw new BotCommandException(
						"Unable to read values of row as address " + cellAddress + " is out of used range");
			}
		}
		List<Object> row = rowsList.get(rowIndex - 1);
		// reading row from specific column
		if (!readFullRow) {
			int colIndex = Utils.getColIndex(cellSplitter.getColumnName());
			// specific column is out of range
			if (colIndex > row.size()) {
				LOGGER.error("Throwing error as column index is greater than used columns.. ");
				throw new BotCommandException(
						"Unable to read values of row as address " + cellAddress + " is out of used range");
			}
			List<Object> rowTemp = new ArrayList<>();
			for (int i = 0; i < row.size(); i++) {
				if (i >= (colIndex - 1))
					rowTemp.add(row.get(i));
			}
			row = rowTemp;
		}

		StringValue[] result = new StringValue[row.size()];
		int i = 0;
		for (Object value : row) {
			result[i++] = (new StringValue(value.toString()));
		}
		LOGGER.info("Returning following list of values for row " + rowIndex + " : " + row);
		return new ListValue<>(result);
	}


	@Override
	public Boolean closeSpreadSheet() {
		LOGGER.info("Closing soreadhseet.....");
		this.spreadSheetDto = null;
		this.activeSheetInfo = null;
		activeCellMap.clear();
		LOGGER.info("Spreadsheet closed....");
		return true;
	}

	@Override
	public Boolean deleteCell(String deleteOption, String cell) throws IOException {
		setupAuthorization();
		DeleteRangeRequest deleteRangeRequest = new DeleteRangeRequest();
		deleteRangeRequest.setRange(Utils.convertToGridRange(cell, this.getActiveSheetInfo().getSheetId()));
		if (LEFT.equals(deleteOption))
			deleteRangeRequest.setShiftDimension(COLUMNS);
		else
			deleteRangeRequest.setShiftDimension(ROWS);
		List<Request> requests = new ArrayList<Request>();
		requests.add(new Request().setDeleteRange(deleteRangeRequest));
		BatchUpdateSpreadsheetRequest batchUpdateSpreadsheetRequest = new BatchUpdateSpreadsheetRequest();
		batchUpdateSpreadsheetRequest.setRequests(requests);
		sheetService.spreadsheets().batchUpdate(this.getSpreadSheetId(), batchUpdateSpreadsheetRequest).execute();
		return true;
	}

	@Override
	public Boolean deleteDimension(String cell, String deleteOption) throws IOException {
		setupAuthorization();
		GridRange gridRange = Utils.convertToGridRange(cell, this.getActiveSheetInfo().getSheetId());
		DeleteDimensionRequest deleteDimensionRequest = new DeleteDimensionRequest();
		DimensionRange dimensionRange = new DimensionRange();
		if (ROWS.equals(deleteOption)) {
			dimensionRange.setSheetId(this.getActiveSheetInfo().getSheetId()).setDimension(ROWS)
					.setStartIndex(gridRange.getStartRowIndex()).setEndIndex(gridRange.getEndRowIndex());
		} else {
			dimensionRange.setSheetId(this.getActiveSheetInfo().getSheetId()).setDimension(COLUMNS)
					.setStartIndex(gridRange.getStartColumnIndex()).setEndIndex(gridRange.getEndColumnIndex());
		}
		deleteDimensionRequest.setRange(dimensionRange);
		List<Request> requests = new ArrayList<Request>();
		requests.add(new Request().setDeleteDimension(deleteDimensionRequest));
		BatchUpdateSpreadsheetRequest batchUpdateSpreadsheetRequest = new BatchUpdateSpreadsheetRequest();
		batchUpdateSpreadsheetRequest.setRequests(requests);
		sheetService.spreadsheets().batchUpdate(this.getSpreadSheetId(), batchUpdateSpreadsheetRequest).execute();
		return true;
	}

	@Override
	public int getNumberOfRows(String spreadSheetId, String fetchRowsType) throws IOException {
		setupAuthorization();

		LOGGER.info("Performing Get number of rows command.");
		String range = this.activeSheetInfo.getSheetName();
		System.out.println("---------------------------------------range " + range);
		Sheets.Spreadsheets.Values.Get request = sheetService.spreadsheets().values().get(spreadSheetId, range);
		LOGGER.info("Current Sheet Name   " + this.activeSheetInfo.getSheetName().toString());
		List<List<Object>> response = request.execute().getValues();
		if (response == null)
			return 0;

		if (fetchRowsType.equals(TOTALROWS)) {
			return response.size();
		} else if (fetchRowsType.equals(NONEMPTYROWS)) {
			int nonEmptyRows = 0;
			for (List<Object> rowList : response) {
				if (!rowList.isEmpty())
					nonEmptyRows++;
			}
			return nonEmptyRows;
		}

		return 0;
	}

	@Override
	public int getUsedRowColumns(String dimension) {
		try {
			setupAuthorization();
			LOGGER.info("Performing Get number of rows/columns command.");
			Sheets.Spreadsheets.Values.Get request = sheetService.spreadsheets().values().get(this.getSpreadSheetId(),
					getActiveSheetName());
			request.setMajorDimension(dimension);

			LOGGER.info("Current Sheet Name   " + getActiveSheetName().toString());
			List<List<Object>> response = request.execute().getValues();
			if (response == null)
				return 0;
			else
				return response.size();
		} catch (IOException exception) {
			LOGGER.error("Exception occurred during execution of Get number of rows/columns .....", exception);
			throw new BotCommandException(exception);
		} catch (Exception exception) {
			LOGGER.error("Exception occurred during execution of Get number of rows/columns ....", exception);
			throw new BotCommandException(exception);
		}
	}

	@Override
	public String setCellFormula(String cellAddress, String formula) throws IOException {
		setupAuthorization();
		String activeSheetName = this.activeSheetInfo.getSheetName();
		String spreadSheetId = this.getSpreadSheetId();
		String[] addresses = cellAddress.split(COLON);
		CellSplitter cellSplitter = new CellSplitter();

		cellSplitter.splitCell(addresses[0]);
		int startRowIndex = Integer.parseInt(cellSplitter.getRowName());
		int startColumnIndex = Utils.getColIndex(cellSplitter.getColumnName());
		cellSplitter.splitCell(addresses[1]);
		int endRowIndex = Integer.parseInt(cellSplitter.getRowName());
		int endColumnIndex = Utils.getColIndex(cellSplitter.getColumnName());
		List<List<String>> values = new ArrayList<List<String>>();
		for (int i = 0; i < (int) Math.abs(endRowIndex - startRowIndex + 1); i++) {
			List<String> rowValues = new ArrayList<String>();
			for (int j = 0; j < (int) Math.abs(endColumnIndex - startColumnIndex + 1); j++) {
				rowValues.add(formula);
			}
			values.add(rowValues);
		}
		cellAddress = activeSheetName + EXCLAMATION + cellAddress;
		List<List<Object>> objectList = (List) values;
		List<ValueRange> data = new ArrayList<ValueRange>();
		data.add(new ValueRange().setRange(cellAddress).setValues(objectList).setMajorDimension(ROWS));
		// Additional ranges to update ...
		BatchUpdateValuesRequest body = new BatchUpdateValuesRequest().setValueInputOption(USER_ENTERED).setData(data);
		sheetService.spreadsheets().values().batchUpdate(spreadSheetId, body).execute();

		setActiveCell(activeSheetName, cellAddress.split(COLON)[1]);

		return spreadSheetId;
	}

	@Override
	public String setSingleCell(String cellAddress, String value) throws IOException {
		setupAuthorization();
		String valueInputOption = USER_ENTERED;
		List<List<Object>> values = Arrays.asList(Arrays.asList(value));
		String sheetName = this.activeSheetInfo.getSheetName();
		String range = sheetName + EXCLAMATION + cellAddress;
		ValueRange requestBody = new ValueRange().setValues(values);
		String spreadSheetId = this.getSpreadSheetId();
		sheetService.spreadsheets().values().update(spreadSheetId, range, requestBody)
				.setValueInputOption(valueInputOption).execute();

		setActiveCell(sheetName, cellAddress);
		return spreadSheetId;
	}

	@Override
	public Boolean formatCell(String spreadSheetId, String cellOption, String cellAddress, String cellRange,
			boolean isFont, String fontName, Number fontSize, boolean isBold, boolean isItalic, boolean isUnderline,
			boolean isStrikethrough, String fontColor, boolean isAlignment, String verticalAlignment,
			String horizontalAlignment, boolean isWrapText, String wrapText, boolean isMerge, String mergeType)
			throws IOException {
		List<Request> requests = new ArrayList<Request>();
		LOGGER.info("Performing Format Cell Command.. ");
		GridRange gridRange;
		int sheetId = this.getActiveSheetInfo().getSheetId();

		LOGGER.info("Performing Format Cell Command On SpreadSheet =" + spreadSheetId + "\tWith Sheet ="
				+ this.getActiveSheetInfo().getSheetName());

		if (MULTIPLE.equals(cellOption))
			gridRange = Utils.convertToGridRange(cellRange, sheetId);
		else
			gridRange = Utils.convertToGridRange(cellAddress, sheetId);

		LOGGER.info("Grid Proerties of Sheet Id : " + sheetId);
		CellFormat cellFormat = new CellFormat();
		ColorUtils colorUtils = new ColorUtils();

		if (isFont) {
			cellFormat.setTextFormat(new TextFormat().setBold(isBold).setItalic(isItalic).setFontFamily(fontName)
					.setFontSize(fontSize.intValue()).setUnderline(isUnderline).setStrikethrough(isStrikethrough)
					.setForegroundColor(colorUtils.getColorObjectFromColorName(fontColor)));
			LOGGER.info(" Font Done ");
		}
		if (isAlignment) {
			if (!NONE.equals(verticalAlignment))
				cellFormat.setVerticalAlignment(verticalAlignment);
			if (!NONE.equals(horizontalAlignment))
				cellFormat.setHorizontalAlignment(horizontalAlignment);
			LOGGER.info(" Alignment  Done ");
		}

		if (isWrapText) {
			cellFormat.setWrapStrategy(wrapText);
			LOGGER.info(" Wrap Text Done ");
		}

		requests.add(new Request().setRepeatCell(new RepeatCellRequest().setRange(gridRange)
				.setCell(new CellData().setUserEnteredFormat(cellFormat)).setFields(
						"userEnteredFormat(backgroundColor,textFormat,horizontalAlignment,verticalAlignment,wrapStrategy)")));

		if (isMerge) {
			if (mergeType.equals("UNMERGE")) {
				UnmergeCellsRequest unmergeCellsRequest = new UnmergeCellsRequest();
				unmergeCellsRequest.setRange(gridRange);
				requests.add(new Request().setUnmergeCells(unmergeCellsRequest));

			} else {
				MergeCellsRequest mergeCellRequest = new MergeCellsRequest();
				mergeCellRequest.setMergeType(mergeType).setRange(gridRange);
				requests.add(new Request().setMergeCells(mergeCellRequest));
			}
			LOGGER.info(" Merge Done ");
		}

		BatchUpdateSpreadsheetRequest batchUpdateSpreadsheetRequest = new BatchUpdateSpreadsheetRequest()
				.setRequests(requests);
		sheetService.spreadsheets().batchUpdate(spreadSheetId, batchUpdateSpreadsheetRequest).execute();
		LOGGER.info(" Service Request Done");
		if (MULTIPLE.equals(cellOption))
			setActiveCell(this.getActiveSheetInfo().getSheetName(), cellRange.split(COLON)[1]);
		else
			setActiveCell(this.getActiveSheetInfo().getSheetName(), cellAddress);

		return true;

	}

	@Override
	public int retrieveSheetCount(boolean excludeHiddenWorksheets) throws IOException {
		setupAuthorization();
		LOGGER.info("Performing Retrieve sheet count command.");

		List<Sheet> sheetList = sheetService.spreadsheets().get(this.getSpreadSheetId()).setIncludeGridData(false)
				.execute().getSheets();

		int sheetCount = sheetList.size();
		if (excludeHiddenWorksheets) {
			for (Sheet sheet : sheetList) {
				if (sheet.getProperties().containsKey(HIDDEN) && sheet.getProperties().getHidden())
					sheetCount--;
			}
			LOGGER.info("Retrieved sheet count " + sheetCount + " when exclude hidden sheets is set to "
					+ excludeHiddenWorksheets);
		}

		return sheetCount;
	}

	@Override
	public Boolean hideWorksheet(String worksheetName) throws IOException {
		setupAuthorization();
		String spreadSheetId = this.getSpreadSheetId();
		LOGGER.info("Performing Hide worksheet command.");
		if (isHidden(worksheetName))
			return true;
		List<String> listOfWorksheets = getWorksheetNames(true);
		if (listOfWorksheets.size() == 1) {
			throw new BotCommandException("Cannot hide only sheet within the workbook.");
		}

		if (indexOf(listOfWorksheets, worksheetName) == -1) {
			throw new BotCommandException("Cannot find worksheet with name " + worksheetName
					+ ". Please ensure the following: The sheet name is valid. The sheet is not hidden.");
		}

		int sheetId = getWorksheetIdByName(worksheetName, true);

		List<Request> requests = new ArrayList<>();
		// add sheet id in SheetProperties object
		SheetProperties sheetProperties = new SheetProperties();
		sheetProperties.setTitle(worksheetName);
		sheetProperties.setSheetId(sheetId);
		sheetProperties.setHidden(true);
		requests.add(new Request().setUpdateSheetProperties(
				new UpdateSheetPropertiesRequest().setProperties(sheetProperties).setFields(HIDDEN)));
		BatchUpdateSpreadsheetRequest batchUpdateSpreadsheetRequest = new BatchUpdateSpreadsheetRequest();
		batchUpdateSpreadsheetRequest.setRequests(requests);
		sheetService.spreadsheets().batchUpdate(spreadSheetId, batchUpdateSpreadsheetRequest).execute();
		LOGGER.info("Worksheet with name " + worksheetName + " has successfully hidden.");
		// change activeSheetInfo if active sheet will be hidden
		String activeSheet = this.activeSheetInfo.getSheetName();
		System.out.println("-----------------------------------------------------" + activeSheet);
		if (activeSheet.equalsIgnoreCase(worksheetName)) {
			int activeSheetIndex = indexOf(listOfWorksheets, activeSheet);
			// if active sheet is on 0th index
			if (activeSheetIndex == 0) {
				setActiveSheetInfo(BYNAME, -1, listOfWorksheets.get(1));
			} else {
				setActiveSheetInfo(BYNAME, -1, listOfWorksheets.get(activeSheetIndex - 1));
			}
		}

		return true;
	}

	@Override
	public Boolean showWorksheet(String spreadSheetId, String worksheetName) throws IOException {
		setupAuthorization();
		LOGGER.info("Performing Show worksheet command.");

		// getting worksheet id from worksheet name
		int sheetId = this.getWorksheetIdByName(worksheetName, true);

		List<Request> requests = new ArrayList<>();
		// add sheet id in SheetProperties object
		SheetProperties sheetProperties = new SheetProperties();
		sheetProperties.setTitle(worksheetName);
		sheetProperties.setSheetId(sheetId);
		sheetProperties.setHidden(false);
		requests.add(new Request().setUpdateSheetProperties(
				new UpdateSheetPropertiesRequest().setProperties(sheetProperties).setFields(HIDDEN)));
		BatchUpdateSpreadsheetRequest batchUpdateSpreadsheetRequest = new BatchUpdateSpreadsheetRequest();
		batchUpdateSpreadsheetRequest.setRequests(requests);
		sheetService.spreadsheets().batchUpdate(spreadSheetId, batchUpdateSpreadsheetRequest).execute();
		return true;
	}

	@Override
	public Boolean autofitRows() {
		try {
			setupAuthorization();
			LOGGER.info("Performing Autofit rows command.");
			Integer sheetId = this.activeSheetId;

			List<Request> requests = new ArrayList<>();
			// Create Dimension range object and set the parameters.
			DimensionRange dimensionRange = new DimensionRange();
			dimensionRange.setSheetId(sheetId);
			dimensionRange.setDimension(ROWS);
			// Add the request.
			requests.add(new Request()
					.setAutoResizeDimensions(new AutoResizeDimensionsRequest().setDimensions(dimensionRange)));
			// Create batch update request.
			BatchUpdateSpreadsheetRequest batchUpdateSpreadsheetRequest = new BatchUpdateSpreadsheetRequest();
			batchUpdateSpreadsheetRequest.setRequests(requests);
			sheetService.spreadsheets()
					.batchUpdate(this.spreadSheetDto.getSpreadSheetId(), batchUpdateSpreadsheetRequest).execute();
			return true;
		} catch (IOException exception) {
			LOGGER.error("Exception occurred during execution of Autofit rows command.....", exception);
			throw new BotCommandException(exception);
		}
	}

	@Override
	public Boolean autofitColumns() {
		try {
			setupAuthorization();
			LOGGER.info("Performing Autofit columns command.");

			List<Request> requests = new ArrayList<>();
			// Create Dimension range object and set the parameters.
			DimensionRange dimensionRange = new DimensionRange();
			dimensionRange.setSheetId(this.activeSheetId);
			dimensionRange.setDimension(COLUMNS);
			// Add the request.
			requests.add(new Request()
					.setAutoResizeDimensions(new AutoResizeDimensionsRequest().setDimensions(dimensionRange)));
			// Create batch update request.
			BatchUpdateSpreadsheetRequest batchUpdateSpreadsheetRequest = new BatchUpdateSpreadsheetRequest();
			batchUpdateSpreadsheetRequest.setRequests(requests);
			sheetService.spreadsheets()
					.batchUpdate(this.spreadSheetDto.getSpreadSheetId(), batchUpdateSpreadsheetRequest).execute();
			return true;
		} catch (IOException exception) {
			LOGGER.error("Exception occurred during execution of Autofit columns command.....", exception);
			throw new BotCommandException(exception);
		}

	}

	@Override
	public boolean setCellColor(String spreadSheetId, String cellAddress, String setColorOption, String color)
			throws IOException {

		setupAuthorization();
		LOGGER.info("Performing Set Cell Color Command.. \n");

		Integer sheetId = this.activeSheetInfo.getSheetId();
		GridRange gridRange;
		gridRange = Utils.convertToGridRange(cellAddress, sheetId);
		// preparing CellData object
		ColorUtils colorUtils = new ColorUtils();
		CellFormat cellFormat = new CellFormat();
		String fieldData = "";
		if (setColorOption.equals(CELL)) {
			cellFormat.setBackgroundColor(colorUtils.getColorObjectFromColorName(color));
			fieldData = "userEnteredFormat.backgroundColor";
		} else if (setColorOption.equals(TEXT)) {
			cellFormat
					.setTextFormat(new TextFormat().setForegroundColor(colorUtils.getColorObjectFromColorName(color)));
			fieldData = "userEnteredFormat.textFormat.foregroundColor";
		}
		CellData cellData = new CellData();
		cellData.setUserEnteredFormat(cellFormat);

		// preparing request to set color.
		List<Request> requests = new ArrayList<>();
		requests.add(new Request()
				.setRepeatCell(new RepeatCellRequest().setRange(gridRange).setCell(cellData).setFields(fieldData)));
		BatchUpdateSpreadsheetRequest batchUpdateSpreadsheetRequest = new BatchUpdateSpreadsheetRequest();
		batchUpdateSpreadsheetRequest.setRequests(requests);
		sheetService.spreadsheets().batchUpdate(spreadSheetId, batchUpdateSpreadsheetRequest).execute();
		return true;
	}

	@Override
	public ListValue<StringValue> getCellColor(String spreadSheetId, String cellAddress, Boolean cellColor,
			Boolean textColor, Boolean getColorOptionByName) throws IOException {

		setupAuthorization();
		LOGGER.info("Performing Get Cell Color Command.. \n");

		Integer sheetId = this.activeSheetInfo.getSheetId();

		List<DataFilter> dataFiltersList = new ArrayList<>();
		DataFilter dataFilter = new DataFilter();

		GridRange gridRange = Utils.convertToGridRange(cellAddress, sheetId);

		dataFilter.setGridRange(gridRange);
		GetSpreadsheetByDataFilterRequest requestBody = new GetSpreadsheetByDataFilterRequest();
		dataFiltersList.add(dataFilter);
		requestBody.setDataFilters(dataFiltersList);
		requestBody.setIncludeGridData(true);
		CellFormat cellFormat = sheetService.spreadsheets().getByDataFilter(spreadSheetId, requestBody).execute()
				.getSheets().get(0).getData().get(0).getRowData().get(0).getValues().get(0).getEffectiveFormat();

		ColorUtils colorUtils = new ColorUtils();
		// prepare cell color object
		Color cellColorObject;
		cellColorObject = cellFormat.getBackgroundColor();
		LOGGER.info(
				"Converting Google API's color object for cell/text color into color name or hex code. Cell Color object: "
						+ cellColorObject);
		String cellColorResponse = colorUtils.getColorFromColorObject(cellColorObject, getColorOptionByName);
		LOGGER.info("Successfully converted.");

		// prepare text color object
		Color textColorObject;
		String textColorResponse = "";
		if (textColor) {
			textColorObject = cellFormat.getTextFormat().getForegroundColor();
			LOGGER.info(
					"Converting Google API's color object for text color into color name or hex code. Text Color object: "
							+ textColorObject);
			textColorResponse = colorUtils.getColorFromColorObject(textColorObject, getColorOptionByName);
			LOGGER.info("Successfully converted.");
		}
		// text color is not selected, return cell color.
		if (!textColor) {
			LOGGER.info("Returning cell color. Cell color: " + cellColorResponse);
			return new ListValue<>(new StringValue(cellColorResponse));
		}
		// when only text color is selected.
		else if (!cellColor) {
			LOGGER.info("Returning text color. Text color: " + textColorResponse);
			return new ListValue<>(new StringValue(textColorResponse));
		} else {
			LOGGER.info("Returning cell color and text color. Cell color: " + cellColorResponse + "	Text color: "
					+ textColorResponse);
			StringValue[] color = new StringValue[2];
			color[0] = new StringValue(cellColorResponse);
			color[1] = new StringValue(textColorResponse);
			return new ListValue<>(color);
		}
	}

	// only used in rename sheet if currenty we are not getting proper error thrown
	// by api on hopper
	public List<String> getWorksheetNames(boolean excludeHiddenWorksheetNames) {
		try {
			// API call to get all the worksheet names within current workbook
			List<Sheet> sheetList = sheetService.spreadsheets().get(this.getSpreadSheetId()).setIncludeGridData(false)
					.execute().getSheets();

			List<String> listOfWorksheetNames = new ArrayList<>();

			// if the worksheet is hidden is skipped if excludeHiddenWorksheetNames is true
			for (int i = 0; i < sheetList.size(); i++) {
				if (excludeHiddenWorksheetNames) {
					Boolean isHidden = sheetList.get(i).getProperties().getHidden();
					if (isHidden != null && isHidden)
						continue;
				}
				listOfWorksheetNames.add(sheetList.get(i).getProperties().getTitle());
			}
			return listOfWorksheetNames;
		} catch (IOException exception) {
			throw new BotCommandException(exception);
		}
	}

	public int indexOf(List<String> worksheetNames, String sheetName) {
		int index = 0;
		Iterator<String> iterator = worksheetNames.iterator();
		while (iterator.hasNext()) {
			if ((iterator.next()).equalsIgnoreCase(sheetName))
				return index;
			else
				index++;
		}

		return -1;
	}

	@Override
	public String getWorksheetNameByIndex(int index, boolean excludeHiddenSheets) {
		try {
			index--;
			List<Sheet> sheetList = sheetService.spreadsheets().get(this.getSpreadSheetId()).setIncludeGridData(false)
					.execute().getSheets();
			// invalid index
			if (index < 0 || index >= sheetList.size())
				throw new BotCommandException("Cannot find worksheet at index " + (index + 1)
						+ ". Please ensure that the sheet index is valid.");
			else {
				// Does not consider hidden worksheet if excludeHiddenSheets is true
				if (excludeHiddenSheets) {
					Boolean isHidden = sheetList.get(index).getProperties().getHidden();
					if (isHidden != null && isHidden)
						throw new BotCommandException("Cannot find worksheet at index " + (index + 1)
								+ ". Please ensure that the sheet is not hidden.");
					else
						return sheetList.get(index).getProperties().getTitle();
				}
				return sheetList.get(index).getProperties().getTitle();
			}
		} catch (IOException e) {
			throw new BotCommandException(e);
		}
	}

	@Override
	public Integer getWorksheetIndexByName(String sheetName, boolean excludeHiddenSheets) {
//		List<Sheet> sheetList;
//		try {
//			sheetList = sheetService.spreadsheets().get(this.getSpreadSheetId()).setIncludeGridData(false).execute()
//					.getSheets();
		int index = -1;
//			// iterate through all the sheets
//			for (int i = 0; i < sheetList.size(); i++) {
//				if (sheetName.equalsIgnoreCase(sheetList.get(i).getProperties().getTitle())) {
//					// Does not consider hidden worksheet if excludeHiddenSheets is true
//					if (excludeHiddenSheets) {
//						Boolean isHidden = sheetList.get(i).getProperties().getHidden();
//						if (isHidden != null && isHidden)
//							throw new BotCommandException("Cannot find worksheet with name " + sheetName
//									+ ". Please ensure that the sheet is not hidden.");
//						else {
//							index = i;
//							break;
//						}
//					}
//					index = i;
//					break;
//				}
//			}
//			// If sheet doesn't exist
//			if (index < 0 || index >= sheetList.size())
//				throw new BotCommandException("Sheet with name " + sheetName + " doesn't exist.");
//
		return index;
//		} catch (IOException e) {
//			throw new BotCommandException(e);
//		}
	}

	@Override
	public int getWorksheetIdByName(String sheetName, boolean excludeHiddenSheets) {
		try {

			List<Sheet> sheetList = sheetService.spreadsheets().get(this.getSpreadSheetId()).setIncludeGridData(false)
					.execute().getSheets();
			int sheetId = -1;
			// iterate through all the sheets
			for (int i = 0; i < sheetList.size(); i++) {
				if (sheetName.equalsIgnoreCase(sheetList.get(i).getProperties().getTitle())) {
					sheetId = sheetList.get(i).getProperties().getSheetId();
					break;
				}
			}
			if (sheetId < 0)
				throw new BotCommandException(
						"Cannot find sheet with name " + sheetName + ". Please specify a valid sheet name.");

			return sheetId;
		} catch (IOException e) {
			throw new BotCommandException(e);
		}
	}

	public void setActiveSheetInfo(String sheetOption, int index, String sheetName) {
		try {
			LOGGER.info("Inside setActiveSheetInfo.....");
			this.activeSheetInfo = new WorksheetDto();
			List<Sheet> sheetsList = sheetService.spreadsheets().get(this.getSpreadSheetId()).setIncludeGridData(false)
					.execute().getSheets();
			SheetProperties sheetProperties = new SheetProperties();
			// If sheet is activated by index.
			if (BYINDEX.equals(sheetOption)) {
				// Check for valid index
				LOGGER.info("Activate sheet by index.....");
				if (index < 0 || index >= sheetsList.size())
					throw new BotCommandException("Sheet index doesn't exist");
				else {
					// Get sheet properties.
					sheetProperties = sheetsList.get(index - 1).getProperties();
				}
			} else {
				LOGGER.info("Activating sheet by name.....");
				index = NEGATIVEONE;
				// Iteration to match sheet name.
				for (int i = 0; i < sheetsList.size(); i++) {
					if (sheetName.equalsIgnoreCase(sheetsList.get(i).getProperties().getTitle())) {
						LOGGER.info("Sheet found at index:" + index);
						index = i;
						sheetProperties = sheetsList.get(index).getProperties();
						break;
					}
				}
				// If sheet is not found.
				if (index == NEGATIVEONE)
					throw new BotCommandException("Sheet with name " + sheetName + " not found.");
			}

//			// Cannot activate hidden worksheet
//			Boolean isHidden = sheetProperties.getHidden();
//			if (isHidden != null && isHidden) {
//				throw new BotCommandException("Cannot find worksheet with name " + sheetProperties.getTitle()
//						+ ". Please ensure that the sheet is not hidden.");
//			}

			this.activeSheetInfo.setSheetName(sheetProperties.getTitle());
			this.activeSheetInfo.setSheetId(sheetProperties.getSheetId());
			this.activeSheetInfo.setIndex(index);
			LOGGER.info("Active sheet changed to " + activeSheetInfo.getSheetName());
			if (!activeCellMap.containsKey(this.activeSheetInfo.getSheetName()))
				this.setActiveCell(this.activeSheetInfo.getSheetName(), STARTCELLADDRESS);
		} catch (IOException e) {
			LOGGER.info("IOException occurred.....");
			throw new BotCommandException(e);
		}
	}

	@Override
	public TableValue getMultipleCells(boolean cellOption, String cellRange) {
		try {
			Row row;
			List<Value> valueList;
			List<Row> tableRow = new ArrayList<>();
			Table table = new Table();
			List<List<Object>> cellValue = null;
			ValueRange cellValueRange = null;
			// To get multiple cells by range.
			if (cellOption)
				cellValueRange = this.getValue(this.getSpreadSheetId(), this.getActiveSheetInfo().getSheetName(),
						cellRange, UNFORMATTED_VALUE);
			else
				cellValueRange = this.getValue(this.getSpreadSheetId(), this.getActiveSheetInfo().getSheetName(), null,
						UNFORMATTED_VALUE);
			LOGGER.info("Valuerange = " + cellValueRange);
			// To get the cellvalues from ValueRange object.
			if (cellValueRange.getValues() != null)
				cellValue = cellValueRange.getValues();
			// If value returned is null.
			if (cellValue == null)
				return null;
			// Iterate over the rows.
			for (List<Object> list : cellValue) {
				if (list != null) {

					row = new Row();
					valueList = new ArrayList<>();
					// Iterate over each cell in the row.
					for (Object object : list) {
						valueList.add(new StringValue("" + object));
					}
					row.setValues(valueList);
					tableRow.add(row);
				}
			}
			table.setRows(tableRow);
			return new TableValue(table);
		} catch (IOException e) {
			LOGGER.error("IOException Occurred.....");
			throw new BotCommandException(e);
		}
	}

	public boolean isHidden(String sheetName) throws IOException {
		List<Sheet> sheetsList = sheetService.spreadsheets().get(this.getSpreadSheetId()).setIncludeGridData(false)
				.execute().getSheets();
		int index = NEGATIVEONE;
		for (int i = 0; i < sheetsList.size(); i++) {
			if (sheetName.equalsIgnoreCase(sheetsList.get(i).getProperties().getTitle())) {
				index = i;
				Boolean isHidden = sheetsList.get(i).getProperties().getHidden();
				if (isHidden != null && isHidden) {
					return true;
				}
			}
		}

		if (index == NEGATIVEONE)
			throw new BotCommandException("Sheet with name " + sheetName + " not found.");
		return false;
	}

}
