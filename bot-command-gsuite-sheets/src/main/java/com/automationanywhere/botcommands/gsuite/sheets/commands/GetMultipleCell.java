/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.DEFAULT_VALUE_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.DESCRIPTION_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.FALSE;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.TRUE;
import static com.automationanywhere.botcommand.gsuite.Constants.rangeValidator;
import static com.automationanywhere.commandsdk.model.AttributeType.RADIO;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
import static com.automationanywhere.commandsdk.model.DataType.TABLE;

import java.util.Map;

import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.MatchesRegex;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

@BotCommand
@CommandPkg(name = "GetMultipleCells", label = "Get multiple cells", node_label = ":| {{cellOption}}| {{multipleCell}}| from {{sheetName}} worksheet", description = "Gets the value(s) of multiple cells within a worksheet", icon = "excel.svg", return_label = "Assign the output to variable", return_required = true, return_type = TABLE, return_sub_type = STRING)

public class GetMultipleCell {

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public TableValue execute(
			@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String sessionName,
			@Idx(index = "2", type = RADIO, options = {
					@Idx.Option(index = "2.1", pkg = @Pkg(label = "Multiple cells", value = TRUE)),
					@Idx.Option(index = "2.2", pkg = @Pkg(label = "All cells", value = FALSE)) }) @Pkg(label = "Cell option", default_value = TRUE, default_value_type = STRING) @NotEmpty String cellOption,
			@Idx(index = "2.1.1", type = TEXT) @Pkg(label = "Cell range", description = "e.g. A1:D10") @MatchesRegex(rangeValidator) @NotEmpty String cellRange) {
		GSheets gSheets = (GSheets) sessionMap.get(sessionName);
		ExecutorValidator.getInstance().validate(gSheets);
		return gSheets.getMultipleCells(Boolean.parseBoolean(cellOption), cellRange);
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}
