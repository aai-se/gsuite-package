package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.impl.GSheetsImpl;
import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommands.gsuite.sheets.commands.GoToCell;
import com.automationanywhere.botcommands.gsuite.sheets.commands.OpenSheets;
import com.automationanywhere.botcommands.gsuite.sheets.test.utils.ConstantsTest;

public class GoToCellJunit extends AbstractAuthUtils {
	GoToCell goToCell;
	OpenSheets openSheets;
	String session = "Default";
	GSheetsImpl gsheet;

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {

		launchOAuthConsentScreen(ConstantsTest.getUserName(), ConstantsTest.getClientid(),
				ConstantsTest.getClientSecret(), ConstantsTest.getRedirectUri());

		openSheets = new OpenSheets();
		openSheets.setSessionMap(sessionMap);
		openSheets.execute(ConstantsTest.getUserName(), session, "BYNAME", "Demo", null, null, true, "Megha", false);
		goToCell = new GoToCell();
		goToCell.setSessionMap(sessionMap);
		gsheet = new GSheetsImpl();
		gsheet.setSessionMap(sessionMap);

	}

	@Test
	public void GoToCellTest() throws JSONException {
		String[] cellAddress = { "K9", "L6", "B7", "ZZZ3", "G10", "O31", "J10"};
		for (int i = 0; i < cellAddress.length; i++) {
			executeGotoCell(cellAddress[i]);
		}
	}
	
	@Test(expectedExceptions = BotCommandException.class)
	public void GotoCellExceptionForLeft() throws JSONException {
			executeGotoCell("A1");
	}
	
	@Test(expectedExceptions = BotCommandException.class)
	public void GotoCellExceptionForUp() throws JSONException {
			executeGotoCell("C1");
	}
	
	@Test(expectedExceptions = BotCommandException.class)
	public void GotoCellExceptionForDown() throws JSONException {
			executeGotoCell("D40001");
	}
	
	@Test(expectedExceptions = BotCommandException.class)
	public void GotoCellExceptionForRight() throws JSONException {
			executeGotoCell("A100");
	}
	
	@Test(expectedExceptions = BotCommandException.class)
	public void GotoCellExceptionForInvalidIndex() throws JSONException {
			executeGotoCell("ZZZZ40000000");
	}
	
	public void executeGotoCell(String cellAddress) throws BotCommandException, JSONException {
		System.out.println("---------------------------------------- Go To cell----------------------------------");
		goToCell.execute(session, "specificCell", cellAddress);

		System.out.println(
				"---------------------------------------- Go To cell Left----------------------------------");
		goToCell.execute(session, "OneCellLeft", cellAddress);

		System.out.println(
				"---------------------------------------- Go To cell Right----------------------------------");
		goToCell.execute(session, "OneCellRight", cellAddress);

		System.out.println(
				"---------------------------------------- Go To cell Up----------------------------------");
		goToCell.execute(session, "OneCellUp", cellAddress);

		System.out.println(
				"---------------------------------------- Go To cell Down----------------------------------");
		goToCell.execute(session, "OneCellDown", cellAddress);

		System.out.println(
				"---------------------------------------- Go To cell Beginning Of Row----------------------------------");
		goToCell.execute(session, "BeginningOfRow", cellAddress);

		System.out.println(
				"---------------------------------------- Go To cell End Of Row----------------------------------");
		goToCell.execute(session, "EndOfRow", cellAddress);

		System.out.println(
				"---------------------------------------- Go To cell Beginning Of Column----------------------------------");
		goToCell.execute(session, "BeginningOfColumn", cellAddress);

		System.out.println(
				"---------------------------------------- Go To cell End Of Column----------------------------------");
		goToCell.execute(session, "EndOfColumn", cellAddress);
	}
}
