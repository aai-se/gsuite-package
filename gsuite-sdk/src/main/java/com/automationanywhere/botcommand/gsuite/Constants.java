/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommand.gsuite;

import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;

public class Constants {
	public static final Messages MESSAGES = MessagesFactory
			.getMessages("com.automationanywhere.botcommand.gsuite.messages.messages");

	public static final String APPLICATION_NAME = "AAE Hopper bot";
	public static final String MIME_TYPE_FOLDER = "application/vnd.google-apps.folder";
	public static final String MIME_TYPE_SPREADSHEET = "application/vnd.google-apps.spreadsheet";
	public static final String MIME_TYPE_DOCUMENT = "application/vnd.google-apps.document";
	public static final String MIME_TYPE_PDF = "application/pdf";
	public static final String MIME_TYPE_PPT = "application/vnd.google-apps.presentation";
	public static final String MIME_TYPE_PLAIN = "text/plain";
	public static final String MIME_TYPE_CSV = "text/csv";
	public static final String MIME_TYPE_IMAGE = "application/vnd.google-apps.drawing";
	public static final String LABEL_ACTIVECELL = "Active cell";
	public static final String LABEL_SPECIFICCELL = "Specific cell";
	public static final String LABEL_MULTIPLECELL = "Multiple cells";
	public static final String ROWS = "ROWS";
	public static final String COLUMNS = "COLUMNS";
	public static final String LABEL_SESSION = "Session name";
	public static final String DESCRIPTION_SESSION = "e.g. Session1 or S1";
	public static final String DESCRIPTION_CELL_ADDRESS = "e.g. A1";
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	public static final String ACTIVECELL = "ACTIVECELL";
	public static final String RANGE = "RANGE";
	public static final String DEFAULT_VALUE_SESSION = "Default";
	public static final String SPECIFICCELL = "SPECIFICCELL";
	public static final String rowIndexValidator = "^((10[0-4][0-8][0-5][0-7][0-6])|([1-9][0-9]{1,5})|([1-9]))$";
	public static final String rowRangeValidator = "^((10[0-4][0-8][0-5][0-7][0-6])|([1-9][0-9]{1,5})|([1-9])):((10[0-4][0-8][0-5][0-7][0-6])|([1-9][0-9]{1,5})|([1-9]))$";
	public static final String columnRangeValidator = "^(([A-W][A-Z]{1,2})|(X[A-E][A-Z])|(XF[A-D])|([A-Z]{1,2})):(([A-W][A-Z]{1,2})|(X[A-E][A-Z])|(XF[A-D])|([A-Z]{1,2}))$";
	public static final String columnIndexValidator = "^(([A-W][A-Z]{1,2})|(X[A-E][A-Z])|(XF[A-D])|([A-Z]{1,2}))$";
	public static final String cellAddressValidator = "^(([A-W][A-Z]{1,2})|(X[A-E][A-Z])|(XF[A-D])|([A-Z]{1,2}))((10[0-4][0-8][0-5][0-7][0-6])|([1-9][0-9]{1,5})|([1-9]))$";
	public static final String rangeValidator = "^(([A-W][A-Z]{1,2})|(X[A-E][A-Z])|(XF[A-D])|([A-Z]{1,2}))((10[0-4][0-8][0-5][0-7][0-6])|([1-9][0-9]{1,5})|([1-9])):(([A-W][A-Z]{1,2})|(X[A-E][A-Z])|(XF[A-D])|([A-Z]{1,2}))((10[0-4][0-8][0-5][0-7][0-6])|([1-9][0-9]{1,5})|([1-9]))$";
	public static final String SHEET_NAME_VALIDATOR = "^[^\\\\\\/\\?\\*\\[\\]]{1,31}$";
	public static final String workbookPathValidator = "^([a-zA-Z0-9 ]+[/])*([a-zA-Z0-9 ])+[.](xls[xm]?)$";
	public static final String NONEMPTYROWS = "NONEMPTYROWS";
	public static final String TOTALROWS = "TOTALROWS";
	public static final String EMPTY = "";
	public static final String HIDDEN = "hidden";

	public static final String HASH = "#";
	public static final String CELL = "CELL";
	public static final String TEXT = "TEXT";
	public static final String FORMATTED_VALUE = "FORMATTED_VALUE";
	public static final String UNFORMATTED_VALUE = "UNFORMATTED_VALUE";
	public static final String FORMULA = "FORMULA";
	public static final String USER_ENTERED = "USER_ENTERED";
	public static final String EQUALS = "=";
	public static final String MULTIPLE = "MULTIPLE";
	public static final String NONE = "NONE";
	public static final String COLON = ":";
	public static final String EXCLAMATION = "!";

	public static final String AUTH_URI = "https://accounts.google.com/o/oauth2/auth";
	public static final String TOKEN_URI = "https://oauth2.googleapis.com/token";
	public static final String AUTH_USER = "UsersCredentialCheckup_";
	public static final String BYINDEX = "BYINDEX";
	public static final String BYNAME = "BYANME";

	public static final String USERNAME = "Username";

	public static final String BYANME = "BYANME";
	public static final String STARTCELLADDRESS = "A1";
	public static final int NEGATIVEONE = -1;
	public static final String BLACK = "Black";
	public static final String HEXCODEFORBLACK = "#000000";
	public static final int STARTCOLUMN = 1;
	public static final int MAXCOLUMN = 18278;
	public static final int STARTROW = 1;
	public static final int ENDROW = 40000;
	public static final String LEFT = "LEFT";
	public static final String UP = "UP";
	public static final String ROWOPERATION = "ROWOPERATION";
	public static final String COLUMNOPERATION = "COLUMNOPERATION";
	public static final String DELETEROWAT = "DELETEROWAT";
	public static final String DELETEROWBY = "DELETEROWBY";
	public static final String DELETECOLUMNAT = "DELETECOLUMNAT";
	public static final String DELETECOLUMNBY = "DELETECOLUMNBY";

}
