/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.USERNAME;
import static com.automationanywhere.commandsdk.model.AttributeType.RADIO;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.api.impl.GDriveImpl;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommand.gsuite.service.impl.AuthenticationServiceImpl;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import com.google.common.annotations.VisibleForTesting;

/**
 * Copies a Google Drive file from a Google Drive location to another Google
 * Drive location.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

@BotCommand
@CommandPkg(label = "Copy file", name = "CopyFile", description = "Copies a file from one location to another", node_label = "|with name {{fileName}}|with id {{fileId}}| to folder {{folderName}}| to folder id {{folderId}}")
public class CopyFile {
	private static final Logger LOGGER = LogManager.getLogger(CopyFile.class);
	private static final String NAME_DESC = "e.g. MyFolder/MyFile.doc";
	private static final String ID_DESC = "e.g. 17dIAwvrEA4JeLysfFky9oZL0qAzesY0IdbOu-AvHB1U from document link https://docs.google.com/spreadsheets/d/17dIAwvrEA4JeLysfFky9oZL0qAzesY0IdbOu-AvHB1U/edit#gid=0";
	private static final String LOC_DESC = "e.g. MyFolder2/MyFolder3";
	private static final String NEW_NAME = "e.g. MyNewFileName.doc";
	private AuthenticationService mAuthenticationService;

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public Value<String> execute(
			@Idx(index = "1", type = AttributeType.CREDENTIAL) @Pkg(label = USERNAME) @NotEmpty SecureString userEmailAddress,
			@Idx(index = "2", type = RADIO, options = {
					@Idx.Option(index = "2.1", pkg = @Pkg(label = "File name (with location)", value = "BYNAME")),
					@Idx.Option(index = "2.2", pkg = @Pkg(label = "File ID", value = "BYID")) }) @Pkg(label = "Source", default_value_type = DataType.STRING, default_value = "BYNAME") @NotEmpty String sourceFileOption,
			@Idx(index = "2.1.1", type = TEXT) @Pkg(label = "", description = NAME_DESC, default_value = "", default_value_type = DataType.STRING) @NotEmpty String fileName,
			@Idx(index = "2.2.1", type = TEXT) @Pkg(label = "", description = ID_DESC, default_value = "", default_value_type = DataType.STRING) @NotEmpty String fileId,
			@Idx(index = "3", type = RADIO, options = {
					@Idx.Option(index = "3.1", pkg = @Pkg(label = "Folder location", value = "BYNAME")),
					@Idx.Option(index = "3.2", pkg = @Pkg(label = "Folder ID", value = "BYID")) }) @Pkg(label = "Destination", default_value_type = DataType.STRING, default_value = "BYNAME") @NotEmpty String destFileOption,
			@Idx(index = "3.1.1", type = TEXT) @Pkg(label = "", description = LOC_DESC, default_value = "", default_value_type = DataType.STRING) @NotEmpty String folderName,
			@Idx(index = "3.2.1", type = TEXT) @Pkg(label = "", description = ID_DESC, default_value = "", default_value_type = DataType.STRING) @NotEmpty String folderId,

			@Idx(index = "4", type = AttributeType.CHECKBOX) @Pkg(label = "Rename file") Boolean renameFile,
			@Idx(index = "4.1", type = AttributeType.TEXT) @Pkg(label = "New file name", description = NEW_NAME) @NotEmpty String newFileName) {
		try {
			JSONObject inputFileParam = new JSONObject();
			if (sourceFileOption.equals("BYNAME")) {
				inputFileParam.put("fileName", fileName);
			} else if (sourceFileOption.equals("BYID")) {
				inputFileParam.put("fileId", fileId);
			}

			JSONObject outputFileParam = new JSONObject();
			if (renameFile) {
				outputFileParam.put("newFileName", newFileName);
			}
			if (destFileOption.equals("BYNAME")) {
				outputFileParam.put("fileName", folderName);
			} else if (destFileOption.equals("BYID")) {
				outputFileParam.put("fileId", folderId);
			}

			return new StringValue(
					getDrive(userEmailAddress).copyFile(inputFileParam.toString(), outputFileParam.toString()));
		} catch (GeneralSecurityException | IOException | JSONException e) {
			LOGGER.error("Exception in copying Google Drive file: ", e);
			String msg = e.getMessage();
			throw new BotCommandException(msg, e);
		}
	}

	GDrive getDrive(SecureString userEmailAddress) throws GeneralSecurityException, IOException {
		AuthenticationService authenticationService = getAuthenticationService();
		return new GDriveImpl(authenticationService, userEmailAddress.getInsecureString(), sessionMap);
	}

	@VisibleForTesting
	void setAuthenticationService(AuthenticationService mAuthenticationService) {
		this.mAuthenticationService = mAuthenticationService;
	}

	synchronized AuthenticationService getAuthenticationService() {
		if (mAuthenticationService == null) {
			mAuthenticationService = new AuthenticationServiceImpl();
		}
		return mAuthenticationService;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
}
