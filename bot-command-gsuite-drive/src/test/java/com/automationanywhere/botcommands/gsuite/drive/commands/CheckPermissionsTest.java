/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommands.gsuite.drive.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

/**
 * Tests get Google Drive file permission functionality.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

public class CheckPermissionsTest {

	public static final SecureString USERNAME = ConstantsTest.getUserName();

	@Spy
	CheckPermissions checkPermission;

	@Mock
	GDrive mockGDrive;

	@Mock
	AuthenticationService mockAuthService;

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		checkPermission.setAuthenticationService(mockAuthService);
	}

	@Test
	public void checkPermission_returnsBoolean_whenRequiredFileNameProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(checkPermission).getDrive(USERNAME);

			String fileName = "trialSheet";
			String folderName = "PK";
			String role = "write";
			String nameOption = "file";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);
			inputFileParamName.put("role", role);

			when(mockGDrive.checkPermissions(inputFileParamName.toString(), nameOption)).thenReturn(true);
			String testId = checkPermission.execute(USERNAME, nameOption, fileName, folderName, role).get();
			Assert.assertEquals("true", testId);
		} catch (JSONException | IOException | GeneralSecurityException e) {
			Assert.fail();
		}
	}

	@Test
	public void checkPermission_returnsFilePermissionList_whenRequiredFolderNameProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(checkPermission).getDrive(USERNAME);

			String fileName = "trialSheet";
			String folderName = "PK";
			String role = "write";
			String nameOption = "folder";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", folderName);
			inputFileParamName.put("role", role);

			when(mockGDrive.checkPermissions(inputFileParamName.toString(), nameOption)).thenReturn(true);
			String testId = checkPermission.execute(USERNAME, nameOption, fileName, folderName, role).get();
			Assert.assertEquals("true", testId);
		} catch (GeneralSecurityException | IOException | JSONException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void checkPermission_throwsBotCommandException_whenGetPermissionAPIthrowsIOException() {
		try {
			Mockito.doReturn(mockGDrive).when(checkPermission).getDrive(USERNAME);

			String fileName = "trialSheet";
			String folderName = "PK";
			String role = "write";
			String nameOption = "file";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);
			inputFileParamName.put("role", role);

			when(mockGDrive.checkPermissions(inputFileParamName.toString(), nameOption)).thenThrow(IOException.class);
			checkPermission.execute(USERNAME, nameOption, fileName, folderName, role).get();
			Assert.fail();
		} catch (GeneralSecurityException | JSONException e) {
			Assert.fail();
		} catch (IOException e) {
			Assert.assertTrue(true);
		}
	}

	@Test
	public void getDrive_newGDriveImplObjectIsNotNull() {
		try {
			GDrive drive = checkPermission.getDrive(USERNAME);
			assertNotNull(drive);
		} catch (GeneralSecurityException | IOException e) {
			fail();
		}
	}
}
