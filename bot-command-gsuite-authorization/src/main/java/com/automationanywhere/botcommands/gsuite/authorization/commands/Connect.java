package com.automationanywhere.botcommands.gsuite.authorization.commands;

import static com.automationanywhere.commandsdk.model.AttributeType.CREDENTIAL;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import com.google.api.client.auth.oauth2.Credential;

@BotCommand
@CommandPkg(name = "connect", label = "Connect", node_label = "", description = "Getting authorization for G-Suite Apps.", icon = "Auth_blue.svg")
public class Connect extends AbstractAuthUtils {

	private static final Logger LOGGER = LogManager.getLogger(Connect.class);

	private static final String CLIENTID_DESC = "Client Id of your OAuth Application";
	private static final String REDIRECT_URI_DESC = "Redirect URI of your OAuth Application";
	private static final String CLIENT_SECRET_DESC = "Client secret";
	private static final String USERNAME = "Username";

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public Value<?> execute(
			@Idx(index = "1", type = CREDENTIAL) @Pkg(label = USERNAME) @NotEmpty SecureString userEmailAddress,
			@Idx(index = "2", type = CREDENTIAL) @Pkg(label = "Client Id", description = CLIENTID_DESC) @NotEmpty SecureString clientId,
			@Idx(index = "3", type = CREDENTIAL) @Pkg(label = "Redirect URI", default_value_type = DataType.STRING, description = REDIRECT_URI_DESC) @NotEmpty SecureString redirectURI,
			@Idx(index = "4", type = CREDENTIAL) @Pkg(label = "Client Secret", default_value_type = DataType.STRING, description = CLIENT_SECRET_DESC) @NotEmpty SecureString clientSecret) {

		try {
			LOGGER.info("Launching consent screen...");
			Credential credentials = launchOAuthConsentScreen(userEmailAddress, clientId, clientSecret, redirectURI);
			setUserSession(sessionMap, userEmailAddress.getInsecureString());
			return new BooleanValue(credentials == null);
			
		} catch (GeneralSecurityException e) {
			LOGGER.error("Security exception : ", e);
			throw new BotCommandException(e.getMessage(), e);
		} catch (IOException e) {
			LOGGER.error("Exception during user authorization: ", e);
			throw new BotCommandException(e.getMessage(), e);
		}
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
}