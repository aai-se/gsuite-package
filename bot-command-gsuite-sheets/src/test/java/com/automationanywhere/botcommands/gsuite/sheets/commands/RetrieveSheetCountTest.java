/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;

public class RetrieveSheetCountTest {
	@Spy
	RetrieveSheetCount retrieveSheetCount;

	@Mock
	GSheets mockGSheets;

	final String session = "session";

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		sessionMap.put("session", mockGSheets);
		retrieveSheetCount.setSessionMap(sessionMap);
	}

	@Test
	public void retrieveSheetCountTest() {
		try {
			when(mockGSheets.retrieveSheetCount(anyBoolean())).thenReturn(10);
			Value<?> returned = retrieveSheetCount.execute(session, true);
			Assert.assertEquals(returned.toString(), new NumberValue(10).toString());
			} catch (IOException ioException) {
				Assert.fail();
			}
	}
	
	@Test(expectedExceptions = BotCommandException.class)
	public void botCommandExceptionTest() {
			try {
				when(mockGSheets.retrieveSheetCount(anyBoolean())).thenThrow(new IOException());
				retrieveSheetCount.execute(session, false);
			} catch (IOException e) {
				Assert.fail();
			}
	}
}