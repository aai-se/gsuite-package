/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommand.gsuite.dto.calendar;

import java.util.Objects;

public class PersonDTO {

    String id;
    String email;
    String displayName;
    boolean self;
    boolean organizer;
    String comment;
    int additionalGuests;
    String responseStatus;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonDTO personDTO = (PersonDTO) o;
        return self == personDTO.self &&
                organizer == personDTO.organizer &&
                additionalGuests == personDTO.additionalGuests &&
                Objects.equals(id, personDTO.id) &&
                Objects.equals(email, personDTO.email) &&
                Objects.equals(displayName, personDTO.displayName) &&
                Objects.equals(comment, personDTO.comment) &&
                Objects.equals(responseStatus, personDTO.responseStatus);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, displayName, self, organizer, comment, additionalGuests, responseStatus);
    }
}
