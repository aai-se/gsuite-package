/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.BYINDEX;
import static com.automationanywhere.botcommand.gsuite.Constants.BYNAME;
import static com.automationanywhere.botcommand.gsuite.Constants.DEFAULT_VALUE_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.DESCRIPTION_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.SHEET_NAME_VALIDATOR;
import static com.automationanywhere.botcommand.gsuite.Constants.USERNAME;
import static com.automationanywhere.commandsdk.model.AttributeType.CREDENTIAL;
import static com.automationanywhere.commandsdk.model.AttributeType.NUMBER;
import static com.automationanywhere.commandsdk.model.AttributeType.RADIO;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.GreaterThanEqualTo;
import com.automationanywhere.commandsdk.annotations.rules.MatchesRegex;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.NumberInteger;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;

@BotCommand
@CommandPkg(label = "Create sheet", 
name = "CreateSheet",
description = "Adds a new google sheet", 
node_label = "with the name {{sheetName}} |at index {{sheetIndex}} |in the current spreadsheet |in session {{session}}")
public class CreateSheet extends AbstractAuthUtils{

	private static final Logger LOGGER = LogManager.getLogger(CreateSheet.class);

	@Sessions
	private Map<String, Object> sessionMap;

	private static final String SHEET_BY_INDEX_EXAMPLE = "e.g. 1 or 3";

	private static final String SHEET_BY_NAME_EXAMPLE = "e.g. Sheet1";

	@Execute
	public BooleanValue execute(

			@Idx(index = "1", type = TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String session,
			@Idx(index = "2", type = RADIO, options = {
					@Idx.Option(index = "2.1", pkg = @Pkg(label = "Index", value = BYINDEX)),
					@Idx.Option(index = "2.2", pkg = @Pkg(label = "Name", value = BYNAME)) }) @Pkg(label = "Create sheet by", default_value_type = DataType.STRING, default_value = BYINDEX) @NotEmpty String sheetOption,
			@Idx(index = "2.1.1", type = NUMBER) @Pkg(label = "", description = SHEET_BY_INDEX_EXAMPLE) @NotEmpty @GreaterThanEqualTo("0") @NumberInteger Number sheetIndex,
			@Idx(index = "2.2.1", type = TEXT) @Pkg(label = "", description = SHEET_BY_NAME_EXAMPLE) @NotEmpty @MatchesRegex(SHEET_NAME_VALIDATOR) String sheetName) {

		try {
			validateSessionNotExists(sessionMap,session);
			GSheets gSheets = (GSheets) sessionMap.get(session);
			ExecutorValidator.getInstance().validate(gSheets);
			return new BooleanValue(gSheets.createWorkSheet(sheetOption, sheetIndex, sheetName));
		} catch (IOException e) {
			LOGGER.error("exception in adding new sheet: ", e);
			String msg = e.getMessage();
			throw new BotCommandException(msg, e);
		}
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
}
