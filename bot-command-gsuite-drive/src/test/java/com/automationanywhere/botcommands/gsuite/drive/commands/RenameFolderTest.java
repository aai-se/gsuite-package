/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommands.gsuite.drive.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

/**
 * Tests rename Google Drive folder functionality.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

public class RenameFolderTest {

	public static final SecureString USERNAME = ConstantsTest.getUserName();

	@Spy
	RenameFolder renameFolder;

	@Mock
	GDrive mockGDrive;

	@Mock
	AuthenticationService mockAuthService;

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		renameFolder.setAuthenticationService(mockAuthService);
	}

	@Test
	public void renameFolder_returnsRenamedFolderId_whenRequiredFolderNameProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(renameFolder).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial";
			String newFileName = "trial_Updated";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);
			inputFileParamName.put("fileType", "folder");

			when(mockGDrive.renameFile(inputFileParamName.toString(), newFileName)).thenReturn(fileId);
			Value<String> testId = renameFolder.execute(USERNAME, "BYNAME", fileName, fileId, newFileName);
			Assert.assertEquals(fileId, testId.toString());
		} catch (JSONException | IOException | GeneralSecurityException e) {
			Assert.fail();
		}
	}

	@Test
	public void renameFolder_returnsRenamedFolderId_whenRequiredFolderIdProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(renameFolder).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			String newFileName = "newtrial_file.txt";

			JSONObject inputFileParamID = new JSONObject();
			inputFileParamID.put("fileId", fileId);
			inputFileParamID.put("fileType", "folder");

			when(mockGDrive.renameFile(inputFileParamID.toString(), newFileName)).thenReturn(fileId);
			Value<String> testId = renameFolder.execute(USERNAME, "BYID", fileName, fileId, newFileName);
			Assert.assertEquals(fileId, testId.toString());
		} catch (JSONException | IOException | GeneralSecurityException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void renameFolder_throwsBotCommandException_whenRenameFolderAPIthrowsIOException() {
		try {
			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			String newFileName = "newtrial_file.txt";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);
			inputFileParamName.put("fileType", "folder");

			Mockito.doReturn(mockGDrive).when(renameFolder).getDrive(USERNAME);
			when(mockGDrive.renameFile(inputFileParamName.toString(), newFileName)).thenThrow(IOException.class);
			renameFolder.execute(USERNAME, "BYNAME", fileName, fileId, newFileName);
			Assert.fail();
		} catch (GeneralSecurityException e) {
			Assert.fail();
		} catch (IOException e) {
			Assert.assertTrue(true);
		} catch (JSONException e) {
			Assert.fail();
		}

	}

	@Test
	public void getDrive_newGDriveImplObjectIsNotNull() {
		try {
			GDrive drive = renameFolder.getDrive(USERNAME);
			assertNotNull(drive);
		} catch (IOException | GeneralSecurityException e) {
			fail();
		}
	}
}
