package com.automationanywhere.botcommands.gsuite.sheets.commands;

import java.util.Map;
import static com.automationanywhere.botcommand.gsuite.Constants.*;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Idx.Option;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.GreaterThanEqualTo;
import com.automationanywhere.commandsdk.annotations.rules.LessThanEqualTo;
import com.automationanywhere.commandsdk.annotations.rules.MatchesRegex;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.NumberInteger;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

@BotCommand
@CommandPkg(label = "Activate sheet", name = "activateSheet", description = "Goes to specific sheet in the excel sheet", node_label = "|by index = {{sheetIndex}} ||by name {{sheetName}} |in session {{sessionName}}", icon = "Excel_blue.svg")

public class ActivateSheet {
    @Sessions
    private Map<String, Object> sessionMap;

    @Execute
    public BooleanValue execute(
            @Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String sessionName,
            @Idx(index = "2", type = AttributeType.RADIO, options = {
                    @Option(index = "2.1", pkg = @Pkg(label = "Index", value = TRUE)),
                    @Option(index = "2.2", pkg = @Pkg(label = "Name", value = FALSE))}) @Pkg(label = "Activate sheet", default_value_type = DataType.STRING, default_value = TRUE) @NotEmpty String sheetOptionIsIndex,
            @Idx(index = "2.1.1", type = AttributeType.NUMBER) @Pkg(label = "", description = "e.g. 1 or 3", default_value = "1", default_value_type = DataType.NUMBER) @GreaterThanEqualTo("1") @LessThanEqualTo("999999999") @NumberInteger @NotEmpty Number sheetIndex,
            @Idx(index = "2.2.1", type = AttributeType.TEXT) @Pkg(label = "", description = "e.g. Final P&L") @NotEmpty @MatchesRegex(SHEET_NAME_VALIDATOR) String sheetName) {

        GSheets gSheets = (GSheets) sessionMap.get(sessionName);
        ExecutorValidator.getInstance().validate(gSheets);     
        return new BooleanValue(gSheets.activateSheet(Boolean.parseBoolean(sheetOptionIsIndex), sheetIndex, sheetName));
        
    }

    public void setSessionMap(Map<String, Object> sessionMap) {
        this.sessionMap = sessionMap;
    }
}
