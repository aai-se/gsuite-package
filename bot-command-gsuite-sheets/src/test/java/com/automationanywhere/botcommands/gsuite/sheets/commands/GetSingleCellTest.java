/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.Constants;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.dto.sheets.WorksheetDto;
import com.google.api.services.sheets.v4.model.ValueRange;

public class GetSingleCellTest {
	@Spy
	GetSingleCell getSingleCell;

	@Mock
	GSheets mockGSheets;

	final String session = "session";

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);

		sessionMap.put("session", mockGSheets);
		getSingleCell.setSessionMap(sessionMap);
	}

	@Test
	public void getSpecificSingleCell() throws IOException {
		WorksheetDto workSheet = mock(WorksheetDto.class);
		List<List<Object>> listOf_ListOfObject = mock(List.class);
		ValueRange value = mock(ValueRange.class);
		List<Object> listOfObjects = mock(List.class);
		when(mockGSheets.getSpreadSheetId()).thenReturn("123456");
		when(mockGSheets.getActiveSheetInfo()).thenReturn(workSheet);
		when(workSheet.getSheetName()).thenReturn("MyWorkSheet");
		when(mockGSheets.getValue(anyString(), anyString(), anyString(), anyString())).thenReturn(value);
		when(value.getValues()).thenReturn(listOf_ListOfObject);
		when(listOf_ListOfObject.get(anyInt())).thenReturn(listOfObjects);
		when((String) listOfObjects.get(anyInt())).thenReturn("GotCellValue");
		String gotValue = (String) getSingleCell.execute(session, Constants.SPECIFICCELL, "A1").get();
		Assert.assertEquals(gotValue, "GotCellValue");

	}

	@Test
	public void getActiveSingleCell() throws IOException {
		WorksheetDto workSheet = mock(WorksheetDto.class);
		List<List<Object>> listOf_ListOfObject = mock(List.class);
		ValueRange value = mock(ValueRange.class);
		List<Object> listOfObjects = mock(List.class);
		when(mockGSheets.getActiveSheetInfo()).thenReturn(workSheet);
		when(workSheet.getSheetName()).thenReturn("MyWorkSheet");
		when(mockGSheets.getActiveCell(anyString())).thenReturn("A1");
		when(mockGSheets.getSpreadSheetId()).thenReturn("123456");
		when(mockGSheets.getValue(anyString(), anyString(), anyString(), anyString())).thenReturn(value);
		when(value.getValues()).thenReturn(listOf_ListOfObject);
		when(listOf_ListOfObject.get(anyInt())).thenReturn(listOfObjects);
		when((String) listOfObjects.get(anyInt())).thenReturn("GotCellValue");
		String gotValue = (String) getSingleCell.execute(session, Constants.ACTIVECELL, "").get();
		Assert.assertEquals(gotValue, "GotCellValue");
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void getSingleCell_BlankCell() throws IOException {
		WorksheetDto workSheet = mock(WorksheetDto.class);
		when(mockGSheets.getActiveSheetInfo()).thenReturn(workSheet);
		when(workSheet.getSheetName()).thenReturn("MyWorkSheet");
		when(mockGSheets.getSpreadSheetId()).thenReturn("123456");
		when(mockGSheets.getValue(anyString(), anyString(), anyString(), anyString())).thenReturn(null);
		Assert.assertNull(getSingleCell.execute(session, Constants.SPECIFICCELL, "A1"));

	}

	@Test(expectedExceptions = BotCommandException.class)
	public void getSingleCell_InvalidCell() throws IOException {
		WorksheetDto workSheet = mock(WorksheetDto.class);
		when(mockGSheets.getActiveSheetInfo()).thenReturn(workSheet);
		when(workSheet.getSheetName()).thenReturn("MyWorkSheet");
		when(mockGSheets.getSpreadSheetId()).thenReturn("123456");
		when(mockGSheets.getValue("123456", "MyWorkSheet", "A100000", Constants.UNFORMATTED_VALUE))
				.thenThrow(new IOException());
		Assert.assertNull(getSingleCell.execute(session, Constants.SPECIFICCELL, "A100000"));

	}

	@Test(expectedExceptions = BotCommandException.class)
	public void getSingleCell_InvalidSession() {
		getSingleCell.execute("default", Constants.SPECIFICCELL, "A1");
	}

}