package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommands.gsuite.sheets.commands.AutofitColumns;
import com.automationanywhere.botcommands.gsuite.sheets.commands.CloseSheet;
import com.automationanywhere.botcommands.gsuite.sheets.commands.OpenSheets;
import com.automationanywhere.botcommands.gsuite.sheets.test.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

public class CloseSpreadSheetJunit extends AbstractAuthUtils {

	CloseSheet closeSheet;
	OpenSheets openSheets;
	final String session = "session";

	public static final SecureString USERNAME =  ConstantsTest.getUserName();
	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {
        launchOAuthConsentScreen(ConstantsTest.getUserName(),ConstantsTest.getClientid(), ConstantsTest.getClientSecret(), ConstantsTest.getRedirectUri());
        closeSheet = new CloseSheet();
        closeSheet.setSessionMap(sessionMap);
		openSheets = new OpenSheets();
		openSheets.setSessionMap(sessionMap);
		openSheets.execute(USERNAME,session, "BYNAME", "Demo", null, null, false, null,false);
	}
	
	
	@Test
	public void closeSheetTest() {
		closeSheet.execute(session);
	}
}
