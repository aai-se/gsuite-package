/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommand.gsuite.dto.drive;

public class FileDto {

    public FileDto() {
    }

    public FileDto(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public boolean isShared() {
        return isShared;
    }

    public void setShared(boolean shared) {
        isShared = shared;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public boolean isFolder() {
        return isFolder;
    }

    public void setFolder(boolean folder) {
        isFolder = folder;
    }

    /**
     * The ID of the file.
     */
    String id;

    /**
     * The name of the file. This is not necessarily unique within a folder.
     */
    String name;

    /**
     * The MIME type of the file.
     */
    String mimeType;

    /**
     * 	A short description of the file.
     */
    String description;

    /**
     * A monotonically increasing version number for the file. This reflects every change made to the
     * file on the server, even those not visible to the user.
     */
    long version;

    /**
     * Whether the file has been shared.
     */
    boolean isShared;

    /**
     * Whether the file has been trashed, either explicitly or from a trashed parent folder.
     * Only the owner may trash a file, and other users cannot see files in the owner's trash.
     */
    boolean isDeleted;

    /**
     * The full file extension extracted from the name field.
     */
    String fileExtension;

    /**
     * if the mime type is "application/vnd.google-apps.folder", then the file is actually a folder.
     */
    boolean isFolder;
}
