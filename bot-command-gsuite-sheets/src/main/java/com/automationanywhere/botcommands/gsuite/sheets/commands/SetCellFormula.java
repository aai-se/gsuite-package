/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.*;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Idx.Option;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.MatchesRegex;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

@BotCommand
@CommandPkg(label = "Set cell formula", name = "setCellFormula", description = "Sets formula of the cells in a google sheet.", node_label = "{{formula}} |to {{cellOption}} |{{cellAddress}} |{{cellRange}} |in session {{sessionName}}", icon = "Excel_blue.svg")
public class SetCellFormula {
	private static final Logger LOGGER = LogManager.getLogger(SetCellColor.class);

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public StringValue execute(
			@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String sessionName,
			@Idx(index = "2", type = AttributeType.RADIO, options = {
					@Option(index = "2.1", pkg = @Pkg(label = LABEL_ACTIVECELL, value = ACTIVECELL)),
					@Option(index = "2.2", pkg = @Pkg(label = LABEL_SPECIFICCELL, value = SPECIFICCELL)),
					@Option(index = "2.3", pkg = @Pkg(label = LABEL_MULTIPLECELL, value = RANGE)) }) @Pkg(label = "Cell options", default_value_type = DataType.STRING, default_value = "active") @NotEmpty String cellOption,
			@Idx(index = "2.2.1", type = AttributeType.TEXT) @Pkg(label = "Cell name", description = "e.g. A1") @MatchesRegex(cellAddressValidator) @VariableType(value = DataType.STRING) @NotEmpty String cellAddress,
			@Idx(index = "2.3.1", type = AttributeType.TEXT) @Pkg(label = "Cell range", description = "e.g. A1:D6") @MatchesRegex(rangeValidator) @VariableType(value = DataType.STRING) @NotEmpty String cellRange,
			@Idx(index = "3", type = AttributeType.TEXT) @Pkg(label = "Cell formula", description = "e.g. A1+A2 or sum(A1:B4)") @VariableType(value = DataType.STRING) @NotEmpty String formula) {
		try {
			GSheets gSheets = (GSheets) sessionMap.get(sessionName);
			ExecutorValidator.getInstance().validate(gSheets);
			LOGGER.info("Performing Set cell formula command.");
			if (formula.charAt(0) != '=')
				formula = EQUALS + formula;

			if (ACTIVECELL.equals(cellOption)) {

				return new StringValue(gSheets
						.setSingleCell(gSheets.getActiveCell(gSheets.getActiveSheetInfo().getSheetName()), formula));

			} else if (SPECIFICCELL.equals(cellOption))
				return new StringValue(gSheets.setSingleCell(cellAddress, formula));
			else
				return new StringValue(gSheets.setCellFormula(cellRange, formula));
		} catch (IOException exception) {
			LOGGER.error("Exception occurred during execution of Set cell color formula.....", exception);
			throw new BotCommandException(exception);
		}
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
}
