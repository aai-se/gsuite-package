/*
 *  Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */

package com.automationanywhere.botcommand.gsuite.api.impl;

import static com.automationanywhere.botcommand.gsuite.Constants.*;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.Constants;
import com.automationanywhere.botcommand.gsuite.dto.sheets.SpreadSheetDto;
import com.automationanywhere.botcommand.gsuite.dto.sheets.WorksheetDto;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommand.gsuite.utils.GoToCellOptions;
import com.automationanywhere.botcommand.gsuite.utils.Utils;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.Sheets.Spreadsheets;
import com.google.api.services.sheets.v4.Sheets.Spreadsheets.BatchUpdate;
import com.google.api.services.sheets.v4.Sheets.Spreadsheets.Values;
import com.google.api.services.sheets.v4.Sheets.Spreadsheets.Values.Update;
import com.google.api.services.sheets.v4.model.AddSheetRequest;
import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetRequest;
import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetResponse;
import com.google.api.services.sheets.v4.model.DeleteDimensionRequest;
import com.google.api.services.sheets.v4.model.DimensionRange;
import com.google.api.services.sheets.v4.model.Request;
import com.google.api.services.sheets.v4.model.Sheet;
import com.google.api.services.sheets.v4.model.SheetProperties;
import com.google.api.services.sheets.v4.model.Spreadsheet;
import com.google.api.services.sheets.v4.model.SpreadsheetProperties;
import com.google.api.services.sheets.v4.model.UpdateSheetPropertiesRequest;
import com.google.api.services.sheets.v4.model.UpdateValuesResponse;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.google.common.annotations.VisibleForTesting;

public class GSheetTest {
	@Mock
	AuthenticationService authService;

	@Mock
	Credential credential;

	@Mock
	Sheets sheetSevice;

	@Mock
	Spreadsheets spreadsheets;

	@Mock
	BatchUpdate batchUpdate;

	@Mock
	SpreadSheetDto spreadSheetDto;

	@Mock
	List<Sheet> sheetList;

	@Mock
	GSheetsImpl gSheetsMock;

	@Spy
	GSheetsImpl gSheets;

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		try {

			/*
			 * Mocking Sheet Services For Services call
			 */
			Sheets.Spreadsheets spredSheets = mock(Sheets.Spreadsheets.class);
			Sheets.Spreadsheets.Get getSoredSheetID = mock(Sheets.Spreadsheets.Get.class);

			/*
			 * Create Actual SpreadSheetDTO by setting Random values
			 */
			SpreadSheetDto spreadSheetDto = new SpreadSheetDto();
			spreadSheetDto.setSpreadSheetId("123456");
			spreadSheetDto.setTitle("title");
			spreadSheetDto.setUrl("abcd");

			/*
			 * Create Actual SpreadSheet Object by setting Random values
			 */
			Spreadsheet spreadsheet = new Spreadsheet();
			spreadsheet.setSpreadsheetId("123456");
			spreadsheet.setProperties(new SpreadsheetProperties());
			spreadsheet.getProperties().setTitle(new String("Sheet1"));
			spreadsheet.setSheets(new ArrayList<Sheet>());
			spreadsheet.setSpreadsheetUrl(new String("https://abcd"));

			/*
			 * Create Actual WorkSheetDTO Object by setting Random values
			 */
			WorksheetDto activeSheetInfo = new WorksheetDto();
			activeSheetInfo.setIndex(0);
			activeSheetInfo.setSheetId(123456);
			activeSheetInfo.setSheetName(new String());

			/*
			 * Setting private members for Services Calls.
			 * 
			 * Set Authenticate Services with mocked.
			 * 
			 * Authenticate Services for Authorization.
			 * 
			 * Set SheetService for Mocking.
			 * 
			 * SeActiveSheet Info
			 */
			gSheets.setAuthService(authService);
			gSheets.setSheetService(sheetSevice);
			gSheets.setSpredSheetDto(spreadSheetDto);
			gSheets.setActiveSheetInfo(activeSheetInfo);

			/*
			 * Setup Authorization Returning Mocked SheetServices By passing Mocked
			 * Credential. Setting up Authorization for Services Call.
			 */
			doReturn(sheetSevice).when(gSheets).getSheetServices(credential);
			doNothing().when(gSheets).setupAuthorization();

			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.get(anyString())).thenReturn(getSoredSheetID);
			when(getSoredSheetID.setFields(anyString())).thenReturn(getSoredSheetID);
			when(getSoredSheetID.execute()).thenReturn(spreadsheet);
		} catch (IOException e) {
			fail();
		}
	}

	@Test
	public void openSheetWith_NullInputs() {
		try {
			setup();
			gSheets.openSpreadSheet(null);
			Assert.fail("Should have come here but in a nullpointer block");
		} catch (NullPointerException e) {
			Assert.assertTrue(true, "Nullpointer was thrown");
		} catch (IOException e) {
			Assert.fail("IOException should not have been thrown.");
		}
	}

	@Test
	public void openSheetWith_ValidInputs() {
		try {
			when(gSheets.getSpreadSheetId()).thenReturn("123456");
			Mockito.doReturn("123456").when(gSheets).getSpreadSheetId();
			SpreadSheetDto spreadSheetDto1 = gSheets.openSpreadSheet("123456");
			Assert.assertEquals(gSheets.getSpreadSheetId(), spreadSheetDto1.getSpreadSheetId());
		} catch (NullPointerException e) {
			e.printStackTrace();
			fail("NullPointerException should not have been thrown.");
		} catch (Exception e) {
			e.printStackTrace();
			fail("IOException should not have been thrown.");
		}
	}

	@Test
	public void AutofitRow_ValidInputs() {
		try {
			BatchUpdateSpreadsheetResponse batchUpdateSpreadsheetResponse = new BatchUpdateSpreadsheetResponse();
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(batchUpdateSpreadsheetResponse);
			gSheets.autofitRows();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void AutofitRow_IOException() {
		try {
			setup();
			Sheets.Spreadsheets spreadSheets = mock(Sheets.Spreadsheets.class);
			Sheets.Spreadsheets.BatchUpdate batchUpdate = mock(Sheets.Spreadsheets.BatchUpdate.class);
			BatchUpdateSpreadsheetResponse batchUpdateSpreadsheetResponse = new BatchUpdateSpreadsheetResponse();
			when(sheetSevice.spreadsheets()).thenReturn(spreadSheets);
			when(spreadSheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenThrow(IOException.class);
			gSheets.autofitRows();
			fail();
		} catch (BotCommandException e) {
			assertTrue(true, "IOExcption Thrown");
		} catch (IOException e) {
		}
	}

	@Test
	public void AutofitColumn_ValidInputs() {
		try {
			setup();
			Sheets.Spreadsheets spreadSheets = mock(Sheets.Spreadsheets.class);
			Sheets.Spreadsheets.BatchUpdate batchUpdate = mock(Sheets.Spreadsheets.BatchUpdate.class);
			BatchUpdateSpreadsheetResponse batchUpdateSpreadsheetResponse = new BatchUpdateSpreadsheetResponse();
			when(sheetSevice.spreadsheets()).thenReturn(spreadSheets);
			when(spreadSheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(batchUpdateSpreadsheetResponse);
			gSheets.autofitColumns();

		} catch (IOException e) {

		}
	}

	@Test
	public void AutofitColumn_IOException() {
		try {
			Sheets.Spreadsheets spreadSheets = mock(Sheets.Spreadsheets.class);
			Sheets.Spreadsheets.BatchUpdate batchUpdate = mock(Sheets.Spreadsheets.BatchUpdate.class);
			BatchUpdateSpreadsheetResponse batchUpdateSpreadsheetResponse = new BatchUpdateSpreadsheetResponse();
			when(sheetSevice.spreadsheets()).thenReturn(spreadSheets);
			when(spreadSheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenThrow(IOException.class);
			gSheets.autofitColumns();
			fail();
		} catch (BotCommandException e) {
			assertTrue(true, "IOExcption Thrown");
		} catch (IOException e) {
		}
	}

	@Test
	public void showWorksheetValidInputs() {
		try {
			BatchUpdateSpreadsheetResponse batchUpdateSpreadsheetResponse = new BatchUpdateSpreadsheetResponse();
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(batchUpdateSpreadsheetResponse);
			doReturn(123).when(gSheets).getWorksheetIdByName(anyString(), anyBoolean());
			gSheets.showWorksheet("spreadSheetId", "sheet");
		} catch (IOException e) {
			Assert.fail();
		}
	}

	@Test
	public void setSingleCellTest() {
		try {
			Values value = mock(Values.class);
			Update updateValue = mock(Update.class);
			UpdateValuesResponse updateValuesResponse = new UpdateValuesResponse();
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.values()).thenReturn(value);
			when(value.update(anyString(), anyString(), anyObject())).thenReturn(updateValue);
			when(updateValue.setValueInputOption(anyString())).thenReturn(updateValue);
			when(updateValue.execute()).thenReturn(updateValuesResponse);
			gSheets.setSingleCell("A1", "Automation");
		} catch (IOException e) {
			Assert.fail();
		}
	}

	@Test
	public void ActivateSheet_ValidInputsByIndex() {
		List<String> listOfWorksheets = mock(List.class);
		Mockito.doReturn(listOfWorksheets).when(gSheets).getWorksheetNames(anyBoolean());
		when(listOfWorksheets.size()).thenReturn(10);
		when(listOfWorksheets.get(anyInt())).thenReturn(new String());
		Mockito.doNothing().when(gSheets).setActiveSheetInfo(anyString(), anyInt(), anyString());
		Boolean returnedValue = gSheets.activateSheet(true, 1, "Megha");
		assertTrue(returnedValue);
	}

	@Test
	public void ActivateSheet_ValidInputByName() {
		List<String> listOfWorksheets = mock(List.class);
		Mockito.doReturn(listOfWorksheets).when(gSheets).getWorksheetNames(anyBoolean());
		Mockito.doNothing().when(gSheets).setActiveSheetInfo(anyString(), anyInt(), anyString());
		Mockito.doReturn(1).when(gSheets).indexOf(anyObject(), anyString());
		Boolean returnedValue = gSheets.activateSheet(false, -1, "Megha");
		assertTrue(returnedValue);
	}

	@Test
	public void setActiveSheetInfo_ValidInputs() {
		try {
			Sheets deepStubSheets = mock(Sheets.class, Mockito.RETURNS_DEEP_STUBS);
			Sheets.Spreadsheets spredSheets = mock(Sheets.Spreadsheets.class);
			Sheets.Spreadsheets.Get spreadSheet = mock(Sheets.Spreadsheets.Get.class);
			Sheet sheet = mock(Sheet.class);
			Spreadsheet spreadSheetObject = mock(Spreadsheet.class);
			SheetProperties sheetProperties = mock(SheetProperties.class);
			when(sheetSevice.spreadsheets()).thenReturn(spredSheets);
			when(spredSheets.get(anyString())).thenReturn(spreadSheet);
			when(spreadSheet.setIncludeGridData(anyBoolean())).thenReturn(spreadSheet);
			when(spreadSheet.execute()).thenReturn(spreadSheetObject);
			when(spreadSheetObject.getSheets()).thenReturn(sheetList);
			when(sheetList.size()).thenReturn(10);
			when(sheetList.get(anyInt())).thenReturn(sheet);
			when(sheet.getProperties()).thenReturn(sheetProperties);
			when(sheetProperties.getTitle()).thenReturn("Megha");
			when(sheetProperties.getSheetId()).thenReturn(123456);
			WorksheetDto activesheetInfo = mock(WorksheetDto.class);
			when(activesheetInfo.getSheetName()).thenReturn(new String());
			when(activesheetInfo.getSheetName()).thenReturn(new String());
			gSheets.setActiveSheetInfo("BYINDEX", 1, null);
			gSheets.setActiveSheetInfo("BYNAME", -1, "Megha");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void setActiveSheetInfo_InvalidSheetIndex() {
		try {
			Sheets deepStubSheets = mock(Sheets.class, Mockito.RETURNS_DEEP_STUBS);
			Sheets.Spreadsheets spredSheets = mock(Sheets.Spreadsheets.class);
			Sheets.Spreadsheets.Get spreadSheet = mock(Sheets.Spreadsheets.Get.class);
			Sheet sheet = mock(Sheet.class);
			Spreadsheet spreadSheetObject = mock(Spreadsheet.class);
			SheetProperties sheetProperties = mock(SheetProperties.class);
			when(sheetSevice.spreadsheets()).thenReturn(spredSheets);
			when(spredSheets.get(anyString())).thenReturn(spreadSheet);
			when(spreadSheet.setIncludeGridData(anyBoolean())).thenReturn(spreadSheet);
			when(spreadSheet.execute()).thenReturn(spreadSheetObject);
			when(spreadSheetObject.getSheets()).thenReturn(sheetList);
			when(sheetList.size()).thenReturn(-1);
			when(sheetList.get(anyInt())).thenReturn(sheet);
			when(sheet.getProperties()).thenReturn(sheetProperties);
			when(sheetProperties.getTitle()).thenReturn("Megha");
			when(sheetProperties.getSheetId()).thenReturn(123456);
			WorksheetDto activesheetInfo = mock(WorksheetDto.class);
			when(activesheetInfo.getSheetName()).thenReturn(new String());
			when(activesheetInfo.getSheetName()).thenReturn(new String());
			gSheets.setActiveSheetInfo("BYINDEX", 1, null);
			fail();
		} catch (BotCommandException ex) {
			assertTrue(true, "Invalid index provided");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void setActiveSheetInfo_InvalidSheetName() {
		try {
			Sheets deepStubSheets = mock(Sheets.class, Mockito.RETURNS_DEEP_STUBS);
			Sheets.Spreadsheets spredSheets = mock(Sheets.Spreadsheets.class);
			Sheets.Spreadsheets.Get spreadSheet = mock(Sheets.Spreadsheets.Get.class);
			Sheet sheet = mock(Sheet.class);
			Spreadsheet spreadSheetObject = mock(Spreadsheet.class);
			SheetProperties sheetProperties = mock(SheetProperties.class);
			when(sheetSevice.spreadsheets()).thenReturn(spredSheets);
			when(spredSheets.get(anyString())).thenReturn(spreadSheet);
			when(spreadSheet.setIncludeGridData(anyBoolean())).thenReturn(spreadSheet);
			when(spreadSheet.execute()).thenReturn(spreadSheetObject);
			when(spreadSheetObject.getSheets()).thenReturn(sheetList);
			when(sheetList.size()).thenReturn(5);
			when(sheetList.get(anyInt())).thenReturn(sheet);
			when(sheet.getProperties()).thenReturn(sheetProperties);
			when(sheetProperties.getTitle()).thenReturn(new String());
			when(sheetProperties.getSheetId()).thenReturn(123456);
			WorksheetDto activesheetInfo = mock(WorksheetDto.class);
			when(activesheetInfo.getSheetName()).thenReturn(new String());
			when(activesheetInfo.getSheetName()).thenReturn(new String());
			gSheets.setActiveSheetInfo("BYNAME", -1, "Megha");
			fail();
		} catch (BotCommandException ex) {
			assertTrue(true, "Invalid sheet name provided");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void setActiveSheetInfo_ForIOException() {
		try {
			Sheets.Spreadsheets spredSheets = mock(Sheets.Spreadsheets.class);
			Sheets.Spreadsheets.Get spreadSheet = mock(Sheets.Spreadsheets.Get.class);
			Sheet sheet = mock(Sheet.class);
			Spreadsheet spreadSheetObject = mock(Spreadsheet.class);
			SheetProperties sheetProperties = mock(SheetProperties.class);
			when(sheetSevice.spreadsheets()).thenReturn(spredSheets);
			when(spredSheets.get(anyString())).thenReturn(spreadSheet);
			when(spreadSheet.setIncludeGridData(anyBoolean())).thenReturn(spreadSheet);
			when(spreadSheet.execute()).thenThrow(IOException.class);
			when(spreadSheetObject.getSheets()).thenReturn(sheetList);
			when(sheetList.size()).thenReturn(5);
			when(sheetList.get(anyInt())).thenReturn(sheet);
			when(sheet.getProperties()).thenReturn(sheetProperties);
			when(sheetProperties.getTitle()).thenReturn(new String());
			when(sheetProperties.getSheetId()).thenReturn(123456);
			WorksheetDto activesheetInfo = mock(WorksheetDto.class);
			when(activesheetInfo.getSheetName()).thenReturn(new String());
			when(activesheetInfo.getSheetName()).thenReturn(new String());
			gSheets.setActiveSheetInfo("BYNAME", -1, "Megha");
			fail();
		} catch (BotCommandException ex) {
			assertTrue(true, "IOException thrown");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void getMultipleCellTestForRange() {
		try {
			ValueRange cellValueRange = mock(ValueRange.class);
			List<List<Object>> cellValue = mock(List.class);
			List<Object> list = mock(List.class);
			Iterator<List<Object>> iterator1 = mock(Iterator.class);
			Iterator<Object> iterator2 = mock(Iterator.class);
			Object object = mock(Object.class);
			Mockito.doReturn(new String()).when(gSheets).getSpreadSheetId();
			WorksheetDto activeSheetInfo = mock(WorksheetDto.class);
			Mockito.doReturn(activeSheetInfo).when(gSheets).getActiveSheetInfo();
			when(activeSheetInfo.getSheetName()).thenReturn(new String());
			Mockito.doReturn(cellValueRange).when(gSheets).getValue(anyString(), anyString(), anyString(), anyString());
			when(cellValueRange.getValues()).thenReturn(cellValue);
			when(cellValue.iterator()).thenReturn(iterator1);
			when(list.iterator()).thenReturn(iterator2);
			// this is to mock list with one element, adjust accordingly
			when(iterator1.hasNext()).thenReturn(true, false);
			when(iterator2.hasNext()).thenReturn(true, false);
			when(iterator1.next()).thenReturn(list);
			when(iterator2.next()).thenReturn(object);
			gSheets.getMultipleCells(true, "A1:B9");
		} catch (BotCommandException ex) {
//			assertTrue(true, "IOException thrown");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void getMultipleCellTestForAll() {
		try {
			ValueRange cellValueRange = mock(ValueRange.class);
			List<List<Object>> cellValue = mock(List.class);
			List<Object> list = mock(List.class);
			Iterator<List<Object>> iterator1 = mock(Iterator.class);
			Iterator<Object> iterator2 = mock(Iterator.class);
			Object object = mock(Object.class);
			Mockito.doReturn(new String()).when(gSheets).getSpreadSheetId();
			WorksheetDto activeSheetInfo = mock(WorksheetDto.class);
			Mockito.doReturn(activeSheetInfo).when(gSheets).getActiveSheetInfo();
			when(activeSheetInfo.getSheetName()).thenReturn(new String());
			Mockito.doReturn(cellValueRange).when(gSheets).getValue(anyString(), anyString(), anyObject(), anyString());
			when(cellValueRange.getValues()).thenReturn(cellValue);
			when(cellValue.iterator()).thenReturn(iterator1);
			when(list.iterator()).thenReturn(iterator2);
			// this is to mock list with one element, adjust accordingly
			when(iterator1.hasNext()).thenReturn(true, false);
			when(iterator2.hasNext()).thenReturn(true, false);
			when(iterator1.next()).thenReturn(list);
			when(iterator2.next()).thenReturn(object);
			gSheets.getMultipleCells(false, "");
		} catch (BotCommandException ex) {
//			assertTrue(true, "IOException thrown");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void getMultipleCellTestInvalidRange() {
		try {
			ValueRange cellValueRange = mock(ValueRange.class);
			List<List<Object>> cellValue = mock(List.class);
			List<Object> list = mock(List.class);
			Iterator<List<Object>> iterator1 = mock(Iterator.class);
			Iterator<Object> iterator2 = mock(Iterator.class);
			Object object = mock(Object.class);
			Mockito.doReturn(new String()).when(gSheets).getSpreadSheetId();
			WorksheetDto activeSheetInfo = mock(WorksheetDto.class);
			Mockito.doReturn(activeSheetInfo).when(gSheets).getActiveSheetInfo();
			when(activeSheetInfo.getSheetName()).thenReturn(new String());
			Mockito.doThrow(IOException.class).when(gSheets).getValue(anyString(), anyString(), anyObject(),
					anyString());
			when(cellValueRange.getValues()).thenReturn(cellValue);
			when(cellValue.iterator()).thenReturn(iterator1);
			when(list.iterator()).thenReturn(iterator2);
			// this is to mock list with one element, adjust accordingly
			when(iterator1.hasNext()).thenReturn(true, false);
			when(iterator2.hasNext()).thenReturn(true, false);
			when(iterator1.next()).thenReturn(list);
			when(iterator2.next()).thenReturn(object);
			gSheets.getMultipleCells(true, "9A9:L34");
			fail();
		} catch (BotCommandException ex) {
			assertTrue(true, "IOException thrown");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void getMultipleCellTestNullResponse() {
		try {
			ValueRange cellValueRange = mock(ValueRange.class);
			List<List<Object>> cellValue = mock(List.class);
			List<Object> list = mock(List.class);
			Iterator<List<Object>> iterator1 = mock(Iterator.class);
			Iterator<Object> iterator2 = mock(Iterator.class);
			Object object = mock(Object.class);
			Mockito.doReturn(new String()).when(gSheets).getSpreadSheetId();
			WorksheetDto activeSheetInfo = mock(WorksheetDto.class);
			Mockito.doReturn(activeSheetInfo).when(gSheets).getActiveSheetInfo();
			when(activeSheetInfo.getSheetName()).thenReturn(new String());
			Mockito.doReturn(cellValueRange).when(gSheets).getValue(anyString(), anyString(), anyObject(), anyString());
			when(cellValueRange.getValues()).thenReturn(null);
			when(cellValue.iterator()).thenReturn(iterator1);
			when(list.iterator()).thenReturn(iterator2);
			// this is to mock list with one element, adjust accordingly
			when(iterator1.hasNext()).thenReturn(true, false);
			when(iterator2.hasNext()).thenReturn(true, false);
			when(iterator1.next()).thenReturn(list);
			when(iterator2.next()).thenReturn(object);
			gSheets.getMultipleCells(true, "9A9:L34");
		} catch (BotCommandException ex) {
//			assertTrue(true, "IOException thrown");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void getValueTest_InvalidInputs() {
		try {
			StringBuilder stringBuilder = mock(StringBuilder.class);
			Sheets.Spreadsheets spredSheets = mock(Sheets.Spreadsheets.class);
			Sheets.Spreadsheets.Values values = mock(Sheets.Spreadsheets.Values.class);
			Sheets.Spreadsheets.Values.Get spreadSheetGet = mock(Sheets.Spreadsheets.Values.Get.class);
			ValueRange valueRange = mock(ValueRange.class);
			List<List<Object>> list = mock(List.class);
			when(stringBuilder.append(anyString())).thenReturn(stringBuilder);
			when(sheetSevice.spreadsheets()).thenReturn(spredSheets);
			when(spredSheets.values()).thenReturn(values);
			when(values.get(anyString(), anyString())).thenReturn(spreadSheetGet);
			when(spreadSheetGet.setValueRenderOption(anyString())).thenReturn(spreadSheetGet);
			when(spreadSheetGet.execute()).thenReturn(valueRange);
			when(valueRange.getValues()).thenReturn(list);
			when(list.size()).thenReturn(10);
			gSheets.getValue("123456", "Megha", "A2:H10", UNFORMATTED_VALUE);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void GoToCellTest() {
		try {
			GoToCellOptions goToCellOptions = mock(GoToCellOptions.class);
			Mockito.doNothing().when(goToCellOptions).goToCell(anyObject(), anyString());
			gSheets.goToCell("specificCell", "A1");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void getAllWorksheetNameTest() throws IOException {
		Spreadsheet spreadsheet = mock(Spreadsheet.class);
		Sheet sheet = mock(Sheet.class);
		Iterator<Sheet> iterator = mock(Iterator.class);
		List<Sheet> listOfSheet = mock(List.class);
		SheetProperties properties = mock(SheetProperties.class);
		doNothing().when(gSheets).setupAuthorization();
		when(spreadsheet.getSpreadsheetId()).thenReturn("123456");
		when(sheetSevice.spreadsheets().get(anyString()).execute()).thenReturn(spreadsheet);
		when(spreadsheet.size()).thenReturn(4);
		when(spreadsheet.getSheets()).thenReturn(listOfSheet);
		when(listOfSheet.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true, false);
		when(iterator.next()).thenReturn(sheet);
		when(sheet.getProperties()).thenReturn(properties);
		when(properties.getTitle()).thenReturn("MyworkSheetName");
		List<?> list = (List<?>) gSheets.getAllWorkSheet().get();
		Assert.assertEquals(list.get(0).toString(), "MyworkSheetName");

	}

	@Test
	public void hideWorkSheet_AlreadyHidden() throws IOException {
		doNothing().when(gSheets).setupAuthorization();
		Mockito.doReturn(new String()).when(gSheets).getSpreadSheetId();
		Mockito.doReturn(true).when(gSheets).isHidden(anyString());
		Boolean value = gSheets.hideWorksheet(anyString());
		Assert.assertTrue(value);
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void hideWorkSheet_OnlyWorksheet() throws IOException {
		List<String> listOfWorksheet = mock(ArrayList.class);
		doNothing().when(gSheets).setupAuthorization();
		Mockito.doReturn(new String()).when(gSheets).getSpreadSheetId();
		Mockito.doReturn(false).when(gSheets).isHidden(anyString());
		Mockito.doReturn(listOfWorksheet).when(gSheets).getWorksheetNames(true);
		when(listOfWorksheet.size()).thenReturn(1);
		gSheets.hideWorksheet(anyString());
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void hideWorkSheet_InvalidSheet() throws IOException {
		List<String> listOfWorksheet = mock(ArrayList.class);
		doNothing().when(gSheets).setupAuthorization();
		Mockito.doReturn(new String()).when(gSheets).getSpreadSheetId();
		Mockito.doReturn(false).when(gSheets).isHidden(anyString());
		Mockito.doReturn(listOfWorksheet).when(gSheets).getWorksheetNames(true);
		when(listOfWorksheet.size()).thenReturn(2);
		Mockito.doReturn(-1).when(gSheets).indexOf(anyList(), anyString());
		gSheets.hideWorksheet(anyString());
	}

	@Test
	public void hideWorkSheet_ActiveSheetZeroIndex() throws IOException {
		Sheets.Spreadsheets spreadSheets = mock(Sheets.Spreadsheets.class);
		WorksheetDto workSheet = new WorksheetDto();
		workSheet.setSheetName("myWorkSheet");
		workSheet.setIndex(0);
		workSheet.setSheetId(123456);
		String MyWorkSheetName = "MyWorkSheet";
		gSheets.setActiveSheetInfo(workSheet);
		Sheets.Spreadsheets.BatchUpdate batchUpdate = mock(Sheets.Spreadsheets.BatchUpdate.class);
		BatchUpdateSpreadsheetResponse response = new BatchUpdateSpreadsheetResponse();
		List<String> listOfWorksheet = mock(ArrayList.class);
		SheetProperties properties = mock(SheetProperties.class);
		doNothing().when(gSheets).setupAuthorization();
		Mockito.doReturn(new String()).when(gSheets).getSpreadSheetId();
		Mockito.doReturn(false).when(gSheets).isHidden(anyString());
		Mockito.doReturn(listOfWorksheet).when(gSheets).getWorksheetNames(true);
		when(listOfWorksheet.size()).thenReturn(2);
		Mockito.doReturn(0).when(gSheets).indexOf(anyList(), anyString());
		Mockito.doReturn(1).when(gSheets).getWorksheetIdByName(anyString(), anyBoolean());
		when(properties.setTitle(MyWorkSheetName)).thenReturn(properties);
		when(properties.setSheetId(1)).thenReturn(properties);
		when(properties.setHidden(true)).thenReturn(properties);
		when(sheetSevice.spreadsheets()).thenReturn(spreadSheets);
		when(spreadSheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
		when(batchUpdate.execute()).thenReturn(response);
		Mockito.doReturn(workSheet).when(gSheets).getActiveSheetInfo();
		when(listOfWorksheet.get(anyInt())).thenReturn(new String());
		Mockito.doNothing().when(gSheets).setActiveSheetInfo(anyString(), anyInt(), anyString());
		Boolean value = gSheets.hideWorksheet("myWorkSheet");
		Assert.assertTrue(value);
	}

	@Test
	public void hideWorkSheet_ActiveSheet() throws IOException {
		Sheets.Spreadsheets spreadSheets = mock(Sheets.Spreadsheets.class);
		WorksheetDto workSheet = new WorksheetDto();
		workSheet.setSheetName("myWorkSheet");
		workSheet.setIndex(0);
		workSheet.setSheetId(123456);
		String MyWorkSheetName = "MyWorkSheet";
		gSheets.setActiveSheetInfo(workSheet);
		Sheets.Spreadsheets.BatchUpdate batchUpdate = mock(Sheets.Spreadsheets.BatchUpdate.class);
		BatchUpdateSpreadsheetResponse response = new BatchUpdateSpreadsheetResponse();
		List<String> listOfWorksheet = mock(ArrayList.class);
		SheetProperties properties = mock(SheetProperties.class);
		doNothing().when(gSheets).setupAuthorization();
		Mockito.doReturn(new String()).when(gSheets).getSpreadSheetId();
		Mockito.doReturn(false).when(gSheets).isHidden(anyString());
		Mockito.doReturn(listOfWorksheet).when(gSheets).getWorksheetNames(true);
		when(listOfWorksheet.size()).thenReturn(2);
		Mockito.doReturn(1).when(gSheets).indexOf(anyList(), anyString());
		Mockito.doReturn(1).when(gSheets).getWorksheetIdByName(anyString(), anyBoolean());
		when(properties.setTitle(MyWorkSheetName)).thenReturn(properties);
		when(properties.setSheetId(1)).thenReturn(properties);
		when(properties.setHidden(true)).thenReturn(properties);
		when(sheetSevice.spreadsheets()).thenReturn(spreadSheets);
		when(spreadSheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
		when(batchUpdate.execute()).thenReturn(response);
		Mockito.doReturn(workSheet).when(gSheets).getActiveSheetInfo();
		when(listOfWorksheet.get(anyInt())).thenReturn(new String());
		Mockito.doNothing().when(gSheets).setActiveSheetInfo(anyString(), anyInt(), anyString());
		Boolean value = gSheets.hideWorksheet("myWorkSheet");
		Assert.assertTrue(value);
	}

	@Test
	public void closeSpreadSheetTest() {
		Boolean response = gSheets.closeSpreadSheet();
		Assert.assertTrue(response);
	}

	@Test
	public void InsertCellTest() throws IOException {
		Sheets.Spreadsheets spreadSheets = mock(Sheets.Spreadsheets.class);
		Sheets.Spreadsheets.BatchUpdate batchUpdate = mock(Sheets.Spreadsheets.BatchUpdate.class);
		BatchUpdateSpreadsheetResponse response = new BatchUpdateSpreadsheetResponse();
		doNothing().when(gSheets).setupAuthorization();
		when(gSheets.getSpreadSheetId()).thenReturn("123456");
		when(sheetSevice.spreadsheets()).thenReturn(spreadSheets);
		when(spreadSheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
		when(batchUpdate.execute()).thenReturn(response);
		Boolean value = gSheets.insertCell("A1", Constants.ROWS);
		Assert.assertTrue(value);

	}

	@Test(expectedExceptions = IOException.class)
	public void InsertCell_Invalid_InputTest() throws IOException {
		Sheets.Spreadsheets spreadSheets = mock(Sheets.Spreadsheets.class);
		Sheets.Spreadsheets.BatchUpdate batchUpdate = mock(Sheets.Spreadsheets.BatchUpdate.class);
		doNothing().when(gSheets).setupAuthorization();
		when(gSheets.getSpreadSheetId()).thenReturn("123456");
		when(sheetSevice.spreadsheets()).thenReturn(spreadSheets);
		when(spreadSheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
		when(batchUpdate.execute()).thenThrow(new IOException());
		gSheets.insertCell("A1", "Left");

	}

	@Test
	public void getActiveSheetName() {
		gSheets.getActiveSheetName();
	}

	/////////////////////////////////////////////////////////////////////
	@Test
	public void setActiveSheetName() {
		gSheets.setActiveSheetName(anyString());
	}

	@Test
	public void setUserName() {
		gSheets.setUserName(anyString());
	}

	@Test
	public void setSessionMap() {
		gSheets.setSessionMap(anyObject());
	}

	@Test
	public void setActiveSheetId() {
		gSheets.setActiveSheetId(anyInt());
	}

	@Test
	public void getActiveSheetId() {
		gSheets.getActiveSheetId();
	}

	@Test
	public void setActiveSheetNameAndId() {
		gSheets.setActiveSheetName(anyString(), anyInt());
	}

	@Test
	public void getActiveCell() {
		gSheets.getActiveCell(anyString());
	}

	@Test
	public void setActiveCell() {
		gSheets.setActiveCell(anyString(), anyString());
	}

	@Test
	public void DeleteRowColumn_ForSpecificRowIndex() {
		try {
			Mockito.doReturn("B5").when(gSheets).getActiveCell(anyString());
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			gSheets.deleteRowColumn(ROWS, SPECIFICCELL, "1");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void DeleteRowColumn_ForActiveCell() {
		try {
			Mockito.doReturn("B5").when(gSheets).getActiveCell(anyString());
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			gSheets.deleteRowColumn(ROWS, ACTIVECELL, "A1");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void DeleteRowColumn_ForSpecificRowRange() {
		try {
			Mockito.doReturn("B5").when(gSheets).getActiveCell(anyString());
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			gSheets.deleteRowColumn(ROWS, RANGE, "5:10");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void DeleteRowColumn_ForSpecificColumnIndex() {
		try {
			Mockito.doReturn("B5").when(gSheets).getActiveCell(anyString());
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			gSheets.deleteRowColumn(COLUMNS, SPECIFICCELL, "2");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void DeleteRowColumn_ForActiveCellColumn() {
		try {
			Mockito.doReturn("B5").when(gSheets).getActiveCell(anyString());
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			gSheets.deleteRowColumn(COLUMNS, ACTIVECELL, "A1");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void DeleteRowColumn_ForSpecificColumnRange() {
		try {
			Mockito.doReturn("B5").when(gSheets).getActiveCell(anyString());
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			gSheets.deleteRowColumn(COLUMNS, RANGE, "A:D");
		} catch (IOException e) {
			System.out.println("IOException occurred");
		}
	}

	@Test
	public void CreateWorksheet_ByIndex() {
		try {
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			Mockito.doReturn(10).when(gSheets).retrieveSheetCount(anyBoolean());
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			gSheets.createWorkSheet(BYINDEX, 1, "Megha");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void CreateWorksheet_ByName() {
		try {
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			Mockito.doReturn(10).when(gSheets).retrieveSheetCount(anyBoolean());
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			gSheets.createWorkSheet(BYANME, 1, "Megha");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void CreateWorksheet_BotCommandException() {
		try {
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			Mockito.doReturn(1).when(gSheets).retrieveSheetCount(anyBoolean());
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			gSheets.createWorkSheet(BYANME, 4, "Megha");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void renameWorksheet_ByIndex() {
		try {
			List<String> listOfWorksheets = mock(List.class);
			Mockito.doReturn(listOfWorksheets).when(gSheets).getWorksheetNames(anyBoolean());
			Mockito.doReturn(6).when(gSheets).getWorksheetIdByName(anyString(), anyBoolean());
			when(listOfWorksheets.size()).thenReturn(10);
			gSheets.setActiveCell("Megha", "A1");
			when(listOfWorksheets.get(anyInt())).thenReturn("Megha");
			Mockito.doReturn(-1).when(gSheets).indexOf(anyObject(), anyString());
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			Mockito.doNothing().when(gSheets).setActiveSheetInfo(anyString(), anyInt(), anyString());
			WorksheetDto activeSheetInfo = new WorksheetDto();
			activeSheetInfo.setIndex(0);
			activeSheetInfo.setSheetId(123456);
			activeSheetInfo.setSheetName("Megha");
			gSheets.setActiveSheetInfo(activeSheetInfo);
			gSheets.renameSheet(true, 1, "Megha", "Apple");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void renameWorksheet_ByName() {
		try {
			List<String> listOfWorksheets = mock(List.class);
			listOfWorksheets.add("Apple");
			Mockito.doReturn(listOfWorksheets).when(gSheets).getWorksheetNames(anyBoolean());
			when(listOfWorksheets.get(anyInt())).thenReturn("Megha");
			Mockito.doReturn(7).when(gSheets).indexOf(listOfWorksheets, "Megha");
			Mockito.doReturn(-1).when(gSheets).indexOf(listOfWorksheets, "Apple");
			Mockito.doReturn(1).when(gSheets).getWorksheetIdByName(anyString(), anyBoolean());
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			Mockito.doNothing().when(gSheets).setActiveSheetInfo(anyString(), anyInt(), anyString());
			gSheets.renameSheet(false, 1, "Megha", "Apple");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void renameWorksheet_InvalidIndex() {
		try {
			List<String> listOfWorksheets = mock(List.class);
			Mockito.doReturn(listOfWorksheets).when(gSheets).getWorksheetNames(anyBoolean());
			when(listOfWorksheets.size()).thenReturn(5);
			Mockito.doReturn(-1).when(gSheets).indexOf(anyObject(), anyString());
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			Mockito.doNothing().when(gSheets).setActiveSheetInfo(anyString(), anyInt(), anyString());
			gSheets.renameSheet(true, 10, "Megha", "Apple");
		} catch (IOException e) {
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void renameWorksheet_NameAlreadyExist() {
		try {
			List<String> listOfWorksheets = mock(List.class);
			Mockito.doReturn(listOfWorksheets).when(gSheets).getWorksheetNames(anyBoolean());
			when(listOfWorksheets.size()).thenReturn(5);
			Mockito.doReturn(-1).when(gSheets).indexOf(anyObject(), anyString());
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			Mockito.doNothing().when(gSheets).setActiveSheetInfo(anyString(), anyInt(), anyString());
			gSheets.renameSheet(false, 10, "Megha", "Apple");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void renameWorksheet_ByIndexSheetAlreadyExist() {
		try {
			List<String> listOfWorksheets = mock(List.class);
			Mockito.doReturn(listOfWorksheets).when(gSheets).getWorksheetNames(anyBoolean());
			when(listOfWorksheets.size()).thenReturn(5);
			when(listOfWorksheets.get(anyInt())).thenReturn("Megha");
			Mockito.doReturn(1).when(gSheets).indexOf(anyObject(), anyString());
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			Mockito.doNothing().when(gSheets).setActiveSheetInfo(anyString(), anyInt(), anyString());
			gSheets.renameSheet(true, 1, "Megha", "Apple");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void insertRange() {
		try {
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			gSheets.insertRange("123456", "A1:B6", "LEFT");
		} catch (IOException e) {
		}
	}

	@Test
	public void deleteRange() {
		try {
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			gSheets.deleteRange("A1:D5", "ROWS");
		} catch (IOException e) {
		}
	}

	@Test
	public void pasteCell() {
		try {
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			gSheets.pasteCell("A1", "B5");
		} catch (IOException e) {
		}
	}

	@Test
	public void deleteWorksheet_ByIndex() {
		try {
			List<String> listOfWorksheets = mock(List.class);
			Mockito.doReturn(listOfWorksheets).when(gSheets).getWorksheetNames(anyBoolean());
			Mockito.doReturn(6).when(gSheets).getWorksheetIdByName(anyString(), anyBoolean());
			when(listOfWorksheets.size()).thenReturn(10);
			gSheets.setActiveCell("Megha", "A1");
			when(listOfWorksheets.get(anyInt())).thenReturn("Megha");
			Mockito.doReturn(-1).when(gSheets).indexOf(anyObject(), anyString());
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			Mockito.doNothing().when(gSheets).setActiveSheetInfo(anyString(), anyInt(), anyString());
			WorksheetDto activeSheetInfo = new WorksheetDto();
			activeSheetInfo.setIndex(0);
			activeSheetInfo.setSheetId(123456);
			activeSheetInfo.setSheetName("Megha");
			gSheets.setActiveSheetInfo(activeSheetInfo);
			gSheets.deleteWorkSheet(true, 1, "Megha");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void deleteWorksheet_AtIndexZero() {
		try {
			List<String> listOfWorksheets = mock(List.class);
			Mockito.doReturn(listOfWorksheets).when(gSheets).getWorksheetNames(anyBoolean());
			Mockito.doReturn(6).when(gSheets).getWorksheetIdByName(anyString(), anyBoolean());
			when(listOfWorksheets.size()).thenReturn(10);
			gSheets.setActiveCell("Megha", "A1");
			when(listOfWorksheets.get(anyInt())).thenReturn("Megha");
			Mockito.doReturn(0).when(gSheets).indexOf(anyObject(), anyString());
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			Mockito.doNothing().when(gSheets).setActiveSheetInfo(anyString(), anyInt(), anyString());
			WorksheetDto activeSheetInfo = new WorksheetDto();
			activeSheetInfo.setIndex(0);
			activeSheetInfo.setSheetId(123456);
			activeSheetInfo.setSheetName("Megha");
			gSheets.setActiveSheetInfo(activeSheetInfo);
			gSheets.deleteWorkSheet(true, 1, "Megha");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void deleteWorksheet_ByName() {
		try {
			List<String> listOfWorksheets = mock(List.class);
			listOfWorksheets.add("Apple");
			Mockito.doReturn(listOfWorksheets).when(gSheets).getWorksheetNames(anyBoolean());
			when(listOfWorksheets.get(anyInt())).thenReturn("Megha");
			Mockito.doReturn(7).when(gSheets).indexOf(listOfWorksheets, "Megha");
			Mockito.doReturn(-1).when(gSheets).indexOf(listOfWorksheets, "Apple");
			Mockito.doReturn(1).when(gSheets).getWorksheetIdByName(anyString(), anyBoolean());
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			Mockito.doNothing().when(gSheets).setActiveSheetInfo(anyString(), anyInt(), anyString());
			gSheets.deleteWorkSheet(false, 1, "Megha");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void deleteWorksheet_InvalidIndex() {
		try {
			List<String> listOfWorksheets = mock(List.class);
			Mockito.doReturn(listOfWorksheets).when(gSheets).getWorksheetNames(anyBoolean());
			when(listOfWorksheets.size()).thenReturn(5);
			Mockito.doReturn(-1).when(gSheets).indexOf(anyObject(), anyString());
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			Mockito.doNothing().when(gSheets).setActiveSheetInfo(anyString(), anyInt(), anyString());
			gSheets.deleteWorkSheet(true, 10, "Megha");
		} catch (IOException e) {
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void deleteWorksheet_InvalidName() {
		try {
			List<String> listOfWorksheets = mock(List.class);
			Mockito.doReturn(listOfWorksheets).when(gSheets).getWorksheetNames(anyBoolean());
			when(listOfWorksheets.size()).thenReturn(5);
			Mockito.doReturn(-1).when(gSheets).indexOf(anyObject(), anyString());
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			Mockito.doNothing().when(gSheets).setActiveSheetInfo(anyString(), anyInt(), anyString());
			gSheets.deleteWorkSheet(false, 10, "Megha");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void deleteWorksheet_OnlySingleSheet() {
		try {
			List<String> listOfWorksheets = mock(List.class);
			Mockito.doReturn(listOfWorksheets).when(gSheets).getWorksheetNames(anyBoolean());
			when(listOfWorksheets.size()).thenReturn(1);
			Mockito.doReturn(-1).when(gSheets).indexOf(anyObject(), anyString());
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			Mockito.doNothing().when(gSheets).setActiveSheetInfo(anyString(), anyInt(), anyString());
			gSheets.deleteWorkSheet(false, 10, "Megha");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void setActiveSheetWhileOpeningByIndex() {
		Mockito.doReturn(true).when(gSheets).activateSheet(anyBoolean(), anyInt(), anyString());
		gSheets.setActiveSheetWhileOpening(true, "Megha");
	}

	@Test
	public void setActiveSheetWhileOpeningByName() {
		Mockito.doReturn(true).when(gSheets).activateSheet(anyBoolean(), anyInt(), anyString());
		gSheets.setActiveSheetWhileOpening(true, "Megha");
	}

	@Test
	public void deleteCell_ShiftLeft() {
		when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
		try {
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			gSheets.deleteCell(LEFT, "B5");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void deleteCell_ShiftUp() {
		when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
		try {
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			gSheets.deleteCell(UP, "B5");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void deleteDimension_ShiftLeft() {
		when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
		try {
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			gSheets.deleteDimension("B5", ROWS);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void deleteDimension_ShiftUp() {
		when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
		try {
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			gSheets.deleteDimension("B5", COLUMNS);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void hideWorksheet_AtIndexZero() {
		try {
			List<String> listOfWorksheets = mock(List.class);
			Mockito.doReturn(false).when(gSheets).isHidden(anyString());
			Mockito.doReturn(listOfWorksheets).when(gSheets).getWorksheetNames(anyBoolean());
			Mockito.doReturn(123456).when(gSheets).getWorksheetIdByName(anyString(), anyBoolean());
			when(listOfWorksheets.size()).thenReturn(10);
			Mockito.doReturn(0).when(gSheets).indexOf(anyObject(), anyString());
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			Mockito.doNothing().when(gSheets).setActiveSheetInfo(anyString(), anyInt(), anyString());
			gSheets.hideWorksheet("Megha");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void hideWorksheet_AtOtherIndex() {
		try {
			List<String> listOfWorksheets = mock(List.class);
			Mockito.doReturn(false).when(gSheets).isHidden(anyString());
			Mockito.doReturn(listOfWorksheets).when(gSheets).getWorksheetNames(anyBoolean());
			when(listOfWorksheets.size()).thenReturn(10);
			Mockito.doReturn(123456).when(gSheets).getWorksheetIdByName(anyString(), anyBoolean());
			Mockito.doReturn(2).when(gSheets).indexOf(anyObject(), anyString());
			BatchUpdate batchUpdate = mock(BatchUpdate.class);
			when(sheetSevice.spreadsheets()).thenReturn(spreadsheets);
			when(spreadsheets.batchUpdate(anyString(), anyObject())).thenReturn(batchUpdate);
			when(batchUpdate.execute()).thenReturn(new BatchUpdateSpreadsheetResponse());
			Mockito.doNothing().when(gSheets).setActiveSheetInfo(anyString(), anyInt(), anyString());
			gSheets.hideWorksheet("Megha");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void hideWorksheet_AlreadyHidden() {
		try {
			Mockito.doReturn(true).when(gSheets).isHidden(anyString());
			gSheets.hideWorksheet("Megha");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void hideWorksheet_OnlySingleSheet() {
		try {
			List<String> listOfWorksheets = mock(List.class);
			Mockito.doReturn(false).when(gSheets).isHidden(anyString());
			Mockito.doReturn(listOfWorksheets).when(gSheets).getWorksheetNames(anyBoolean());
			when(listOfWorksheets.size()).thenReturn(1);
			gSheets.hideWorksheet("Megha");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void hideWorksheet_InvalidName() {
		try {
			List<String> listOfWorksheets = mock(List.class);
			Mockito.doReturn(false).when(gSheets).isHidden(anyString());
			Mockito.doReturn(listOfWorksheets).when(gSheets).getWorksheetNames(anyBoolean());
			when(listOfWorksheets.size()).thenReturn(5);
			Mockito.doReturn(-1).when(gSheets).indexOf(anyObject(), anyString());
			gSheets.hideWorksheet("Megha");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void formatCell_MultipleCellTest( ) {
		
	}
	
	@Test
	public void formatCell_SingleCellWithMergeTest( ) {
		
	}
	
	@Test
	public void formatCell_SingleCellWithUnmergeTest( ) {
		
	}
}
