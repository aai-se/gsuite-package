/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */

package com.automationanywhere.botcommands.gsuite.calendar.commands;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GCalendar;
import com.automationanywhere.botcommand.gsuite.dto.calendar.EventDTO;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

public class DeleteEventTest {

    @Spy
    DeleteEvent deleteEvent;

    @Mock
    GCalendar gCalendar;

    Map<String,Object> sessionMap;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        sessionMap = new HashMap<>();

    }

    @Test
    public void delete(){
        try {
            doReturn(gCalendar).when(deleteEvent).getCalendar();
            deleteEvent.setSessionMap(sessionMap);

            when(gCalendar.deleteEvent(any(EventDTO.class))).thenReturn(true);

            deleteEvent.execute("s1","12345");

        } catch (GeneralSecurityException e) {
            fail();
        } catch (IOException e) {
            fail();
        }

    }


    @Test
    public void deleteThrowsException(){
        try {
            doReturn(gCalendar).when(deleteEvent).getCalendar();
            deleteEvent.setSessionMap(sessionMap);

            // Match the expected exception we will be getting from the code.
            thrown.expect(BotCommandException.class);
            thrown.expectMessage("Exception deleting");

            when(gCalendar.deleteEvent(any(EventDTO.class))).thenThrow(new IOException("Exception deleting"));

            deleteEvent.execute("s1","12345");

        } catch (GeneralSecurityException e) {
            fail();
        } catch (IOException e) {
            assertTrue(true);
        }

    }
}
