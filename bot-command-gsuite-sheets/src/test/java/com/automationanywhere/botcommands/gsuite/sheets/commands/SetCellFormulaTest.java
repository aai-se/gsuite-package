/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.ACTIVECELL;
import static com.automationanywhere.botcommand.gsuite.Constants.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.dto.sheets.WorksheetDto;

public class SetCellFormulaTest {

	@Spy
	SetCellFormula setCellFormula;
	
	@Spy
	SetSingleCell setSingleCell;

	@Mock
	GSheets mockGSheets;

	@Mock
	WorksheetDto worksheetDto;

	final String session = "session";
	final String spreadSheetId = "spreadsheetId";

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		sessionMap.put("session", mockGSheets);
		setCellFormula.setSessionMap(sessionMap);
	}

	@Test
	public void setFormulaForActiveCellTest() {
		try {
			when(mockGSheets.getSpreadSheetId()).thenReturn(spreadSheetId);
			when(mockGSheets.getActiveSheetInfo()).thenReturn(worksheetDto);
			when(worksheetDto.getSheetName()).thenReturn("Sheet1");
			when(mockGSheets.getActiveCell("Sheet1")).thenReturn("A1");
			when(mockGSheets.setSingleCell( "A1","=A1+A2" )).thenReturn(spreadSheetId);
			Assert.assertEquals(setCellFormula.execute(session, ACTIVECELL, null,null, "A1+A2").get(), spreadSheetId);
		} catch (IOException e) {
			Assert.fail();
		}
	}

	@Test
	public void setFormulaForSpecificCellTest() {
		try {
			when(mockGSheets.getSpreadSheetId()).thenReturn(spreadSheetId);
			when(mockGSheets.setSingleCell("A1", "=sum(A1:B5)")).thenReturn(spreadSheetId);
			Assert.assertEquals(setCellFormula.execute(session, SPECIFICCELL, "A1",null, "sum(A1:B5)").get(), spreadSheetId);
		} catch (IOException e) {
			Assert.fail();
		}
	}
	
	@Test
	public void setFormulaForRangeTest() {
		try {
			when(mockGSheets.getSpreadSheetId()).thenReturn(spreadSheetId);
			when(mockGSheets.setCellFormula("A1:B5", "=2*A1")).thenReturn(spreadSheetId);
			Assert.assertEquals(setCellFormula.execute(session, RANGE, null, "A1:B5", "2*A1").get(), spreadSheetId);
		} catch (IOException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void BotCommandExceptionTest() {
		try {
			when(mockGSheets.getSpreadSheetId()).thenReturn(spreadSheetId);
			when(mockGSheets.setSingleCell("A1", "=2*A1")).thenThrow(new IOException());
			setCellFormula.execute(session, SPECIFICCELL, "A1", null,"2*A1");
		} catch (IOException e) {
			Assert.fail();
		}
	}

}
