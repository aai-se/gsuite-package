/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.dto.sheets.WorksheetDto;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;

public class PasteCellTest {
	@Spy
	PasteCell pasteCelltest;

	@Mock
	AuthenticationService authenticationService;

	@Mock
	WorksheetDto worksheetDto;

	@Mock
	GSheets mockGSheets;

	final String session = "session";

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		sessionMap.put(session, mockGSheets);
		pasteCelltest.setSessionMap(sessionMap);

	}

	@Test
	public void pasteCell_ActiveCell_Test() {
		try {
			when(mockGSheets.getSpreadSheetId()).thenReturn("123456");
			when(mockGSheets.getActiveSheetInfo()).thenReturn(worksheetDto);
			when(worksheetDto.getSheetName()).thenReturn("Sheet1");
			when(mockGSheets.getActiveCell(anyString())).thenReturn("A1");
			when(mockGSheets.pasteCell("123456", "A1", "A2")).thenReturn(true);
			pasteCelltest.execute(session, "TRUE", null, "A2");

		} catch (IOException i) {

		}
	}

	@Test
	public void pasteCell_SpecificCell_Test() {
		try {
			when(mockGSheets.getSpreadSheetId()).thenReturn("123456");
			when(mockGSheets.getActiveSheetInfo()).thenReturn(worksheetDto);
			when(worksheetDto.getSheetName()).thenReturn("Sheet1");
			when(mockGSheets.getActiveCell(anyString())).thenReturn("A1");
			when(mockGSheets.pasteCell("123456", "A1", "A2")).thenReturn(true);
			pasteCelltest.execute(session, "FALSE", "A1", "A2");

		} catch (IOException i) {

		}
	}

	@SuppressWarnings("unchecked")
	@Test(expectedExceptions = BotCommandException.class)
	public void pasteCell_WrongCell_Test() {
		try {
			when(mockGSheets.getSpreadSheetId()).thenReturn("123456");
			when(mockGSheets.getActiveSheetInfo()).thenReturn(worksheetDto);
			when(worksheetDto.getSheetName()).thenReturn("Sheet1");
			when(mockGSheets.getActiveCell(anyString())).thenReturn("A1");
			when(mockGSheets.pasteCell("123456", "A1", "A2")).thenThrow(IOException.class);
			pasteCelltest.execute(session, "TRUE", "", "A2");

		} catch (IOException i) {
			Assert.assertTrue(true);
		}
	}

}