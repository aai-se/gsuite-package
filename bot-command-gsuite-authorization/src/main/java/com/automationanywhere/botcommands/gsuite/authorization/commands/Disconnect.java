package com.automationanywhere.botcommands.gsuite.authorization.commands;

import static com.automationanywhere.commandsdk.model.AttributeType.CHECKBOX;
import static com.automationanywhere.commandsdk.model.AttributeType.CREDENTIAL;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.core.security.SecureString;

@BotCommand
@CommandPkg(name = "disconnect", label = "Disconnect", description = "Disconnects user.", node_label = "", icon = "Auth_blue.svg")
public class Disconnect extends AbstractAuthUtils {

	private static final Logger LOGGER = LogManager.getLogger(Disconnect.class);
	private static final String LOGOUT_URL = "https://www.google.com/accounts/Logout";
	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public void execute(
			@Idx(index = "1", type = CREDENTIAL) @Pkg(label = "Username", description = "Enter the username") @NotEmpty SecureString userEmailAddress,
			@Idx(index = "2", type = CHECKBOX) @Pkg(label = "Logout from browser?") @NotEmpty Boolean isLogout) {
		validateAndRemoveUserSession(sessionMap, userEmailAddress.getInsecureString());
		if (isLogout)
			browserLogout();
	}

	public static void browserLogout() {
		try {
			HttpURLConnection con = (HttpURLConnection) new URL(LOGOUT_URL).openConnection();
			con.setRequestMethod("GET");
			con.setDoOutput(true);
			int response = con.getResponseCode();
			LOGGER.info("Response  = " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
}
