/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommands.gsuite.drive.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

/**
 * Tests copy Google Drive file functionality.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

public class CopyFileTest {

	public static final SecureString USERNAME = ConstantsTest.getUserName();

	@Spy
	CopyFile copyFile;

	@Mock
	GDrive mockGDrive;

	@Mock
	AuthenticationService mockAuthService;

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
		copyFile.setAuthenticationService(mockAuthService);
	}

	@Test
	public void copyFile_returnsCopiedFileId_whenRequiredFileNameProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(copyFile).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trailTest.txt";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			String folderId = "trial_file123";
			String folderName = "PKTest";
			JSONObject outputFileParamName = new JSONObject();
			outputFileParamName.put("fileName", folderName);

			when(mockGDrive.copyFile(inputFileParamName.toString(), outputFileParamName.toString())).thenReturn(fileId);
			Value<String> testId = copyFile.execute(USERNAME, "BYNAME", fileName, fileId, "BYNAME", folderName,
					folderId, false, "");
			Assert.assertEquals(fileId, testId.toString());
		} catch (JSONException | IOException | GeneralSecurityException e) {
			Assert.fail();
		}
	}

	@Test
	public void copyFile_returnsCopiedFileId_whenRequiredFileIdProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(copyFile).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			JSONObject inputFileParamID = new JSONObject();
			inputFileParamID.put("fileId", fileId);

			String folderId = "trial_file123";
			String folderName = "trial_file.txt";
			JSONObject outputFileParamID = new JSONObject();
			outputFileParamID.put("fileId", folderId);

			when(mockGDrive.copyFile(inputFileParamID.toString(), outputFileParamID.toString())).thenReturn(fileId);
			Value<String> testId = copyFile.execute(USERNAME, "BYID", fileName, fileId, "BYID", folderName, folderId,
					false, "newtrialtext.txt");
			Assert.assertEquals(fileId, testId.toString());
		} catch (GeneralSecurityException e) {
			Assert.fail();
		} catch (IOException e) {
			Assert.fail();
		} catch (JSONException e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@Test
	public void copyFile_returnsCopiedFileId_renameCopiedFileDuringCopy() {
		try {
			Mockito.doReturn(mockGDrive).when(copyFile).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			String newFileName = "newtrial_file.txt";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			String folderId = "trial_file123";
			String folderName = "trial_file.txt";
			JSONObject outputFileParamName = new JSONObject();
			outputFileParamName.put("fileName", folderName);
			outputFileParamName.put("newFileName", newFileName);
			when(mockGDrive.copyFile(inputFileParamName.toString(), outputFileParamName.toString())).thenReturn(fileId);
			Value<String> testId = copyFile.execute(USERNAME, "BYNAME", fileName, fileId, "BYNAME", folderName,
					folderId, true, newFileName);
			Assert.assertEquals(fileId, testId.toString());
		} catch (GeneralSecurityException e) {
			Assert.fail();
		} catch (IOException e) {
			Assert.fail();
		} catch (JSONException e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void copyFile_throwsBotCommandException_whenCopyFileAPIthrowsIOException() {
		try {
			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			String newFileName = "newtrial_file.txt";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			String folderId = "trial_file123";
			String folderName = "trial_file.txt";
			JSONObject outputFileParamName = new JSONObject();
			outputFileParamName.put("fileName", folderName);
			outputFileParamName.put("newFileName", newFileName);

			Mockito.doReturn(mockGDrive).when(copyFile).getDrive(USERNAME);
			when(mockGDrive.copyFile(inputFileParamName.toString(), outputFileParamName.toString()))
					.thenThrow(IOException.class);
			copyFile.execute(USERNAME, "BYNAME", fileName, fileId, "BYNAME", folderName, folderId, true, newFileName);
			Assert.fail();
		} catch (GeneralSecurityException e) {
			Assert.fail();
		} catch (IOException e) {
			Assert.assertTrue(true);
		} catch (JSONException e) {
			e.printStackTrace();
			Assert.fail();
		}

	}

	@Test
	public void getDrive_newGDriveImplObjectIsNotNull() {
		try {
			GDrive drive = copyFile.getDrive(USERNAME);
			assertNotNull(drive);
		} catch (GeneralSecurityException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
	}
}
