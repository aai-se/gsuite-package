/* Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.ACTIVECELL;
import static com.automationanywhere.botcommand.gsuite.Constants.*;
import static org.mockito.Matchers.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.dto.sheets.WorksheetDto;
import com.google.api.services.sheets.v4.model.ValueRange;

public class ReadCellFormulaTest {

	@Spy
	ReadCellFormula readCellFormula;

	@Mock
	GSheets mockGSheets;

	@Mock
	WorksheetDto worksheetDto;

	@Mock
	List<List<Object>> rowList;
	
	@Mock
	List<Object> values;

	final String session = "session";

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);

		sessionMap.put("session", mockGSheets);
		readCellFormula.setSessionMap(sessionMap);
		when(mockGSheets.getSpreadSheetId()).thenReturn("spreadsheetId");
		when(mockGSheets.getActiveSheetInfo()).thenReturn(worksheetDto);
		when(worksheetDto.getSheetName()).thenReturn("Sheet1");
		when(mockGSheets.getActiveCell(anyString())).thenReturn("A1");
	}

	@Test
	public void readCellFormulaByActiveCell() {
		try {
			ValueRange valueRange= Mockito.mock(ValueRange.class);
			
			when(valueRange.getValues()).thenReturn(rowList);
			when(rowList.get(anyInt())).thenReturn(values);
			when(values.get(anyInt())).thenReturn("=A2+B2");
			when(mockGSheets.getValue(anyString(), anyString(), anyString(),eq(FORMULA))).thenReturn(valueRange);
			
			Assert.assertEquals(readCellFormula.execute(session, ACTIVECELL, "").get(), "=A2+B2");
		} catch (IOException e) {
			fail();
		}
	}
	
	@Test
	public void readCellFormulaBySpecificCell() {
		try {
			ValueRange valueRange= Mockito.mock(ValueRange.class);
			when(valueRange.getValues()).thenReturn(rowList);
			when(rowList.get(anyInt())).thenReturn(values);
			when(values.get(anyInt())).thenReturn("=A2+B2");
			when(mockGSheets.getValue(anyString(), anyString(), anyString(),eq(FORMULA))).thenReturn(valueRange);
			
			Assert.assertEquals(readCellFormula.execute(session, SPECIFICCELL, "A3").get(), "=A2+B2");
		} catch (IOException e) {
			fail();
		}
	}

	@Test
	public void noFormulaSetOnCell() {
		try {
			ValueRange valueRange= Mockito.mock(ValueRange.class);
			when(valueRange.getValues()).thenReturn(rowList);
			when(rowList.get(anyInt())).thenReturn(values);
			when(values.get(anyInt())).thenReturn("value");
			when(mockGSheets.getValue(anyString(), anyString(), anyString(),eq(FORMULA))).thenReturn(valueRange);
			
			Assert.assertEquals(readCellFormula.execute(session, SPECIFICCELL, "A3").get(), "");
		} catch (IOException e) {
			fail();
		}
	}
	
	
	@Test
	public void cellValueIsNull() {
		try {
			ValueRange valueRange= Mockito.mock(ValueRange.class);
			when(valueRange.getValues()).thenReturn(null);
			when(mockGSheets.getValue(anyString(), anyString(), anyString(),eq(FORMULA))).thenReturn(valueRange);
			
			Assert.assertEquals(readCellFormula.execute(session, SPECIFICCELL, "A3").get(), "");
		} catch (IOException e) {
			fail();
		}
	}
	

	@Test(expectedExceptions = BotCommandException.class)
	public void botCommandExceptionTest() {
		try {
			ValueRange valueRange= Mockito.mock(ValueRange.class);
			when(valueRange.getValues()).thenReturn(rowList);
			when(rowList.get(anyInt())).thenReturn(values);
			when(values.get(anyInt())).thenReturn("=A2+B2");
			when(mockGSheets.getValue(anyString(), anyString(), anyString(),eq(FORMULA))).thenThrow(new IOException());
			readCellFormula.execute(session, SPECIFICCELL, "A3");
			} catch (IOException e) {
			Assert.fail();
		}
	}
}
