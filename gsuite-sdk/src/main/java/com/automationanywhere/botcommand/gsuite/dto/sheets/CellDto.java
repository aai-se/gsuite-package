/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommand.gsuite.dto.sheets;

public class CellDto {

  private final String session;
  private final String sheetName;
  private final String cell;
  private final String multipleCell;

  public CellDto(String session, String sheetName, String cell, String multipleCell) {
    this.session = session;
    this.sheetName = sheetName;
    this.cell = cell;
    this.multipleCell = multipleCell;
  }

  public String getSession() {
    return session;
  }

  public String getSheetName() {
    return sheetName;
  }

  public String getCell() {
    return cell;
  }

  public String getMultipleCell() {
    return multipleCell;
  }
}
