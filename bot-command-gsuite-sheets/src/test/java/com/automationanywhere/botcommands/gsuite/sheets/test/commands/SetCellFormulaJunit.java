/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.*;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommands.gsuite.sheets.commands.OpenSheets;
import com.automationanywhere.botcommands.gsuite.sheets.commands.SetCellFormula;
import com.automationanywhere.core.security.SecureString;

public class SetCellFormulaJunit extends AbstractAuthUtils {
	SetCellFormula setCellFormula;
	OpenSheets openSheets;

	final String session = "session";
	final String user = "saanchi@aademo.page";
	final String ClientID = "1045113049707-u3fhdtroech2uch19jogs52fg98t3lpu.apps.googleusercontent.com";
	final String redirectUrl = "http://localhost";
	final String clientSecreet = "6rjSOWzAEKEdiFqpCDGxirkk";

	final SecureString userName = new SecureString(user.toCharArray());
	final SecureString clientId = new SecureString(ClientID.toCharArray());
	final SecureString redirectURI = new SecureString(redirectUrl.toCharArray());
	final SecureString clientSecret = new SecureString(clientSecreet.toCharArray());
	
	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {
		launchOAuthConsentScreen(userName, clientId, clientSecret, redirectURI);

		openSheets = new OpenSheets();
		openSheets.setSessionMap(sessionMap);
		openSheets.execute(userName, session, "BYNAME", "Demo", null, null, true, "Megha", false);
		
		setCellFormula = new SetCellFormula();
		setCellFormula.setSessionMap(sessionMap);
	}

	@Test
	public void setCellFormulaTest() {
//		setCellFormula.execute(session, ACTIVECELL, null	, null, "2*K1*2");
//		setCellFormula.execute(session, SPECIFICCELL, "L1"	, null, "2*K1");
		setCellFormula.execute(session, RANGE, null	, "K2:L5", "sum(K1:L1)");
//		setCellFormula.execute(session, ACTIVECELL, null	, null, "2*F9");
	}
}
