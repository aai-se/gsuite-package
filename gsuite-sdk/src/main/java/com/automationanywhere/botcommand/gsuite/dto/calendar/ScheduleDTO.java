/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommand.gsuite.dto.calendar;

import com.google.api.client.util.DateTime;

import java.util.Objects;

public class ScheduleDTO {

    DateTime date;      //The date, in the format "yyyy-mm-dd", if this is an all-day event.
    DateTime dateTime; // The time, as a combined date-time value
    String timeZone;  // The time zone in which the time is specified

    public ScheduleDTO() {
    }

    public ScheduleDTO(DateTime date, DateTime dateTime, String timeZone) {
        this.date = date;
        this.dateTime = dateTime;
        this.timeZone = timeZone;
    }

    public DateTime getDate() {
        return date;
    }

    public DateTime getDateTime() {
        return dateTime;
    }

    public String getTimeZone() {
        return timeZone;
    }

    @Override
    public String toString() {
        return "Event times {" +
                "date=" + date +
                ", dateTime=" + dateTime +
                ", timeZone=" + timeZone +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScheduleDTO that = (ScheduleDTO) o;
        return Objects.equals(date, that.date) &&
                Objects.equals(dateTime, that.dateTime) &&
                Objects.equals(timeZone, that.timeZone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, dateTime, timeZone);
    }
}
