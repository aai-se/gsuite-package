/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommands.gsuite.drive.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

/**
 * Tests Get Google Drive file information functionality.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

public class GetFileInfoTest  {

	public static final SecureString USERNAME = ConstantsTest.getUserName();
	
	@Spy
	GetFileInfo getFileInfo;

	@Mock
	GDrive mockGDrive;

	@Mock
	AuthenticationService mockAuthService;

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {
		MockitoAnnotations.initMocks(this);
		getFileInfo.setAuthenticationService(mockAuthService);
	}

	@Test
	public void getFileInfo_returnsFileInfoMap_whenRequiredFileNameProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(getFileInfo).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			Map<String, Value> outputMap = new HashMap<>();
			outputMap.put("fileId", new StringValue(fileId));
			outputMap.put("fileName", new StringValue(fileName));
			outputMap.put("fileKind", new StringValue("sampleKind"));
			outputMap.put("fileMimeType", new StringValue("sampleMimeType"));
			outputMap.put("fileParents", new StringValue("sampleParent"));
			outputMap.put("filePermissions", new StringValue("samplePermission"));

			when(mockGDrive.getFileInformation(inputFileParamName.toString())).thenReturn(outputMap);
			DictionaryValue testId = getFileInfo.execute(USERNAME, "BYNAME", fileName, fileId);
			Assert.assertEquals(testId.get(), outputMap);
		} catch (GeneralSecurityException | IOException | JSONException e) {
			Assert.fail();
		}
	}

	@Test
	public void getFileInfo_returnsFileInfoMap_whenRequiredFileIdProvided() {
		try {
			Mockito.doReturn(mockGDrive).when(getFileInfo).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			JSONObject inputFileParamID = new JSONObject();
			inputFileParamID.put("fileId", fileId);

			Map<String, Value> outputMap = new HashMap<>();
			outputMap.put("fileId", new StringValue(fileId));
			outputMap.put("fileName", new StringValue(fileName));
			outputMap.put("fileKind", new StringValue("sampleKind"));
			outputMap.put("fileMimeType", new StringValue("sampleMimeType"));
			outputMap.put("fileParents", new StringValue("sampleParent"));
			outputMap.put("filePermissions", new StringValue("samplePermission"));

			when(mockGDrive.getFileInformation(inputFileParamID.toString())).thenReturn(outputMap);
			DictionaryValue testId = getFileInfo.execute(USERNAME, "BYID", fileName, fileId);
			Assert.assertEquals(testId.get(), outputMap);
		} catch (GeneralSecurityException | IOException | JSONException e) {
			Assert.fail();
		}
	}

	@Test(expectedExceptions = BotCommandException.class)
	public void getFileInfo_throwsBotCommandException_whenGetFileAPIthrowsIOException() {
		try {
			Mockito.doReturn(mockGDrive).when(getFileInfo).getDrive(USERNAME);

			String fileId = "trial_file123";
			String fileName = "trial_file.txt";
			JSONObject inputFileParamName = new JSONObject();
			inputFileParamName.put("fileName", fileName);

			when(mockGDrive.getFileInformation(inputFileParamName.toString())).thenThrow(IOException.class);
			getFileInfo.execute(USERNAME, "BYNAME", fileName, fileId);

			Assert.fail();
		} catch (GeneralSecurityException | JSONException e) {
			Assert.fail();
		} catch (IOException e) {
			Assert.assertTrue(true);
		}
	}

	@Test
	public void getDrive_newGDriveImplObjectIsNotNull() {
		try {
			GDrive drive = getFileInfo.getDrive(USERNAME);
			assertNotNull(drive);
		} catch (GeneralSecurityException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
	}
}
