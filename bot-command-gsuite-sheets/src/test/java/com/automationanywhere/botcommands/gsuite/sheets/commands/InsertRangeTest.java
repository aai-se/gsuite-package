/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.google.api.services.sheets.v4.model.ValueRange;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.mockito.Mockito.*;


public class InsertRangeTest {
    @Spy
    InsertRange insertRange;

    @Mock
    AuthenticationService authenticationService;

    @Mock
    GSheets mockGSheets;


    final String session = "session";


    Map<String, Object> sessionMap = new HashMap<>();

    @BeforeMethod
    public void setup() {
        MockitoAnnotations.initMocks(this);       
        sessionMap.put("session", mockGSheets);
         insertRange.setSessionMap(sessionMap);
    }

    @Test
    public void insertRange_test(){
        try{            
        	
        	when(mockGSheets.getSpreadSheetId()).thenReturn("123456");
        	//when(mockGSheets.get).thenReturn("123456");
        	
        	//Mockito.doNothing().when(mockGSheets.insertRange(mockGSheets.getSpreadSheetId(), rangeAddress, shiftType));
        	
        	
        	
             }catch(Exception i){

        }
    }
    
   
}