/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommand.gsuite.api;

import java.io.IOException;
import java.util.Map;

import org.json.JSONException;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.gsuite.dto.sheets.SpreadSheetDto;
import com.automationanywhere.botcommand.gsuite.dto.sheets.WorksheetDto;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.google.api.services.sheets.v4.model.ValueRange;

public interface GSheets extends GoogleSuite {

	SpreadSheetDto openSpreadSheet(String spreadSheetId) throws IOException; // changed
																				// declaration

//	/**
//	 * Creates a new Spreadsheet
//	 *
//	 * @return
//	 */
//	String getSpreadSheet(SpreadSheetDto spreadSheetDTO) throws IOException;

	/**
	 * Creates a new sheet inside the spreadsheet.
	 *
	 * @param sheetOption
	 * @param worksheetDto
	 * @return true if the sheet was created successfully
	 * @throws IOException
	 */
	Boolean createWorkSheet(String sheetOption, Number sheetIndex, String sheetName) throws IOException;

	/**
	 * Deletes a sheet from the spreadsheet.
	 *
	 * @param sheetOption
	 * @param worksheetDto
	 * @return
	 * @throws IOException
	 */
	boolean deleteWorkSheet(boolean byIndex, int sheetIndex, String sheetName) throws IOException;

	/**
	 * @param spreadSheetId
	 * @param sheetName
	 * @param cell
	 * @return
	 * @throws IOException
	 */
	ValueRange getValue(String spreadSheetId, String sheetName, String cellAddress, String formatType)
			throws IOException;


	/**
	 * @param spreadSheetId
	 * @param cell
	 * @param value
	 * @return
	 * @throws IOException
	 */
	String setSingleCell(String cellAddress, String value) throws IOException;

	public Boolean activateSheet(boolean sheetOptionIsIndex, Number sheetIndex, String sheetName);

	/**
	 * This function is used to go to specific cell.
	 * 
	 * @param cellOption different moment option(up, left, right, down, beginning
	 *                   and end).
	 * @param cell       address of the cell.
	 * @return
	 */
	Boolean goToCell(String cellOption, String cell) throws IOException;

	String getSpreadSheetId();

	/**
	 * @return
	 */
	AuthenticationService getAuthenticationService();

	/**
	 * @param spreadSheetId
	 * @param sheetName
	 * @param cellAddress
	 * @param readFullColumn
	 * @return
	 * @throws IOException
	 */
	ListValue<StringValue> readColumn(String cellAddress, boolean readFullColumn) throws IOException;

	/**
	 * Reads row values from activate sheet
	 * 
	 * @param cellAddress
	 * @param readFullRow
	 * @return
	 */
	ListValue<StringValue> readRow(String cellAddress, boolean readFullRow) throws IOException;

	/**
	 * @param deleteOption
	 * @param cell
	 * @return
	 * @throws IOException
	 */
	Boolean deleteCell(String deleteOption, String cell) throws IOException;

	/**
	 * @param spreadsheetId
	 * @param cell
	 * @param deleteOption
	 * @return
	 * @throws IOException
	 */
	Boolean deleteDimension(String cell, String deleteOption) throws IOException;

//	/**
//	 * Creates a new copy of the spreadsheet.
//	 *
//	 * @param oldSheetId
//	 * @param newSheetId
//	 * @param spreadsheetName
//	 * @return
//	 */
//	String saveAs(String oldSheetId, String newSheetId, String spreadsheetName) throws IOException;


	/**
	 * Closes a spreadsheet and removes it from the session
	 *
	 * @return
	 */
	Boolean closeSpreadSheet();

	/**
	 * @return
	 * @throws IOException
	 */

	/**
	 * Inserts new row(s) or column(s) in sheet.
	 *
	 * @param speadsheetId
	 * @param dimension    : Row or column.
	 * @param cellOption   : Defines how range is provided.
	 * @param cellAddress  : Address of the cell.
	 * @throws IOException
	 */
	void insertRowColumn(String speadsheetId, String dimension, String cellOption, String cellAddress)
			throws IOException;

	/**
	 * Delete row(s) or column(s) in sheet.
	 *
	 * @param speadsheetId
	 * @param dimension    : Row or column.
	 * @param cellOption   : Defines how range is provided.
	 * @param cellAddress  : Address of the cell.
	 * @throws IOException
	 */
	Boolean deleteRowColumn(String dimension, String cellOption, String cellAddress) throws IOException;

	/**
	 * Insert new cell in sheet.
	 *
	 * @param speadsheetId
	 * @param cellAddress  : Cell address.
	 * @param shiftType    : Shift cell right or down.
	 * @return
	 * @throws IOException
	 */
	Boolean insertCell(String cellAddress, String shiftType) throws IOException;

	/**
	 * Insert new range in sheet.
	 *
	 * @param speadsheetId
	 * @param rangeAddress : Range address.
	 * @param shiftType    : Shift cell right or down.
	 * @throws IOException
	 */
	void insertRange(String speadsheetId, String rangeAddress, String shiftType) throws IOException;

	/**
	 * Delete new range in sheet.
	 * 
	 * @param speadsheetId
	 * @param rangeAddress : Range address.
	 * @param shiftType    : Shift cell right or down.
	 * @return
	 * @throws IOException
	 */

//	/**
//	 * 
//	 * @param cellOption : Type of address provided.
//	 * @param cellAddress : Address of single cell.
//	 * @param cellRange : Address range.
//	 * @param formulaList : Formula list.
//	 * @param formula : Single Formula.
//	 * @throws IOException
//	 */
//	void setCellFormula(String cellOption, String cellAddress, String cellRange, List<String> formulaList, String formula)
//			throws IOException;
	/**
	 * 
	 * @param spreadSheetId
	 * @param cellAddress   : Address of single cell.
	 * @param cellFormula   : Formula for the cell.
	 * @return
	 * @throws IOException
	 */
	String setCellFormula(String cellAddress, String cellFormula) throws IOException;

//	/**
//	 * 
//	 * @param spreadSheetId
//	 * @param cellRange     : Range of cell addresses.
//	 * @param formulaList   : List of formula's for the cell range.
//	 * @return
//	 * @throws IOException
//	 */
//	String setMultipleCellFormula(String spreadSheetId, String cellRange, String formulaList) throws IOException;

	/**
	 * 
	 * @param cellOption
	 * @param cellAddress
	 * @param cellRange
	 * @param isFont
	 * @param fontName
	 * @param fontSize
	 * @param isBold
	 * @param isItalic
	 * @param underline
	 * @param fontColor
	 * @param isAlignment
	 * @param verticalAlignment
	 * @param horizontalAlignment
	 * @param isWrapText
	 * @param isMerge
	 * @param mergeType
	 * @throws IOException
	 */
	Boolean formatCell(String spreadSheetId, String cellOption, String cellAddress, String cellRange, boolean isFont,
			String fontName, Number fontSize, boolean isBold, boolean isItalic, boolean isUnderline,
			boolean isStrikethrough, String fontColor, boolean isAlignment, String verticalAlignment,
			String horizontalAlignment, boolean isWrapText, String wrapText, boolean isMerge, String mergeType)
			throws IOException;

	String getActiveSheetName();

	void setActiveSheetName(String activeSheetName);

	void setUserName(String userName);

	void setActiveSheetName(String activeSheetName, int activeSheetId);

	String getActiveCell(String sheetName);

	WorksheetDto getActiveSheetInfo();

	void setActiveCell(String sheetName, String activeCell);

	/**
	 * This method will count the total no of sheets in a workbook with hidden
	 * sheets or without hidden sheets.
	 * 
	 * @param excludeHiddenWorksheets: Whether hidden sheets should be included or
	 *                                 not.
	 * @throws IOException
	 */
	int retrieveSheetCount(boolean excludeHiddenWorksheets) throws IOException;

	/**
	 * Renames a worksheet.
	 * 
	 * @param sheetOption
	 * @param worksheetDto
	 * @param newSheetName
	 * @return
	 * @throws IOException
	 */
	boolean renameSheet(boolean byIndex, int sheetIndex, String sheetName, String newSheetName) throws IOException;

	/**
	 * Returns the total number of rows in active worksheet.
	 * 
	 * @param spreadSheetId Id of the spreadsheet.
	 * @param fetchRowsType Can be NONEMPTYROWS or TOTALROWS.
	 * @return
	 */
	int getNumberOfRows(String spreadSheetId, String fetchRowsType) throws IOException;

	/**
	 * Hides a worksheet.
	 * 
	 * @param spreadSheetId Id of the spreadsheet.
	 * @param worksheetName Name of the worksheet to hide.
	 * @return
	 * @throws IOException
	 */
	Boolean hideWorksheet(String worksheetName) throws IOException;

	/**
	 * Shows the hidden worksheet.
	 * 
	 * @param spreadSheetId Id of the spreadsheet.
	 * @param worksheetName Name of worksheet which is hidden.
	 * @return
	 */
	Boolean showWorksheet(String spreadSheetId, String worksheetName) throws IOException;

	/**
	 * Autofits rows of the active worksheet.
	 * 
	 * @param spreadSheetId Id of the spreadsheet
	 * @return
	 */
	Boolean autofitRows();

	/**
	 * Autofits columns of the active worksheet.
	 * 
	 * @param spreadSheetId Id of the spreadsheet
	 * @return
	 */
	Boolean autofitColumns();

	/**
	 * Sets the color of specified cell(s).
	 * 
	 * @param spreadSheetId  Id of the spreadsheet.
	 * @param cellAddress    cell address on which operation need to be perform
	 * @param setColorOption can be either "cell" or "text"
	 * @param color          color name or hex code.
	 * @return
	 */
	boolean setCellColor(String spreadSheetId, String cellAddress, String setColorOption, String color)
			throws IOException;;

	/**
	 * Gets color of the specified cell
	 * 
	 * @param spreadSheetId
	 * @param isActiveCell
	 * @param cellAddress
	 * @param cellColor
	 * @param textColor
	 * @param valueOf2
	 * @return
	 */
	ListValue<StringValue> getCellColor(String spreadSheetId, String cellAddress, Boolean cellColor, Boolean textColor,
			Boolean getColorOptionByName) throws IOException;

	/**
	 * This method will copy the Source cell to destination cell.
	 * 
	 * @return
	 */
	Boolean pasteCell(String cellAddress, String pasteCellAddress) throws IOException;

	/**
	 * Setter for setting session map
	 */
	void setSessionMap(Map<String, Object> sessionMap);

	void setActiveSheetWhileOpening(boolean isSpecificSheet, String sheetName);

//	Integer getWorksheetIndexByName(String sheetName);

	/**
	 * Returns Id of worksheet having name as sheetName.
	 * 
	 * @param sheetName
	 * @return
	 */

	TableValue getMultipleCells(boolean cellOption, String cellAddress);

	int getUsedRowColumns(String dimension);

	int getWorksheetIdByName(String sheetName, boolean excludeHiddenSheets);

	String getAuthCredentials(String text);

	/**
	 * 
	 * @param rangeAddress
	 * @param shiftType
	 * @return
	 * @throws IOException
	 */
	Boolean deleteRange(String rangeAddress, String shiftType) throws IOException;

	/**
	 * 
	 * @return
	 * @throws IOException
	 */
	Value<?> getAllWorkSheet() throws IOException;

	String getWorksheetNameByIndex(int index, boolean excludeHiddenSheets);

	Integer getWorksheetIndexByName(String sheetName, boolean excludeHiddenSheets);

}
