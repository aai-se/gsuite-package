/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */

package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.commandsdk.model.AttributeType.CHECKBOX;
import static com.automationanywhere.commandsdk.model.AttributeType.RADIO;
import static com.automationanywhere.botcommand.gsuite.Constants.*;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.api.impl.GDriveImpl;
import com.automationanywhere.botcommand.gsuite.api.impl.GSheetsImpl;
import com.automationanywhere.botcommand.gsuite.dto.drive.FileDto;
import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommand.gsuite.service.impl.AuthenticationServiceImpl;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.MatchesRegex;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import com.google.api.services.sheets.v4.Sheets;

@BotCommand
@CommandPkg(label = "Open spreadsheet", name = "Open", description = "Opens an existing spreadsheet", node_label = "from {{urlPath}} |with sheet named as {{!sheetName}}|")
public class OpenSheets extends AbstractAuthUtils {

	private static final Logger LOGGER = LogManager.getLogger(OpenSheets.class);
	private static final String SESSION_EXAMPLE = "e.g. Session1 or S1";

	private static final String NAME_DESC = "e.g. 1baZfKEURJW6zPFZfKEURJWP-ZfKEURJWZg5Xhyzs";
	private static final String SHEET_BY_URL_EXAMPLE = "e.g. https://docs.google.com/spreadsheets/d/1baZfKEURJW6zPFZfKEURJWP-ZfKEURJWZg5Xhyzs/edit#gid=0";
	private static final String SHEET_BY_NAME_EXAMPLE = "e.g. Invoice, Orders etc";
	private static final String WORKSHEET_BY_NAME_EXAMPLE = "e.g. Sheet1";

	private static Sheets sheetService;

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public Value execute(
			@Idx(index = "1", type = AttributeType.CREDENTIAL) @Pkg(label = USERNAME) @NotEmpty SecureString userEmailAddress,
			@Idx(index = "2", type = AttributeType.TEXT) @Pkg(label = "Session name", description = SESSION_EXAMPLE, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @NotEmpty String session,
			@Idx(index = "3", type = RADIO, options = {
					@Idx.Option(index = "3.1", pkg = @Pkg(label = "By Name", value = "BYNAME")),
					@Idx.Option(index = "3.2", pkg = @Pkg(label = "From URL", value = "BYURL")),
					@Idx.Option(index = "3.3", pkg = @Pkg(label = "From Spreadsheet Id", value = "BYID")) }) @Pkg(label = "Open sheet", default_value_type = DataType.STRING, default_value = "BYURL") @NotEmpty String sheetOption,
			@Idx(index = "3.1.1", type = AttributeType.TEXT) @Pkg(label = "", description = SHEET_BY_NAME_EXAMPLE, default_value = "", default_value_type = DataType.STRING) @NotEmpty String sheetName,
			@Idx(index = "3.2.1", type = AttributeType.TEXT) @Pkg(label = "", description = SHEET_BY_URL_EXAMPLE, default_value = "", default_value_type = DataType.STRING) @NotEmpty String sheetURL,
			@Idx(index = "3.3.1", type = AttributeType.TEXT) @Pkg(label = "", description = NAME_DESC, default_value = "", default_value_type = DataType.STRING) @NotEmpty String id,
			@Idx(index = "4", type = CHECKBOX) @Pkg(label = "Specific sheet name", default_value = "false", default_value_type = DataType.BOOLEAN) @NotEmpty Boolean isSpecificWorksheet,
			@Idx(index = "4.1", type = AttributeType.TEXT) @Pkg(label = "", default_value_type = DataType.STRING, description = WORKSHEET_BY_NAME_EXAMPLE, default_value = "") @NotEmpty @MatchesRegex("^[^\\\\\\/\\?\\*\\[\\]]{1,31}$") String worksheetName,
			@Idx(index = "5", type = AttributeType.CHECKBOX) @Pkg(label = "Sheet contains header", default_value = "false", default_value_type = DataType.BOOLEAN) Boolean containsHeader) {

		try {
			validateSessionExists(sessionMap, session);
			GSheets gSheets = getSheets(session, userEmailAddress);
			sessionMap.put(session, gSheets);
			String name = "";
			if (sheetOption.equalsIgnoreCase("BYURL") && sheetURL != null) {
				int start = sheetURL.indexOf("/d/");
				int end = sheetURL.indexOf("/edit#");
				if (start == -1) {
					LOGGER.error("Invalid URL: " + sheetURL);
					throw new BotCommandException("Invalid URL: " + sheetURL);
				}
				name = sheetURL.substring(start + 3, end);

			} else if (sheetOption.equalsIgnoreCase("BYID")) {
				name = id;
			} else if (sheetOption.equalsIgnoreCase("BYNAME")) {
				GDrive drive = getDrive(userEmailAddress);
				FileDto fileDto = drive.findFile(sheetName);
				if (fileDto != null)
					name = fileDto.getId();
				else {
					throw new IllegalArgumentException("No such file found");
				}
			}
			LOGGER.info("Opening spreadsheet by " + sheetOption + ":" + name);

			gSheets.openSpreadSheet(name);
			gSheets.setActiveSheetWhileOpening(isSpecificWorksheet, worksheetName);

			sessionMap.put(session, gSheets);
		} catch (BotCommandException bcEx) {
			throw bcEx;
		} catch (IllegalArgumentException e) {
			String msg = e.getMessage();
			LOGGER.error("exception in finding file: " + msg);
			throw new BotCommandException(msg, e);
		} catch (GeneralSecurityException | IOException | JSONException e) {
			String msg = e.getMessage();
			LOGGER.error("exception in user authorization: " + msg);
			throw new BotCommandException(msg, e);
		}
		return null;
	}

	GSheets getSheets(String session, SecureString userEmailAddress) throws GeneralSecurityException, IOException {
		GSheets gSheets = (GSheets) sessionMap.get(session);

		if (gSheets == null) {
			AuthenticationService authenticationService = getAuthenticationService();

			gSheets = new GSheetsImpl(authenticationService, session, sessionMap, userEmailAddress.getInsecureString());
		}
		return gSheets;
	}

	GDrive getDrive(SecureString userEmailAddress) throws GeneralSecurityException, IOException {
		AuthenticationService authenticationService = getAuthenticationService();
		return new GDriveImpl(authenticationService, userEmailAddress.getInsecureString(), sessionMap);
	}

	AuthenticationService getAuthenticationService() {
		return new AuthenticationServiceImpl();
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
}
