/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.USERNAME;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.api.impl.GDriveImpl;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommand.gsuite.service.impl.AuthenticationServiceImpl;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import com.google.api.services.drive.model.File;
import com.google.common.annotations.VisibleForTesting;

/**
 * Downloads a Google Drive file from a Google Drive location to the specified
 * location on the device where the Bot Agent is running.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

@BotCommand
@CommandPkg(label = "Find file/folder", name = "FindFilesAndFolders", description = "Searches a file/folder in specific directory", node_label = "with name {{name}} in {{fileLocation}}", return_type = DataType.DICTIONARY, return_required = false, return_label = "Returns the list of files or folders")
public class FindFilesFolders {
	private static final Logger LOGGER = LogManager.getLogger(FindFilesFolders.class);
	private static final String LOC_DESC = "Directory to be searched e.g. /Home/MyFolder/";
	private static final String NAME_DESC = "File name example: MyFile.doc, Folder name example: Accounts2019";
	private AuthenticationService mAuthenticationService;

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public DictionaryValue execute(
			@Idx(index = "1", type = AttributeType.CREDENTIAL) @Pkg(label = USERNAME) @NotEmpty SecureString userEmailAddress,
			@Idx(index = "2", type = AttributeType.RADIO, options = {
					@Idx.Option(index = "2.1", pkg = @Pkg(label = "File", value = "file")),
					@Idx.Option(index = "2.2", pkg = @Pkg(label = "Folder", value = "folder")) }) @Pkg(label = "Find", value = "Find", default_value_type = DataType.STRING, default_value = "file") String findType,
			@Idx(index = "3", type = TEXT) @Pkg(label = "Source folder", description = LOC_DESC) @NotEmpty String fileLocation,
			@Idx(index = "4", type = AttributeType.SELECT, options = {
					@Idx.Option(index = "4.1", pkg = @Pkg(label = "Exactly matches", value = "MAT")),
					@Idx.Option(index = "4.2", pkg = @Pkg(label = "Contains", value = "CON")) }) @Pkg(label = "Match type", description = NAME_DESC, default_value_type = STRING, default_value = "MAT") @NotEmpty String searchType,
			@Idx(index = "5", type = TEXT) @Pkg(label = "File/Folder name") @NotEmpty String name) {
		try {
			List<File> fileList = getDrive(userEmailAddress).findFileReturnList(fileLocation, name, searchType);

			Map<String, Value> outputMap = new HashMap<>();
			outputMap.put("NumberOfFiles", new StringValue("" + fileList.size()));
			int counter = 1;
			for (File file : fileList) {
				JSONObject json = new JSONObject();
				json.put("fileId", file.getId());
				json.put("fileName", file.getName());
				outputMap.put("File" + counter++, new StringValue(json.toString()));
			}
			return new DictionaryValue(outputMap);
		} catch (GeneralSecurityException | IOException | JSONException e) {
			LOGGER.error("Exception in finding Google Drive file: ", e);
			String msg = e.getMessage();
			throw new BotCommandException(msg, e);
		}
	}

	GDrive getDrive(SecureString userEmailAddress) throws GeneralSecurityException, IOException {
		AuthenticationService authenticationService = getAuthenticationService();
		return new GDriveImpl(authenticationService, userEmailAddress.getInsecureString(), sessionMap);
	}

	@VisibleForTesting
	void setAuthenticationService(AuthenticationService mAuthenticationService) {
		this.mAuthenticationService = mAuthenticationService;
	}

	synchronized AuthenticationService getAuthenticationService() {
		if (mAuthenticationService == null) {
			mAuthenticationService = new AuthenticationServiceImpl();
		}
		return mAuthenticationService;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
}
