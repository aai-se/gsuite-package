/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static com.automationanywhere.botcommand.gsuite.Constants.TRUE;
import static com.automationanywhere.botcommand.gsuite.Constants.FALSE;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommands.gsuite.sheets.commands.GetMultipleCell;
import com.automationanywhere.botcommands.gsuite.sheets.commands.OpenSheets;
import com.automationanywhere.botcommands.gsuite.sheets.test.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;


//Done by Vipin


public class GetMultipleCellsJunit extends AbstractAuthUtils {
	
	GetMultipleCell getMultipleCell;

	OpenSheets openSheets;
	final String session = "session";

	public static final SecureString USERNAME =  ConstantsTest.getUserName();
	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {
        launchOAuthConsentScreen(ConstantsTest.getUserName(),ConstantsTest.getClientid(), ConstantsTest.getClientSecret(), ConstantsTest.getRedirectUri());
		getMultipleCell = new GetMultipleCell();
		getMultipleCell.setSessionMap(sessionMap);
		openSheets = new OpenSheets();
		openSheets.setSessionMap(sessionMap);
		openSheets.execute(ConstantsTest.getUserName(), session, "BYNAME", "Demo", null, null, true, "Megha", false);
	}

	@Test
	public void getMultipleCellRangeTest() {
		System.out.println("----------------------------------Get Multiple Cells-----------------------------------------");
		System.out.println(getMultipleCell.execute(session, TRUE, "A9:L12"));
	}
	
	@Test
	public void getMultipleCellAllTest() {
		System.out.println("----------------------------------Get Multiple Cells-----------------------------------------");
		System.out.println(getMultipleCell.execute(session, FALSE, null));
	}
	
	@Test(expectedExceptions = BotCommandException.class)
	public void getMultipleCellTest_NullSession() {
		getMultipleCell.execute(null, FALSE, "null");
	}
	
	@Test(expectedExceptions = BotCommandException.class)
	public void getMultipleCellTest_WrongSession() {
		getMultipleCell.execute("wrong_session", FALSE, "null");
	}
	
	@Test(expectedExceptions = BotCommandException.class)
	public void getMultipleCellTest_InvalidRange() {
		getMultipleCell.execute(session, TRUE, "9A3:L9");
	}
}

