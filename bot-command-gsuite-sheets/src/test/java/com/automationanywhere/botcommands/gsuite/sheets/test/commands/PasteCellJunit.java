/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommands.gsuite.sheets.commands.OpenSheets;
import com.automationanywhere.botcommands.gsuite.sheets.commands.PasteCell;
import com.automationanywhere.botcommands.gsuite.sheets.test.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;


public class PasteCellJunit extends AbstractAuthUtils {

	PasteCell pasteCell;

	OpenSheets openSheets;
	
	
	public final static SecureString USERNAME = ConstantsTest.getUserName();
	final String session = "session";
	final String newSession = "newsession";
	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {
		launchOAuthConsentScreen(ConstantsTest.getUserName(), ConstantsTest.getClientid(), ConstantsTest.getClientSecret(), ConstantsTest.getRedirectUri());
		
		openSheets = new OpenSheets();
		openSheets.setSessionMap(sessionMap);
		openSheets.execute(ConstantsTest.getUserName(),session, "BYNAME", "Demo",
				null,null, true, "ArpitTest",false);
		pasteCell = new PasteCell();
		pasteCell.setSessionMap(sessionMap);

	}

	@Test
	public void pasteCell_test() {
		
	 pasteCell.execute(session, "TRUE", null, "A10");
	 pasteCell.execute(session, "FALSE", "A10", "A11");
	}

}
