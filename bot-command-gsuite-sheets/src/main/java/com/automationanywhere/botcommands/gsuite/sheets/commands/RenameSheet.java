/* Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.*;
import static com.automationanywhere.botcommands.gsuite.sheets.utils.Utils.getException;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.botcommand.gsuite.validators.ExecutorValidator;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Idx.Option;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.GreaterThan;
import com.automationanywhere.commandsdk.annotations.rules.MatchesRegex;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;

@BotCommand
@CommandPkg(label = "Rename sheet", name = "renameSheet", description = "Renames specified google sheet", node_label = "with the name {{sheetName}}||at index {{sheetIndex}}| in session {{session}}", icon = "excel.svg")
public class RenameSheet {

	private static final Logger LOGGER = LogManager.getLogger(RenameSheet.class);

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public BooleanValue execute(
			@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String session,
			@Idx(index = "2", type = AttributeType.RADIO, options = {
					@Option(index = "2.1", pkg = @Pkg(label = "Index", value = TRUE)),
					@Option(index = "2.2", pkg = @Pkg(label = "Name", value = FALSE)) }) @Pkg(label = "Sheet", default_value = TRUE, default_value_type = DataType.STRING) @NotEmpty String sheetOption,
			@Idx(index = "2.1.1", type = AttributeType.NUMBER) @Pkg(label = "", description = "e.g. 1 or 3", default_value = "1", default_value_type = DataType.NUMBER) @NotEmpty @GreaterThan("0") Number sheetIndex,
			@Idx(index = "2.2.1", type = AttributeType.TEXT) @Pkg(description = "e.g. Sheet1, sheet name should not exceed 31 characters.", default_value_type = DataType.STRING) @NotEmpty @VariableType(value = DataType.STRING) @MatchesRegex(SHEET_NAME_VALIDATOR) String sheetName,
			@Idx(index = "3", type = AttributeType.TEXT) @Pkg(label = "Enter new name for the sheet", description = "e.g. Final P&L, sheet name should not exceed 31 characters.") @VariableType(value = DataType.STRING) @MatchesRegex(SHEET_NAME_VALIDATOR) @NotEmpty String newSheetName) {
		try {
			GSheets gSheets = (GSheets) sessionMap.get(session);
			ExecutorValidator.getInstance().validate(gSheets);

			int sheetIndexInt = sheetIndex == null ? 0 : sheetIndex.intValue();
			return new BooleanValue(gSheets.renameSheet(Boolean.valueOf(sheetOption), sheetIndexInt, sheetName, newSheetName));
		} catch (IOException exception) {
			LOGGER.error("Exception occurred during execution of Rename sheet .....", exception);
			throw new BotCommandException(exception);
		}
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}
