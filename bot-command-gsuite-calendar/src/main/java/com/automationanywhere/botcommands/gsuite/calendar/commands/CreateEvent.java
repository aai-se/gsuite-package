/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */

package com.automationanywhere.botcommands.gsuite.calendar.commands;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GCalendar;
import com.automationanywhere.botcommand.gsuite.api.impl.GCalendarImpl;
import com.automationanywhere.botcommand.gsuite.dto.calendar.EventDTO;
import com.automationanywhere.botcommand.gsuite.dto.calendar.ScheduleDTO;
import com.automationanywhere.botcommand.gsuite.dto.calendar.Visibility;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommand.gsuite.service.impl.AuthenticationServiceImpl;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.util.DateTime;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import static com.automationanywhere.botcommands.gsuite.calendar.utils.Utils.getException;
import static com.automationanywhere.commandsdk.model.AttributeType.RADIO;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;

@BotCommand
@CommandPkg(label = "Create new calendar event", name = "Create", description = "Creates a new event",
        node_label = "with the name {{eventName}}| from {{session}}",
        return_type = DataType.STRING,
        return_required = true,
        return_label = "Returns the id of the created event")
public class CreateEvent {
    private static final Logger LOGGER = LogManager.getLogger(CreateEvent.class);
    private static final String SESSION_EXAMPLE = "e.g. Session1 or S1";

    private static final String NAME_DESC = "Title of the Event";
    private static final String ATTENDEE_DESC = "List of Attendee email addresses separated by comma e.g. john@company.com, doe@company.com";
    private static final String LOCATION_DESC = "Location of the event";
    private static final String START_DATE_DESC = "Event start date as yyyy-MM-dd";
    private static final String START_TIME_DESC = "Event start time as HH:mm:ss";
    private static final String END_TIME_DESC = "Event end time as HH:mm:ss";
    private static final String END_DATE_DESC = "Event end date as yyyy-MM-dd";
    private static final String DESCRIPTION_DESC = "Description of the event";

    @Idx(index = "9.1", type = AttributeType.LIST)
    @Pkg(label = "")
    @NotEmpty
    @Inject
    private List<String> recurringRule;

    @Sessions
    private Map<String, Object> sessionMap;

    @Execute
    public Value<String> execute(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", description = SESSION_EXAMPLE, default_value = "Default", default_value_type = DataType.STRING) @NotEmpty String session,
            @Idx(index = "2", type = TEXT) @Pkg(label = "Title", description = NAME_DESC) @NotEmpty String eventName,
            @Idx(index = "3", type = TEXT) @Pkg(label = "Location", description = LOCATION_DESC) String location,
            @Idx(index = "4", type = TEXT) @Pkg(label = "Attendees", description = ATTENDEE_DESC) String attendee,
            @Idx(index = "5", type = TEXT) @Pkg(label = "Start Date", description = START_DATE_DESC) @NotEmpty String startDate,
            @Idx(index = "6", type = TEXT) @Pkg(label = "End Date", description = END_DATE_DESC) @NotEmpty String endDate,

            @Idx(index = "7", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "7.1", pkg = @Pkg(label = "All Day", value = "ALL_DAY")),
                    @Idx.Option(index = "7.2", pkg = @Pkg(label = "Specify Time", value = "SPECIFY_TIME"))})
            @Pkg(label = "Event Time", default_value = "SPECIFY_TIME", default_value_type = DataType.STRING) @NotEmpty String eventTime,

            @Idx(index = "7.2.1", type = TEXT) @Pkg(label = "Start Time", description = START_TIME_DESC) @NotEmpty String startTime,
            @Idx(index = "7.2.2", type = TEXT) @Pkg(label = "End Time", description = END_TIME_DESC)  @NotEmpty String endTime,

            @Idx(index = "8", type = RADIO, options = {
            @Idx.Option(index = "8.1", pkg = @Pkg(label = "Use System TimeZone", value = "SYSTEM_TIMEZONE")),
            @Idx.Option(index = "8.2", pkg = @Pkg(label = "Specify TimeZone", value = "SPECIFY_TIMEZONE"))})
            @Pkg(label = "TimeZone", default_value = "SYSTEM_TIMEZONE", default_value_type = DataType.STRING) @NotEmpty String defaultTimezone,

            @Idx(index = "8.2.1", type = AttributeType.TEXT) @Pkg(label = "Start TimeZone") @VariableType(value = DataType.STRING) @NotEmpty String startTZ,
            @Idx(index = "8.2.2", type = AttributeType.TEXT) @Pkg(label = "End TimeZone")  @VariableType(value = DataType.STRING) @NotEmpty String endTZ,

            @Idx(index = "9", type = AttributeType.CHECKBOX) @Pkg(label = "Recurring", default_value = "false", default_value_type = DataType.BOOLEAN) @NotEmpty @VariableType(value = DataType.BOOLEAN) Boolean recurring,

            @Idx(index = "10", type = AttributeType.SELECT, options = {
                    @Idx.Option(index = "10.1", pkg = @Pkg(label = "Default", value = "DEFAULT")),
                    @Idx.Option(index = "10.2", pkg = @Pkg(label = "Public", value = "PUBLIC")),
                    @Idx.Option(index = "10.3", pkg = @Pkg(label = "Private", value = "PRIVATE")) })
            @Pkg(label = "Visibility", description = DESCRIPTION_DESC) @NotEmpty String visibility,
            @Idx(index = "11", type = TEXT) @Pkg(label = "Description", description = DESCRIPTION_DESC) String desc
            ){

        try {

            GCalendar gCalendar = getCalendar();

            EventDTO eventDTO = getEventDTO(eventName, location, attendee, startDate, endDate, eventTime, startTime, endTime, defaultTimezone, startTZ, endTZ, recurring, visibility, desc);

            LOGGER.debug("Event data = " + eventDTO.toString());

            EventDTO updatedEvent = gCalendar.createEvent(eventDTO);

            sessionMap.put(session, gCalendar);

            LOGGER.info("Event info = " + updatedEvent.toString());
            return new StringValue(updatedEvent.getId());
        }catch ( ParseException | DateTimeParseException e) {
            throw new BotCommandException(e.getMessage(), e);
        }catch (GeneralSecurityException | IOException e){
            LOGGER.error("exception in user authorization: ",e);
            String msg = getException((GoogleJsonResponseException) e);
            throw new BotCommandException(msg, e);
        }
    }

    EventDTO getEventDTO(String eventName, String location,  String attendee, String startDate, String endDate,
                         String eventTime, String startTime, String endTime,  String defaultTimezone,
                         String startTZ, String endTZ, Boolean recurring, String visibility,
                         String desc) throws ParseException {

        EventDTO eventDTO = new EventDTO();
        eventDTO.setSummary(eventName);
        eventDTO.setDescription(desc);
        eventDTO.setLocation(location);
        if(attendee != null  && !"".equals(attendee)){
            if(attendee.contains(",")) {
                String[] attendees = attendee.split(",");
                eventDTO.setAttendees(attendees);
            }else{
                eventDTO.setAttendees(new String[]{attendee});
            }
        }else {
            eventDTO.setAttendees(null);
        }

        boolean isAllDay = "ALL_DAY".equals(eventTime);
        LOGGER.debug("is this all day event ? " + isAllDay);
        eventDTO.setRecurring(recurring);
        eventDTO.setRecurrence(recurringRule!= null ?recurringRule.toArray(new String[] {}) : null);
        eventDTO.setEndTimeUnspecified(isAllDay);

        TimeZone sTZ;
        TimeZone eTZ;
        if("SPECIFY_TIMEZONE".equals(defaultTimezone)){
            sTZ = TimeZone.getTimeZone(startTZ);
            eTZ = TimeZone.getTimeZone(endTZ);
        }else{
            sTZ = TimeZone.getDefault();
            eTZ = TimeZone.getDefault();
            startTZ = "GMT";
            endTZ = "GMT";
        }
        LOGGER.trace("Start Timezone = " + sTZ + ", end Timezone = " + eTZ);

        if(isAllDay)
            eventDTO.setAllDay(true);

        ScheduleDTO startSchedule;
        DateTime startDateTime = getDate(startDate,startTime,sTZ,isAllDay);
        if(isAllDay) {
            startSchedule = new ScheduleDTO(startDateTime,null, startTZ);
        }else{
            startSchedule = new ScheduleDTO(null,startDateTime, startTZ);
        }

        eventDTO.setStart(startSchedule);

        ScheduleDTO endSchedule;
        DateTime endDateTime = getDate(endDate,endTime,eTZ,isAllDay);
        if(isAllDay) {
            endSchedule = new ScheduleDTO(endDateTime,null, endTZ);
        }else{
            endSchedule = new ScheduleDTO(null,endDateTime, endTZ);
        }

        eventDTO.setEnd(endSchedule);

        eventDTO.setVisibility(Visibility.valueOf(visibility));
        return eventDTO;
    }

    GCalendar getCalendar() throws GeneralSecurityException, IOException {
        AuthenticationService authenticationService = new AuthenticationServiceImpl();

        return new GCalendarImpl(authenticationService);
    }

    public void setSessionMap(Map<String,Object> sessionMap){
        this.sessionMap = sessionMap;
    }


    public void setRecurringRule(List<String> recurringRule) {
        this.recurringRule = recurringRule;
    }

    // returns date in the format yyyy-mm-dd
    private static DateTime getDate(String date, String time, TimeZone timeZone, boolean isAllDay) throws ParseException {

        SimpleDateFormat sdf;
        DateTime dateTime;
        if(isAllDay) {
            sdf = new SimpleDateFormat("yyyy-MM-dd",java.util.Locale.getDefault());
            sdf.setTimeZone(timeZone);
            Date d = sdf.parse(date);
            dateTime = new DateTime(true, d.getTime(), 0);
        }else{
            sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss",java.util.Locale.getDefault());
            sdf.setTimeZone(timeZone);
            Date dFull = sdf.parse(date+"T" + time);
            dateTime = new DateTime(dFull,timeZone);
        }

        LOGGER.debug(" date = " + date + ", time =  " + time + ", converted to -->" + dateTime.toStringRfc3339());
        return dateTime;
    }

}
