/*
* Copyright (c) 2019 Automation Anywhere.
* All rights reserved.
*
* This software is the proprietary information of Automation Anywhere.
* You shall use it only in accordance with the terms of the license agreement
* you entered into with Automation Anywhere.
*
*/
package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.ROWS;
import static com.automationanywhere.botcommand.gsuite.Constants.SPECIFICCELL;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommands.gsuite.sheets.commands.HideWorksheet;
import com.automationanywhere.botcommands.gsuite.sheets.commands.InsertCell;
import com.automationanywhere.botcommands.gsuite.sheets.commands.OpenSheets;
import com.automationanywhere.botcommands.gsuite.sheets.test.utils.ConstantsTest;
import com.automationanywhere.core.security.SecureString;

public class HideWorkSheetJunit extends AbstractAuthUtils {
	HideWorksheet hideWorkSheet;
	OpenSheets openSheets;
	InsertCell insertCell;

	final String session = "session";
	private final static SecureString USERNAME = ConstantsTest.getUserName();

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {
		launchOAuthConsentScreen(ConstantsTest.getUserName(), ConstantsTest.getClientid(),
				ConstantsTest.getClientSecret(), ConstantsTest.getRedirectUri());
		hideWorkSheet = new HideWorksheet();
		hideWorkSheet.setSessionMap(sessionMap);
		openSheets = new OpenSheets();
		openSheets.setSessionMap(sessionMap);
		insertCell = new InsertCell();
		insertCell.setSessionMap(sessionMap);
		openSheets.execute(USERNAME, session, "BYNAME", "Demo111", null, null, true, "Sheet2", false);
//openSheets.execute(USERNAME, session, "BYNAME", "Demo111", null, null, true, "Sheet3", false); specific sheet
	}

	@Test
	public void hideWorkSheetTest() throws IOException {
// try to hide already hidden worksheet--- nothing happen
		hideWorkSheet.execute(session, "Sheet2");
		insertCell.execute(session, SPECIFICCELL, "A10", ROWS);

//	hideWorkSheet.execute(session, "DemoPart");
//	hideWorkSheet.execute(session, "Sheet3");
	}

}