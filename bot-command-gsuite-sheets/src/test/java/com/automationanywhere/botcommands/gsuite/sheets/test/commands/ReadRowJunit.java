package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.SPECIFICCELL;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.gsuite.service.AbstractAuthUtils;
import com.automationanywhere.botcommands.gsuite.sheets.commands.OpenSheets;
import com.automationanywhere.botcommands.gsuite.sheets.commands.ReadRow;
import com.automationanywhere.core.security.SecureString;

public class ReadRowJunit extends AbstractAuthUtils {
	ReadRow readRow;
	OpenSheets openSheets;

	final String session = "session";
	final String user = "saanchi@aademo.page";
	final String ClientID = "1045113049707-u3fhdtroech2uch19jogs52fg98t3lpu.apps.googleusercontent.com";
	final String redirectUrl = "http://localhost";
	final String clientSecreet = "6rjSOWzAEKEdiFqpCDGxirkk";

	final SecureString userName = new SecureString(user.toCharArray());
	final SecureString clientId = new SecureString(ClientID.toCharArray());
	final SecureString redirectURI = new SecureString(redirectUrl.toCharArray());
	final SecureString clientSecret = new SecureString(clientSecreet.toCharArray());

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() throws IOException, GeneralSecurityException {
		launchOAuthConsentScreen(userName, clientId, clientSecret, redirectURI);

		openSheets = new OpenSheets();
		openSheets.setSessionMap(sessionMap);
		openSheets.execute(userName, session, "BYNAME", "Demo", null, null, true, "Megha", false);

		readRow = new ReadRow();
		readRow.setSessionMap(sessionMap);
	}

	@Test
	public void readRowTest() {
		//valid inputs
//		readRow.execute(session, SPECIFICCELL, "C1", true );				//list af all values
		readRow.execute(session, SPECIFICCELL, "C1", false );				//list from 'c'
		
		//specific column out of bound
//		readRow.execute(session, SPECIFICCELL, "M6", true );				// list values
//		readRow.execute(session, SPECIFICCELL, "M6", false );				// throw error

		// if row is empty but in used range
//		readRow.execute(session, SPECIFICCELL, "E7", true );				//empty list
//		readRow.execute(session, SPECIFICCELL, "E7", false );				//throw error

		// if row is empty but out of used range
//		readRow.execute(session, SPECIFICCELL, "D20", true );				//it should return empty list instead of throwing error
//		readRow.execute(session, SPECIFICCELL, "D20", false); 				// should throw error
//
//		readRow.execute(session, SPECIFICCELL, "Q25", true );				//empty list
//		readRow.execute(session, SPECIFICCELL, "Q25", false );				//throw error
	}

}
