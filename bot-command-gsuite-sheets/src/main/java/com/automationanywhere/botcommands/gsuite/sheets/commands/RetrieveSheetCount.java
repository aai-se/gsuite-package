/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */

package com.automationanywhere.botcommands.gsuite.sheets.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.DEFAULT_VALUE_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.DESCRIPTION_SESSION;
import static com.automationanywhere.botcommand.gsuite.Constants.LABEL_SESSION;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GSheets;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

/**
 * This command will count the total no of sheets in a workbook
 * 
 * @author Administrator
 *
 */
@BotCommand
@CommandPkg(label = "Retrieve sheets count", name = "retrieveSheetsCount", description = "Retrieves the number of google sheets in a workbook", node_label = "and assign output to a variable {{returnTo}} |session {{sessionName}}", return_type = DataType.NUMBER, return_required = true, return_label = "Assign output to a variable", icon = "Excel_blue.svg")
public class RetrieveSheetCount {

	private static final Logger LOGGER = LogManager.getLogger(RenameSheet.class);

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public NumberValue execute(
			@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = LABEL_SESSION, description = DESCRIPTION_SESSION, default_value = DEFAULT_VALUE_SESSION, default_value_type = DataType.STRING) @VariableType(value = DataType.STRING) @NotEmpty String sessionName,

			@Idx(index = "2", type = AttributeType.CHECKBOX) @Pkg(label = "Exclude Hidden Worksheets", default_value = "false", default_value_type = DataType.BOOLEAN) @NotEmpty Boolean excludeHiddenWorksheets) {

		try {
			GSheets gSheets = (GSheets) sessionMap.get(sessionName);
			int count = gSheets.retrieveSheetCount(excludeHiddenWorksheets);
			return new NumberValue(count);
		} catch (IOException exception) {
			LOGGER.error("Exception occurred during execution of Retrieve sheet count.....", exception);
			throw new BotCommandException(exception);
		}		
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}
