/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommands.gsuite.sheets.commands.InsertRowColumn;

public class InsertRowColumnJunit {
	InsertRowColumn insertRowColumn;

	final String session = "session";

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {

		insertRowColumn = new InsertRowColumn();
		insertRowColumn.setSessionMap(sessionMap);
	}

	@Test
	public void insertRowColumnTest() {

	}
}
