/**
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommands.gsuite.drive.commands;

import static com.automationanywhere.botcommand.gsuite.Constants.USERNAME;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.gsuite.api.GDrive;
import com.automationanywhere.botcommand.gsuite.api.impl.GDriveImpl;
import com.automationanywhere.botcommand.gsuite.service.AuthenticationService;
import com.automationanywhere.botcommand.gsuite.service.impl.AuthenticationServiceImpl;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import com.google.common.annotations.VisibleForTesting;

/**
 * Gets the list of permissions of the Google Drive File.
 *
 * @author Saanchi Muthya
 * @version 1.0
 * @since 2019-07-01
 */

@BotCommand
@CommandPkg(label = "Check Permissions", name = "CheckPermissions", description = "Checks for read/write/delete permission for file or folder on google drive", node_label = "for | file {{fileName}} | folder {{folderName}}", return_type = DataType.LIST, return_sub_type = DataType.STRING, return_required = true, return_label = "Returns the file's permissions")
public class CheckPermissions {
	private static final Logger LOGGER = LogManager.getLogger(CheckPermissions.class);

	private static final String FILE_NAME_DESC = "e.g. myFile.xlsx or MyFolder/MyFile.doc";
	private static final String FOLDER_NAME_DESC = "e.g. MyFolder/Folder";
	private AuthenticationService mAuthenticationService;

	@Sessions
	private Map<String, Object> sessionMap;

	@Execute
	public StringValue execute(
			@Idx(index = "1", type = AttributeType.CREDENTIAL) @Pkg(label = USERNAME) @NotEmpty SecureString userEmailAddress,
			@Idx(index = "2", type = AttributeType.RADIO, options = {
					@Idx.Option(index = "2.1", pkg = @Pkg(label = "File name (including path and extension)", value = "file")),
					@Idx.Option(index = "2.2", pkg = @Pkg(label = "Folder name (including path)", value = "folder")) }) @Pkg(label = "Check Permissions for", default_value_type = DataType.STRING, default_value = "file") @NotEmpty String nameOption,
			@Idx(index = "2.1.1", type = TEXT) @Pkg(label = "", description = FILE_NAME_DESC) @NotEmpty String fileName,
			@Idx(index = "2.2.1", type = TEXT) @Pkg(label = "", description = FOLDER_NAME_DESC) @NotEmpty String folderName,
			@Idx(index = "3", type = AttributeType.RADIO, options = {
					@Idx.Option(index = "3.1", pkg = @Pkg(label = "Read", value = "read")),
					@Idx.Option(index = "3.2", pkg = @Pkg(label = "Write", value = "write")),
					@Idx.Option(index = "3.3", pkg = @Pkg(label = "Delete", value = "delete")) }) @Pkg(label = "Check Permissions type", default_value_type = DataType.STRING, default_value = "read") @NotEmpty String role) {
		try {
			JSONObject inputParam = new JSONObject();
			if (nameOption.equals("file")) {
				inputParam.put("fileName", fileName);
			} else if (nameOption.equals("folder")) {
				inputParam.put("fileName", folderName);
			}
			inputParam.put("role", role);
			GDrive gdrive = getDrive(userEmailAddress);
			String testId = gdrive.checkPermissions(inputParam.toString(), nameOption).toString();
			return new StringValue(testId);
		} catch (GeneralSecurityException | IOException | JSONException e) {
			LOGGER.error("Exception in copying Google Drive file: ", e);
			String msg = e.getMessage();
			throw new BotCommandException(msg, e);
		}
	}

	GDrive getDrive(SecureString userEmailAddress) throws GeneralSecurityException, IOException {
		AuthenticationService authenticationService = getAuthenticationService();
		return new GDriveImpl(authenticationService, userEmailAddress.getInsecureString(), sessionMap);
	}

	@VisibleForTesting
	void setAuthenticationService(AuthenticationService mAuthenticationService) {
		this.mAuthenticationService = mAuthenticationService;
	}

	synchronized AuthenticationService getAuthenticationService() {
		if (mAuthenticationService == null) {
			mAuthenticationService = new AuthenticationServiceImpl();
		}
		return mAuthenticationService;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
}
