/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */
package com.automationanywhere.botcommands.gsuite.sheets.test.commands;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.botcommands.gsuite.sheets.commands.DeleteRowColumn;
import com.automationanywhere.botcommands.gsuite.sheets.commands.OpenSheets;
import com.automationanywhere.botcommands.gsuite.sheets.test.utils.ConstantsTest;

public class DeleteRowColumnJunit {
	DeleteRowColumn deleteRowColumn;
	OpenSheets openSheets;
	final String session = "session";

	Map<String, Object> sessionMap = new HashMap<>();

	@BeforeMethod
	public void setup() {

		deleteRowColumn = new DeleteRowColumn();
		deleteRowColumn.setSessionMap(sessionMap);
		openSheets = new OpenSheets();
		openSheets.setSessionMap(sessionMap);
		openSheets.execute(ConstantsTest.getUserName(), session, "BYNAME", "Demo", null, null, true, "Megha", false);
	}

	@Test
	public void deleteRowColumnTest() {
		try {
			deleteRowColumn.execute(session, "ROWOPERATION", "DELETEROWAT", "1", null, null, null, null, null,
					null); 
			deleteRowColumn.execute(session, "COLUMNOPERATION", null, "1", null, null, "DELETECOLUMNAT", "A", null,
					null);
			deleteRowColumn.execute(session, "ROWOPERATION", "DELETEROWBY", null, "ACTIVECELL", null, null, null, null,
					null);
			deleteRowColumn.execute(session, "COLUMNOPERATION", null, null, null, null, "DELETECOLUMNBY", null, "ACTIVECELL",
					null);
			deleteRowColumn.execute(session, "ROWOPERATION", "DELETEROWBY", null, "RANGE", "9:12", null, null, null,
					null);
			deleteRowColumn.execute(session, "COLUMNOPERATION", null, null, null, null, "DELETECOLUMNBY", null, "RANGE",
					"C:E");
		} catch (Exception e) {

		}

	}
}
