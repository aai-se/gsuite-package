package com.automationanywhere.botcommands.gsuite.drive.utils;

import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;

public class Constants {

	private static final Messages MESSAGES = MessagesFactory
			.getMessages("com.automationanywhere.botcommands.gsuite.messages.messages");

	public static final String DEFAULT_DRIVE_PARENT = "My Drive";
	public static final String ROOT_ID = "0AOK6yMFkd6ACUk9PVA";

	public static final String MIME_TYPE_FOLDER = "application/vnd.google-apps.folder";
	public static final String MIME_TYPE_SPREADSHEET = "application/vnd.google-apps.spreadsheet";
	public static final String MIME_TYPE_DOCUMENT = "application/vnd.google-apps.document";
	public static final String MIME_TYPE_PDF = "application/pdf";
	public static final String MIME_TYPE_PPT = "application/vnd.google-apps.presentation";
	public static final String MIME_TYPE_PLAIN = "text/plain";
	public static final String MIME_TYPE_CSV = "text/csv";
	public static final String MIME_TYPE_IMAGE = "application/vnd.google-apps.drawing";

	public static final String QUOTE = "'";
	public static final String FORWARD_SLASH = "/";
	public static final String PATH_SEPERATOR = "/";
	public static final String EXPORT_ACCEPTED_FORMATS = "csv, doc, docx, odp, ods, odt, pot, potm, potx, pps, ppsx, ppsxm, ppt, pptm, pptx, rtf, xls, xlsx.";

	public static final String FILE = "file";
	public static final String FOLDER = "folder";

	public static final String ROOT = "root";
	public static final String ISOVERRIDE = "isOverride";
	public static final String ID = "id";
	public static final String ROLE = "role";

	public static final String FILENAME = "fileName";
	public static final String FILEID = "fileId";
	public static final String CHILDNAME = "childName";

	public static final String EXPORTASPDF = "exportAsPDF";
	public static final String CREATEFOLDER = "created";
	public static final String DELETEFODLER = "deleted";
	public static final String CHECKPERMISSIONS = "checkPermission";

	public static final String CHECKPERMISSIONS_FIELDS = "permissions(domain,id,kind,role,type,emailAddress)";
	public static final String MOVE_FOLDER_FIELDS = "id, parents";
	public static final String COPY_FOLDER_FIELDS = "id,name";
	public static final String FILE_LIST_FIELDS = "files(id,kind,name,mimeType),kind";
	public static final String GET_FILE_FIELDS = "id,kind,name,mimeType,kind,parents,permissions";
	public static final String GET_CHILD_FIELDS = "files(id,kind,name,mimeType,createdTime,modifiedTime,owners,lastModifyingUser,parents,version,permissions),kind";

	public static final String CHOICE_DELETE = "delete";
	public static final String CHOICE_WRITER = "write";
	public static final String CHOICE_READ = "read";

	public static final String FILE_OWNER_ROLE = "owner";
	public static final String ORGANIZER_ROLE = "organizer";
	public static final String FILE_ORGANIZER_ROLE = "fileOrganizer";
	public static final String FILE_WRITER_ROLE = "writer";
	public static final String FILE_READ_ROLE = "reader";
	public static final String FILE_COMMENTER_ROLE = "commenter";

	public static final String SET_PARENTS = "parents = '";
	public static final String SET_NAME = "name = '";
	public static final String SET_TRASH = "' and trashed = false";

}
