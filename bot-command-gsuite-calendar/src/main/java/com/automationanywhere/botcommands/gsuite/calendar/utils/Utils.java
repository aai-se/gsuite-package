/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 *
 */

package com.automationanywhere.botcommands.gsuite.calendar.utils;

import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;

public class Utils {

    public static String getException(GoogleJsonResponseException e) {
        GoogleJsonError error = e.getDetails();
        String errorMsg = error.getMessage();
        return errorMsg;
    }
}
